package com.ruoyi.business.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * 账期-单价对象 t_hm_customerpaydays_price
 *
 * @author ruoyi
 * @date 2021-12-30
 */
@Data
public class THmCustomerpaydaysPrice extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 账期id
     */
    @Excel(name = "账期id")
    private Integer customerpaydaysId;

    /**
     * 单价
     */
    @Excel(name = "单价")
    private String price;

    /**
     * 操作人
     */
    @Excel(name = "操作人")
    private Long addUser;

    /**
     * 操作时间
     */
    @Excel(name = "操作时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date addTime;

    /**
     * 面积
     */
    @Excel(name = "面积")
    private String area;

    @Excel(name = "系数")
    private String xishu;

    @Excel(name = "金额")
    private String total;

    private Integer type;

    //单价类别
    private String priceType;

    private String customerCode;
    private String customerName;

    private String nian;
}
