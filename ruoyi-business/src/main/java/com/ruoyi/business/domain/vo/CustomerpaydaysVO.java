package com.ruoyi.business.domain.vo;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;

@Data
public class CustomerpaydaysVO {

    private Integer hcustomerId;

    private Integer hareaId;

    @Excel(name = "用户编号")
    private String hcustomerCode;

    @Excel(name = "用户姓名")
    private String hcustomerName;

    @Excel(name = "手机号码")
    private String hcustomerMobile;

    @Excel(name = "用户身份证")
    private String hcustomerPersonlcard;

    @Excel(name = "所属分区")
    private String hcustomerArea;

    @Excel(name = "详细地址")
    private String hcustomerDetailaddress;

    @Excel(name = "开户状态", readConverterExp = "0=否,1=是")
    private Integer hcustomerKhstate;

    @Excel(name = "用户余额")
    private String balance;

    private Integer hcustomerTapstate;

    @Excel(name = "房屋面积")
    private String hcustomerHomearea;

    @Excel(name = "超高面积")
    private String hcustomerAbovearea;

    @Excel(name = "实供面积")
    private String hcustomerActualarea;

    @Excel(name = "停供面积")
    private String hcustomerTgarea;

    //开始年度
    private String hzqGrstarttime;

    //结束年度
    private String hzqGrendtime;

    private Integer wqId;

    @Excel(name = "用户年度")
    private String niandu;

    @Excel(name = "账期类型", readConverterExp = "0=基础热,1=全额,2=续费,3=延期,4=平账,5=面积变更欠费,99=欠费")
    private Integer wqInt1;

    private String hcustomerMoney;

    private String hzqGrdays;

    private String wqStr3;

    @Excel(name = "当期金额")
    private String wqStr1;

    @Excel(name = "延期金额")
    private String wqStr4;

    @Excel(name = "延期欠费金额")
    private String wqStr2;

    @Excel(name = "面积变更金额")
    private String wqStr5;

    @Excel(name = "分期欠费金额")
    private String wqStr6;

    private Integer isTiao;

    @Excel(name = "账期生成时间")
    private String wqAddtime;

    private Integer payType;
    //缴费状态  1已缴 2未缴
    // 0对应int1的 0 1 2
    // 1对应int1的 3 5 6 99
    private Integer jiaoType;
}
