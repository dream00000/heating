package com.ruoyi.business.mapper;

import com.ruoyi.business.domain.THmPaydays;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 分区账期Mapper接口
 */
public interface THmPaydaysMapper {
    /**
     * 查询分区账期
     */
    THmPaydays selectTHmPaydaysById(@Param("id") int id);

    /**
     * 查询分区账期列表
     */
    List<THmPaydays> selectTHmPaydaysList(THmPaydays tHmPaydays);

    /**
     * 新增分区账期
     */
    int insertTHmPaydays(THmPaydays tHmPaydays);

    /**
     * 修改分区账期
     */
    int updateTHmPaydays(THmPaydays tHmPaydays);
}
