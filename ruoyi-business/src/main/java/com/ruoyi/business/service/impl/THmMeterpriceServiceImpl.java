package com.ruoyi.business.service.impl;

import com.ruoyi.business.domain.THmMeterprice;
import com.ruoyi.business.mapper.THmMeterpriceMapper;
import com.ruoyi.business.service.ITHmMeterpriceService;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.service.ISysDictDataService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 用热单价Service业务层处理
 *
 * @author ruoyi
 * @date 2021-08-13
 */
@Service("MeterpriceService")
public class THmMeterpriceServiceImpl implements ITHmMeterpriceService {
    @Resource
    private THmMeterpriceMapper tHmMeterpriceMapper;

    @Resource
    private ISysDictDataService iSysDictDataService;

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * 查询用热单价
     *
     * @param id
     * @return 用热单价
     */
    @Override
    public THmMeterprice selectTHmMeterpriceById(int id) {
        THmMeterprice tHmMeterprice = tHmMeterpriceMapper.selectTHmMeterpriceById(id);
        tHmMeterprice.setDjid(tHmMeterprice.getRjPrice() + "元 - " + iSysDictDataService.selectDictLabel("data_price", tHmMeterprice.getRjDjid().toString()));
        return tHmMeterprice;
    }

    /**
     * 查询用热单价列表
     *
     * @param tHmMeterprice 用热单价
     * @return 用热单价
     */
    @Override
    public List<THmMeterprice> selectTHmMeterpriceList(THmMeterprice tHmMeterprice) {
        List<THmMeterprice> list = tHmMeterpriceMapper.selectTHmMeterpriceList(tHmMeterprice);
        for (THmMeterprice meterprice : list) {
            meterprice.setDjid(meterprice.getRjPrice() + "元 - " + iSysDictDataService.selectDictLabel("data_price", meterprice.getRjDjid().toString()));
        }
        return list;
    }

    public List<THmMeterprice> selectTHmMeterpriceList() {
        THmMeterprice tHmMeterprice = new THmMeterprice();
        List<THmMeterprice> list = tHmMeterpriceMapper.selectTHmMeterpriceList(tHmMeterprice);
        for (THmMeterprice meterprice : list) {
            meterprice.setDjid(meterprice.getRjPrice() + "元 - " + iSysDictDataService.selectDictLabel("data_price", meterprice.getRjDjid().toString()));
        }
        return list;
    }

    public List<THmMeterprice> selectTHmMeterpriceLists() {
        THmMeterprice tHmMeterprice = new THmMeterprice();
        List<THmMeterprice> list = tHmMeterpriceMapper.selectTHmMeterpriceList(tHmMeterprice);
        for (THmMeterprice meterprice : list) {
            meterprice.setDjid(iSysDictDataService.selectDictLabel("data_price", meterprice.getRjDjid().toString()));
        }
        return list;
    }

    /**
     * 新增用热单价
     *
     * @param tHmMeterprice 用热单价
     * @return 结果
     */
    @Override
    public int insertTHmMeterprice(THmMeterprice tHmMeterprice) {
        tHmMeterprice.setRjAdduserid(ShiroUtils.getSysUser().getUserId());
        tHmMeterprice.setRjAddtime(sdf.format(new Date()));
        tHmMeterprice.setRjState(0);
        tHmMeterprice.setRjDjname(iSysDictDataService.selectDictLabel("data_price", tHmMeterprice.getRjDjid().toString()));
        return tHmMeterpriceMapper.insertTHmMeterprice(tHmMeterprice);
    }

    /**
     * 修改用热单价
     *
     * @param tHmMeterprice 用热单价
     * @return 结果
     */
    @Override
    public int updateTHmMeterprice(THmMeterprice tHmMeterprice) {
//        tHmMeterprice.setRjDjname(iSysDictDataService.selectDictLabel("data_price", tHmMeterprice.getRjDjid().toString()));
        return tHmMeterpriceMapper.updateTHmMeterprice(tHmMeterprice);
    }

}
