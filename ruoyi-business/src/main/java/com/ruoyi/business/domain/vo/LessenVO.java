package com.ruoyi.business.domain.vo;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;

@Data
public class LessenVO {

    private Integer hcustomerId;

    private Integer hareaId;

    @Excel(name = "用户编号")
    private String hcustomerCode;

    @Excel(name = "用户姓名")
    private String hcustomerName;

    @Excel(name = "手机号码")
    private String hcustomerMobile;

    @Excel(name = "用户身份证")
    private String hcustomerPersonlcard;

    @Excel(name = "所属分区")
    private String hcustomerArea;

    @Excel(name = "详细地址")
    private String hcustomerDetailaddress;

    @Excel(name = "开户状态", readConverterExp = "0=否,1=是")
    private Integer hcustomerKhstate;

    @Excel(name = "减免金额")
    private String jmMoney;

    @Excel(name = "减免原因")
    private String jmMemo;

    @Excel(name = "操作人员")
    private String sysUserName;

    @Excel(name = "减免时间")
    private String jmAddtime;
}
