package com.ruoyi.business.mapper;

import com.ruoyi.business.domain.THmMessage;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 推送消息记录Mapper接口
 *
 * @author ruoyi
 * @date 2022-03-12
 */
public interface THmMessageMapper {
    /**
     * 查询推送消息记录
     *
     * @param id
     * @return 推送消息记录
     */
    THmMessage selectTHmMessageById(@Param("id") int id);

    /**
     * 查询推送消息记录列表
     *
     * @param tHmMessage 推送消息记录
     * @return 推送消息记录集合
     */
    List<THmMessage> selectTHmMessageList(THmMessage tHmMessage);

    /**
     * 新增推送消息记录
     *
     * @param tHmMessage 推送消息记录
     * @return 结果
     */
    int insertTHmMessage(THmMessage tHmMessage);

    /**
     * 修改推送消息记录
     *
     * @param tHmMessage 推送消息记录
     * @return 结果
     */
    int updateTHmMessage(THmMessage tHmMessage);

    int deleteTHmMessage(@Param("customerId") int customerId, @Param("openId") String openId);
}
