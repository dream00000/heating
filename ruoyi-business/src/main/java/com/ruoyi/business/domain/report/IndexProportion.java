package com.ruoyi.business.domain.report;

import lombok.Data;

@Data
public class IndexProportion {

    private String type;//类型 0，20% 1，全额 3，延期  99（其他），欠费
    private int count;//用户数量
}
