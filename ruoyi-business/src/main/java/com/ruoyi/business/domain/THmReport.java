package com.ruoyi.business.domain;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;

@Data
public class THmReport {

    private Integer sysUserId;//操作员id

    @Excel(name = "操作员")
    private String sysUserName;//操作员name

    private String startTime;//开始时间
    private String endTime;//结束时间
    private Integer timeType;//时间类型   1:年 2：月 3：日

    @Excel(name = "时间")
    private String time;

    private Integer openNum;//开户数

    private Integer payNum;//缴费户数

    @Excel(name = "充值总金额")
    private String payTotal;//总金额

    private Integer makeCardNum;//补卡户数

    private Integer makeCard0Num;//补卡免费户数

    private String makeCardTotal;//补卡费用

    @Excel(name = "退费总金额")
    private String refundTotal;

    @Excel(name = "其他收费总金额")
    private String otherTotal;

    private String cashTotal;//现金总额

    private String wxTotal;//微信总额

    private String transferTotal;//转账总额
}
