package com.ruoyi.business.domain.vo;

import lombok.Data;

@Data
public class Bao {
    private String success;
    private String message;
    private String object;
    private String obj;
    private String msg;
    private String count;
    private String code;
    private String error;
    private String print;

    private BaoDatas data;
}
