package com.ruoyi.business.service;

import com.ruoyi.business.domain.THmCustomerpaydaysRecord;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 账期操作日志Service接口
 */
public interface ITHmCustomerpaydaysRecordService {
    /**
     * 查询账期操作日志
     */
    public THmCustomerpaydaysRecord selectTHmCustomerpaydaysRecordById(@Param("id") Long id);

    /**
     * 查询账期操作日志列表
     */
    public List<THmCustomerpaydaysRecord> selectTHmCustomerpaydaysRecordList(THmCustomerpaydaysRecord tHmCustomerpaydaysRecord);

    /**
     * 新增账期操作日志
     */
    public int insertTHmCustomerpaydaysRecord(THmCustomerpaydaysRecord tHmCustomerpaydaysRecord);

    /**
     * 修改账期操作日志
     */
    public int updateTHmCustomerpaydaysRecord(THmCustomerpaydaysRecord tHmCustomerpaydaysRecord);
}
