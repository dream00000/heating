package com.ruoyi.business.domain.vo;

import lombok.Data;

@Data
public class THmStationDetail {

    private Integer hstationId;
    private String parentName;
    private String hstationName;
    private String tAreaA;
    private String tAreaB;
    private String sAreaA;
    private String sAreaB;
    private String gHu;
    private String dHu;
}
