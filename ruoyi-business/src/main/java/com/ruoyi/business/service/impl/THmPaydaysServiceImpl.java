package com.ruoyi.business.service.impl;

import com.ruoyi.business.domain.THmPaydays;
import com.ruoyi.business.mapper.THmPaydaysMapper;
import com.ruoyi.business.service.ITHmPaydaysService;
import com.ruoyi.framework.util.ShiroUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 分区账期Service业务层处理
 *
 * @author ruoyi
 * @date 2021-08-13
 */
@Service("PaydaysService")
public class THmPaydaysServiceImpl implements ITHmPaydaysService {

    @Resource
    private THmPaydaysMapper tHmPaydaysMapper;

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * 查询分区账期
     *
     * @param id
     * @return 分区账期
     */
    @Override
    public THmPaydays selectTHmPaydaysById(int id) {
        return tHmPaydaysMapper.selectTHmPaydaysById(id);
    }

    /**
     * 查询分区账期列表
     *
     * @param tHmPaydays 分区账期
     * @return 分区账期
     */
    @Override
    public List<THmPaydays> selectTHmPaydaysList(THmPaydays tHmPaydays) {
        return tHmPaydaysMapper.selectTHmPaydaysList(tHmPaydays);
    }

    public List<THmPaydays> selectTHmPaydaysList() {
        THmPaydays tHmPaydays = new THmPaydays();
        List<THmPaydays> list = tHmPaydaysMapper.selectTHmPaydaysList(tHmPaydays);
        for (THmPaydays paydays : list) {
            paydays.setPaydays("计费开始日期: " + paydays.getHzqStartime() + " / " + "账单生成日期：" + paydays.getHzqEndtime() + " / " + "供热开始时间：" + paydays.getHzqGrstarttime() + " / " + "供热天数：" + paydays.getHzqGrdays());
        }

        return list;
    }

    /**
     * 新增分区账期
     *
     * @param tHmPaydays 分区账期
     * @return 结果
     */
    @Override
    public int insertTHmPaydays(THmPaydays tHmPaydays) {
        tHmPaydays.setHzqAdduserid(ShiroUtils.getSysUser().getUserId());
        tHmPaydays.setHzqAddtime(sdf.format(new Date()));
        tHmPaydays.setHzqState(0);
        return tHmPaydaysMapper.insertTHmPaydays(tHmPaydays);
    }

    /**
     * 修改分区账期
     *
     * @param tHmPaydays 分区账期
     * @return 结果
     */
    @Override
    public int updateTHmPaydays(THmPaydays tHmPaydays) {
        return tHmPaydaysMapper.updateTHmPaydays(tHmPaydays);
    }

}
