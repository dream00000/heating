package com.ruoyi.business.domain.vo;

import lombok.Data;

@Data
public class BaoDataList {
    private String jdsj;
    private String gdbh;
    private String addTime;
    private String wcsj;
    private String orderType;
    private String customerName;
    private String dz;
    private String content;
    private String sysName;
    private String endSetState;
    private String sysPhone;
}
