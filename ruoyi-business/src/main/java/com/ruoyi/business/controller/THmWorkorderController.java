package com.ruoyi.business.controller;

import cn.hutool.http.HttpUtil;
import com.google.gson.Gson;
import com.ruoyi.business.domain.THmWorkorder;
import com.ruoyi.business.domain.vo.BaoData;
import com.ruoyi.business.domain.vo.BaoInfo;
import com.ruoyi.business.service.ITHmWorkorderService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 客服工单Controller
 *
 * @author ruoyi
 * @date 2022-02-26
 */
@Controller
@RequestMapping("/business/workorder")
public class THmWorkorderController extends BaseController {
    private String prefix = "business/workorder";

    @Resource
    private ITHmWorkorderService tHmWorkorderService;

    @GetMapping()
    public String workorder() {
        return prefix + "/workorder";
    }

    /**
     * 查询客服工单列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(THmWorkorder tHmWorkorder) {
        startPage();
        List<THmWorkorder> list = tHmWorkorderService.selectTHmWorkorderList(tHmWorkorder);
        return getDataTable(list);
    }

    /**
     * 导出客服工单列表
     */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(THmWorkorder tHmWorkorder) {
        List<THmWorkorder> list = tHmWorkorderService.selectTHmWorkorderList(tHmWorkorder);
        ExcelUtil<THmWorkorder> util = new ExcelUtil<THmWorkorder>(THmWorkorder.class);
        return util.exportExcel(list, "workorder");
    }

    /**
     * 获取工单详情
     */
    @GetMapping("/getById/{id}")
    public String getById(@PathVariable("id") Integer id, ModelMap mmap) {

        if (id != null) {
            Map<String, Object> map = new HashMap<>();
            map.put("workorderid", id);

            String result = HttpUtil.post("http://jrkjserver.cn:9200/wechat/RepairsRecordData", map);
            Gson gson = new Gson();
            BaoInfo baoInfo = gson.fromJson(result, BaoInfo.class);
            List<BaoData> baoData = baoInfo.getData();

            if (baoData.size() > 0) {
                mmap.put("workorder", baoData.get(0));
                return "business/workorder" + "/detail";
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}
