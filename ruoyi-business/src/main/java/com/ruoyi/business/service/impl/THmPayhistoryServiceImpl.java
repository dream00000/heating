package com.ruoyi.business.service.impl;

import com.ruoyi.business.domain.*;
import com.ruoyi.business.domain.data.THmCustomerpaydaysPriceCopy;
import com.ruoyi.business.domain.vo.DetailListVO;
import com.ruoyi.business.domain.vo.DetailVO;
import com.ruoyi.business.domain.vo.PayhistoryVO;
import com.ruoyi.business.mapper.*;
import com.ruoyi.business.service.ITHmPayhistoryService;
import com.ruoyi.business.util.LingUtil;
import com.ruoyi.business.util.MoneyToCNFormat;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.mapper.SysUserMapper;
import com.ruoyi.system.service.ISysDictDataService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 充值记录Service业务层处理
 *
 * @author ruoyi
 * @date 2021-08-13
 */
@Service
public class THmPayhistoryServiceImpl implements ITHmPayhistoryService {

    @Resource
    private THmPayhistoryMapper tHmPayhistoryMapper;

    @Resource
    private THmCustomerMapper tHmCustomerMapper;

    @Resource
    private THmAreaMapper tHmAreaMapper;

    @Resource
    private THmCustomerpaydaysMapper tHmCustomerpaydaysMapper;

    @Resource
    private ISysDictDataService iSysDictDataService;

    @Resource
    private SysUserMapper sysUserMapper;

    @Resource
    private THmCustomerpaydaysPriceMapper tHmCustomerpaydaysPriceMapper;

    @Resource
    private THmPayhistoryPriceMapper tHmPayhistoryPriceMapper;

    @Resource
    private THmDelayMapper tHmDelayMapper;

    @Resource
    private THmCustomerpaydaysRecordMapper tHmCustomerpaydaysRecordMapper;

    @Resource
    private THmPayhistoryRecordMapper tHmPayhistoryRecordMapper;

    @Resource
    private THmCustomerpriceMapper tHmCustomerpriceMapper;

    @Resource
    private THmMeterpriceMapper tHmMeterpriceMapper;

    SimpleDateFormat sd = new SimpleDateFormat("yyyy年MM月dd日");
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * 查询充值记录
     */
    @Override
    public THmPayhistory selectTHmPayhistoryById(int id) {

        THmPayhistory tHmPayhistory = tHmPayhistoryMapper.selectTHmPayhistoryById(id);
        THmCustomer tHmCustomer = tHmCustomerMapper.selectTHmCustomerById(tHmPayhistory.getHcustomerId());

        tHmPayhistory.setHcustomerId(id);
        tHmPayhistory.setCustomerName(tHmCustomer.getHcustomerName());
        tHmPayhistory.setCustomerCode(tHmCustomer.getHcustomerCode());
        tHmPayhistory.setCustomerPhone(tHmCustomer.getHcustomerMobile());
        tHmPayhistory.setBalance(tHmCustomer.getBalance());
        THmArea tHmArea = tHmAreaMapper.selectTHmAreaById(tHmCustomer.getHareaId());
        if (tHmArea != null) {
            if (tHmArea.getHareaLevel() == 2) {
                tHmPayhistory.setCustomerArea(tHmArea.getHareaName());
            } else {
                THmArea tHmArea1 = tHmAreaMapper.selectTHmAreaById(tHmArea.getHareaParentid());
                tHmPayhistory.setCustomerArea(tHmArea1.getHareaName());
            }
        }
        tHmPayhistory.setHcustomerDetailaddress(tHmCustomer.getHcustomerDetailaddress());

        return tHmPayhistory;
    }

    /**
     * 查询充值记录
     */
    @Override
    public THmPayhistory selectTHmPayhistoryByIdt(int id, Integer type) {
        THmPayhistory tHmPayhistory = new THmPayhistory();
        THmCustomer tHmCustomer = tHmCustomerMapper.selectTHmCustomerById(id);
        tHmPayhistory.setHcustomerId(id);
        tHmPayhistory.setCustomerName(tHmCustomer.getHcustomerName());
        tHmPayhistory.setCustomerCode(tHmCustomer.getHcustomerCode());
        tHmPayhistory.setCustomerPhone(tHmCustomer.getHcustomerMobile());
        tHmPayhistory.setBalance(tHmCustomer.getBalance());
        tHmPayhistory.setHpaySfqe(tHmCustomer.getPayType());
        THmArea tHmArea = tHmAreaMapper.selectTHmAreaById(tHmCustomer.getHareaId());
        if (tHmArea != null) {
            if (tHmArea.getHareaLevel() == 2) {
                tHmPayhistory.setCustomerArea(tHmArea.getHareaName());
            } else {
                THmArea tHmArea1 = tHmAreaMapper.selectTHmAreaById(tHmArea.getHareaParentid());
                tHmPayhistory.setCustomerArea(tHmArea1.getHareaName());
            }
        }
        tHmPayhistory.setHcustomerDetailaddress(tHmCustomer.getHcustomerDetailaddress());

        THmCustomerpaydays tHmCustomerpaydays = new THmCustomerpaydays();
        tHmCustomerpaydays.setHcustomerId(id);
        tHmCustomerpaydays.setType(type);
        List<THmCustomerpaydays> list = tHmCustomerpaydaysMapper.selectTHmCustomerpaydaysListm(tHmCustomerpaydays);
        if (list.size() > 0) {
            for (THmCustomerpaydays hmCustomerpaydays : list) {
                if (hmCustomerpaydays.getPayType() != null) {
                    if (hmCustomerpaydays.getPayType() == 0) {
                        hmCustomerpaydays.setPayTypeName("基础热");
                    } else if (hmCustomerpaydays.getPayType() == 1) {
                        hmCustomerpaydays.setPayTypeName("全热");
                    } else if (hmCustomerpaydays.getPayType() == 5) {
                        hmCustomerpaydays.setPayTypeName("复合");
                    }
                }
            }
        }
        if (type == 2) {
            //只取第一条
            List<THmCustomerpaydays> tHmCustomerpaydaysList = new ArrayList<>();
            tHmCustomerpaydaysList.add(list.get(0));
            tHmPayhistory.setWqId(list.get(0).getWqId());
            tHmPayhistory.setCustomerpaydaysList(tHmCustomerpaydaysList);
            //当期总额
            tHmPayhistory.setHpayYjmoney(list.get(0).getHcustomerMoney());
            THmPayhistory tHmPayhistory1 = tHmPayhistoryMapper.getByWqIdForXuFei(list.get(0).getWqId());
            //已缴金额
            tHmPayhistory.setHpayStr2(tHmPayhistory1.getHpayMoney());
            //当期总额
            tHmPayhistory.setHpayStr1(list.get(0).getWqStr1());
        } else {
            tHmPayhistory.setCustomerpaydaysList(list);
        }
        return tHmPayhistory;
    }

    /**
     * 查询充值记录
     */
    @Override
    public THmPayhistory selectTHmPayhistoryById(int id, Integer type) {
        THmPayhistory tHmPayhistory = new THmPayhistory();
        THmCustomer tHmCustomer = tHmCustomerMapper.selectTHmCustomerById(id);
        tHmPayhistory.setHcustomerId(id);
        tHmPayhistory.setCustomerName(tHmCustomer.getHcustomerName());
        tHmPayhistory.setCustomerCode(tHmCustomer.getHcustomerCode());
        tHmPayhistory.setCustomerPhone(tHmCustomer.getHcustomerMobile());
        tHmPayhistory.setBalance(tHmCustomer.getBalance());
        THmArea tHmArea = tHmAreaMapper.selectTHmAreaById(tHmCustomer.getHareaId());
        if (tHmArea != null) {
            if (tHmArea.getHareaLevel() == 2) {
                tHmPayhistory.setCustomerArea(tHmArea.getHareaName());
            } else {
                THmArea tHmArea1 = tHmAreaMapper.selectTHmAreaById(tHmArea.getHareaParentid());
                tHmPayhistory.setCustomerArea(tHmArea1.getHareaName());
            }
        }
        tHmPayhistory.setHcustomerDetailaddress(tHmCustomer.getHcustomerDetailaddress());

        THmCustomerpaydays tHmCustomerpaydays = new THmCustomerpaydays();
        tHmCustomerpaydays.setHcustomerId(id);
        tHmCustomerpaydays.setType(type);
        List<THmCustomerpaydays> list = tHmCustomerpaydaysMapper.selectTHmCustomerpaydaysList(tHmCustomerpaydays);
        if (list.size() > 0) {
            for (THmCustomerpaydays hmCustomerpaydays : list) {
                if (hmCustomerpaydays.getWqStr4() == null) {
                    hmCustomerpaydays.setWqStr4("0.00");
                }
            }
        }
        if (type == 2) {
            //只取第一条
            List<THmCustomerpaydays> tHmCustomerpaydaysList = new ArrayList<>();
            tHmCustomerpaydaysList.add(list.get(0));
            tHmPayhistory.setWqId(list.get(0).getWqId());
            tHmPayhistory.setCustomerpaydaysList(tHmCustomerpaydaysList);
            //当期总额
            tHmPayhistory.setHpayYjmoney(list.get(0).getWqStr1());
            THmPayhistory tHmPayhistory1 = tHmPayhistoryMapper.getByWqIdForXuFei(list.get(0).getWqId());
            //已缴金额
            tHmPayhistory.setHpayStr2(tHmPayhistory1.getHpayMoney());
            //当期总额
            tHmPayhistory.setHpayStr1(list.get(0).getWqStr1());
            tHmPayhistory.setLing(list.get(0).getLing().toString());
        } else {
            tHmPayhistory.setCustomerpaydaysList(list);
        }
        return tHmPayhistory;
    }

    //打印充值记录
    @Override
    public THmPayhistory selectTHmPayhistoryByIds(int id) {

        THmPayhistory tHmPayhistory = tHmPayhistoryMapper.selectTHmPayhistoryById(id);
        THmCustomerpaydays tHmCustomerpaydays = tHmCustomerpaydaysMapper.selectTHmCustomerpaydaysById(tHmPayhistory.getWqId());

        THmCustomer tHmCustomer = tHmCustomerMapper.selectTHmCustomerById(tHmPayhistory.getHcustomerId());
        tHmPayhistory.setCustomerCode(tHmCustomer.getHcustomerCode());
        tHmPayhistory.setCustomerName(tHmCustomer.getHcustomerName());
        THmArea tHmArea = tHmAreaMapper.selectTHmAreaById(tHmCustomer.getHareaId());
        if (tHmArea != null) {
            if (tHmArea.getHareaLevel() == 2) {
                tHmPayhistory.setCustomerArea(tHmArea.getHareaName());
            } else {
                THmArea tHmArea1 = tHmAreaMapper.selectTHmAreaById(tHmArea.getHareaParentid());
                tHmPayhistory.setCustomerArea(tHmArea1.getHareaName());
            }
        }
        //缴费清单
        THmPayhistoryPrice tHmPayhistoryPrice = new THmPayhistoryPrice();
        tHmPayhistoryPrice.setPayhistoryId(id);
        List<THmPayhistoryPrice> customerpriceList = tHmPayhistoryPriceMapper.selectTHmPayhistoryPriceList(tHmPayhistoryPrice);
        //缴费类型为基础热的时候 将系数默认为 20
        for (THmPayhistoryPrice hmPayhistoryPrice : customerpriceList) {
            if (tHmPayhistory.getHpaySfqe() == 0 && customerpriceList.size() > 0) {
                hmPayhistoryPrice.setXishu("20");
            }
            switch (tHmCustomerpaydays.getLing()) {
                //取整舍
                case 1:
                    hmPayhistoryPrice.setTotal(LingUtil.zhengShe(hmPayhistoryPrice.getTotal()));
                    break;
                //取整入
                case 2:
                    hmPayhistoryPrice.setTotal(LingUtil.zhengRu(hmPayhistoryPrice.getTotal()));
                    break;
                //按原价
                case 3:
                    hmPayhistoryPrice.setTotal(LingUtil.erRu(hmPayhistoryPrice.getTotal()));
                    break;
                //保留一位小数舍
                case 4:
                    hmPayhistoryPrice.setTotal(LingUtil.yiShe(hmPayhistoryPrice.getTotal()));
                    break;
                //保留一位小数入
                case 5:
                    hmPayhistoryPrice.setTotal(LingUtil.yiRu(hmPayhistoryPrice.getTotal()));
                    break;
                //保留两位小数入
                case 6:
                    hmPayhistoryPrice.setTotal(LingUtil.erRu(hmPayhistoryPrice.getTotal()));
                    break;
            }
        }
        tHmPayhistory.setTHmCustomerpriceList(customerpriceList);
        //物业费
        List<THmCustomerpaydaysPriceCopy> tHmCustomerpaydaysPriceCopyList = tHmCustomerpaydaysPriceMapper.getCopyList(tHmPayhistory.getWqId());
        tHmPayhistory.setTHmCustomerpaydaysPriceCopyList(tHmCustomerpaydaysPriceCopyList);
        //大写金额
//        tHmPayhistory.setHpayMoneyd(MoneyToCNFormat.formatToCN(Double.parseDouble((new BigDecimal(tHmPayhistory.getHpayMoney()).subtract(new BigDecimal(tHmPayhistory.getHpayStr4()))).toString())));
        tHmPayhistory.setHpayMoneyd(MoneyToCNFormat.formatToCN(Double.parseDouble((new BigDecimal(tHmPayhistory.getHpayMoney())).toString())));
        //发票日期
        tHmPayhistory.setHpayTradetime(sd.format(new Date()));
        //收款人，开票人
        SysUser sysUser = sysUserMapper.selectUserById(tHmPayhistory.getHpayAdduserid());
        tHmPayhistory.setSysname(sysUser.getUserName());
        //延期金额

        if ("0.00".equals(tHmCustomerpaydays.getWqStr4())) {
            tHmPayhistory.setHpayStr3("0.00");
        } else {
            tHmPayhistory.setHpayStr3(String.valueOf(Double.parseDouble(tHmCustomerpaydays.getWqStr4())));
        }
        //收费方式
        tHmPayhistory.setPayType(iSysDictDataService.selectDictLabel("data_paytype", tHmPayhistory.getHpayType().toString()));
        //支付金额
        if (tHmPayhistory.getHpaySfqe() == 0) {
            tHmPayhistory.setHpayMoney(tHmPayhistory.getHpayMoney() + "（基础热）");
        } else if (tHmPayhistory.getHpaySfqe() == 1) {
            tHmPayhistory.setHpayMoney(tHmPayhistory.getHpayMoney() + "（全额）");
        } else if (tHmPayhistory.getHpaySfqe() == 2) {
            tHmPayhistory.setHpayMoney(tHmPayhistory.getHpayMoney() + "（续费）");
            THmPayhistory hmPayhistory = tHmPayhistoryMapper.getByWqIdForXuFei(tHmPayhistory.getWqId());
            if (hmPayhistory != null) {
                tHmPayhistory.setHpayStr2(hmPayhistory.getHpayMoney());
            }
        } else if (tHmPayhistory.getHpaySfqe() == 3) {
            tHmPayhistory.setHpayMoney(tHmPayhistory.getHpayMoney() + "（面积变更）");
            THmCustomerpaydaysRecord tHmCustomerpaydaysRecord = tHmCustomerpaydaysRecordMapper.getRecordByWqId(tHmPayhistory.getWqId());
            if (tHmCustomerpaydaysRecord != null) {
                tHmPayhistory.setHpayStr2(tHmCustomerpaydaysRecord.getQianTotal());
            }
        } else if (tHmPayhistory.getHpaySfqe() == 4) {
            tHmPayhistory.setHpayMoney(tHmPayhistory.getHpayMoney() + "（部分缴费）");
        }
        return tHmPayhistory;
    }

    @Override
    public List<THmPayhistory> selectTHmPayhistoryListForWeChat(THmPayhistory tHmPayhistory) {
        return tHmPayhistoryMapper.selectTHmPayhistoryListForWeChat(tHmPayhistory);
    }

    //查询充值记录列表
    @Override
    public List<THmPayhistory> selectTHmPayhistoryList(THmPayhistory tHmPayhistory) {
        List<THmPayhistory> list = tHmPayhistoryMapper.selectTHmPayhistoryList(tHmPayhistory);
        for (THmPayhistory hmPayhistory : list) {
            THmCustomer tHmCustomer = tHmCustomerMapper.selectTHmCustomerById(hmPayhistory.getHcustomerId());
            hmPayhistory.setCustomerName(tHmCustomer.getHcustomerName());
            hmPayhistory.setCustomerCode(tHmCustomer.getHcustomerCode());
            hmPayhistory.setCustomerPhone(tHmCustomer.getHcustomerMobile());
            hmPayhistory.setHcustomerDetailaddress(tHmCustomer.getHcustomerDetailaddress());
            THmArea tHmArea = tHmAreaMapper.selectTHmAreaById(tHmCustomer.getHareaId());
            if (tHmArea != null) {
                if (tHmArea.getHareaLevel() == 2) {
                    hmPayhistory.setCustomerArea(tHmArea.getHareaName());
                } else {
                    THmArea tHmArea1 = tHmAreaMapper.selectTHmAreaById(tHmArea.getHareaParentid());
                    hmPayhistory.setCustomerArea(tHmArea1.getHareaName());
                }
            }
            hmPayhistory.setPayType(iSysDictDataService.selectDictLabel("data_paytype", hmPayhistory.getHpayType().toString()));
            THmCustomerpaydays tHmCustomerpaydays = tHmCustomerpaydaysMapper.selectTHmCustomerpaydaysById(hmPayhistory.getWqId());
            hmPayhistory.setNiandu(tHmCustomerpaydays.getHzqGrstarttime() + "-" + tHmCustomerpaydays.getHzqGrendtime());

            //操作人员
            SysUser sysUser = sysUserMapper.selectUserById(hmPayhistory.getHpayAdduserid());
            hmPayhistory.setSysname(sysUser.getUserName());
        }
        return list;
    }

    @Override
    public List<PayhistoryVO> selectTHmPayhistoryListt(THmCustomer tHmCustomer) {
        List<PayhistoryVO> list = tHmPayhistoryMapper.selectTHmPayhistoryListt(tHmCustomer);
        return list;
    }

    @Override
    public int insertTHmPayhistoryForWxChat(THmPayhistory tHmPayhistory) {
        return tHmPayhistoryMapper.insertTHmPayhistory(tHmPayhistory);
    }

    //充值
    @Override
    public int insertTHmPayhistory(THmPayhistory tHmPayhistory) {
        //前端给的复合视为全额缴纳
        if (tHmPayhistory.getHpaySfqe() == 5) {
            tHmPayhistory.setHpaySfqe(1);
        }
        //账期id-抵扣 例如： 9-30  账期id为9，抵扣为30
        String[] wqIds = tHmPayhistory.getWqIds();
        for (String wqIdt : wqIds) {
            String[] wqIdm = wqIdt.split("-");
            //账期id
            int wqId = Integer.parseInt(wqIdm[0]);
            //抵扣金额
            String kou = "0.00";
            if (tHmPayhistory.getIsChangeYue() == 1) {
                kou = wqIdm[1];
            }
            //更新账期
            THmCustomerpaydays hmCustomerpaydays = tHmCustomerpaydaysMapper.selectTHmCustomerpaydaysById(wqId);
            //面积变更缴费
            if (hmCustomerpaydays.getWqInt1() == 5) {
                switch (hmCustomerpaydays.getLing()) {
                    //取整舍
                    case 1:
                        //实缴
                        tHmPayhistory.setHpayMoney(LingUtil.zhengShe(new BigDecimal(hmCustomerpaydays.getWqStr5()).subtract(new BigDecimal(kou)).toString()));
                        //应缴
                        tHmPayhistory.setHpayYjmoney(LingUtil.zhengShe(hmCustomerpaydays.getWqStr5()));
                        tHmPayhistory.setLing("取整（只舍不入）");
                        break;
                    //取整入
                    case 2:
                        //实缴
                        tHmPayhistory.setHpayMoney(LingUtil.zhengRu(new BigDecimal(hmCustomerpaydays.getWqStr5()).subtract(new BigDecimal(kou)).toString()));
                        //应缴
                        tHmPayhistory.setHpayYjmoney(LingUtil.zhengRu(hmCustomerpaydays.getWqStr5()));
                        tHmPayhistory.setLing("取整（四舍五入）");
                        break;
                    //按原价
                    case 3:
                        //实缴
                        tHmPayhistory.setHpayMoney(LingUtil.erRu(new BigDecimal(hmCustomerpaydays.getWqStr5()).subtract(new BigDecimal(kou)).toString()));
                        //应缴
                        tHmPayhistory.setHpayYjmoney(LingUtil.erRu(hmCustomerpaydays.getWqStr5()));
                        tHmPayhistory.setLing("实际金额");
                        break;
                    //保留一位小数舍
                    case 4:
                        //实缴
                        tHmPayhistory.setHpayMoney(LingUtil.yiShe(new BigDecimal(hmCustomerpaydays.getWqStr5()).subtract(new BigDecimal(kou)).toString()));
                        //应缴
                        tHmPayhistory.setHpayYjmoney(LingUtil.yiShe(hmCustomerpaydays.getWqStr5()));
                        tHmPayhistory.setLing("保留一位小数点（只舍不入）");
                        break;
                    //保留一位小数入
                    case 5:
                        //实缴
                        tHmPayhistory.setHpayMoney(LingUtil.yiRu(new BigDecimal(hmCustomerpaydays.getWqStr5()).subtract(new BigDecimal(kou)).toString()));
                        //应缴
                        tHmPayhistory.setHpayYjmoney(LingUtil.yiRu(hmCustomerpaydays.getWqStr5()));
                        tHmPayhistory.setLing("保留一位小数点（四舍五入）");
                        break;
                    //保留两位小数入
                    case 6:
                        //实缴
                        tHmPayhistory.setHpayMoney(LingUtil.erRu(new BigDecimal(hmCustomerpaydays.getWqStr5()).subtract(new BigDecimal(kou)).toString()));
                        //应缴
                        tHmPayhistory.setHpayYjmoney(LingUtil.erRu(hmCustomerpaydays.getWqStr5()));
                        tHmPayhistory.setLing("保留两位小数点（四舍五入）");
                        break;
                }
                //保存账期上一个状态
                hmCustomerpaydays.setWqInt1(hmCustomerpaydays.getWqInt2());
                hmCustomerpaydays.setWqInt2(5);
                tHmCustomerpaydaysMapper.updateTHmCustomerpaydays(hmCustomerpaydays);
                //抵扣
                tHmPayhistory.setHpayStr4(kou);
                //缴费类型
                tHmPayhistory.setHpaySfqe(3);
            }
            //延期缴费
            if (hmCustomerpaydays.getWqInt1() == 3) {
                //比较一下欠款金额是否为0
                if ("0.00".equals(hmCustomerpaydays.getWqStr2())) {
                    //证明是今年延期
                    if (tHmPayhistory.getHpaySfqe() == 0) {
                        switch (hmCustomerpaydays.getLing()) {
                            //取整舍
                            case 1:
                                //实缴
                                tHmPayhistory.setHpayMoney(LingUtil.zhengShe(new BigDecimal(hmCustomerpaydays.getWqStr1()).add(new BigDecimal(hmCustomerpaydays.getWqStr4())).subtract(new BigDecimal(kou)).toString()));
                                //应缴
                                tHmPayhistory.setHpayYjmoney(LingUtil.zhengShe(new BigDecimal(hmCustomerpaydays.getWqStr1()).add(new BigDecimal(hmCustomerpaydays.getWqStr4())).toString()));
                                tHmPayhistory.setLing("取整（只舍不入）");
                                break;
                            //取整入
                            case 2:
                                //实缴
                                tHmPayhistory.setHpayMoney(LingUtil.zhengRu(new BigDecimal(hmCustomerpaydays.getWqStr1()).add(new BigDecimal(hmCustomerpaydays.getWqStr4())).subtract(new BigDecimal(kou)).toString()));
                                //应缴
                                tHmPayhistory.setHpayYjmoney(LingUtil.zhengRu(new BigDecimal(hmCustomerpaydays.getWqStr1()).add(new BigDecimal(hmCustomerpaydays.getWqStr4())).toString()));
                                tHmPayhistory.setLing("取整（四舍五入）");
                                break;
                            //按原价
                            case 3:
                                //实缴
                                tHmPayhistory.setHpayMoney(LingUtil.erRu(new BigDecimal(hmCustomerpaydays.getWqStr1()).add(new BigDecimal(hmCustomerpaydays.getWqStr4())).subtract(new BigDecimal(kou)).toString()));
                                //应缴
                                tHmPayhistory.setHpayYjmoney(LingUtil.erRu(new BigDecimal(hmCustomerpaydays.getWqStr1()).add(new BigDecimal(hmCustomerpaydays.getWqStr4())).toString()));
                                tHmPayhistory.setLing("实际金额");
                                break;
                            //保留一位小数舍
                            case 4:
                                //实缴
                                tHmPayhistory.setHpayMoney(LingUtil.yiShe(new BigDecimal(hmCustomerpaydays.getWqStr1()).add(new BigDecimal(hmCustomerpaydays.getWqStr4())).subtract(new BigDecimal(kou)).toString()));
                                //应缴
                                tHmPayhistory.setHpayYjmoney(LingUtil.yiShe(new BigDecimal(hmCustomerpaydays.getWqStr1()).add(new BigDecimal(hmCustomerpaydays.getWqStr4())).toString()));

                                break;
                            //保留一位小数入
                            case 5:
                                //实缴
                                tHmPayhistory.setHpayMoney(LingUtil.yiRu(new BigDecimal(hmCustomerpaydays.getWqStr1()).add(new BigDecimal(hmCustomerpaydays.getWqStr4())).subtract(new BigDecimal(kou)).toString()));
                                //应缴
                                tHmPayhistory.setHpayYjmoney(LingUtil.yiRu(new BigDecimal(hmCustomerpaydays.getWqStr1()).add(new BigDecimal(hmCustomerpaydays.getWqStr4())).toString()));
                                tHmPayhistory.setLing("保留一位小数点（四舍五入）");
                                break;
                            //保留两位小数入
                            case 6:
                                //实缴
                                tHmPayhistory.setHpayMoney(LingUtil.erRu(new BigDecimal(hmCustomerpaydays.getWqStr1()).add(new BigDecimal(hmCustomerpaydays.getWqStr4())).subtract(new BigDecimal(kou)).toString()));
                                //应缴
                                tHmPayhistory.setHpayYjmoney(LingUtil.erRu(new BigDecimal(hmCustomerpaydays.getWqStr1()).add(new BigDecimal(hmCustomerpaydays.getWqStr4())).toString()));
                                tHmPayhistory.setLing("保留两位小数点（四舍五入）");
                                break;
                        }
                        //保存账期上一个状态
                        hmCustomerpaydays.setWqInt2(hmCustomerpaydays.getWqInt1());
                        hmCustomerpaydays.setWqInt1(0);
                        tHmPayhistory.setHpayStr4(kou);
                        tHmCustomerpaydaysMapper.updateTHmCustomerpaydays(hmCustomerpaydays);
                    } else if (tHmPayhistory.getHpaySfqe() == 1) {
                        switch (hmCustomerpaydays.getLing()) {
                            //取整舍
                            case 1:
                                //实缴
                                tHmPayhistory.setHpayMoney(LingUtil.zhengShe(new BigDecimal(hmCustomerpaydays.getWqStr1()).subtract(new BigDecimal(kou)).toString()));
                                //应缴
                                tHmPayhistory.setHpayYjmoney(LingUtil.zhengShe(hmCustomerpaydays.getWqStr1()));
                                tHmPayhistory.setLing("取整（只舍不入）");
                                break;
                            //取整入
                            case 2:
                                //实缴
                                tHmPayhistory.setHpayMoney(LingUtil.zhengRu(new BigDecimal(hmCustomerpaydays.getWqStr1()).subtract(new BigDecimal(kou)).toString()));
                                //应缴
                                tHmPayhistory.setHpayYjmoney(LingUtil.zhengRu(hmCustomerpaydays.getWqStr1()));
                                tHmPayhistory.setLing("取整（四舍五入）");
                                break;
                            //按原价
                            case 3:
                                //实缴
                                tHmPayhistory.setHpayMoney(LingUtil.erRu(new BigDecimal(hmCustomerpaydays.getWqStr1()).subtract(new BigDecimal(kou)).toString()));
                                //应缴
                                tHmPayhistory.setHpayYjmoney(LingUtil.erRu(hmCustomerpaydays.getWqStr1()));
                                tHmPayhistory.setLing("实际金额");
                                break;
                            //保留一位小数舍
                            case 4:
                                //实缴
                                tHmPayhistory.setHpayMoney(LingUtil.yiShe(new BigDecimal(hmCustomerpaydays.getWqStr1()).subtract(new BigDecimal(kou)).toString()));
                                //应缴
                                tHmPayhistory.setHpayYjmoney(LingUtil.yiShe(hmCustomerpaydays.getWqStr1()));
                                tHmPayhistory.setLing("保留一位小数点（只舍不入）");
                                break;
                            //保留一位小数入
                            case 5:
                                //实缴
                                tHmPayhistory.setHpayMoney(LingUtil.yiRu(new BigDecimal(hmCustomerpaydays.getWqStr1()).subtract(new BigDecimal(kou)).toString()));
                                //应缴
                                tHmPayhistory.setHpayYjmoney(LingUtil.yiRu(hmCustomerpaydays.getWqStr1()));
                                tHmPayhistory.setLing("保留一位小数点（四舍五入）");
                                break;
                            //保留两位小数入
                            case 6:
                                //实缴
                                tHmPayhistory.setHpayMoney(LingUtil.erRu(new BigDecimal(hmCustomerpaydays.getWqStr1()).subtract(new BigDecimal(kou)).toString()));
                                //应缴
                                tHmPayhistory.setHpayYjmoney(LingUtil.erRu(hmCustomerpaydays.getWqStr1()));
                                tHmPayhistory.setLing("保留两位小数点（四舍五入）");
                                break;
                        }
                        //保存账期上一个状态
                        hmCustomerpaydays.setWqInt2(hmCustomerpaydays.getWqInt1());
                        hmCustomerpaydays.setWqInt1(1);
                        tHmPayhistory.setHpayStr4(kou);
                        tHmCustomerpaydaysMapper.updateTHmCustomerpaydays(hmCustomerpaydays);
                    }
                } else {
                    //不是本年的延期账期 先判断有没有延期费用
                    switch (hmCustomerpaydays.getLing()) {
                        //取整舍
                        case 1:
                            //实缴
                            tHmPayhistory.setHpayMoney(LingUtil.zhengShe(new BigDecimal(hmCustomerpaydays.getWqStr1()).add(new BigDecimal(hmCustomerpaydays.getWqStr4())).subtract(new BigDecimal(kou)).toString()));
                            //应缴
                            tHmPayhistory.setHpayYjmoney(LingUtil.zhengShe(new BigDecimal(hmCustomerpaydays.getWqStr1()).add(new BigDecimal(hmCustomerpaydays.getWqStr4())).toString()));
                            tHmPayhistory.setLing("取整（只舍不入）");
                            break;
                        //取整入
                        case 2:
                            //实缴
                            tHmPayhistory.setHpayMoney(LingUtil.zhengRu(new BigDecimal(hmCustomerpaydays.getWqStr1()).add(new BigDecimal(hmCustomerpaydays.getWqStr4())).subtract(new BigDecimal(kou)).toString()));
                            //应缴
                            tHmPayhistory.setHpayYjmoney(LingUtil.zhengRu(new BigDecimal(hmCustomerpaydays.getWqStr1()).add(new BigDecimal(hmCustomerpaydays.getWqStr4())).toString()));
                            tHmPayhistory.setLing("取整（四舍五入）");
                            break;
                        //按原价
                        case 3:
                            //实缴
                            tHmPayhistory.setHpayMoney(LingUtil.erRu(new BigDecimal(hmCustomerpaydays.getWqStr1()).add(new BigDecimal(hmCustomerpaydays.getWqStr4())).subtract(new BigDecimal(kou)).toString()));
                            //应缴
                            tHmPayhistory.setHpayYjmoney(LingUtil.erRu(new BigDecimal(hmCustomerpaydays.getWqStr1()).add(new BigDecimal(hmCustomerpaydays.getWqStr4())).toString()));
                            tHmPayhistory.setLing("实际金额");
                            break;
                        //保留一位小数舍
                        case 4:
                            //实缴
                            tHmPayhistory.setHpayMoney(LingUtil.yiShe(new BigDecimal(hmCustomerpaydays.getWqStr1()).add(new BigDecimal(hmCustomerpaydays.getWqStr4())).subtract(new BigDecimal(kou)).toString()));
                            //应缴
                            tHmPayhistory.setHpayYjmoney(LingUtil.yiShe(new BigDecimal(hmCustomerpaydays.getWqStr1()).add(new BigDecimal(hmCustomerpaydays.getWqStr4())).toString()));
                            tHmPayhistory.setLing("保留一位小数点（只舍不入）");
                            break;
                        //保留一位小数入
                        case 5:
                            //实缴
                            tHmPayhistory.setHpayMoney(LingUtil.yiRu(new BigDecimal(hmCustomerpaydays.getWqStr1()).add(new BigDecimal(hmCustomerpaydays.getWqStr4())).subtract(new BigDecimal(kou)).toString()));
                            //应缴
                            tHmPayhistory.setHpayYjmoney(LingUtil.yiRu(new BigDecimal(hmCustomerpaydays.getWqStr1()).add(new BigDecimal(hmCustomerpaydays.getWqStr4())).toString()));
                            tHmPayhistory.setLing("保留一位小数点（四舍五入）");
                            break;
                        //保留两位小数入
                        case 6:
                            //实缴
                            tHmPayhistory.setHpayMoney(LingUtil.erRu(new BigDecimal(hmCustomerpaydays.getWqStr1()).add(new BigDecimal(hmCustomerpaydays.getWqStr4())).subtract(new BigDecimal(kou)).toString()));
                            //应缴
                            tHmPayhistory.setHpayYjmoney(LingUtil.erRu(new BigDecimal(hmCustomerpaydays.getWqStr1()).add(new BigDecimal(hmCustomerpaydays.getWqStr4())).toString()));
                            tHmPayhistory.setLing("保留两位小数点（四舍五入）");
                            break;
                    }
                    //保存账期上一个状态
                    hmCustomerpaydays.setWqInt2(hmCustomerpaydays.getWqInt1());
                    hmCustomerpaydays.setWqInt1(0);
                    tHmPayhistory.setHpaySfqe(0);
                    tHmPayhistory.setHpayStr4(kou);
                    tHmCustomerpaydaysMapper.updateTHmCustomerpaydays(hmCustomerpaydays);
                }
            }
            //正常缴费
            if (hmCustomerpaydays.getWqInt1() == 99) {
                if (tHmPayhistory.getHpaySfqe() == 0) {
                    switch (hmCustomerpaydays.getLing()) {
                        //取整舍
                        case 1:
                            //实缴
                            tHmPayhistory.setHpayMoney(LingUtil.zhengShe(new BigDecimal(hmCustomerpaydays.getWqStr1()).subtract(new BigDecimal(kou)).toString()));
                            //应缴
                            tHmPayhistory.setHpayYjmoney(LingUtil.zhengShe(hmCustomerpaydays.getWqStr1()));
                            tHmPayhistory.setLing("取整（只舍不入）");
                            break;
                        //取整入
                        case 2:
                            //实缴
                            tHmPayhistory.setHpayMoney(LingUtil.zhengRu(new BigDecimal(hmCustomerpaydays.getWqStr1()).subtract(new BigDecimal(kou)).toString()));
                            //应缴
                            tHmPayhistory.setHpayYjmoney(LingUtil.zhengRu(hmCustomerpaydays.getWqStr1()));
                            tHmPayhistory.setLing("取整（四舍五入）");
                            break;
                        //按原价
                        case 3:
                            //实缴
                            tHmPayhistory.setHpayMoney(LingUtil.erRu(new BigDecimal(hmCustomerpaydays.getWqStr1()).subtract(new BigDecimal(kou)).toString()));
                            //应缴
                            tHmPayhistory.setHpayYjmoney(LingUtil.erRu(hmCustomerpaydays.getWqStr1()));
                            tHmPayhistory.setLing("实际金额");
                            break;
                        //保留一位小数舍
                        case 4:
                            //实缴
                            tHmPayhistory.setHpayMoney(LingUtil.yiShe(new BigDecimal(hmCustomerpaydays.getWqStr1()).subtract(new BigDecimal(kou)).toString()));
                            //应缴
                            tHmPayhistory.setHpayYjmoney(LingUtil.yiShe(hmCustomerpaydays.getWqStr1()));
                            tHmPayhistory.setLing("保留一位小数点（只舍不入）");
                            break;
                        //保留一位小数入
                        case 5:
                            //实缴
                            tHmPayhistory.setHpayMoney(LingUtil.yiRu(new BigDecimal(hmCustomerpaydays.getWqStr1()).subtract(new BigDecimal(kou)).toString()));
                            //应缴
                            tHmPayhistory.setHpayYjmoney(LingUtil.yiRu(hmCustomerpaydays.getWqStr1()));
                            tHmPayhistory.setLing("保留一位小数点（四舍五入）");
                            break;
                        //保留两位小数入
                        case 6:
                            //实缴
                            tHmPayhistory.setHpayMoney(LingUtil.erRu(new BigDecimal(hmCustomerpaydays.getWqStr1()).subtract(new BigDecimal(kou)).toString()));
                            //应缴
                            tHmPayhistory.setHpayYjmoney(LingUtil.erRu(hmCustomerpaydays.getWqStr1()));
                            tHmPayhistory.setLing("保留两位小数点（四舍五入）");
                            break;
                    }
                    //保存账期上一个状态
                    hmCustomerpaydays.setWqInt2(hmCustomerpaydays.getWqInt1());
                    hmCustomerpaydays.setWqInt1(0);
                    tHmPayhistory.setHpayStr4(kou);
                    tHmCustomerpaydaysMapper.updateTHmCustomerpaydays(hmCustomerpaydays);
                } else if (tHmPayhistory.getHpaySfqe() == 1) {
                    switch (hmCustomerpaydays.getLing()) {
                        //取整舍
                        case 1:
                            //实缴
                            tHmPayhistory.setHpayMoney(LingUtil.zhengShe(new BigDecimal(hmCustomerpaydays.getWqStr1()).subtract(new BigDecimal(kou)).toString()));
                            //应缴
                            tHmPayhistory.setHpayYjmoney(LingUtil.zhengShe(hmCustomerpaydays.getWqStr1()));
                            tHmPayhistory.setLing("取整（只舍不入）");
                            break;
                        //取整入
                        case 2:
                            //实缴
                            tHmPayhistory.setHpayMoney(LingUtil.zhengRu(new BigDecimal(hmCustomerpaydays.getWqStr1()).subtract(new BigDecimal(kou)).toString()));
                            //应缴
                            tHmPayhistory.setHpayYjmoney(LingUtil.zhengRu(hmCustomerpaydays.getWqStr1()));
                            tHmPayhistory.setLing("取整（四舍五入）");
                            break;
                        //按原价
                        case 3:
                            //实缴
                            tHmPayhistory.setHpayMoney(LingUtil.erRu(new BigDecimal(hmCustomerpaydays.getWqStr1()).subtract(new BigDecimal(kou)).toString()));
                            //应缴
                            tHmPayhistory.setHpayYjmoney(LingUtil.erRu(hmCustomerpaydays.getWqStr1()));
                            tHmPayhistory.setLing("实际金额");
                            break;
                        //保留一位小数舍
                        case 4:
                            //实缴
                            tHmPayhistory.setHpayMoney(LingUtil.yiShe(new BigDecimal(hmCustomerpaydays.getWqStr1()).subtract(new BigDecimal(kou)).toString()));
                            //应缴
                            tHmPayhistory.setHpayYjmoney(LingUtil.yiShe(hmCustomerpaydays.getWqStr1()));
                            tHmPayhistory.setLing("保留一位小数点（只舍不入）");
                            break;
                        //保留一位小数入
                        case 5:
                            //实缴
                            tHmPayhistory.setHpayMoney(LingUtil.yiRu(new BigDecimal(hmCustomerpaydays.getWqStr1()).subtract(new BigDecimal(kou)).toString()));
                            //应缴
                            tHmPayhistory.setHpayYjmoney(LingUtil.yiRu(hmCustomerpaydays.getWqStr1()));
                            tHmPayhistory.setLing("保留一位小数点（四舍五入）");
                            break;
                        //保留两位小数入
                        case 6:
                            //实缴
                            tHmPayhistory.setHpayMoney(LingUtil.erRu(new BigDecimal(hmCustomerpaydays.getWqStr1()).subtract(new BigDecimal(kou)).toString()));
                            //应缴
                            tHmPayhistory.setHpayYjmoney(LingUtil.erRu(hmCustomerpaydays.getWqStr1()));
                            tHmPayhistory.setLing("保留两位小数点（四舍五入）");
                            break;
                    }
                    //保存账期上一个状态
                    hmCustomerpaydays.setWqInt2(hmCustomerpaydays.getWqInt1());
                    hmCustomerpaydays.setWqInt1(1);
                    tHmPayhistory.setHpayStr4(kou);
                    tHmCustomerpaydaysMapper.updateTHmCustomerpaydays(hmCustomerpaydays);
                }
            }
            //此账期生成充值记录
            tHmPayhistory.setWqId(hmCustomerpaydays.getWqId());
            tHmPayhistory.setHpayTradeuserid(ShiroUtils.getSysUser().getUserId());
            tHmPayhistory.setHpayTradetime(sdf.format(new Date()));
            tHmPayhistory.setHpayAdduserid(ShiroUtils.getSysUser().getUserId());
            tHmPayhistory.setHpayAddtime(sdf.format(new Date()));
            tHmPayhistory.setHpayState(0);
            //单号
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            Instant instant = timestamp.toInstant();
            tHmPayhistory.setHpayOuttradeno("KD" + instant.toEpochMilli());
            tHmPayhistoryMapper.insertTHmPayhistory(tHmPayhistory);
            //缴费price详情
            THmCustomerpaydaysPrice tHmCustomerpaydaysPrice = new THmCustomerpaydaysPrice();
            tHmCustomerpaydaysPrice.setCustomerpaydaysId(hmCustomerpaydays.getWqId());
            List<THmCustomerpaydaysPrice> tHmCustomerpaydaysPriceList = tHmCustomerpaydaysPriceMapper.selectTHmCustomerpaydaysPriceList(tHmCustomerpaydaysPrice);
            for (THmCustomerpaydaysPrice hmCustomerpaydaysPrice : tHmCustomerpaydaysPriceList) {
                THmPayhistoryPrice tHmPayhistoryPrice = new THmPayhistoryPrice();
                tHmPayhistoryPrice.setPayhistoryId(tHmPayhistory.getHpayId());
                tHmPayhistoryPrice.setArea(hmCustomerpaydaysPrice.getArea());
                tHmPayhistoryPrice.setAreac("0");
                tHmPayhistoryPrice.setPriceType(hmCustomerpaydaysPrice.getPriceType());
                tHmPayhistoryPrice.setPrice(hmCustomerpaydaysPrice.getPrice());
                tHmPayhistoryPrice.setTotal(new BigDecimal(hmCustomerpaydaysPrice.getArea()).multiply(new BigDecimal(tHmPayhistoryPrice.getPrice())).multiply((new BigDecimal(hmCustomerpaydaysPrice.getXishu()).divide(new BigDecimal("100")))).toString());
                tHmPayhistoryPrice.setNian(hmCustomerpaydays.getHzqGrstarttime());
                tHmPayhistoryPrice.setType(hmCustomerpaydaysPrice.getType());
                tHmPayhistoryPrice.setXishu(hmCustomerpaydaysPrice.getXishu());
                //保存
                tHmPayhistoryPriceMapper.insertTHmPayhistoryPrice(tHmPayhistoryPrice);
            }
            if (!hmCustomerpaydays.getWqStr4().equals("0.00")) {
                List<THmDelay> tHmDelayList = tHmDelayMapper.getByWqId(wqId);
                for (THmDelay tHmDelay : tHmDelayList) {
                    THmCustomerprice tHmCustomerprice = tHmCustomerpriceMapper.selectTHmCustomerpriceById(tHmDelay.getPriceId());
                    THmPayhistoryPrice tHmPayhistoryPrice = new THmPayhistoryPrice();
                    tHmPayhistoryPrice.setPayhistoryId(tHmPayhistory.getHpayId());
                    tHmPayhistoryPrice.setArea(tHmCustomerprice.getArea());
                    tHmPayhistoryPrice.setAreac("0");
                    tHmPayhistoryPrice.setTotal(tHmCustomerprice.getTotal());
                    tHmPayhistoryPrice.setNian(hmCustomerpaydays.getHzqGrstarttime());
                    tHmPayhistoryPrice.setType(tHmCustomerprice.getType());
                    tHmPayhistoryPrice.setXishu(tHmCustomerprice.getRemark());
                    THmMeterprice tHmMeterprice1 = tHmMeterpriceMapper.selectTHmMeterpriceById(tHmCustomerprice.getPriceId());
                    tHmPayhistoryPrice.setPrice(tHmMeterprice1.getRjPrice());
                    tHmPayhistoryPrice.setPriceType(iSysDictDataService.selectDictLabel("data_price", tHmMeterprice1.getRjDjid().toString()));
                    //保存
                    tHmPayhistoryPriceMapper.insertTHmPayhistoryPrice(tHmPayhistoryPrice);
                }
            }

            //更新延期金额
            THmDelay tHmDelay = new THmDelay();
            tHmDelay.setWqId(wqId);
            tHmDelay.setIsGai(1);
            tHmDelayMapper.updateTHmDelay(tHmDelay);
        }

        //更新用户int4 最后一次账期状态
        THmCustomerpaydays tHmCustomerpaydays = tHmCustomerpaydaysMapper.getLast(tHmPayhistory.getHcustomerId());
        THmCustomer tHmCustomer = new THmCustomer();
        tHmCustomer.setHcustomerInt4(tHmCustomerpaydays.getWqInt1());
        tHmCustomer.setBalance(tHmPayhistory.getBalance());
        tHmCustomer.setHcustomerId(tHmPayhistory.getHcustomerId());
        //查所有账期如果有不是全额，基础热，续费，平账的账单即为0
        tHmCustomer.setIsZheng(1);
        THmCustomerpaydays tHmCustomerpaydays1 = new THmCustomerpaydays();
        tHmCustomerpaydays1.setHcustomerId(tHmCustomer.getHcustomerId());
        List<THmCustomerpaydays> tHmCustomerpaydaysLists = tHmCustomerpaydaysMapper.selectTHmCustomerpaydaysList(tHmCustomerpaydays1);
        for (THmCustomerpaydays customerpaydays : tHmCustomerpaydaysLists) {
            if (customerpaydays.getWqInt1() != 0 && customerpaydays.getWqInt1() != 1 && customerpaydays.getWqInt1() != 2 && customerpaydays.getWqInt1() != 4) {
                tHmCustomer.setIsZheng(0);
            }
        }
        return tHmCustomerMapper.updateTHmCustomer(tHmCustomer);
    }

    //分期充值
    @Override
    public int fenqi(THmPayhistory tHmPayhistory) {
        //前端给的复合视为全额缴纳
        if (tHmPayhistory.getHpaySfqe() == 5) {
            tHmPayhistory.setHpaySfqe(1);
        }
        //账期id-抵扣 例如： 9-30  账期id为9，抵扣为30
        String[] wqIds = tHmPayhistory.getWqIds();
        String[] wqIdm = wqIds[0].split("-");
        //账期id
        int wqId = Integer.parseInt(wqIdm[0]);
        //抵扣金额
        String kou = "0.00";
        if (tHmPayhistory.getIsChangeYue() == 1) {
            kou = wqIdm[1];
        }
        //更新账期
        THmCustomerpaydays hmCustomerpaydays = tHmCustomerpaydaysMapper.selectTHmCustomerpaydaysById(wqId);
        //更新用户
        THmCustomer tHmCustomer = tHmCustomerMapper.selectTHmCustomerById(tHmPayhistory.getHcustomerId());
        //更新延期金额
        THmDelay tHmDelay = new THmDelay();
        tHmDelay.setWqId(wqId);
        tHmDelay.setIsGai(1);
        tHmDelayMapper.updateTHmDelay(tHmDelay);

        THmCustomerpaydays tHmCustomerpaydays = new THmCustomerpaydays();
        tHmCustomerpaydays.setWqId(hmCustomerpaydays.getWqId());
        //分期欠费金额
        if ("0.00".equals(hmCustomerpaydays.getWqStr6())) {
            tHmCustomerpaydays.setWqStr6(new BigDecimal(hmCustomerpaydays.getWqStr1()).subtract(new BigDecimal(tHmPayhistory.getHpayMoney())).subtract(new BigDecimal(kou)).toString());
        } else {
            tHmCustomerpaydays.setWqStr6(new BigDecimal(hmCustomerpaydays.getWqStr6()).subtract(new BigDecimal(tHmPayhistory.getHpayMoney())).subtract(new BigDecimal(kou)).toString());
        }
        //用户余额
        tHmCustomer.setBalance(tHmPayhistory.getBalance());
        //当期欠款金额是否小于0.00
        //小于0.00时 多交钱了，需要把多的更新到用户余额
        //等于0.00时 正好缴全
        //大于0.00时 还没有缴完 不做处理
        int i = new BigDecimal("0.00").compareTo(new BigDecimal(tHmCustomerpaydays.getWqStr6()));
        //如果当期欠费是不是 0.00
        if (i == 0) {
            //是
            if (tHmPayhistory.getHpaySfqe() == 0) {
                tHmCustomerpaydays.setWqInt1(0);
            } else if (tHmPayhistory.getHpaySfqe() == 1) {
                tHmCustomerpaydays.setWqInt1(1);
            }
        } else if (i == 1) {
            //缴多了
            if (tHmPayhistory.getHpaySfqe() == 0) {
                tHmCustomerpaydays.setWqInt1(0);
            } else if (tHmPayhistory.getHpaySfqe() == 1) {
                tHmCustomerpaydays.setWqInt1(1);
            }
            //多了钱
            tHmCustomer.setBalance(new BigDecimal(tHmCustomer.getBalance()).subtract(new BigDecimal(tHmCustomerpaydays.getWqStr6())).toString());
            //本期多缴的钱
            tHmPayhistory.setHpayStr5(new BigDecimal("0.00").subtract(new BigDecimal(tHmCustomerpaydays.getWqStr6())).toString());
            tHmCustomerpaydays.setWqStr6("0.00");
        }
        tHmCustomerpaydays.setWqInt2(99);
        tHmCustomerpaydaysMapper.updateTHmCustomerpaydays(tHmCustomerpaydays);
        //生成充值记录
        tHmPayhistory.setHpayTradeuserid(ShiroUtils.getSysUser().getUserId());
        tHmPayhistory.setHpayTradetime(sdf.format(new Date()));
        tHmPayhistory.setHpayAdduserid(ShiroUtils.getSysUser().getUserId());
        tHmPayhistory.setHpayAddtime(sdf.format(new Date()));
        tHmPayhistory.setHpaySfqe(4);
        tHmPayhistory.setHpayState(0);
        tHmPayhistory.setWqId(wqId);
        //单号
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        Instant instant = timestamp.toInstant();
        tHmPayhistory.setHpayOuttradeno("KD" + instant.toEpochMilli());
        tHmPayhistory.setHpayStr4(kou);
        switch (hmCustomerpaydays.getLing()) {
            //取整舍
            case 1:
                //实缴
                tHmPayhistory.setHpayMoney(LingUtil.zhengShe(tHmPayhistory.getHpayMoney()));
                tHmPayhistory.setLing("取整（只舍不入）");
                break;
            //取整入
            case 2:
                //实缴
                tHmPayhistory.setHpayMoney(LingUtil.zhengRu(tHmPayhistory.getHpayMoney()));
                tHmPayhistory.setLing("取整（四舍五入）");
                break;
            //按原价
            case 3:
                //实缴
                tHmPayhistory.setHpayMoney(LingUtil.erRu(tHmPayhistory.getHpayMoney()));
                tHmPayhistory.setLing("实际金额");
                break;
            //保留一位小数舍
            case 4:
                //实缴
                tHmPayhistory.setHpayMoney(LingUtil.yiShe(tHmPayhistory.getHpayMoney()));
                tHmPayhistory.setLing("保留一位小数点（只舍不入）");
                break;
            //保留一位小数入
            case 5:
                //实缴
                tHmPayhistory.setHpayMoney(LingUtil.yiRu(tHmPayhistory.getHpayMoney()));
                tHmPayhistory.setLing("保留一位小数点（四舍五入）");
                break;
            //保留两位小数入
            case 6:
                //实缴
                tHmPayhistory.setHpayMoney(LingUtil.erRu(tHmPayhistory.getHpayMoney()));
                tHmPayhistory.setLing("保留两位小数点（四舍五入）");
                break;
        }
        tHmPayhistoryMapper.insertTHmPayhistory(tHmPayhistory);
        //生成充值缴费单价表，用于打印
        THmCustomerpaydaysPrice tHmCustomerpaydaysPrice = new THmCustomerpaydaysPrice();
        tHmCustomerpaydaysPrice.setCustomerpaydaysId(hmCustomerpaydays.getWqId());
        List<THmCustomerpaydaysPrice> tHmCustomerpaydaysPriceList = tHmCustomerpaydaysPriceMapper.selectTHmCustomerpaydaysPriceList(tHmCustomerpaydaysPrice);
        for (THmCustomerpaydaysPrice hmCustomerpaydaysPrice : tHmCustomerpaydaysPriceList) {
            THmPayhistoryPrice tHmPayhistoryPrice = new THmPayhistoryPrice();
            tHmPayhistoryPrice.setPayhistoryId(tHmPayhistory.getHpayId());
            tHmPayhistoryPrice.setArea(hmCustomerpaydaysPrice.getArea());
            tHmPayhistoryPrice.setAreac("0");
            tHmPayhistoryPrice.setPriceType(hmCustomerpaydaysPrice.getPriceType());
            tHmPayhistoryPrice.setPrice(hmCustomerpaydaysPrice.getPrice());
            tHmPayhistoryPrice.setTotal(new BigDecimal(hmCustomerpaydaysPrice.getArea()).multiply(new BigDecimal(tHmPayhistoryPrice.getPrice())).multiply((new BigDecimal(hmCustomerpaydaysPrice.getXishu()).divide(new BigDecimal("100")))).toString());
            tHmPayhistoryPrice.setNian(hmCustomerpaydays.getHzqGrstarttime());
            tHmPayhistoryPrice.setType(hmCustomerpaydaysPrice.getType());
            tHmPayhistoryPrice.setXishu(hmCustomerpaydaysPrice.getXishu());
            //保存
            tHmPayhistoryPriceMapper.insertTHmPayhistoryPrice(tHmPayhistoryPrice);
        }
        //查所有账期如果有不是全额，基础热，续费，平账的账单即为0
        tHmCustomer.setIsZheng(1);
        THmCustomerpaydays tHmCustomerpaydays1 = new THmCustomerpaydays();
        tHmCustomerpaydays1.setHcustomerId(tHmCustomer.getHcustomerId());
        List<THmCustomerpaydays> tHmCustomerpaydaysLists = tHmCustomerpaydaysMapper.selectTHmCustomerpaydaysList(tHmCustomerpaydays1);
        for (THmCustomerpaydays customerpaydays : tHmCustomerpaydaysLists) {
            if (customerpaydays.getWqInt1() != 0 && customerpaydays.getWqInt1() != 1 && customerpaydays.getWqInt1() != 2 && customerpaydays.getWqInt1() != 4) {
                tHmCustomer.setIsZheng(0);
            }
        }
        //更新用户余额
        //更新用户int4 最后一次账期状态
        THmCustomerpaydays tHmCustomerpaydays2 = tHmCustomerpaydaysMapper.getLast(tHmPayhistory.getHcustomerId());
        tHmCustomer.setHcustomerInt4(tHmCustomerpaydays2.getWqInt1());
        return tHmCustomerMapper.updateTHmCustomer(tHmCustomer);
    }

    //续费
    @Override
    public int xufei(THmPayhistory tHmPayhistory) {
        //更新账期
        THmCustomerpaydays hmCustomerpaydays = tHmCustomerpaydaysMapper.selectTHmCustomerpaydaysById(tHmPayhistory.getWqId());
        //生成充值记录
        tHmPayhistory.setHpayTradeuserid(ShiroUtils.getSysUser().getUserId());
        tHmPayhistory.setHpayTradetime(sdf.format(new Date()));
        tHmPayhistory.setHpayAdduserid(ShiroUtils.getSysUser().getUserId());
        tHmPayhistory.setHpayAddtime(sdf.format(new Date()));
        tHmPayhistory.setHpaySfqe(2);
        tHmPayhistory.setHpayState(0);
        //单号
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        Instant instant = timestamp.toInstant();
        tHmPayhistory.setHpayOuttradeno("KD" + instant.toEpochMilli());
        switch (hmCustomerpaydays.getLing()) {
            //取整舍
            case 1:
                tHmPayhistory.setLing("取整（只舍不入）");
                break;
            //取整入
            case 2:
                tHmPayhistory.setLing("取整（四舍五入）");
                break;
            //按原价
            case 3:
                tHmPayhistory.setLing("实际金额");
                break;
            //保留一位小数舍
            case 4:
                tHmPayhistory.setLing("保留一位小数点（只舍不入）");
                break;
            //保留一位小数入
            case 5:
                tHmPayhistory.setLing("保留一位小数点（四舍五入）");
                break;
            //保留两位小数入
            case 6:
                tHmPayhistory.setLing("保留两位小数点（四舍五入）");
                break;
        }
        tHmPayhistoryMapper.insertTHmPayhistory(tHmPayhistory);
        //更新账期
        THmCustomerpaydays tHmCustomerpaydays = new THmCustomerpaydays();
        tHmCustomerpaydays.setWqId(tHmPayhistory.getWqId());
        tHmCustomerpaydays.setWqInt1(2);
        tHmCustomerpaydaysMapper.updateTHmCustomerpaydays(tHmCustomerpaydays);
        //生成充值缴费单价表
        THmCustomerpaydaysPrice tHmCustomerpaydaysPrice = new THmCustomerpaydaysPrice();
        tHmCustomerpaydaysPrice.setCustomerpaydaysId(hmCustomerpaydays.getWqId());
        List<THmCustomerpaydaysPrice> tHmCustomerpaydaysPriceList = tHmCustomerpaydaysPriceMapper.selectTHmCustomerpaydaysPriceList(tHmCustomerpaydaysPrice);
        for (THmCustomerpaydaysPrice hmCustomerpaydaysPrice : tHmCustomerpaydaysPriceList) {
            THmPayhistoryPrice tHmPayhistoryPrice = new THmPayhistoryPrice();
            tHmPayhistoryPrice.setPayhistoryId(tHmPayhistory.getHpayId());
            tHmPayhistoryPrice.setArea(hmCustomerpaydaysPrice.getArea());
            tHmPayhistoryPrice.setAreac("0");
            tHmPayhistoryPrice.setPriceType(hmCustomerpaydaysPrice.getPriceType());
            tHmPayhistoryPrice.setPrice(hmCustomerpaydaysPrice.getPrice());
            tHmPayhistoryPrice.setTotal(new BigDecimal(hmCustomerpaydaysPrice.getArea()).multiply(new BigDecimal(tHmPayhistoryPrice.getPrice())).multiply((new BigDecimal(hmCustomerpaydaysPrice.getXishu()).divide(new BigDecimal("100")))).toString());
            tHmPayhistoryPrice.setNian(hmCustomerpaydays.getHzqGrstarttime());
            tHmPayhistoryPrice.setType(hmCustomerpaydaysPrice.getType());
            tHmPayhistoryPrice.setXishu(hmCustomerpaydaysPrice.getXishu());
            //保存
            tHmPayhistoryPriceMapper.insertTHmPayhistoryPrice(tHmPayhistoryPrice);
        }
        //更新用户
        THmCustomer tHmCustomer = new THmCustomer();
        tHmCustomer.setHcustomerId(tHmPayhistory.getHcustomerId());
        //查所有账期如果有不是全额，基础热，续费，平账的账单即为0
        tHmCustomer.setIsZheng(1);
        THmCustomerpaydays tHmCustomerpaydays1 = new THmCustomerpaydays();
        tHmCustomerpaydays1.setHcustomerId(tHmCustomer.getHcustomerId());
        List<THmCustomerpaydays> tHmCustomerpaydaysList = tHmCustomerpaydaysMapper.selectTHmCustomerpaydaysList(tHmCustomerpaydays1);
        for (THmCustomerpaydays customerpaydays : tHmCustomerpaydaysList) {
            if (customerpaydays.getWqInt1() != 0 && customerpaydays.getWqInt1() != 1 && customerpaydays.getWqInt1() != 2 && customerpaydays.getWqInt1() != 4) {
                tHmCustomer.setIsZheng(0);
            }
        }
        //更新用户余额
        //更新用户int4 最后一次账期状态
        tHmCustomer.setHcustomerInt4(tHmPayhistory.getHpaySfqe());
        tHmCustomer.setBalance(tHmPayhistory.getBalance());
        tHmCustomerMapper.updateTHmCustomer(tHmCustomer);
        return tHmPayhistory.getHpayId();
    }

    //修改充值记录
    @Override
    public int updateTHmPayhistory(THmPayhistory tHmPayhistory) {
        //调整记录
        THmPayhistoryRecord tHmPayhistoryRecord = new THmPayhistoryRecord();
        tHmPayhistoryRecord.setHpayId(tHmPayhistory.getHpayId());
        tHmPayhistoryRecord.setHouYtotal(tHmPayhistory.getHpayYjmoney());
        tHmPayhistoryRecord.setHouZtotal(tHmPayhistory.getHpayMoney());
        tHmPayhistoryRecord.setHouType(tHmPayhistory.getHpayType());
        tHmPayhistoryRecord.setHouLei(tHmPayhistory.getHpaySfqe());
        THmPayhistory tHmPayhistory1 = tHmPayhistoryMapper.selectTHmPayhistoryById(tHmPayhistory.getHpayId());
        tHmPayhistoryRecord.setQianYtotal(tHmPayhistory1.getHpayYjmoney());
        tHmPayhistoryRecord.setQianZtotal(tHmPayhistory1.getHpayMoney());
        tHmPayhistoryRecord.setQianType(tHmPayhistory1.getHpayType());
        tHmPayhistoryRecord.setQianLei(tHmPayhistory1.getHpaySfqe());
        tHmPayhistoryRecord.setAddTime(new Date());
        tHmPayhistoryRecord.setAddUser((long) 102);
        tHmPayhistoryRecord.setStatus(0);
        tHmPayhistoryRecordMapper.insertTHmPayhistoryRecord(tHmPayhistoryRecord);

        return tHmPayhistoryMapper.updateTHmPayhistory(tHmPayhistory);
    }

    //销售明细
    @Override
    public DetailVO detail(DetailVO detailVO) {

        DetailVO detailVO1 = new DetailVO();
        if (detailVO.getAreaid() == null) {
            detailVO.setAreaid(1);
        }

        List<DetailListVO> detailListVOList = tHmPayhistoryMapper.detail(detailVO);
        detailListVOList = detailListVOList.stream().filter(o -> o.getAddTime() != null).collect(Collectors.toList());
        BigDecimal pay = new BigDecimal("0.00");
        BigDecimal opay = new BigDecimal("0.00");
        BigDecimal tfpay = new BigDecimal("0.00");
        for (DetailListVO detailListVO : detailListVOList) {
            //操作人员
            SysUser sysUser = sysUserMapper.selectUserById(detailListVO.getAdduserid());
            detailListVO.setAddUserName(sysUser.getUserName());

            THmArea tHmArea = tHmAreaMapper.selectTHmAreaById(detailListVO.getAreaid());
            if (tHmArea.getHareaLevel() == 2) {
                detailListVO.setAreaName(tHmArea.getHareaName());
            } else {
                THmArea tHmArea1 = tHmAreaMapper.selectTHmAreaById(tHmArea.getHareaParentid());
                detailListVO.setAreaName(tHmArea1.getHareaName());
            }
            if (detailListVO.getPay() != null && !detailListVO.getPay().equals("")) {
                pay = pay.add(new BigDecimal(detailListVO.getPay()));
                detailListVO.setType("1");
                detailListVO.setTypeName("充值缴费");
                //支付方式
                detailListVO.setPaytypeName(iSysDictDataService.selectDictLabel("data_paytype", detailListVO.getPaytype()));
            } else if (detailListVO.getOpay() != null && !detailListVO.getOpay().equals("")) {
                opay = opay.add(new BigDecimal(detailListVO.getOpay()));
                detailListVO.setType("2");
                detailListVO.setTypeName("其他缴费");
                detailListVO.setPaytypeName(iSysDictDataService.selectDictLabel("data_paytype", detailListVO.getPaytype()));
            } else if (detailListVO.getTfpay() != null && !detailListVO.getTfpay().equals("")) {
                tfpay = tfpay.add(new BigDecimal(detailListVO.getTfpay()));
                detailListVO.setTfpay("-" + detailListVO.getTfpay());
                detailListVO.setType("3");
                detailListVO.setTypeName("退费");
                detailListVO.setPaytype("");
                detailListVO.setPaytypeName("");
            }
        }

        detailVO1.setPayTotal(pay.toString());
        detailVO1.setOpayTotal(opay.toString());
        detailVO1.setTfpayTotal("-" + tfpay.toString());

        switch (detailVO.getType()) {
            case "1":
                detailVO1.setOpayTotal("");
                detailVO1.setTfpayTotal("");
                detailListVOList = detailListVOList.stream().filter(o -> o.getType().equals("1")).collect(Collectors.toList());
                break;
            case "2":
                detailVO1.setPayTotal("");
                detailVO1.setTfpayTotal("");
                detailListVOList = detailListVOList.stream().filter(o -> o.getType().equals("2")).collect(Collectors.toList());
                break;
            case "3":
                detailVO1.setPayTotal("");
                detailVO1.setOpayTotal("");
                detailListVOList = detailListVOList.stream().filter(o -> o.getType().equals("3")).collect(Collectors.toList());
                break;
        }
        detailVO1.setList(detailListVOList);

        return detailVO1;
    }

    @Override
    public DetailVO yanDetail(DetailVO detailVO) {

        DetailVO detailVO1 = new DetailVO();
        if (detailVO.getAreaid() == null) {
            detailVO.setAreaid(1);
        }

        List<DetailListVO> detailListVOList = tHmPayhistoryMapper.yanDetail(detailVO);
        detailListVOList = detailListVOList.stream().filter(o -> o.getAddTime() != null).collect(Collectors.toList());
        BigDecimal pay = new BigDecimal("0.00");
        for (DetailListVO detailListVO : detailListVOList) {
            //操作人员
            SysUser sysUser = sysUserMapper.selectUserById(detailListVO.getAdduserid());
            detailListVO.setAddUserName(sysUser.getUserName());

            THmArea tHmArea = tHmAreaMapper.selectTHmAreaById(detailListVO.getAreaid());
            if (tHmArea.getHareaLevel() == 2) {
                detailListVO.setAreaName(tHmArea.getHareaName());
            } else {
                THmArea tHmArea1 = tHmAreaMapper.selectTHmAreaById(tHmArea.getHareaParentid());
                detailListVO.setAreaName(tHmArea1.getHareaName());
            }
            if (detailListVO.getPay() != null && !detailListVO.getPay().equals("")) {
                pay = pay.add(new BigDecimal(detailListVO.getPay()));
                //支付方式
                detailListVO.setPaytypeName(iSysDictDataService.selectDictLabel("data_paytype", detailListVO.getPaytype()));
            }
        }

        detailVO1.setPayTotal(pay.toString());
        detailVO1.setList(detailListVOList);

        return detailVO1;
    }
}
