package com.ruoyi.business.mapper;

import com.ruoyi.business.domain.THmCustomerpaydaysPrice;
import com.ruoyi.business.domain.data.THmCustomerpaydaysPriceCopy;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 账期-单价Mapper接口
 */
public interface THmCustomerpaydaysPriceMapper {
    /**
     * 查询账期-单价
     */
    THmCustomerpaydaysPrice selectTHmCustomerpaydaysPriceById(@Param("id") int id);

    /**
     * 查询账期-单价列表
     */
    List<THmCustomerpaydaysPrice> selectTHmCustomerpaydaysPriceList(THmCustomerpaydaysPrice tHmCustomerpaydaysPrice);

    /**
     * 新增账期-单价
     */
    int insertTHmCustomerpaydaysPrice(THmCustomerpaydaysPrice tHmCustomerpaydaysPrice);

    /**
     * 修改账期-单价
     */
    int updateTHmCustomerpaydaysPrice(THmCustomerpaydaysPrice tHmCustomerpaydaysPrice);

    int delTHmCustomerpaydaysPrice(@Param("id") int id);

    List<THmCustomerpaydaysPrice> getByWqId(@Param("customerpaydaysId") int customerpaydaysId);

    THmCustomerpaydaysPrice getOneByWqId(@Param("customerpaydaysId") int customerpaydaysId);

    void insertTHmCustomerpaydaysPriceCopy(THmCustomerpaydaysPriceCopy tHmCustomerpaydaysPriceCopy);

    List<THmCustomerpaydaysPriceCopy> getCopyList(@Param("customerpaydaysId") int customerpaydaysId);
}
