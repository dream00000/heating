package com.ruoyi.business.service;

import com.ruoyi.business.domain.THmPayhistoryRecord;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 充值调整日志Service接口
 *
 * @author ruoyi
 * @date 2022-04-11
 */
public interface ITHmPayhistoryRecordService {
    /**
     * 查询充值调整日志
     *
     * @param id
     * @return 充值调整日志
     */
    public THmPayhistoryRecord selectTHmPayhistoryRecordById(@Param("id") int id);

    /**
     * 查询充值调整日志列表
     *
     * @param tHmPayhistoryRecord 充值调整日志
     * @return 充值调整日志集合
     */
    public List<THmPayhistoryRecord> selectTHmPayhistoryRecordList(THmPayhistoryRecord tHmPayhistoryRecord);

    /**
     * 新增充值调整日志
     *
     * @param tHmPayhistoryRecord 充值调整日志
     * @return 结果
     */
    public int insertTHmPayhistoryRecord(THmPayhistoryRecord tHmPayhistoryRecord);

    /**
     * 修改充值调整日志
     *
     * @param tHmPayhistoryRecord 充值调整日志
     * @return 结果
     */
    public int updateTHmPayhistoryRecord(THmPayhistoryRecord tHmPayhistoryRecord);

}
