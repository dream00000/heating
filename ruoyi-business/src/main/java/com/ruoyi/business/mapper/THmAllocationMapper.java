package com.ruoyi.business.mapper;

import java.util.List;
import com.ruoyi.business.domain.THmAllocation;

/**
 * 金额划拨Mapper接口
 *
 * @author ruoyi
 * @date 2022-03-16
 */
public interface THmAllocationMapper
{
    /**
     * 查询金额划拨
     *
     * @param id 金额划拨主键
     * @return 金额划拨
     */
    public THmAllocation selectTHmAllocationById(Long id);

    /**
     * 查询金额划拨列表
     *
     * @param tHmAllocation 金额划拨
     * @return 金额划拨集合
     */
    public List<THmAllocation> selectTHmAllocationList(THmAllocation tHmAllocation);


    /**
     * 新增金额划拨
     *
     * @param tHmAllocation 金额划拨
     * @return 结果
     */
    public int insertTHmAllocation(THmAllocation tHmAllocation);

    /**
     * 修改金额划拨
     *
     * @param tHmAllocation 金额划拨
     * @return 结果
     */
    public int updateTHmAllocation(THmAllocation tHmAllocation);

    /**
     * 删除金额划拨
     *
     * @param id 金额划拨主键
     * @return 结果
     */
    public int deleteTHmAllocationById(Long id);

    /**
     * 批量删除金额划拨
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTHmAllocationByIds(Long[] ids);
}
