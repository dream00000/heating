package com.ruoyi.business.service.impl;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.ruoyi.business.domain.*;
import com.ruoyi.business.mapper.*;
import com.ruoyi.business.service.ITHmCustomerpriceService;
import com.ruoyi.business.service.ITHmPaydaysService;
import com.ruoyi.business.util.LingUtil;
import com.ruoyi.framework.util.ShiroUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 用户单价Service业务层处理
 *
 * @author ruoyi
 * @date 2021-09-23
 */
@Service
public class THmCustomerpriceServiceImpl implements ITHmCustomerpriceService {
    @Resource
    private THmCustomerpriceMapper tHmCustomerpriceMapper;

    @Resource
    private THmCustomerpaydaysMapper tHmCustomerpaydaysMapper;

    @Resource
    private THmFeedbackMapper tHmFeedbackMapper;

    @Resource
    private ITHmPaydaysService itHmPaydaysService;

    @Resource
    private THmCustomerMapper tHmCustomerMapper;

    @Resource
    private THmDelayMapper tHmDelayMapper;

    @Resource
    private THmPayhistoryMapper tHmPayhistoryMapper;

    @Resource
    private THmCustomerpaydaysPriceMapper tHmCustomerpaydaysPriceMapper;

    SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * 查询用户单价
     *
     * @param id
     * @return 用户单价
     */
    @Override
    public THmCustomerprice selectTHmCustomerpriceById(int id) {
        return tHmCustomerpriceMapper.selectTHmCustomerpriceById(id);
    }

    /**
     * 查询用户单价列表
     *
     * @param tHmCustomerprice 用户单价
     * @return 用户单价
     */
    @Override
    public List<THmCustomerprice> selectTHmCustomerpriceList(THmCustomerprice tHmCustomerprice) {
        return tHmCustomerpriceMapper.selectTHmCustomerpriceList(tHmCustomerprice);
    }

    /**
     * 新增用户单价
     *
     * @param tHmCustomerprice 用户单价
     * @return 结果
     */
    @Override
    public int insertTHmCustomerprice(THmCustomerprice tHmCustomerprice) {
        return tHmCustomerpriceMapper.insertTHmCustomerprice(tHmCustomerprice);
    }

    @Override
    public int updateTHmCustomerprice(THmCustomerprice tHmCustomerprice) {
        return tHmCustomerpriceMapper.updateTHmCustomerprice(tHmCustomerprice);
    }

    /**
     * 阀控专用
     */
    @Override
    public int updateTHmCustomerprices(THmCustomerprice tHmCustomerprice) {

        THmCustomer tHmCustomer = tHmCustomerMapper.selectTHmCustomerById(tHmCustomerprice.getCustomerId());

        //添加阀控记录
        THmFeedback tHmFeedback = new THmFeedback();
        tHmFeedback.setFdType(1);//开关阀操作
        if (tHmCustomerprice.getTap() == 0) {
            tHmFeedback.setFdFmzt(1);
        } else if (tHmCustomerprice.getTap() == 1) {
            tHmFeedback.setFdFmzt(0);
        } else {
            tHmFeedback.setFdFmzt(tHmCustomerprice.getTap());
        }
        tHmFeedback.setHcustomerId(tHmCustomerprice.getCustomerId());
        tHmFeedback.setFdAddtime(sdf.format(new Date()));
        tHmFeedback.setFdAdduserName(ShiroUtils.getSysUser().getUserName());
        tHmFeedback.setFdState(0);
        tHmFeedback.setCustomerpriceId(Long.valueOf(tHmCustomerprice.getId()));
        tHmFeedback.setFdTitle("开关阀操作");
        tHmFeedback.setFdContent(ShiroUtils.getSysUser().getUserName() + "进行了阀控操作");
        tHmFeedbackMapper.insertTHmFeedback(tHmFeedback);
        //查出用户有没有延期的记录，有的话去最后一条记录，查出供热开始日期，和今日做比较，算出延期天数，再计算延期费用
        THmCustomerpaydays tHmCustomerpaydays = tHmCustomerpaydaysMapper.getLast(tHmCustomerprice.getCustomerId());
        if (tHmCustomerpaydays != null) {
            //查延期记录
            List<THmDelay> tHmDelayList = tHmDelayMapper.getByWqId(tHmCustomerpaydays.getWqId());
            //生成延期费用
            if (tHmCustomerprice.getTap() == 1 && (tHmCustomer.getPayType() == 1 || tHmDelayList.size() > 0)) {
                //生成延期记录
                THmDelay tHmDelay = new THmDelay();
                if (tHmCustomerpaydays.getWqInt1() == 3) {
                    System.out.println(tHmCustomerprice.getTopTime());
                    Date date1 = DateUtil.parse(tHmCustomerprice.getTopTime());
                    //取出供热开始、结束日期
                    THmPaydays tHmPaydays = itHmPaydaysService.selectTHmPaydaysById(tHmCustomerpaydays.getHzqId());
                    Date date2 = DateUtil.parse(tHmCustomerpaydays.getHzqGrstarttime() + "-" + tHmPaydays.getHzqGrstarttime());
                    //延期天数
                    long betweenDay = DateUtil.between(date1, date2, DateUnit.DAY);
                    //此阀门的延期金额  （日均价 - 20%日均价）*延期天数
                    THmCustomerprice tHmCustomerprice1 = tHmCustomerpriceMapper.selectTHmCustomerpriceById(tHmCustomerprice.getId());
                    BigDecimal bd = new BigDecimal(tHmCustomerprice1.getTotal());
                    BigDecimal bds = new BigDecimal(tHmCustomerpaydays.getHzqGrdays());
                    String jun = bd.divide(bds, 4, BigDecimal.ROUND_HALF_UP).toString();//日均价
                    tHmCustomerpaydays.setWqStr3(String.valueOf(betweenDay));
                    //累加延期金额
                    BigDecimal str = (new BigDecimal(jun).multiply(new BigDecimal("0.8")).multiply(new BigDecimal(betweenDay)));
                    BigDecimal strs = str.add(new BigDecimal(tHmCustomerpaydays.getWqStr4()));
                    switch (tHmCustomerpaydays.getLing()) {
                        //取整舍
                        case 1:
                            tHmCustomerpaydays.setWqStr4(LingUtil.zhengShe(strs.toString()));
                            tHmDelay.setTotal(LingUtil.zhengShe(str.toString()));//延期金额
                            break;
                        //取整入
                        case 2:
                            tHmCustomerpaydays.setWqStr4(LingUtil.zhengRu(strs.toString()));
                            tHmDelay.setTotal(LingUtil.zhengRu(str.toString()));//延期金额
                            break;
                        //按原价
                        case 3:
                            tHmCustomerpaydays.setWqStr4(LingUtil.erRu(strs.toString()));
                            tHmDelay.setTotal(LingUtil.erRu(str.toString()));//延期金额
                            break;
                        //保留一位小数舍
                        case 4:
                            tHmCustomerpaydays.setWqStr4(LingUtil.yiShe(strs.toString()));
                            tHmDelay.setTotal(LingUtil.yiShe(str.toString()));//延期金额
                            break;
                        //保留一位小数入
                        case 5:
                            tHmCustomerpaydays.setWqStr4(LingUtil.yiRu(strs.toString()));
                            tHmDelay.setTotal(LingUtil.yiRu(str.toString()));//延期金额
                            break;
                        //保留两位小数入
                        case 6:
                            tHmCustomerpaydays.setWqStr4(LingUtil.erRu(strs.toString()));
                            tHmDelay.setTotal(LingUtil.erRu(str.toString()));//延期金额
                            break;
                    }

                    tHmCustomerpaydaysMapper.updateTHmCustomerpaydays(tHmCustomerpaydays);

                    tHmDelay.setPriceId(tHmCustomerprice.getId());
                    tHmDelay.setCustomerId(tHmCustomerprice.getCustomerId());
                    tHmDelay.setDays(String.valueOf(betweenDay));//延期天数
                    tHmDelay.setDtotal(LingUtil.zhengShe(tHmCustomerprice1.getTotal()));//单价金额
                    tHmDelay.setAddTime(sdf.format(new Date()));
                    tHmDelay.setXishu("80");
                    tHmDelay.setZtotal(tHmCustomerpaydays.getWqStr1());//当期金额
                    tHmDelay.setStartDate(tHmCustomerpaydays.getHzqGrstarttime() + "-" + tHmPaydays.getHzqGrstarttime());//开始日期
                    tHmDelay.setEndDate(sd.format(new Date()));//结束日期
                    tHmDelay.setWqId(tHmCustomerpaydays.getWqId());
                    tHmDelay.setFbId(tHmFeedback.getFdId());
                    tHmDelay.setIsGai(0);
                    tHmDelay.setNian(tHmCustomerpaydays.getHzqGrstarttime() + "-" + tHmCustomerpaydays.getHzqGrendtime());//年度
                    tHmDelayMapper.insertTHmDelay(tHmDelay);
                }

                //修改账期price 系数 类型
                THmCustomerpaydaysPrice tHmCustomerpaydaysPrice = new THmCustomerpaydaysPrice();
                tHmCustomerpaydaysPrice.setCustomerpaydaysId(tHmCustomerpaydays.getWqId());
                List<THmCustomerpaydaysPrice> tHmCustomerpaydaysPriceList = tHmCustomerpaydaysPriceMapper.selectTHmCustomerpaydaysPriceList(tHmCustomerpaydaysPrice);
                if (tHmCustomerpaydaysPriceList.size() > 0) {
                    for (THmCustomerpaydaysPrice price : tHmCustomerpaydaysPriceList) {
                        price.setType(3);
                        price.setXishu("20");
                        tHmCustomerpaydaysPriceMapper.updateTHmCustomerpaydaysPrice(price);
                    }
                }
            }
        }

        //自动变更账期系数
        if (tHmCustomerpaydays != null) {
            List<THmPayhistory> tHmPayhistoryList = tHmPayhistoryMapper.getByWqId(tHmCustomerpaydays.getWqId());
            THmCustomerprice tHmCustomerprice1 = tHmCustomerpriceMapper.selectTHmCustomerpriceById(tHmCustomerprice.getId());
            if (tHmCustomerprice.getTap() == 1) {
                //查用户的最后一个账期 查看有没有充值记录
                //没有充值记录 将此单价 系数改为20% 实供改为停供
                if (tHmPayhistoryList.size() == 0) {
                    tHmCustomerprice.setRemark("20");
                    tHmCustomerprice.setTotal(new BigDecimal(tHmCustomerprice1.getTotal()).multiply(new BigDecimal("0.2")).toString());
                    tHmCustomerprice.setType(3);
                } else {
                    if (tHmPayhistoryList.get(0).getHpaySfqe() == 0) {
                        tHmCustomerprice.setRemark("20");
                        tHmCustomerprice.setTotal(new BigDecimal(tHmCustomerprice1.getTotal()).multiply(new BigDecimal("0.2")).toString());
                        tHmCustomerprice.setType(3);
                    }
                }
            } else if (tHmCustomerprice.getTap() == 0) {
                tHmCustomerprice.setRemark("100");
                tHmCustomerprice.setType(2);
            }
        }
        tHmCustomerpriceMapper.updateTHmCustomerprice(tHmCustomerprice);

        //更新用户费用类型
        List<THmCustomerprice> tHmCustomerpriceList = tHmCustomerpriceMapper.getByCustomerId(tHmCustomerprice.getCustomerId());
        if (tHmCustomerpriceList.size() > 0) {
            List<THmCustomerprice> list1 = tHmCustomerpriceList.stream().filter(o -> o.getType() == 2).collect(Collectors.toList());
            List<THmCustomerprice> list2 = tHmCustomerpriceList.stream().filter(o -> o.getType() == 3).collect(Collectors.toList());
            if (list1.size() == 0 && list2.size() > 0) {
                tHmCustomer.setPayType(0);
            } else if (list1.size() > 0 && list2.size() == 0) {
                tHmCustomer.setPayType(1);
            } else {
                tHmCustomer.setPayType(5);
            }
        }

        return tHmCustomerMapper.updateTHmCustomer(tHmCustomer);
    }

    @Override
    public void delTHmCustomerprice(Integer customerId) {
        tHmCustomerpriceMapper.delTHmCustomerprice(customerId);
    }

}
