package com.ruoyi.business.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 客服工单对象 t_hm_workorder
 *
 * @author ruoyi
 * @date 2022-02-26
 */
@Data
public class THmWorkorder extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 电话
     */
    @Excel(name = "电话")
    private String phone;

    /**
     * 用户id
     */
    @Excel(name = "用户id")
    private Integer customerId;

    /**
     * 用户名称
     */
    @Excel(name = "用户名称")
    private String customerName;

    /**
     * 内容
     */
    @Excel(name = "内容")
    private String content;

    /**
     * 工单类型
     */
    @Excel(name = "工单类型")
    private String workType;

    /**
     * 服务类型
     */
    @Excel(name = "服务类型")
    private String sonType;

    private String addTime;

    //工单id
    private Integer workorderid;
}
