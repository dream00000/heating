package com.ruoyi.business.controller;

import com.ruoyi.business.domain.THmCustomer;
import com.ruoyi.business.domain.THmNameList;
import com.ruoyi.business.service.ITHmCustomerService;
import com.ruoyi.business.service.THmNameListService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 黑白名单
 */
@Controller
@RequestMapping("/business/namelist")
public class THmNameListController extends BaseController {

    private String prefix = "business/namelist";

    @Resource
    private THmNameListService tHmNameListService;

    @Resource
    private ITHmCustomerService tHmCustomerService;

    @GetMapping()
    public String namelist() {
        return prefix + "/customer";
    }

    /**
     * 用户列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(THmCustomer tHmCustomer) {
        startPage();
        List<THmCustomer> list = tHmCustomerService.selectTHmCustomerList(tHmCustomer);
        return getDataTable(list);
    }


    /**
     * 黑白名单列表
     */
    @PostMapping("/list/{id}")
    @ResponseBody
    public TableDataInfo list(@PathVariable("id") Integer id) {
        startPage();
        List<THmNameList> list = tHmNameListService.getByCustomerId(id);
        return getDataTable(list);
    }

    /**
     * 新增黑白名单
     */
    @GetMapping("/add/{id}")
    public String add(@PathVariable("id") int id, ModelMap mmap) {
        THmNameList tHmNameList = new THmNameList();
        tHmNameList.setCustomerId(id);
        mmap.put("tHmNameList", tHmNameList);
        return prefix + "/add";
    }

    /**
     * 新增黑白名单
     */
    @Log(title = "新增黑白名单", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(THmNameList tHmNameList) {
        return toAjax(tHmNameListService.insertTHmNameList(tHmNameList));
    }

    /**
     * 获取工单详情
     */
    @GetMapping("/getById/{id}")
    public String getById(@PathVariable("id") Integer id, ModelMap mmap) {
        mmap.put("customerId", id);
        return prefix + "/detail";
    }
}
