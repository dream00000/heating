package com.ruoyi.business.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.TreeEntity;
import lombok.Data;

/**
 * 供热分区对象 t_hm_area
 *
 * @author lihao
 * @date 2021-08-13
 */
@Data
public class THmArea extends TreeEntity {

    //主键
    private Integer hareaId;

    @Excel(name = "区域名称")
    private String hareaName;

    @Excel(name = "区域编号")
    private String hareaCode;

    //上级id
    private Integer hareaParentid;

    @Excel(name = "上级分区名称")
    private String parentName;

    @Excel(name = "排序")
    private Integer hareaOrderid;

    //是否删除0否1是
    private Integer hareaState;

    //添加人id
    private Long hareaAdduserid;

    @Excel(name = "添加人")
    private String hareaAdduserName;

    //添加时间
    @Excel(name = "添加时间")
    private String hareaAddtime;

    //账期外键
    private Integer paydaysId;

    //等级
    private Integer hareaLevel;

    //热源站外键
    private Long hstationId;
    private String hstationName;

    //批量创建下级
    private String areaName;
}
