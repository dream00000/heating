package com.ruoyi.business.mapper;

import com.ruoyi.business.domain.THmMeterprice;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用热单价Mapper接口
 */
public interface THmMeterpriceMapper {
    /**
     * 查询用热单价
     */
    THmMeterprice selectTHmMeterpriceById(@Param("id") int id);

    /**
     * 查询用热单价列表
     */
    List<THmMeterprice> selectTHmMeterpriceList(THmMeterprice tHmMeterprice);

    /**
     * 新增用热单价
     */
    int insertTHmMeterprice(THmMeterprice tHmMeterprice);

    /**
     * 修改用热单价
     */
    int updateTHmMeterprice(THmMeterprice tHmMeterprice);
}
