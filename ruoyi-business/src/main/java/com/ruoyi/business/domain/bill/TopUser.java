package com.ruoyi.business.domain.bill;

import lombok.Data;

import java.util.List;

@Data
public class TopUser {
    private String subTotalMoney;//小计金额
    private Integer subTotalCount;//小计笔数
    private String name;//姓名

    private List<Recharge> rechargeList;//充值
    private List<Other> otherList;//充值
    private List<Refund> refundList;//退费
}
