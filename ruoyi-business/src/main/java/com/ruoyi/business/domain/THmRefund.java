package com.ruoyi.business.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 退费对象 t_hm_refund
 *
 * @author ruoyi
 * @date 2021-08-13
 */
@Data
public class THmRefund extends BaseEntity {

    private Integer tfId;

    /**
     * 用户id
     */
    private Integer hcustomerId;

    @Excel(name = "所属分区")
    private String customerArea;

    @Excel(name = "详细地址")
    private String address;

    @Excel(name = "用户编号")
    private String customerCode;

    @Excel(name = "用户姓名")
    private String customerName;

    @Excel(name = "手机号码")
    private String customerPhone;

    private String balance;

    private String customerCard;

    /**
     * 账期id
     */
    private Integer wqId;

    //退费类型 0=现金支付 1=添加到余额
    private Integer tfType;

    /**
     * 退费金额
     */
    @Excel(name = "退费金额")
    private String tfMoney;

    private String tfMoneyd;

    /**
     * 退费原因
     */
    @Excel(name = "退费原因")
    private String tfMemo;

    private String sysname;

    /**
     * 是否删除0否1是
     */
    private Integer tfState;

    /**
     * 操作人
     */
    private Long tfAdduserid;

    /**
     * 操作时间
     */
    private String tfAddtime;
}
