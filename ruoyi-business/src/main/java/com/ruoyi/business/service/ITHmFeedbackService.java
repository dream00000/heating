package com.ruoyi.business.service;

import com.ruoyi.business.domain.THmCustomer;
import com.ruoyi.business.domain.THmFeedback;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 稽查反馈信息Service接口
 */
public interface ITHmFeedbackService {
    /**
     * 查询稽查反馈信息
     */
    public THmFeedback selectTHmFeedbackById(@Param("id") int id);

    /**
     * 查询稽查反馈信息列表
     */
    public List<THmFeedback> selectTHmFeedbackList(THmFeedback tHmFeedback);

    public List<THmFeedback> selectTHmFeedbackListt(THmCustomer tHmCustomer);

    public List<THmCustomer> selectTHmFeedbackLists(THmCustomer tHmCustomer);

    /**
     * 新增稽查反馈信息
     */
    public int insertTHmFeedback(THmFeedback tHmFeedback);

    /**
     * 修改稽查反馈信息
     */
    public int updateTHmFeedback(THmFeedback tHmFeedback);

    //新版阀控 阀门管理列表
    List<THmCustomer> tapCustomers(THmCustomer tHmCustomer);

    List<THmCustomer> tapCustomer(THmCustomer tHmCustomer);

    List<THmCustomer> tapCustomerd(THmCustomer tHmCustomer);
    List<THmCustomer> tapCustomerd1(THmCustomer tHmCustomer);
    List<THmCustomer> tapCustomerd2(THmCustomer tHmCustomer);
    List<THmCustomer> tapCustomerd3(THmCustomer tHmCustomer);

    //根据单价id查用户
    THmCustomer tapCustomerByPriceId(@Param("pricesId") int id);
}
