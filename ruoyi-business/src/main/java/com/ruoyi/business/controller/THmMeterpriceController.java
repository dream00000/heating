package com.ruoyi.business.controller;

import com.ruoyi.business.domain.THmCustomerprice;
import com.ruoyi.business.domain.THmMeterprice;
import com.ruoyi.business.service.ITHmCustomerpriceService;
import com.ruoyi.business.service.ITHmMeterpriceService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用热单价Controller
 *
 * @author ruoyi
 * @date 2021-08-13
 */
@Controller
@RequestMapping("/business/meterprice")
public class THmMeterpriceController extends BaseController {
    private String prefix = "business/meterprice";

    @Resource
    private ITHmMeterpriceService tHmMeterpriceService;

    @Resource
    private ITHmCustomerpriceService itHmCustomerpriceService;

    @GetMapping()
    public String meterprice() {
        return prefix + "/meterprice";
    }

    /**
     * 查询用热单价列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(THmMeterprice tHmMeterprice) {
        startPage();
        List<THmMeterprice> list = tHmMeterpriceService.selectTHmMeterpriceList(tHmMeterprice);
        return getDataTable(list);
    }

    /**
     * 导出用热单价列表
     */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(THmMeterprice tHmMeterprice) {
        List<THmMeterprice> list = tHmMeterpriceService.selectTHmMeterpriceList(tHmMeterprice);
        ExcelUtil<THmMeterprice> util = new ExcelUtil<THmMeterprice>(THmMeterprice.class);
        return util.exportExcel(list, "meterprice");
    }

    /**
     * 新增用热单价
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存用热单价
     */
    @Log(title = "新增用热单价", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(THmMeterprice tHmMeterprice) {
        return toAjax(tHmMeterpriceService.insertTHmMeterprice(tHmMeterprice));
    }

    /**
     * 修改用热单价
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") int id, ModelMap mmap) {
        THmMeterprice tHmMeterprice = tHmMeterpriceService.selectTHmMeterpriceById(id);
        mmap.put("tHmMeterprice", tHmMeterprice);
        return prefix + "/edit";
    }

    /**
     * 修改保存用热单价
     */
    @Log(title = "修改用热单价", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(THmMeterprice tHmMeterprice) {
        return toAjax(tHmMeterpriceService.updateTHmMeterprice(tHmMeterprice));
    }

    /**
     * 删除用热单价
     */
    @Log(title = "删除用热单价", businessType = BusinessType.DELETE)
    @PostMapping("/remove/{id}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("id") int id) {

        THmCustomerprice tHmCustomerprice = new THmCustomerprice();
        tHmCustomerprice.setPriceId(id);
        List<THmCustomerprice> list = itHmCustomerpriceService.selectTHmCustomerpriceList(tHmCustomerprice);

        if(list.size() > 0){
            return AjaxResult.warn("单价已绑定用户不能删除");
        }else{
            THmMeterprice tHmMeterprice = new THmMeterprice();
            tHmMeterprice.setRjId(id);
            tHmMeterprice.setRjState(1);
            return toAjax(tHmMeterpriceService.updateTHmMeterprice(tHmMeterprice));
        }
    }
}
