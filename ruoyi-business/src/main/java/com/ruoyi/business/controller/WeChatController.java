package com.ruoyi.business.controller;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.ijpay.core.kit.HttpKit;
import com.ijpay.core.kit.WxPayKit;
import com.ruoyi.business.domain.*;
import com.ruoyi.business.domain.data.Sheet1;
import com.ruoyi.business.domain.data.THmCustomerpaydaysPriceCopy;
import com.ruoyi.business.domain.data.WuYe;
import com.ruoyi.business.domain.vo.*;
import com.ruoyi.business.mapper.*;
import com.ruoyi.business.service.*;
import com.ruoyi.business.util.LingUtil;
import com.ruoyi.business.util.WeChatPayUtils;
import com.ruoyi.business.util.WeChatUtils;
import com.ruoyi.common.config.Global;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.IpUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.mapper.SysUserMapper;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 微信登录控制接口
 */
@Controller
@RequestMapping("/kddz/wechat")
public class WeChatController extends BaseController {

    @Resource
    private ITHmCustomerService itHmCustomerService;

    @Resource
    private ITHmFeedbackService itHmFeedbackService;

    @Resource
    private ITHmEnclosureService itHmEnclosureService;

    @Resource
    private ITHmCustomerpaydaysService itHmCustomerpaydaysService;

    @Resource
    private ITHmCustomerpriceService itHmCustomerpriceService;

    @Resource
    private SysUserMapper sysUserMapper;

    @Resource
    private ITHmPayhistoryService itHmPayhistoryService;

    @Resource
    private ITHmPaydaysService itHmPaydaysService;

    @Resource
    private THmDelayMapper tHmDelayMapper;

    @Resource
    private THmCustomerpaydaysMapper tHmCustomerpaydaysMapper;

    @Resource
    private THmPayhistoryMapper tHmPayhistoryMapper;

    @Resource
    private THmCustomerMapper tHmCustomerMapper;

    @Resource
    private THmCustomerpriceMapper tHmCustomerpriceMapper;

    @Resource
    private THmCustomerpaydaysPriceMapper tHmCustomerpaydaysPriceMapper;

    @Resource
    private WxPayBean wxPayBean;

    SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    //工作人员登录
    @PostMapping("/login")
    @ResponseBody
    public AjaxResult login(String username, String password, Boolean rememberMe) {
        UsernamePasswordToken token = new UsernamePasswordToken(username, password, rememberMe);
        Subject subject = SecurityUtils.getSubject();
        try {
            subject.login(token);
            SysUser sysUser = sysUserMapper.selectUserByLoginName(username);
            return AjaxResult.success("成功", sysUser.getUserName());
        } catch (AuthenticationException e) {
            String msg = "用户或密码错误";
            if (StringUtils.isNotEmpty(e.getMessage())) {
                msg = e.getMessage();
            }
            return AjaxResult.success("失败", msg);
        }
    }

    //用户列表
    @PostMapping("/customerList")
    @ResponseBody
    public AjaxResult customerList(Integer type, String context) {
        THmCustomer tHmCustomer = new THmCustomer();
        if (type == 1) {
            tHmCustomer.setHcustomerName(context);
        } else if (type == 2) {
            tHmCustomer.setHcustomerMobile(context);
        } else if (type == 3) {
            tHmCustomer.setHareaName(context);
        } else if (type == 4) {
            tHmCustomer.setHcustomerCode(context);
        }
        tHmCustomer.setHareaId(1);
        startPage();
        List<THmCustomer> tHmCustomerList = itHmCustomerService.getTHmCustomerListForWechart(tHmCustomer);
        return AjaxResult.success("成功", tHmCustomerList);
    }

    //用户详情
    @PostMapping("/customerInfo")
    @ResponseBody
    public AjaxResult customerInfo(Integer pricesId) {
        return AjaxResult.success("成功", itHmFeedbackService.tapCustomerByPriceId(pricesId));
    }

    //操作历史记录
    @PostMapping("/feedbackList")
    @ResponseBody
    public AjaxResult feedbackList(Integer hcustomerId, Integer pricesId) {
        THmCustomer tHmCustomer = new THmCustomer();
        tHmCustomer.setPricesId(pricesId);
        List<THmFeedback> tHmFeedbackList = itHmFeedbackService.selectTHmFeedbackListt(tHmCustomer);
        return AjaxResult.success("成功", tHmFeedbackList);
    }

    //用户历史详情
    @PostMapping("/feedbackInfo")
    @ResponseBody
    public AjaxResult feedbackInfo(Integer fdId) {
        THmFeedback tHmFeedback = itHmFeedbackService.selectTHmFeedbackById(fdId);
        return AjaxResult.success("成功", tHmFeedback);
    }

    //阀控
    @PostMapping("/tap")
    @ResponseBody
    public AjaxResult tap(Integer fdFmzt, Integer hcustomerId, String context, String username, Integer pricesId) {

        THmCustomer tHmCustomer = itHmCustomerService.selectTHmCustomerById(hcustomerId);

        //增加阀控记录
        THmFeedback tHmFeedback = new THmFeedback();
        tHmFeedback.setFdType(1);//开关阀操作
        tHmFeedback.setFdFmzt(fdFmzt);
        tHmFeedback.setHcustomerId(hcustomerId);
        tHmFeedback.setFdAddtime(sdf.format(new Date()));
        tHmFeedback.setFdState(0);
        tHmFeedback.setFdTitle("开关阀操作");
        tHmFeedback.setFdContent(context);
        tHmFeedback.setFdAdduserName(username);
        tHmFeedback.setCustomerpriceId(Long.valueOf(pricesId));
        itHmFeedbackService.insertTHmFeedback(tHmFeedback);
        //查出用户有没有延期的记录，有的话去最后一条记录，查出供热开始日期，和今日做比较，算出延期天数，再计算延期费用
        THmCustomerpaydays tHmCustomerpaydays = tHmCustomerpaydaysMapper.getLast(hcustomerId);
        if (tHmCustomerpaydays != null) {
            //查延期记录
            List<THmDelay> tHmDelayList = tHmDelayMapper.getByWqId(tHmCustomerpaydays.getWqId());
            //生成延期费用
            if (fdFmzt == 0 && (tHmCustomer.getPayType() == 1 || tHmDelayList.size() > 0)) {
                //生成延期记录
                THmDelay tHmDelay = new THmDelay();
                if (tHmCustomerpaydays.getWqInt1() == 3) {
                    Date date1 = DateUtil.parse(sd.format(new Date()));
                    //取出供热开始日期
                    THmPaydays tHmPaydays = itHmPaydaysService.selectTHmPaydaysById(tHmCustomerpaydays.getHzqId());
                    Date date2 = DateUtil.parse(tHmCustomerpaydays.getHzqGrstarttime() + "-" + tHmPaydays.getHzqGrstarttime());
                    //延期天数
                    long betweenDay = DateUtil.between(date1, date2, DateUnit.DAY);
                    //延期金额  （日均价 - 20%日均价）*延期天数
                    BigDecimal bd = new BigDecimal(tHmCustomerpaydays.getWqStr1());
                    BigDecimal bds = new BigDecimal(tHmCustomerpaydays.getHzqGrdays());
                    String jun = bd.divide(bds, 4, BigDecimal.ROUND_HALF_UP).toString();//日均价
                    tHmCustomerpaydays.setWqStr3(String.valueOf(betweenDay));
                    //累加延期金额
                    BigDecimal str = (new BigDecimal(jun).multiply(new BigDecimal("0.8")).multiply(new BigDecimal(betweenDay)));
                    BigDecimal strs = str.add(new BigDecimal(tHmCustomerpaydays.getWqStr4()));
                    switch (tHmCustomerpaydays.getLing()) {
                        //取整舍
                        case 1:
                            tHmCustomerpaydays.setWqStr4(LingUtil.zhengShe(strs.toString()));
                            tHmDelay.setTotal(LingUtil.zhengShe(str.toString()));//延期金额
                            break;
                        //取整入
                        case 2:
                            tHmCustomerpaydays.setWqStr4(LingUtil.zhengRu(strs.toString()));
                            tHmDelay.setTotal(LingUtil.zhengRu(str.toString()));//延期金额
                            break;
                        //按原价
                        case 3:
                            tHmCustomerpaydays.setWqStr4(LingUtil.erRu(strs.toString()));
                            tHmDelay.setTotal(LingUtil.erRu(str.toString()));//延期金额
                            break;
                        //保留一位小数舍
                        case 4:
                            tHmCustomerpaydays.setWqStr4(LingUtil.yiShe(strs.toString()));
                            tHmDelay.setTotal(LingUtil.yiShe(str.toString()));//延期金额
                            break;
                        //保留一位小数入
                        case 5:
                            tHmCustomerpaydays.setWqStr4(LingUtil.yiRu(strs.toString()));
                            tHmDelay.setTotal(LingUtil.yiRu(str.toString()));//延期金额
                            break;
                        //保留两位小数入
                        case 6:
                            tHmCustomerpaydays.setWqStr4(LingUtil.erRu(strs.toString()));
                            tHmDelay.setTotal(LingUtil.erRu(str.toString()));//延期金额
                            break;
                    }

                    itHmCustomerpaydaysService.updateTHmCustomerpaydays(tHmCustomerpaydays);

                    //生成延期记录
                    THmCustomerprice tHmCustomerprice = itHmCustomerpriceService.selectTHmCustomerpriceById(pricesId);
                    tHmDelay.setPriceId(tHmCustomerprice.getId());
                    tHmDelay.setCustomerId(tHmCustomerprice.getCustomerId());
                    tHmDelay.setDays(String.valueOf(betweenDay));//延期天数
                    tHmDelay.setDtotal(tHmCustomerprice.getTotal());//单价金额
                    tHmDelay.setAddTime(sdf.format(new Date()));
                    tHmDelay.setXishu("80");
                    tHmDelay.setZtotal(tHmCustomerpaydays.getWqStr1());//当期金额
                    tHmDelay.setStartDate(tHmCustomerpaydays.getHzqGrstarttime() + "-" + tHmPaydays.getHzqGrstarttime());//开始日期
                    tHmDelay.setEndDate(sd.format(new Date()));//结束日期
                    tHmDelay.setWqId(tHmCustomerpaydays.getWqId());
                    tHmDelay.setFbId(tHmFeedback.getFdId());
                    tHmDelay.setIsGai(0);
                    tHmDelay.setNian(tHmCustomerpaydays.getHzqGrstarttime() + "-" + tHmCustomerpaydays.getHzqGrendtime());//年度
                    tHmDelayMapper.insertTHmDelay(tHmDelay);

                    //修改账期price 系数 类型
                    THmCustomerpaydaysPrice tHmCustomerpaydaysPrice = new THmCustomerpaydaysPrice();
                    tHmCustomerpaydaysPrice.setCustomerpaydaysId(tHmCustomerpaydays.getWqId());
                    List<THmCustomerpaydaysPrice> tHmCustomerpaydaysPriceList = tHmCustomerpaydaysPriceMapper.selectTHmCustomerpaydaysPriceList(tHmCustomerpaydaysPrice);
                    if (tHmCustomerpaydaysPriceList.size() > 0) {
                        for (THmCustomerpaydaysPrice price : tHmCustomerpaydaysPriceList) {
                            price.setType(3);
                            price.setXishu("20");
                            tHmCustomerpaydaysPriceMapper.updateTHmCustomerpaydaysPrice(price);
                        }
                    }
                }
            }
        }
        //自动变更账期系数
        THmCustomerprice tHmCustomerprice = new THmCustomerprice();
        if (tHmCustomerpaydays != null) {
            List<THmPayhistory> tHmPayhistoryList = tHmPayhistoryMapper.getByWqId(tHmCustomerpaydays.getWqId());
            THmCustomerprice tHmCustomerprice1 = tHmCustomerpriceMapper.selectTHmCustomerpriceById(tHmCustomerprice.getId());
            if (fdFmzt == 0) {
                //查用户的最后一个账期 查看有没有充值记录
                //没有充值记录 将此单价 系数改为20% 实供改为停供
                if (tHmPayhistoryList.size() == 0) {
                    tHmCustomerprice.setRemark("20");
                    tHmCustomerprice.setTotal(new BigDecimal(tHmCustomerprice1.getTotal()).multiply(new BigDecimal("0.2")).toString());
                    tHmCustomerprice.setType(3);
                } else {
                    if (tHmPayhistoryList.get(0).getHpaySfqe() == 0) {
                        tHmCustomerprice.setRemark("20");
                        tHmCustomerprice.setTotal(new BigDecimal(tHmCustomerprice1.getTotal()).multiply(new BigDecimal("0.2")).toString());
                        tHmCustomerprice.setType(3);
                    }
                }
            } else if (fdFmzt == 1) {
                tHmCustomerprice.setRemark("100");
                tHmCustomerprice.setType(2);
            }
        }
        if (fdFmzt == 0) {
            tHmCustomerprice.setTap(1);
        } else if (fdFmzt == 1) {
            tHmCustomerprice.setTap(0);
        } else {
            tHmCustomerprice.setTap(2);
        }
        tHmCustomerprice.setId(pricesId);
        itHmCustomerpriceService.updateTHmCustomerprice(tHmCustomerprice);
        //更新用户费用类型
        List<THmCustomerprice> tHmCustomerpriceList = tHmCustomerpriceMapper.getByCustomerId(hcustomerId);
        if (tHmCustomerpriceList.size() > 0) {
            List<THmCustomerprice> list1 = tHmCustomerpriceList.stream().filter(o -> o.getType() == 2).collect(Collectors.toList());
            List<THmCustomerprice> list2 = tHmCustomerpriceList.stream().filter(o -> o.getType() == 3).collect(Collectors.toList());
            if (list1.size() == 0 && list2.size() > 0) {
                tHmCustomer.setPayType(0);
            } else if (list1.size() > 0 && list2.size() == 0) {
                tHmCustomer.setPayType(1);
            } else {
                tHmCustomer.setPayType(5);
            }
        }
        tHmCustomerMapper.updateTHmCustomer(tHmCustomer);

        return AjaxResult.success("成功");
    }

    //普通上传
    @PostMapping("/check")
    @ResponseBody
    public AjaxResult check(Integer fdFmzt, Integer hcustomerId, String title, String context, String username, Long pricesId) {
        THmFeedback tHmFeedback = new THmFeedback();
        tHmFeedback.setFdType(0);//普通反馈
        tHmFeedback.setFdFmzt(fdFmzt);//开，关
        tHmFeedback.setHcustomerId(hcustomerId);
        tHmFeedback.setFdAddtime(sdf.format(new Date()));
        tHmFeedback.setFdAdduserName(username);
        tHmFeedback.setFdState(0);
        tHmFeedback.setFdTitle(title);
        tHmFeedback.setFdContent(context);
        tHmFeedback.setFdFmztName(username);
        tHmFeedback.setFdFjcode(String.valueOf(System.currentTimeMillis()));
        tHmFeedback.setCustomerpriceId(pricesId);
        itHmFeedbackService.insertTHmFeedback(tHmFeedback);
        return AjaxResult.success("成功", itHmFeedbackService.selectTHmFeedbackById(tHmFeedback.getFdId()));
    }

    //拍照稽查上传
    @PostMapping("/upload")
    @ResponseBody
    public AjaxResult upload(String code, @RequestParam("file") MultipartFile file) {
        THmEnclosure tHmEnclosure = new THmEnclosure();
        try {
            if (!file.isEmpty()) {
                String url = FileUploadUtils.upload(Global.getAvatarPath(), file);
                tHmEnclosure.setEnFjcode(code);
                tHmEnclosure.setEnFjurl(url);
                tHmEnclosure.setEnAddtime(sdf.format(new Date()));
                tHmEnclosure.setEnState(0);
                tHmEnclosure.setEnZcstate(1);//正式
                itHmEnclosureService.insertTHmEnclosure(tHmEnclosure);
                return AjaxResult.success("成功", tHmEnclosure);
            }
            return AjaxResult.success("失败", tHmEnclosure);
        } catch (Exception e) {
            return AjaxResult.success("失败", tHmEnclosure);
        }
    }

    //拍照稽查上传
    @PostMapping("/getUpload")
    @ResponseBody
    public AjaxResult getUpload(String code) {
        THmEnclosure tHmEnclosure = new THmEnclosure();
        tHmEnclosure.setEnFjcode(code);
        List<THmEnclosure> tHmEnclosureList = itHmEnclosureService.selectTHmEnclosureList(tHmEnclosure);
        for (THmEnclosure hmEnclosure : tHmEnclosureList) {
            hmEnclosure.setEnFjurl("http://" + wxPayBean.getDomain() + "/" + hmEnclosure.getEnFjurl());
        }
        return AjaxResult.success("成功", tHmEnclosureList);
    }

    //-------------------------------------微信支付-------------------------------

    @Resource
    private WeChatUtils weChatUtils;

    @Resource
    private WeChatPayUtils weChatPayUtils;

    @Resource
    private IWechatUserService wechatUserService;

    @Resource
    private IPaymentRecordService paymentRecordService;

    /**
     * 授权认证跳转
     */
    @GetMapping("/accredit")
    public String electricmeteraccreditOpen() {
        String url = weChatUtils.weChatGetCodes();
        return redirect(url);
    }

    /**
     * 授权认证跳转
     */
    @GetMapping("/accredits")
    public String electricmeteraccreditOpens(String c) {
        String url = weChatUtils.weChatGetCode(wxPayBean, c);
        return redirect(url);
    }

    @GetMapping("/getInfo")
    public String getInfo(String code, String state) {
        if (StrUtil.isNotBlank(code)) {
            WechatUser wechatUser = weChatUtils.weChatGetOpenid(wxPayBean, code);
            WechatUser wechatUser1 = wechatUserService.getWechatUser(wechatUser.getOpenId());
            if (wechatUser1 == null) {
                WechatUser wechatUser2 = weChatUtils.wechatGetUserInfo(wxPayBean, wechatUser);
                wechatUserService.insertWechatUser(wechatUser2);
            } else {
                WechatUser wechatUser3 = weChatUtils.wechatGetUserInfo(wxPayBean, wechatUser);
                wechatUser3.setWechatId(wechatUser1.getWechatId());
                wechatUserService.updateWechatUser(wechatUser3);
            }
            return redirect("/wechatheat/index.html#/?c=" + state + "&openId=" + wechatUser.getOpenId());
        } else {
            return "错误页面";
        }
    }

    @PostMapping("getWxCodeConfig")
    @ResponseBody
    public AjaxResult getWxCodeConfig(String url) {
        if (StrUtil.isNotBlank(url)) {
            Map<String, String> sdkMap = weChatUtils.wechatGetSDKJSON(wxPayBean, url);
            if (sdkMap != null) {
                return AjaxResult.success(sdkMap);
            } else {
                return null;
            }
        } else {
            return AjaxResult.error("授权失败！");
        }
    }

    //绑定列表
    @PostMapping("customerLists")
    @ResponseBody
    public AjaxResult customerList(String openId) {
        WechatUser wechatUser = wechatUserService.getWechatUser(openId);
        return AjaxResult.success(itHmCustomerService.getWechatBindlist(wechatUser.getWechatId()));
    }

    //用户信息
    @PostMapping("customerInfos")
    @ResponseBody
    public AjaxResult customerInfo(String openId) {
        return AjaxResult.success(wechatUserService.getWechatUser(openId));
    }

    //用户信息
    @PostMapping("customerInfosByCode")
    @ResponseBody
    public AjaxResult customerInfosByCode(String code) {
        return AjaxResult.success(itHmCustomerService.selectTHmCustomerByHcustomerCode(code));
    }

    //绑定
    @PostMapping("bindCustomerCode")
    @ResponseBody
    public AjaxResult bindCustomerCode(String openId, String code) {
        //根据用户code和手机号查询是否存在用户
        THmCustomer tHmCustomer = itHmCustomerService.selectTHmCustomerByHcustomerCode(code);
        if (tHmCustomer == null) {
            return AjaxResult.error("1", "查询无此用户信息");
        } else {
            //根据openId获取用户信息
            WechatUser wechatUser = wechatUserService.getWechatUser(openId);
            //绑定
            WechatBind wechatBind = new WechatBind();
            wechatBind.setCustomerId(tHmCustomer.getHcustomerId());
            wechatBind.setWechatuserId(wechatUser.getWechatId());
            wechatBind.setStatus(0);
            return AjaxResult.success(itHmCustomerService.bindCustomer(wechatBind));
        }
    }

    //解绑
    @PostMapping("delbind")
    @ResponseBody
    public AjaxResult delbind(Integer bindId) {
        return AjaxResult.success(itHmCustomerService.delBind(bindId));
    }

    //返回可以支付的账期、账期查询
    @PostMapping("billList")
    @ResponseBody
    public AjaxResult billList(Integer customerId) {
        THmCustomer tHmCustomer = itHmCustomerService.selectTHmCustomerById(customerId);
        if (tHmCustomer != null) {
            THmCustomerpaydays tHmCustomerpaydays = tHmCustomerpaydaysMapper.getLast(tHmCustomer.getHcustomerId());
            if (tHmCustomerpaydays != null) {
                //最后一个账期为 欠费
                if (tHmCustomerpaydays.getWqInt1() == 99) {
                    List<THmCustomerpaydays> tHmCustomerpaydaysList = itHmCustomerpaydaysService.selectTHmCustomerpaydaysListm(tHmCustomerpaydays);
                    tHmCustomerpaydaysList.remove(0);
                    //遍历除最后一期外的所有账期
                    for (THmCustomerpaydays hmCustomerpaydays : tHmCustomerpaydaysList) {
                        //如果有延期 面积变更欠费 部分缴费 则返回改账期有往期欠费
                        if (hmCustomerpaydays.getWqInt1() == 3 || hmCustomerpaydays.getWqInt1() == 5 || hmCustomerpaydays.getWqInt1() == 6) {
                            hmCustomerpaydays.setIsNeng(1);
                            hmCustomerpaydays.setReason("往期欠费");
                            return AjaxResult.success(hmCustomerpaydays);
                        }
                    }
                    //如果在循环中没有return则代表改账期可以微信支付
                    tHmCustomerpaydays.setCustomerName(tHmCustomer.getHcustomerName());
                    tHmCustomerpaydays.setCustomerCode(tHmCustomer.getHcustomerCode());
                    tHmCustomerpaydays.setAreaName(tHmCustomer.getHareaName());
                    tHmCustomerpaydays.setHcustomerDetailaddress(tHmCustomer.getHcustomerDetailaddress());
                    tHmCustomerpaydays.setIsNeng(0);
                    return AjaxResult.success(tHmCustomerpaydays);
                } else {
                    tHmCustomerpaydays.setCustomerName(tHmCustomer.getHcustomerName());
                    tHmCustomerpaydays.setCustomerCode(tHmCustomer.getHcustomerCode());
                    tHmCustomerpaydays.setAreaName(tHmCustomer.getHareaName());
                    tHmCustomerpaydays.setHcustomerDetailaddress(tHmCustomer.getHcustomerDetailaddress());
                    tHmCustomerpaydays.setIsNeng(1);
                    switch (tHmCustomerpaydays.getWqInt1()) {
                        case 0:
                            tHmCustomerpaydays.setReason("已缴基础热");
                            break;
                        case 1:
                            tHmCustomerpaydays.setReason("已缴全热");
                            break;
                        case 2:
                            tHmCustomerpaydays.setReason("已缴续费");
                            break;
                        case 3:
                            tHmCustomerpaydays.setReason("延期欠费");
                            break;
                        case 4:
                            tHmCustomerpaydays.setReason("已平账");
                            break;
                        case 5:
                            tHmCustomerpaydays.setReason("面积变更欠费");
                            break;
                        case 6:
                            tHmCustomerpaydays.setReason("部分缴费欠费");
                            break;
                    }
                    return AjaxResult.success(tHmCustomerpaydays);
                }
            } else {
                return AjaxResult.error("1", "本年度无账期信息");
            }
        } else {
            return AjaxResult.error("1", "获取用户信息失败");
        }
    }

    //生成订单
    @PostMapping("creatOrder")
    @ResponseBody
    public AjaxResult creatOrder(String openId, Integer wqId, Integer customerId, String bankCode) {

        THmCustomerpaydays tHmCustomerpaydays = itHmCustomerpaydaysService.selectTHmCustomerpaydaysById(wqId);

        if (tHmCustomerpaydays != null) {
            THmCustomer tHmCustomer = itHmCustomerService.selectTHmCustomerById(tHmCustomerpaydays.getHcustomerId());
            if (tHmCustomer != null) {
                //根据账期id 生成微信订单
                THmPayhistory tHmPayhistory = new THmPayhistory();
                tHmPayhistory.setWqId(wqId);
                tHmPayhistory.setHcustomerId(tHmCustomerpaydays.getHcustomerId());
                tHmPayhistory.setHpaySfqe(1);//支付方式只能为全额
                tHmPayhistory.setHpayAddtime(sdf.format(new Date()));
                //应缴和实缴都是全额
                tHmPayhistory.setHpayYjmoney(tHmCustomerpaydays.getWqStr1());
                tHmPayhistory.setHpayMoney(tHmCustomerpaydays.getWqStr1());
                //支付方式为 微信
                tHmPayhistory.setHpayType(5);
                //状态为 2 表示未完成支付的订单
                tHmPayhistory.setHpayState(2);
                //微信订单号
                Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                Instant instant = timestamp.toInstant();
                tHmPayhistory.setHpayTradeno("KD" + instant.toEpochMilli());
                //微信充值人员id为99
                tHmPayhistory.setHpayAdduserid((long) 102);
                tHmPayhistory.setHpayTradeuserid((long) 102);
                //生成充值记录
                itHmPayhistoryService.insertTHmPayhistoryForWxChat(tHmPayhistory);
                //生成订单返回 --》 用于查询微信支付是否成功
                PaymentRecord paymentRecord = new PaymentRecord();
                paymentRecord.setCustomerId(customerId);
                paymentRecord.setBankCode(bankCode);
                paymentRecord.setPaymentOrder("KD" + instant.toEpochMilli());
                paymentRecord.setPaymentCreateDate(DateUtil.date());
                paymentRecord.setPaymentCreateOpenId(openId);
                //微信支付金额
//                paymentRecord.setPaymentPayMoney(tHmCustomerpaydays.getWqStr1());
                paymentRecord.setPaymentPayMoney("0.01");
                paymentRecord.setPaymentPayStuts("0");
                //保存充值
                paymentRecordService.insertPaymentRecord(paymentRecord);
                return AjaxResult.success(paymentRecord);
            } else {
                return AjaxResult.error("1", "用户信息错误");
            }
        } else {
            return AjaxResult.error("1", "账期错误");
        }
    }

    //订单信息
    @PostMapping("paymentInfo")
    @ResponseBody
    public AjaxResult paymentInfo(Long paymentId) {
        PaymentRecord paymentRecord = paymentRecordService.selectPaymentRecordById(paymentId);
        if (paymentRecord != null) {
            WechatUser wechatUser = wechatUserService.getWechatUser(paymentRecord.getPaymentCreateOpenId());
            paymentRecord.setWechatUser(wechatUser);
            if (wechatUser != null) {
                List<WechatBind> wechatBindList = itHmCustomerService.getWechatBindlist(wechatUser.getWechatId());
                if (wechatBindList.size() > 0) {
                    THmCustomer tHmCustomer = itHmCustomerService.selectTHmCustomerByIdForWeChat(wechatBindList.get(0).getCustomerId());
                    paymentRecord.setTHmCustomer(tHmCustomer);
                }
            }
            THmPayhistory tHmPayhistory = new THmPayhistory();
            tHmPayhistory.setHpayTradeno(paymentRecord.getPaymentOrder());
            List<THmPayhistory> tHmPayhistoryList = itHmPayhistoryService.selectTHmPayhistoryListForWeChat(tHmPayhistory);
            if (tHmPayhistoryList.size() > 0) {
                paymentRecord.setTHmPayhistory(tHmPayhistoryList.get(0));
                THmCustomerpaydays tHmCustomerpaydays = itHmCustomerpaydaysService.selectTHmCustomerpaydaysById(tHmPayhistoryList.get(0).getWqId());
                if (tHmCustomerpaydays != null) {
                    paymentRecord.setTHmCustomerpaydays(tHmCustomerpaydays);
                }
            }
        }
        return AjaxResult.success(paymentRecord);
    }

    /**
     * 生成支付信息
     */
    @PostMapping("/paymentPay")
    @ResponseBody
    public AjaxResult paymentPay(Long paymentId, Long wechatuserId) {
        WechatUser wechatUser = wechatUserService.selectWechatUserById(wechatuserId);
        PaymentRecord paymentRecord = paymentRecordService.selectPaymentRecordById(paymentId);
        Map<String, String> remap = weChatPayUtils.getWeChatPayJSON(wxPayBean, wechatUser, paymentRecord, IpUtils.getIpAddr(getRequest()).split(",")[0], "/kddz/wechat/paymentPayNotify");
        if (StrUtil.equals("1", remap.get("code"))) {
            return AjaxResult.success("生成支付信息成功", remap.get("data"));
        } else {
            return AjaxResult.error(remap.get("data"));
        }

    }

    //完成支付返回订单信息
    @PostMapping("orderEnd")
    @ResponseBody
    public AjaxResult orderEnd(Long paymentId) {
        //查微信订单 是否成功
        paymentRecordService.orderReset(paymentId);
        //订单信息
        PaymentRecord paymentRecord = paymentRecordService.selectPaymentRecordById(paymentId);
        if (paymentRecord != null) {
            WechatUser wechatUser = wechatUserService.getWechatUser(paymentRecord.getPaymentCreateOpenId());
            paymentRecord.setWechatUser(wechatUser);
            if (wechatUser != null) {
                List<WechatBind> wechatBindList = itHmCustomerService.getWechatBindlist(wechatUser.getWechatId());
                if (wechatBindList.size() > 0) {
                    THmCustomer tHmCustomer = itHmCustomerService.selectTHmCustomerByIdForWeChat(wechatBindList.get(0).getCustomerId());
                    paymentRecord.setTHmCustomer(tHmCustomer);
                }
            }
        }
        return AjaxResult.success(paymentRecord);
    }

    //充值记录列表
    @PostMapping("orderList")
    @ResponseBody
    public AjaxResult orderList(String openId, Integer customerId) {
        PaymentRecord paymentRecord = new PaymentRecord();
        paymentRecord.setPaymentCreateOpenId(openId);
        paymentRecord.setCustomerId(customerId);
        paymentRecord.setPaymentPayStuts("1");
        List<PaymentRecord> list = paymentRecordService.selectPaymentRecordList(paymentRecord);
        List<PaymentRecord> list1 = new ArrayList<>();
        list1.add(list.get(0));
        return AjaxResult.success(list1);
    }

    //微信通知
    @PostMapping("/paymentPayNotify")
    @ResponseBody
    public String paymentPayNotify() {
        System.out.println("--------------进了回调paymentPayNotify------------");
        String xmlMsg = HttpKit.readData(getRequest());
        Map<String, String> params = WxPayKit.xmlToMap(xmlMsg);
        if (weChatPayUtils.WxVerifyNotify(wxPayBean, params)) {
            String attach = params.get("attach");
            PaymentRecord paymentRecord = paymentRecordService.selectPaymentRecordById(Long.valueOf(attach));
            boolean state = paymentRecordService.orderReset(paymentRecord.getPaymentId());
            if (state) {
                Map<String, String> xml = new HashMap<>(2);
                xml.put("return_code", "SUCCESS");
                xml.put("return_msg", "订单修改成功");
                return WxPayKit.toXml(xml);
            } else {
                Map<String, String> xml = new HashMap<>(2);
                xml.put("return_code", "FAIL");
                xml.put("return_msg", "订单状态修改失败");
                return WxPayKit.toXml(xml);
            }
        } else {
            Map<String, String> xml = new HashMap<>(2);
            xml.put("return_code", "FAIL");
            xml.put("return_msg", "订单状态修改失败");
            return WxPayKit.toXml(xml);
        }
    }

    //发送消息
    @PostMapping("/sendWxMessage")
    @ResponseBody
    public AjaxResult sendWxMessage() {
        wechatUserService.sendWxMessage();
        return AjaxResult.success("发送成功");
    }


    //--------------------------------------客服系统-----------------------------=

    @Resource
    private ITHmWorkorderService itHmWorkorderService;

    //用户信息
    @PostMapping("customerKeFuList")
    @ResponseBody
    public AjaxResult customerKeFuList(Integer pageNum, Integer pageSize) {
        if (pageNum == null || pageSize == null) {
            return AjaxResult.error("分页数据不能为空");
        } else {
            startPage();
            List<CustomerKeFu> customerKeFuList = itHmCustomerService.selectTHmCustomerListForKefu();
            return AjaxResult.success(customerKeFuList);
        }
    }

    //账期
    @PostMapping("orderKeFuList")
    @ResponseBody
    public AjaxResult orderKeFuList(Integer pageNum, Integer pageSize, String payStatus) {
        if (pageNum == null || pageSize == null) {
            return AjaxResult.error("分页数据不能为空");
        } else {
            startPage();
            List<OrderKeFu> orderKeFuList = itHmCustomerpaydaysService.orderKeFuList(payStatus);
            return AjaxResult.success(orderKeFuList);
        }
    }

    Gson gson = new Gson();
    JsonParser jsonParser = new JsonParser();

    //转 工单类型
    @PostMapping("workType")
    @ResponseBody
    public AjaxResult workType() {

        String result = HttpUtil.post("http://jrkjserver.cn:9200/wechat/workordertypeidComboBox", "");

        JsonArray jsonArray = jsonParser.parse(result).getAsJsonArray();
        List<WorkType> list = new ArrayList<>();

        for (JsonElement jsonElement : jsonArray) {
            list.add(gson.fromJson(jsonElement, WorkType.class));
        }

        return AjaxResult.success("成功", list);
    }

    //转 服务类型
    @PostMapping("sonType")
    @ResponseBody
    public AjaxResult sonType(Integer workordertypeid) {

        Map<String, Object> map = new HashMap<>();
        map.put("workordertypeid", workordertypeid);

        String result = HttpUtil.post("http://jrkjserver.cn:9200/wechat/sontypeComboBox", map);

        JsonArray jsonArray = jsonParser.parse(result).getAsJsonArray();
        List<WorkType> list = new ArrayList<>();

        for (JsonElement jsonElement : jsonArray) {
            list.add(gson.fromJson(jsonElement, WorkType.class));
        }

        return AjaxResult.success("成功", list);
    }

    //转 保修
    @PostMapping("bao")
    @ResponseBody
    public AjaxResult bao(String cid, String lxr, String lxdh, String content, Integer workordertypeId, Integer sontypeId) {

        Map<String, Object> map = new HashMap<>();
        map.put("cid", cid);
        map.put("lxr", lxr);
        map.put("lxdh", lxdh);
        map.put("content", content);
        map.put("workordertypeId", workordertypeId);
        map.put("sontypeId", sontypeId);

        String result = HttpUtil.post("http://jrkjserver.cn:9200/wechat/wechatRepairs", map);

        return AjaxResult.success("成功", gson.fromJson(result, Bao.class));
    }

    //转 保修信息
    @PostMapping("baoInfo")
    @ResponseBody
    public AjaxResult baoInfo(String cid, Integer status) {

        Map<String, Object> map = new HashMap<>();
        map.put("cid", cid);
        map.put("status", status);

        String result = HttpUtil.post("http://jrkjserver.cn:9200/wechat/RepairsRecordData", map);

        return AjaxResult.success("成功", gson.fromJson(result, BaoInfo.class));
    }

    //保存工单
    @PostMapping("/addWorkOrder")
    @ResponseBody
    public AjaxResult addWorkOrder(Integer workorderid, Integer cid, Integer workordertypeId, String worktype, Integer sontypeId, String sontype, String lxr, String lxdh, String content) {
        THmWorkorder tHmWorkorder = new THmWorkorder();
        tHmWorkorder.setContent(content);
        tHmWorkorder.setPhone(lxdh);
        tHmWorkorder.setCustomerName(lxr);
        tHmWorkorder.setCustomerId(cid);
        tHmWorkorder.setSonType(sontype);
        tHmWorkorder.setWorkorderid(workorderid);
        tHmWorkorder.setWorkType(worktype);
        tHmWorkorder.setAddTime(sdf.format(new Date()));
        return toAjax(itHmWorkorderService.insertTHmWorkorder(tHmWorkorder));
    }

    //客服单子
    @GetMapping("/list")
    public String customerpaydays() {
        return "business/workorder/list";
    }

    //客服单子
    @PostMapping("infoList")
    @ResponseBody
    public TableDataInfo infoList(Integer pageNum, Integer pageSize, String customerName, String content, String orderType, String addTime, String endSetState) {

        Map<String, Object> map = new HashMap<>();
        map.put("pageNum", pageNum);
        map.put("pageSize", pageSize);
        map.put("customerName", customerName);
        map.put("content", content);
        map.put("orderType", orderType);
        map.put("addTime", addTime);
        map.put("endSetState", endSetState);

        String result = HttpUtil.post("http://jrkjserver.cn:9200/wechat/RepairsRecordDataList", map);

        BaoInfoList baoInfoList = gson.fromJson(result, BaoInfoList.class);

        TableDataInfo rspData = new TableDataInfo();
        rspData.setCode(0);
        rspData.setRows(baoInfoList.getData());
        rspData.setTotal(baoInfoList.getCount());

        return rspData;
    }

//------------------------------数据处理-----------------------

    @Resource
    private WuYeMapper wuYeMapper;

    //物业费用
    @GetMapping("/wuye")
    public void wuye() {
        //取出原始数据
        List<WuYe> wuYeList = wuYeMapper.select();
        //根据 customerId和time取出 账期id
        for (WuYe wuYe : wuYeList) {
            String[] str = wuYe.getTime().split("-");
            THmCustomerpaydays tHmCustomerpaydays = tHmCustomerpaydaysMapper.getByCustomerIdAndYear(wuYe.getCustomerId(), str[0]);
            if (tHmCustomerpaydays != null) {
                //添加到copy1
                THmCustomerpaydaysPriceCopy tHmCustomerpaydaysPriceCopy = new THmCustomerpaydaysPriceCopy();
                tHmCustomerpaydaysPriceCopy.setPriceType(wuYe.getName());
                tHmCustomerpaydaysPriceCopy.setAddTime(wuYe.getTime());
                tHmCustomerpaydaysPriceCopy.setAddUser((long) 1);
                tHmCustomerpaydaysPriceCopy.setPrice(wuYe.getMoney());
                tHmCustomerpaydaysPriceCopy.setCustomerpaydaysId(tHmCustomerpaydays.getWqId());
                tHmCustomerpaydaysPriceMapper.insertTHmCustomerpaydaysPriceCopy(tHmCustomerpaydaysPriceCopy);
            }
        }
    }

    //划入划出
    @GetMapping("/hua")
    public void hua() {
        //查 sheet
        List<Sheet1> sheet1List = tHmCustomerMapper.sheet1List();
        for (Sheet1 sheet1 : sheet1List) {
            if (sheet1.getCode() != null) {
                THmCustomer tHmCustomer = tHmCustomerMapper.selectTHmCustomerByHcustomerCode(sheet1.getCode());
                if (tHmCustomer != null) {
                    //根据sheet的用户编号和年度 查充值记录 如果有的话 不用操作
                    List<THmPayhistory> tHmPayhistoryList = tHmPayhistoryMapper.getByCustomerIdAndNian(tHmCustomer.getHcustomerId(), sheet1.getNian().split("-")[0]);
                    if (tHmPayhistoryList.size() > 0) {
                        System.out.println("已存在");
                    } else {
                        //如果没有的话 将账期查出来 并生成充值记录
                        THmCustomerpaydays tHmCustomerpaydays = tHmCustomerpaydaysMapper.getByCustomerIdAndYear(tHmCustomer.getHcustomerId(), sheet1.getNian().split("-")[0]);
                        if (tHmCustomerpaydays != null) {

                            THmPayhistory tHmPayhistory = new THmPayhistory();

                            tHmPayhistory.setWqId(tHmCustomerpaydays.getWqId());
                            tHmPayhistory.setHpaySfqe(1);//全额
                            tHmPayhistory.setHpayYjmoney(tHmCustomerpaydays.getWqStr1());//应缴
                            tHmPayhistory.setHpayMoney(tHmCustomerpaydays.getWqStr1());//已缴
                            tHmPayhistory.setHpayStr1(tHmCustomerpaydays.getWqStr1());//当期
                            tHmPayhistory.setHpayStr2(tHmCustomerpaydays.getWqStr1());//已缴
                            tHmPayhistory.setHpayStr4("0.00");//已缴
                            tHmPayhistory.setHpayType(0);//现金
                            tHmPayhistory.setHcustomerId(tHmCustomerpaydays.getHcustomerId());
                            tHmPayhistory.setHcustomerId(tHmCustomerpaydays.getHcustomerId());

                            //此账期生成充值记录
                            tHmPayhistory.setHpayTradeuserid(1L);
                            tHmPayhistory.setHpayTradetime("2021-11-15 00:00:00");
                            tHmPayhistory.setHpayAdduserid(1L);
                            tHmPayhistory.setHpayAddtime("2021-11-15 00:00:00");
                            tHmPayhistory.setHpayState(0);
                            //单号
                            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                            Instant instant = timestamp.toInstant();
                            tHmPayhistory.setHpayOuttradeno("KD" + instant.toEpochMilli());
                            tHmPayhistoryMapper.insertTHmPayhistory(tHmPayhistory);

                            //更新账期
                            tHmCustomerpaydays.setWqInt1(1);
                            tHmCustomerpaydaysMapper.updateTHmCustomerpaydays(tHmCustomerpaydays);

                            //更新用户int4 最后一次账期状态
                            THmCustomerpaydays tHmCustomerpaydays1 = tHmCustomerpaydaysMapper.getLast(tHmCustomerpaydays.getHcustomerId());
                            THmCustomer tHmCustomer1 = new THmCustomer();
                            tHmCustomer1.setHcustomerInt4(tHmCustomerpaydays1.getWqInt1());
                            tHmCustomer1.setHcustomerId(tHmCustomerpaydays.getHcustomerId());
                            //查所有账期如果有不是全额，基础热，续费，平账的账单即为0
                            tHmCustomer1.setIsZheng(1);
                            THmCustomerpaydays tHmCustomerpaydays2 = new THmCustomerpaydays();
                            tHmCustomerpaydays2.setHcustomerId(tHmCustomerpaydays.getHcustomerId());
                            List<THmCustomerpaydays> tHmCustomerpaydaysLists = tHmCustomerpaydaysMapper.selectTHmCustomerpaydaysList(tHmCustomerpaydays2);
                            for (THmCustomerpaydays customerpaydays : tHmCustomerpaydaysLists) {
                                if (customerpaydays.getWqInt1() != 0 && customerpaydays.getWqInt1() != 1 && customerpaydays.getWqInt1() != 2 && customerpaydays.getWqInt1() != 4) {
                                    tHmCustomer.setIsZheng(0);
                                }
                            }
                            itHmCustomerService.updateTHmCustomer(tHmCustomer);
                            System.out.println("生成充值记录");
                        }
                    }
                }
            }
        }
    }
}