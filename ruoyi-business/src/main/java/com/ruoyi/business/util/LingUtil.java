package com.ruoyi.business.util;

import java.math.BigDecimal;

public class LingUtil {

    public static String zhengShe(String money) {
        return String.valueOf(Math.floor(Double.parseDouble(money)));
    }

    public static String zhengRu(String money) {
        return String.valueOf(Math.round(Double.parseDouble(money)));
    }

    public static String yiShe(String money) {
        BigDecimal b = new BigDecimal(money);
        b = b.setScale(1, BigDecimal.ROUND_HALF_DOWN);
        return b.toString();
    }

    public static String yiRu(String money) {
        BigDecimal b = new BigDecimal(money);
        b = b.setScale(1, BigDecimal.ROUND_HALF_UP);
        return b.toString();
    }

    public static String erRu(String money) {
        BigDecimal b = new BigDecimal(money);
        b = b.setScale(2, BigDecimal.ROUND_HALF_UP);
        return b.toString();
    }
}
