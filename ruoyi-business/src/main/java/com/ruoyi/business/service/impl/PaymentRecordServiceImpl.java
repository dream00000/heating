package com.ruoyi.business.service.impl;

import cn.hutool.core.date.DateUtil;
import com.ruoyi.business.domain.*;
import com.ruoyi.business.mapper.*;
import com.ruoyi.business.service.IPaymentRecordService;
import com.ruoyi.business.service.ITHmPayhistoryService;
import com.ruoyi.business.service.IWechatUserService;
import com.ruoyi.business.util.WeChatPayUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 订单Service业务层处理
 */
@Service
public class PaymentRecordServiceImpl implements IPaymentRecordService {

    @Resource
    private PaymentRecordMapper paymentRecordMapper;

    @Resource
    private IWechatUserService wechatUserService;

    @Resource
    private WxPayBean wxPayBean;

    @Resource
    private WeChatPayUtils weChatPayUtils;

    @Resource
    private ITHmPayhistoryService itHmPayhistoryService;

    @Resource
    private THmCustomerpaydaysPriceMapper tHmCustomerpaydaysPriceMapper;

    @Resource
    private THmCustomerMapper tHmCustomerMapper;

    @Resource
    private THmCustomerpaydaysMapper tHmCustomerpaydaysMapper;

    @Resource
    private THmPayhistoryPriceMapper tHmPayhistoryPriceMapper;

    @Resource
    private THmPayhistoryMapper tHmPayhistoryMapper;

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * 查询订单
     *
     * @param paymentId 订单ID
     * @return 订单
     */
    @Override
    public PaymentRecord selectPaymentRecordById(Long paymentId) {
        return paymentRecordMapper.selectPaymentRecordById(paymentId);
    }

    /**
     * 查询订单列表
     *
     * @param paymentRecord 订单
     * @return 订单
     */
    @Override
    public List<PaymentRecord> selectPaymentRecordList(PaymentRecord paymentRecord) {
        return paymentRecordMapper.selectPaymentRecordList(paymentRecord);
    }

    @Override
    public boolean orderReset(Long paymentId) {
        //获取订单、用户信息
        PaymentRecord paymentRecord = paymentRecordMapper.selectPaymentRecordById(paymentId);
        WechatUser wechatUser = wechatUserService.getWechatUser(paymentRecord.getPaymentCreateOpenId());
        //校验是否支付成功
        Map<String, Object> returnMap = weChatPayUtils.paymentRecordIsPay(wxPayBean, wechatUser, paymentRecord);
        paymentRecord = (PaymentRecord) returnMap.get("paymentRecord");
        System.out.println("---------------paymentRecord-------------" + paymentRecord);
        //支付是否成功
        if ((boolean) returnMap.get("stuts")) {
            THmPayhistory tHmPayhistory = tHmPayhistoryMapper.payHistoryForWX(paymentRecord.getPaymentOrder());
            System.out.println("---------------tHmPayhistory-------------" + tHmPayhistory);
            if (tHmPayhistory != null) {
                //支付完成不在做更新
                if (tHmPayhistory.getHpayState() != 0) {
                    //完成充值
                    tHmPayhistory.setHpayStr4("0.00");
                    tHmPayhistory.setHpayState(0);
                    tHmPayhistory.setHpayTradetime(sdf.format(new Date()));
                    tHmPayhistory.setHpayOuttradeno(tHmPayhistory.getHpayTradeno());
                    itHmPayhistoryService.updateTHmPayhistory(tHmPayhistory);
                    //完成账期
                    THmCustomerpaydays tHmCustomerpaydays1 = new THmCustomerpaydays();
                    tHmCustomerpaydays1.setWqId(tHmPayhistory.getWqId());
                    tHmCustomerpaydays1.setWqInt1(1);
                    tHmCustomerpaydays1.setWqInt2(99);
                    tHmCustomerpaydaysMapper.updateTHmCustomerpaydays(tHmCustomerpaydays1);
                    //更新用户状态
                    THmCustomer tHmCustomer = new THmCustomer();
                    tHmCustomer.setHcustomerId(tHmPayhistory.getHcustomerId());
                    tHmCustomer.setHcustomerInt4(1);
                    //查所有账期如果有不是全额，基础热，续费，平账的账单即为0
                    tHmCustomer.setIsZheng(1);
                    THmCustomerpaydays tHmCustomerpaydays2 = new THmCustomerpaydays();
                    tHmCustomerpaydays2.setHcustomerId(tHmPayhistory.getHcustomerId());
                    List<THmCustomerpaydays> tHmCustomerpaydaysList = tHmCustomerpaydaysMapper.selectTHmCustomerpaydaysList(tHmCustomerpaydays2);
                    for (THmCustomerpaydays customerpaydays : tHmCustomerpaydaysList) {
                        if (customerpaydays.getWqInt1() != 0 && customerpaydays.getWqInt1() != 1 && customerpaydays.getWqInt1() != 2 && customerpaydays.getWqInt1() != 4) {
                            tHmCustomer.setIsZheng(0);
                        }
                    }
                    tHmCustomerMapper.updateTHmCustomer(tHmCustomer);
                    System.out.println("customer :" + tHmCustomer);
                    //生成充值附表
                    THmCustomerpaydays tHmCustomerpaydays = tHmCustomerpaydaysMapper.selectTHmCustomerpaydaysById(tHmPayhistory.getWqId());
                    THmCustomerpaydaysPrice tHmCustomerpaydaysPrice = new THmCustomerpaydaysPrice();
                    tHmCustomerpaydaysPrice.setCustomerpaydaysId(tHmPayhistory.getWqId());
                    List<THmCustomerpaydaysPrice> tHmCustomerpaydaysPriceList = tHmCustomerpaydaysPriceMapper.selectTHmCustomerpaydaysPriceList(tHmCustomerpaydaysPrice);
                    for (THmCustomerpaydaysPrice hmCustomerpaydaysPrice : tHmCustomerpaydaysPriceList) {
                        THmPayhistoryPrice tHmPayhistoryPrice = new THmPayhistoryPrice();
                        tHmPayhistoryPrice.setPayhistoryId(tHmPayhistory.getHpayId());
                        tHmPayhistoryPrice.setArea(hmCustomerpaydaysPrice.getArea());
                        tHmPayhistoryPrice.setAreac("0");
                        tHmPayhistoryPrice.setPriceType(hmCustomerpaydaysPrice.getPriceType());
                        tHmPayhistoryPrice.setPrice(hmCustomerpaydaysPrice.getPrice());
                        tHmPayhistoryPrice.setTotal(new BigDecimal(hmCustomerpaydaysPrice.getArea()).multiply(new BigDecimal(tHmPayhistoryPrice.getPrice())).toString());
                        tHmPayhistoryPrice.setNian(tHmCustomerpaydays.getHzqGrstarttime());
                        tHmPayhistoryPrice.setType(hmCustomerpaydaysPrice.getType());
                        //保存
                        tHmPayhistoryPriceMapper.insertTHmPayhistoryPrice(tHmPayhistoryPrice);
                        System.out.println("---------------tHmPayhistoryPrice-------------" + tHmPayhistoryPrice);
                    }
                    //完成订单
                    paymentRecord.setPaymentSellelerecordId(tHmPayhistory.getHpayId());
                    paymentRecord.setPaymentSellelerecordDate(DateUtil.date());
                    paymentRecordMapper.updatePaymentRecord(paymentRecord);
                    System.out.println("---------------paymentRecord-------------" + paymentRecord);
                }
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int insertPaymentRecord(PaymentRecord paymentRecord) {
        return paymentRecordMapper.insertPaymentRecord(paymentRecord);
    }
}
