package com.ruoyi.business.service.impl;

import com.ruoyi.business.domain.THmCustomerpaydaysPrice;
import com.ruoyi.business.mapper.THmCustomerpaydaysPriceMapper;
import com.ruoyi.business.service.ITHmCustomerpaydaysPriceService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 账期-单价Service业务层处理
 *
 * @author ruoyi
 * @date 2021-12-30
 */
@Service
public class THmCustomerpaydaysPriceServiceImpl implements ITHmCustomerpaydaysPriceService {
    @Resource
    private THmCustomerpaydaysPriceMapper tHmCustomerpaydaysPriceMapper;

    /**
     * 查询账期-单价
     *
     * @param id
     * @return 账期-单价
     */
    @Override
    public THmCustomerpaydaysPrice selectTHmCustomerpaydaysPriceById(int id) {
        return tHmCustomerpaydaysPriceMapper.selectTHmCustomerpaydaysPriceById(id);
    }

    /**
     * 查询账期-单价列表
     *
     * @param tHmCustomerpaydaysPrice 账期-单价
     * @return 账期-单价
     */
    @Override
    public List<THmCustomerpaydaysPrice> selectTHmCustomerpaydaysPriceList(THmCustomerpaydaysPrice tHmCustomerpaydaysPrice) {
        return tHmCustomerpaydaysPriceMapper.selectTHmCustomerpaydaysPriceList(tHmCustomerpaydaysPrice);
    }

    /**
     * 新增账期-单价
     *
     * @param tHmCustomerpaydaysPrice 账期-单价
     * @return 结果
     */
    @Override
    public int insertTHmCustomerpaydaysPrice(THmCustomerpaydaysPrice tHmCustomerpaydaysPrice) {
        return tHmCustomerpaydaysPriceMapper.insertTHmCustomerpaydaysPrice(tHmCustomerpaydaysPrice);
    }

    /**
     * 修改账期-单价
     *
     * @param tHmCustomerpaydaysPrice 账期-单价
     * @return 结果
     */
    @Override
    public int updateTHmCustomerpaydaysPrice(THmCustomerpaydaysPrice tHmCustomerpaydaysPrice) {
        return tHmCustomerpaydaysPriceMapper.updateTHmCustomerpaydaysPrice(tHmCustomerpaydaysPrice);
    }

    //根据  账期id 删除
    @Override
    public void delTHmCustomerpaydaysPrice(int id) {
        tHmCustomerpaydaysPriceMapper.delTHmCustomerpaydaysPrice(id);
    }

}
