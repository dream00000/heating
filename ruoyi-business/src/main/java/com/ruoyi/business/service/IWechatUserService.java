package com.ruoyi.business.service;


import com.ruoyi.business.domain.WechatUser;

import java.util.List;

/**
 * 微信用户Service接口
 */
public interface IWechatUserService {
    /**
     * 查询微信用户
     */
    public WechatUser selectWechatUserById(Long wechatId);

    public WechatUser getWechatUser(String openId);

    public List<WechatUser> selectWechatUserListByOpenid(String openid);

    public List<WechatUser> selectWechatUserListByCustomerId(String ecode);

    /**
     * 查询微信用户列表
     */
    public List<WechatUser> selectWechatUserList(WechatUser wechatUser);

    /**
     * 新增微信用户
     */
    public int insertWechatUser(WechatUser wechatUser);

    /**
     * 修改微信用户
     */
    public int updateWechatUser(WechatUser wechatUser);

    /**
     * 删除微信用户信息
     */
    public int deleteWechatUserById(Long wechatId);

    //发送微信信息
    public void sendWxMessage();
}
