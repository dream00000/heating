package com.ruoyi.business.service.impl;

import com.ruoyi.business.domain.THmArea;
import com.ruoyi.business.domain.THmCustomer;
import com.ruoyi.business.domain.THmOtherpayhistory;
import com.ruoyi.business.domain.vo.OtherpayhistoryVO;
import com.ruoyi.business.mapper.THmAreaMapper;
import com.ruoyi.business.mapper.THmCustomerMapper;
import com.ruoyi.business.mapper.THmOtherpayhistoryMapper;
import com.ruoyi.business.service.ITHmOtherpayhistoryService;
import com.ruoyi.business.util.MoneyToCNFormat;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.mapper.SysUserMapper;
import com.ruoyi.system.service.ISysDictDataService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 其他收费Service业务层处理
 *
 * @author ruoyi
 * @date 2021-08-13
 */
@Service
public class THmOtherpayhistoryServiceImpl implements ITHmOtherpayhistoryService {
    @Resource
    private THmOtherpayhistoryMapper tHmOtherpayhistoryMapper;

    @Resource
    private THmCustomerMapper tHmCustomerMapper;

    @Resource
    private THmAreaMapper tHmAreaMapper;

    @Resource
    private ISysDictDataService iSysDictDataService;

    @Resource
    private SysUserMapper sysUserMapper;

    SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * 查询其他收费
     *
     * @param id
     * @return 其他收费
     */
    @Override
    public THmOtherpayhistory selectTHmOtherpayhistoryById(int id) {
        THmOtherpayhistory tHmOtherpayhistory = new THmOtherpayhistory();
        THmCustomer tHmCustomer = tHmCustomerMapper.selectTHmCustomerById(id);
        tHmOtherpayhistory.setHcustomerId(id);
        tHmOtherpayhistory.setHcustomerName(tHmCustomer.getHcustomerName());
        tHmOtherpayhistory.setHcustomerCode(tHmCustomer.getHcustomerCode());
        tHmOtherpayhistory.setHcustomerMobile(tHmCustomer.getHcustomerMobile());
        tHmOtherpayhistory.setHcustomerAddress(tHmCustomer.getHcustomerDetailaddress());
        tHmOtherpayhistory.setHcustomerArea(tHmAreaMapper.selectTHmAreaById(tHmCustomer.getHareaId()).getHareaName());
        //所属分区
        THmArea tHmArea = tHmAreaMapper.selectTHmAreaById(tHmCustomer.getHareaId());
        if (tHmArea != null) {
            if (tHmArea.getHareaLevel() == 2) {
                tHmOtherpayhistory.setHcustomerArea(tHmArea.getHareaName());
            } else {
                THmArea tHmArea1 = tHmAreaMapper.selectTHmAreaById(tHmArea.getHareaParentid());
                tHmOtherpayhistory.setHcustomerArea(tHmArea1.getHareaName());
            }
        }
        return tHmOtherpayhistory;
    }

    /**
     * 查询其他收费列表
     *
     * @param tHmOtherpayhistory 其他收费
     * @return 其他收费
     */
    @Override
    public List<THmOtherpayhistory> selectTHmOtherpayhistoryList(THmOtherpayhistory tHmOtherpayhistory) {
        List<THmOtherpayhistory> list = tHmOtherpayhistoryMapper.selectTHmOtherpayhistoryList(tHmOtherpayhistory);
        for (THmOtherpayhistory hmOtherpayhistory : list) {
            THmCustomer tHmCustomer = tHmCustomerMapper.selectTHmCustomerById(hmOtherpayhistory.getHcustomerId());
            hmOtherpayhistory.setHcustomerName(tHmCustomer.getHcustomerName());
            hmOtherpayhistory.setHcustomerCode(tHmCustomer.getHcustomerCode());
            hmOtherpayhistory.setHcustomerMobile(tHmCustomer.getHcustomerMobile());
            hmOtherpayhistory.setHcustomerAddress(tHmCustomer.getHcustomerDetailaddress());
            hmOtherpayhistory.setHcustomerArea(tHmAreaMapper.selectTHmAreaById(tHmCustomer.getHareaId()).getHareaName());
            hmOtherpayhistory.setPayType(iSysDictDataService.selectDictLabel("data_paytype", hmOtherpayhistory.getHpayType().toString()));
            //操作人员
            SysUser sysUser = sysUserMapper.selectUserById(hmOtherpayhistory.getOpayAdduserid());
            hmOtherpayhistory.setSysname(sysUser.getUserName());
        }
        return list;
    }

    /**
     * 查询其他收费列表
     *
     * @param tHmOtherpayhistory 其他收费
     * @return 其他收费
     */
    @Override
    public List<THmOtherpayhistory> lists(THmOtherpayhistory tHmOtherpayhistory) {
        List<THmOtherpayhistory> list = tHmOtherpayhistoryMapper.lists(tHmOtherpayhistory);
        for (THmOtherpayhistory hmOtherpayhistory : list) {
            THmCustomer tHmCustomer = tHmCustomerMapper.selectTHmCustomerById(hmOtherpayhistory.getHcustomerId());
            hmOtherpayhistory.setHcustomerName(tHmCustomer.getHcustomerName());
            hmOtherpayhistory.setHcustomerCode(tHmCustomer.getHcustomerCode());
            hmOtherpayhistory.setHcustomerMobile(tHmCustomer.getHcustomerMobile());
            hmOtherpayhistory.setHcustomerAddress(tHmCustomer.getHcustomerDetailaddress());
            hmOtherpayhistory.setHcustomerArea(tHmAreaMapper.selectTHmAreaById(tHmCustomer.getHareaId()).getHareaName());
            hmOtherpayhistory.setPayType(iSysDictDataService.selectDictLabel("data_paytype", hmOtherpayhistory.getHpayType().toString()));
            //操作人员
            SysUser sysUser = sysUserMapper.selectUserById(hmOtherpayhistory.getOpayAdduserid());
            hmOtherpayhistory.setSysname(sysUser.getUserName());
        }
        return list;
    }

    @Override
    public List<OtherpayhistoryVO> selectTHmOtherpayhistoryListt(THmCustomer tHmCustomer) {
        List<OtherpayhistoryVO> otherpayhistoryVOList = tHmOtherpayhistoryMapper.selectTHmOtherpayhistoryListt(tHmCustomer);
        for (OtherpayhistoryVO otherpayhistoryVO : otherpayhistoryVOList) {
            //所属分区
            THmArea tHmArea = tHmAreaMapper.selectTHmAreaById(otherpayhistoryVO.getHareaId());
            if (tHmArea != null) {
                if (tHmArea.getHareaLevel() == 2) {
                    otherpayhistoryVO.setHcustomerArea(tHmArea.getHareaName());
                } else {
                    THmArea tHmArea1 = tHmAreaMapper.selectTHmAreaById(tHmArea.getHareaParentid());
                    otherpayhistoryVO.setHcustomerArea(tHmArea1.getHareaName());
                }
            }
        }
        return otherpayhistoryVOList;
    }

    @Override
    public List<THmOtherpayhistory> selectTHmOtherpayhistoryLists(int hcustomerId) {
        return tHmOtherpayhistoryMapper.selectTHmOtherpayhistoryLists(hcustomerId);
    }

    /**
     * 新增其他收费
     *
     * @param tHmOtherpayhistory 其他收费
     * @return 结果
     */
    @Override
    public int insertTHmOtherpayhistory(THmOtherpayhistory tHmOtherpayhistory) {
        tHmOtherpayhistory.setOpayAddtime(sdf.format(new Date()));
        tHmOtherpayhistory.setOpayAdduserid(ShiroUtils.getSysUser().getUserId());
        tHmOtherpayhistory.setOpayState(2);
        tHmOtherpayhistoryMapper.insertTHmOtherpayhistory(tHmOtherpayhistory);
        return tHmOtherpayhistory.getOpayId();
    }

    /**
     * 修改其他收费
     *
     * @param tHmOtherpayhistory 其他收费
     * @return 结果
     */
    @Override
    public int updateTHmOtherpayhistory(THmOtherpayhistory tHmOtherpayhistory) {
        return tHmOtherpayhistoryMapper.updateTHmOtherpayhistory(tHmOtherpayhistory);
    }

    //打印专用查询
    @Override
    public THmOtherpayhistory getForPrint(int opayId) {
        THmOtherpayhistory tHmOtherpayhistory = tHmOtherpayhistoryMapper.selectTHmOtherpayhistoryById(opayId);
        THmCustomer tHmCustomer = tHmCustomerMapper.selectTHmCustomerById(tHmOtherpayhistory.getHcustomerId());

        tHmOtherpayhistory.setHcustomerName(tHmCustomer.getHcustomerName());
        tHmOtherpayhistory.setHcustomerCode(tHmCustomer.getHcustomerCode());
        //所属分区
        THmArea tHmArea = tHmAreaMapper.selectTHmAreaById(tHmCustomer.getHareaId());
        if (tHmArea != null) {
            if (tHmArea.getHareaLevel() == 2) {
                tHmOtherpayhistory.setHcustomerArea(tHmArea.getHareaName());
            } else {
                THmArea tHmArea1 = tHmAreaMapper.selectTHmAreaById(tHmArea.getHareaParentid());
                tHmOtherpayhistory.setHcustomerArea(tHmArea1.getHareaName());
            }
        }
        //大写金额
        tHmOtherpayhistory.setHpayMoneyd(MoneyToCNFormat.formatToCN(Double.parseDouble(tHmOtherpayhistory.getOpayMoney())));
        //发票日期
        tHmOtherpayhistory.setOpayAddtime(sd.format(new Date()));
        //收款人，开票人
        SysUser sysUser = sysUserMapper.selectUserById(tHmOtherpayhistory.getOpayAdduserid());
        tHmOtherpayhistory.setSysname(sysUser.getUserName());
        //收费方式
        tHmOtherpayhistory.setPayType(iSysDictDataService.selectDictLabel("data_paytype", tHmOtherpayhistory.getHpayType().toString()));

        return tHmOtherpayhistory;
    }

}
