package com.ruoyi.business.service;

import com.ruoyi.business.domain.THmCustomer;
import com.ruoyi.business.domain.WechatBind;
import com.ruoyi.business.domain.vo.CustomerImport;
import com.ruoyi.business.domain.vo.CustomerKeFu;
import com.ruoyi.business.domain.vo.ReportVO;
import com.ruoyi.business.domain.vo.StationDetailVO;
import com.ruoyi.system.domain.SysUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户信息Service接口
 */
public interface ITHmCustomerService {
    /**
     * 查询用户信息
     */
    public THmCustomer selectTHmCustomerById(@Param("id") int id);

    public THmCustomer selectTHmCustomerByIdForWeChat(@Param("id") int id);

    public THmCustomer selectTHmCustomerByHcustomerCode(@Param("hcustomerCode") String hcustomerCode);

    /**
     * 查询用户信息列表
     */
    public List<THmCustomer> selectTHmCustomerList(THmCustomer tHmCustomer);

    public List<CustomerKeFu> selectTHmCustomerListForKefu();

    public List<THmCustomer> checkAddress(THmCustomer tHmCustomer);

    public Integer getMaxCode();

    public List<THmCustomer> getTHmCustomerListForWechart(THmCustomer tHmCustomer);

    /**
     * 导入用户数据 验证
     */
    public CustomerImport importCustomerCheck(List<THmCustomer> userList);

    /**
     * 导入用户数据 导入
     */
    public String importCustomer(List<THmCustomer> userList);

    /**
     * 新增用户信息
     */
    public int insertTHmCustomer(THmCustomer tHmCustomer);

    /**
     * 修改用户信息
     */
    public int updateTHmCustomer(THmCustomer tHmCustomer);

    public int updateTHmCustomerForWeChat(THmCustomer tHmCustomer);

    public List<SysUser> getUserList();

    public List<ReportVO> getSales(ReportVO reportVO);

    //高低热区统计
    public List<StationDetailVO> stationDetailLists(StationDetailVO stationDetailVO);

    public List<StationDetailVO> stationDetailListss(StationDetailVO stationDetailVO);

    public List<StationDetailVO> stationDetailListsss(StationDetailVO stationDetailVO);

    //微信支付根据用户code和手机号查询用户
    public THmCustomer getCustomerByCodeAndPhone(@Param("code") String code, @Param("phone") String phone);

    //微信支付绑定用户
    public int bindCustomer(WechatBind wechatBind);

    //微信支付获取绑定的用户列表
    public List<WechatBind> getWechatBindlist(@Param("wechatuserId") Integer wechatuserId);

    //解除绑定
    int delBind(@Param("bindId") Integer bindId);

    //绑定用户
    List<THmCustomer> bindlist(THmCustomer tHmCustomer);
}
