package com.ruoyi.business.service;

import com.ruoyi.business.domain.THmArea;
import com.ruoyi.common.core.domain.Ztree;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 供热分区Service接口
 */
public interface ITHmAreaService {
    /**
     * 查询供热分区
     */
    public THmArea selectTHmAreaById(@Param("id") int id);

    /**
     * 查询供热分区列表
     */
    public List<THmArea> selectTHmAreaList(THmArea tHmArea);

    public List<THmArea> selectTHmAreaListt(THmArea tHmArea);

    //获取parentId下边的id最大的区域 -- 用于计算code
    THmArea getMaxCode(THmArea tHmArea);

    //根据编号获取区域
    THmArea getAreaByCode(String code);

    /**
     * 新增供热分区
     */
    public int insertTHmArea(THmArea tHmArea);

    /**
     * 修改供热分区
     */
    public int updateTHmArea(THmArea tHmArea);

    public int setting(THmArea tHmArea);

    /**
     * 查询供热分区树列表
     */
    public List<Ztree> selectTHmAreaTree();

    public List<THmArea> getLevel2();
}
