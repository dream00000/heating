package com.ruoyi.business.domain.vo;

import lombok.Data;

@Data
public class WorkType {

    private String name;
    private String value;
}
