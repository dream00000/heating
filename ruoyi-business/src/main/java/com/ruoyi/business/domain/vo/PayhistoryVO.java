package com.ruoyi.business.domain.vo;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;

@Data
public class PayhistoryVO {

    private Integer hcustomerId;
    private Integer hareaId;
    private Integer hpayId;

    @Excel(name = "用户编号")
    private String hcustomerCode;

    @Excel(name = "用户姓名")
    private String hcustomerName;

    @Excel(name = "手机号码")
    private String hcustomerMobile;

    @Excel(name = "用户身份证")
    private String hcustomerPersonlcard;

    @Excel(name = "所属分区")
    private String hcustomerArea;

    @Excel(name = "详细地址")
    private String hcustomerDetailaddress;

    @Excel(name = "开户状态", readConverterExp = "0=否,1=是")
    private Integer hcustomerKhstate;

    @Excel(name = "应缴金额")
    private String hpayYjmoney;

    @Excel(name = "支付金额")
    private String hpayMoney;

    @Excel(name = "缴费类型", readConverterExp = "0=已缴基础热,1=已缴全热,2=已缴续费,3=已缴面积变更,4=部分缴费")
    private String hpaySfqe;

    @Excel(name = "支付方式")
    private String hpayType;

    @Excel(name = "商户订单号")
    private String hpayOuttradeno;

    @Excel(name = "交易时间")
    private String hpayTradetime;

    @Excel(name = "操作人员")
    private String sysname;

    private Long hpayAdduserid;

    private String nian;
}
