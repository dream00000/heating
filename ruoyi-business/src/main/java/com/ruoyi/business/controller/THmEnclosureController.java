package com.ruoyi.business.controller;

import com.ruoyi.business.domain.THmEnclosure;
import com.ruoyi.business.service.ITHmEnclosureService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 附件Controller
 *
 * @author ruoyi
 * @date 2021-08-13
 */
@Controller
@RequestMapping("/business/enclosure")
public class THmEnclosureController extends BaseController {
    private String prefix = "business/enclosure";

    @Resource
    private ITHmEnclosureService tHmEnclosureService;

    @GetMapping()
    public String enclosure() {
        return prefix + "/enclosure";
    }

    /**
     * 查询附件列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(THmEnclosure tHmEnclosure) {
        startPage();
        List<THmEnclosure> list = tHmEnclosureService.selectTHmEnclosureList(tHmEnclosure);
        return getDataTable(list);
    }

    /**
     * 导出附件列表
     */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(THmEnclosure tHmEnclosure) {
        List<THmEnclosure> list = tHmEnclosureService.selectTHmEnclosureList(tHmEnclosure);
        ExcelUtil<THmEnclosure> util = new ExcelUtil<THmEnclosure>(THmEnclosure.class);
        return util.exportExcel(list, "enclosure");
    }

    /**
     * 新增附件
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存附件
     */
    @Log(title = "附件", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(THmEnclosure tHmEnclosure) {
        return toAjax(tHmEnclosureService.insertTHmEnclosure(tHmEnclosure));
    }

    /**
     * 修改附件
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") int id, ModelMap mmap) {
        THmEnclosure tHmEnclosure = tHmEnclosureService.selectTHmEnclosureById(id);
        mmap.put("tHmEnclosure", tHmEnclosure);
        return prefix + "/edit";
    }

    /**
     * 修改保存附件
     */
    @Log(title = "附件", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(THmEnclosure tHmEnclosure) {
        return toAjax(tHmEnclosureService.updateTHmEnclosure(tHmEnclosure));
    }


    /**
     * 删除附件
     */
    @Log(title = "附件", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(@PathVariable("id") int id) {
        THmEnclosure tHmEnclosure = new THmEnclosure();
        tHmEnclosure.setEnId(id);
        tHmEnclosure.setEnState(0);
        return toAjax(tHmEnclosureService.updateTHmEnclosure(tHmEnclosure));
    }
}
