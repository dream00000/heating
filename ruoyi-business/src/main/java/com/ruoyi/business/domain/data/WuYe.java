package com.ruoyi.business.domain.data;

import lombok.Data;

@Data
public class WuYe {
    private Integer customerId;
    private String name;
    private String money;
    private String time;
}
