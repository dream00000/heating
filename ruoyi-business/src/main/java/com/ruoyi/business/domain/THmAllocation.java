package com.ruoyi.business.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.system.domain.SysUser;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 金额划拨对象 t_hm_allocation
 *
 * @author ruoyi
 * @date 2022-03-16
 */
public class THmAllocation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    private String customerCode;
    private String customerName;



    /** 划入账期id */
    @Excel(name = "划入账期id")
    private Integer fWqid;

    private String wqname; //账期
    private BigDecimal wqAmount; //账期已经缴纳金额

    /** 划出客户 */
    @Excel(name = "划出客户")

    private Long fCustomerid;

    /*
    * 划出客户信息
    * */
    private String fcustomerCode;
    private String fcustomerName;
    private String fcustomerMobile;
    private String fareaName;
    private String faddress;


    /*
     * 划入客户信息
     * */
    private String tcustomerCode;
    private String tcustomerName;
    private String tcustomerMobile;
    private String tareaName;
    private String taddress;



    /** 划转类型 */
    @Excel(name = "划转类型")
    private String cType;

    /** 入账日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "入账日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date fDate;

    /** 年度 */
    @Excel(name = "年度")
    private String fNian;

    /** 划转金额 */
    @Excel(name = "划转金额")
    private BigDecimal fAmount;



    /** 划出备注 */
    @Excel(name = "划出备注")
    private String fMemo;

    /** 划转编号 */
    @Excel(name = "划转编号")
    private String fCode;

    /** 划出前金额 */
    @Excel(name = "划出前金额")
    private BigDecimal fBeforebalance;

    /** 划出后金额 */
    @Excel(name = "划出后金额")
    private BigDecimal fAfterbalance;

    /** 划入客户 */
    @Excel(name = "划入客户")
    private Long tCustomerid;

    /** 划入备注 */
    @Excel(name = "划入备注")
    private String tMemo;

    /** 划入金额 */
    @Excel(name = "划入金额")
    private BigDecimal tAmount;

    /** 划入前金额 */
    @Excel(name = "划入前金额")
    private BigDecimal tBeforebalance;

    /** 划入后金额 */
    @Excel(name = "划入后金额")
    private BigDecimal tAfterbalance;

    /** 添加时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "添加时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date addtime;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long adduserid;
    private String username;


    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }

    public Integer getfWqid() {
        return fWqid;
    }

    public void setfWqid(Integer fWqid) {
        this.fWqid = fWqid;
    }

    public String getWqname() {
        return wqname;
    }

    public void setWqname(String wqname) {
        this.wqname = wqname;
    }

    public BigDecimal getWqAmount() {
        return wqAmount;
    }

    public void setWqAmount(BigDecimal wqAmount) {
        this.wqAmount = wqAmount;
    }

    public void setfCustomerid(Long fCustomerid)
    {
        this.fCustomerid = fCustomerid;
    }

    public Long getfCustomerid()
    {
        return fCustomerid;
    }
    public void setcType(String cType)
    {
        this.cType = cType;
    }

    public String getcType()
    {
        return cType;
    }
    public void setfDate(Date fDate)
    {
        this.fDate = fDate;
    }

    public Date getfDate()
    {
        return fDate;
    }
    public void setfNian(String fNian)
    {
        this.fNian = fNian;
    }

    public String getfNian()
    {
        return fNian;
    }
    public void setfAmount(BigDecimal fAmount)
    {
        this.fAmount = fAmount;
    }

    public BigDecimal getfAmount()
    {
        return fAmount;
    }
    public void setfMemo(String fMemo)
    {
        this.fMemo = fMemo;
    }

    public String getfMemo()
    {
        return fMemo;
    }
    public void setfCode(String fCode)
    {
        this.fCode = fCode;
    }

    public String getfCode()
    {
        return fCode;
    }
    public void setfBeforebalance(BigDecimal fBeforebalance)
    {
        this.fBeforebalance = fBeforebalance;
    }

    public BigDecimal getfBeforebalance()
    {
        return fBeforebalance;
    }
    public void setfAfterbalance(BigDecimal fAfterbalance)
    {
        this.fAfterbalance = fAfterbalance;
    }

    public BigDecimal getfAfterbalance()
    {
        return fAfterbalance;
    }
    public void settCustomerid(Long tCustomerid)
    {
        this.tCustomerid = tCustomerid;
    }

    public Long gettCustomerid()
    {
        return tCustomerid;
    }
    public void settMemo(String tMemo)
    {
        this.tMemo = tMemo;
    }

    public String gettMemo()
    {
        return tMemo;
    }
    public void settAmount(BigDecimal tAmount)
    {
        this.tAmount = tAmount;
    }

    public BigDecimal gettAmount()
    {
        return tAmount;
    }
    public void settBeforebalance(BigDecimal tBeforebalance)
    {
        this.tBeforebalance = tBeforebalance;
    }

    public BigDecimal gettBeforebalance()
    {
        return tBeforebalance;
    }
    public void settAfterbalance(BigDecimal tAfterbalance)
    {
        this.tAfterbalance = tAfterbalance;
    }

    public BigDecimal gettAfterbalance()
    {
        return tAfterbalance;
    }
    public void setAddtime(Date addtime)
    {
        this.addtime = addtime;
    }

    public Date getAddtime()
    {
        return addtime;
    }
    public void setAdduserid(Long adduserid)
    {
        this.adduserid = adduserid;
    }

    public Long getAdduserid()
    {
        return adduserid;
    }

    public String getFcustomerCode() {
        return fcustomerCode;
    }

    public void setFcustomerCode(String fcustomerCode) {
        this.fcustomerCode = fcustomerCode;
    }

    public String getFcustomerName() {
        return fcustomerName;
    }

    public void setFcustomerName(String fcustomerName) {
        this.fcustomerName = fcustomerName;
    }

    public String getFcustomerMobile() {
        return fcustomerMobile;
    }

    public void setFcustomerMobile(String fcustomerMobile) {
        this.fcustomerMobile = fcustomerMobile;
    }

    public String getFareaName() {
        return fareaName;
    }

    public void setFareaName(String fareaName) {
        this.fareaName = fareaName;
    }

    public String getFaddress() {
        return faddress;
    }

    public void setFaddress(String faddress) {
        this.faddress = faddress;
    }

    public String getTcustomerCode() {
        return tcustomerCode;
    }

    public void setTcustomerCode(String tcustomerCode) {
        this.tcustomerCode = tcustomerCode;
    }

    public String getTcustomerName() {
        return tcustomerName;
    }

    public void setTcustomerName(String tcustomerName) {
        this.tcustomerName = tcustomerName;
    }

    public String getTcustomerMobile() {
        return tcustomerMobile;
    }

    public void setTcustomerMobile(String tcustomerMobile) {
        this.tcustomerMobile = tcustomerMobile;
    }

    public String getTareaName() {
        return tareaName;
    }

    public void setTareaName(String tareaName) {
        this.tareaName = tareaName;
    }

    public String getTaddress() {
        return taddress;
    }

    public void setTaddress(String taddress) {
        this.taddress = taddress;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("fWqid", getfWqid())
            .append("fCustomerid", getfCustomerid())
            .append("cType", getcType())
            .append("fDate", getfDate())
            .append("fNian", getfNian())
            .append("fAmount", getfAmount())
            .append("fMemo", getfMemo())
            .append("fCode", getfCode())
            .append("fBeforebalance", getfBeforebalance())
            .append("fAfterbalance", getfAfterbalance())
            .append("tCustomerid", gettCustomerid())
            .append("tMemo", gettMemo())
            .append("tAmount", gettAmount())
            .append("tBeforebalance", gettBeforebalance())
            .append("tAfterbalance", gettAfterbalance())
            .append("addtime", getAddtime())
            .append("adduserid", getAdduserid())
            .toString();
    }
}
