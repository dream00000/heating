package com.ruoyi.business.service;

import com.ruoyi.business.domain.THmCustomer;
import com.ruoyi.business.domain.THmRefund;
import com.ruoyi.business.domain.vo.RefundVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 退费Service接口
 */
public interface ITHmRefundService {
    /**
     * 查询退费
     */
    public THmRefund selectTHmRefundById(@Param("id") int id);

    public THmRefund selectTHmRefundByIds(@Param("id") int id);

    /**
     * 查询退费列表
     */
    public List<THmRefund> selectTHmRefundList(THmRefund tHmRefund);

    public List<RefundVO> selectTHmRefundListt(THmCustomer tHmCustomer);

    /**
     * 新增退费
     */
    public int insertTHmRefund(THmRefund tHmRefund);

    /**
     * 修改退费
     */
    public int updateTHmRefund(THmRefund tHmRefund);
}
