package com.ruoyi.business.controller;

import com.ruoyi.business.domain.THmCustomer;
import com.ruoyi.business.domain.THmEnclosure;
import com.ruoyi.business.domain.THmFeedback;
import com.ruoyi.business.mapper.THmFeedbackMapper;
import com.ruoyi.business.service.ITHmEnclosureService;
import com.ruoyi.business.service.ITHmFeedbackService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 稽查反馈信息Controller
 *
 * @author ruoyi
 * @date 2021-08-13
 */
@Controller
@RequestMapping("/business/feedback")
public class THmFeedbackController extends BaseController {
    private String prefix = "business/feedback";

    @Resource
    private ITHmFeedbackService tHmFeedbackService;

    @Resource
    private THmFeedbackMapper tHmFeedbackMapper;

    @Resource
    private ITHmEnclosureService itHmEnclosureService;

    @GetMapping()
    public String feedback() {
        return prefix + "/feedback";
    }

    @GetMapping("/customer")
    public String customer() {
        return prefix + "/customer";
    }

    /**
     * 查询稽查反馈信息列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(THmFeedback tHmFeedback) {
        startPage();
        List<THmFeedback> list = tHmFeedbackService.selectTHmFeedbackList(tHmFeedback);
        return getDataTable(list);
    }

    /**
     * 查询稽查反馈信息列表
     */
    @PostMapping("/listt")
    @ResponseBody
    public TableDataInfo listt(THmCustomer tHmCustomer) {
        startPage();
        List<THmFeedback> list = tHmFeedbackService.selectTHmFeedbackListt(tHmCustomer);
        return getDataTable(list);
    }

    /**
     * 导出稽查反馈信息列表
     */
    @PostMapping("/exports")
    @ResponseBody
    public AjaxResult exports(THmCustomer tHmCustomer) {
//        int count = tHmFeedbackMapper.exportCountt(tHmCustomer);
//        if (count > 9999) {
//            return AjaxResult.error("导出数据不能超过9999");
//        } else {
        List<THmFeedback> list = tHmFeedbackService.selectTHmFeedbackListt(tHmCustomer);
        ExcelUtil<THmFeedback> util = new ExcelUtil<THmFeedback>(THmFeedback.class);
        return util.exportExcel(list, "feedback");
//        }
    }

    /**
     * 导出稽查反馈信息列表
     */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(THmFeedback tHmFeedback) {
        List<THmFeedback> list = tHmFeedbackService.selectTHmFeedbackList(tHmFeedback);
        ExcelUtil<THmFeedback> util = new ExcelUtil<THmFeedback>(THmFeedback.class);
        return util.exportExcel(list, "feedback");
    }

    /**
     * 导出稽查反馈信息列表
     */
    @PostMapping("/exportt")
    @ResponseBody
    public AjaxResult export(THmCustomer tHmCustomer) {
        List<THmCustomer> list = tHmFeedbackService.selectTHmFeedbackLists(tHmCustomer);
        ExcelUtil<THmCustomer> util = new ExcelUtil<THmCustomer>(THmCustomer.class);
        return util.exportExcel(list, "feedback");
    }

    /**
     * 新增稽查反馈信息
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存稽查反馈信息
     */
    @Log(title = "稽查反馈信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(THmFeedback tHmFeedback) {
        return toAjax(tHmFeedbackService.insertTHmFeedback(tHmFeedback));
    }

    /**
     * 修改稽查反馈信息
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") int id, ModelMap mmap) {
        THmFeedback tHmFeedback = tHmFeedbackService.selectTHmFeedbackById(id);
        mmap.put("tHmFeedback", tHmFeedback);
        return prefix + "/edit";
    }

    /**
     * 修改保存稽查反馈信息
     */
    @Log(title = "稽查反馈信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(THmFeedback tHmFeedback) {
        return toAjax(tHmFeedbackService.updateTHmFeedback(tHmFeedback));
    }


    /**
     * 删除稽查反馈信息
     */
    @Log(title = "稽查反馈信息", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(@PathVariable("id") int id) {
        THmFeedback tHmFeedback = new THmFeedback();
        tHmFeedback.setFdId(id);
        tHmFeedback.setFdState(0);
        return toAjax(tHmFeedbackService.updateTHmFeedback(tHmFeedback));
    }

    @GetMapping("/imgs/{fdFjcode}")
    public String imgs(@PathVariable("fdFjcode") String fdFjcode, Model model) {
        Map<String, Object> map = new HashMap<>();
        map.put("fdFjcode", fdFjcode);
        model.addAllAttributes(map);
        return prefix + "/imgs";
    }

    /**
     * 本次账期充值记录
     */
    @PostMapping("/imgsList/{fdFjcode}")
    @ResponseBody
    public TableDataInfo imgsList(@PathVariable("fdFjcode") String fdFjcode) {
        THmEnclosure tHmEnclosure = new THmEnclosure();
        tHmEnclosure.setEnFjcode(fdFjcode);
        startPage();
        List<THmEnclosure> list = itHmEnclosureService.selectTHmEnclosureList(tHmEnclosure);
        return getDataTable(list);
    }

    //-----------------新版阀控-----------------------
    //阀门管理 - 跳转
    @GetMapping("/tapCustomer")
    public String tapCustomer() {
        return prefix + "/customer";
    }

    //阀门管理
    @PostMapping("/tapCustomerList")
    @ResponseBody
    public TableDataInfo lists(THmCustomer tHmCustomer) {
        startPage();
        List<THmCustomer> list = tHmFeedbackService.tapCustomers(tHmCustomer);
        return getDataTable(list);
    }

    //阀门管理
    @PostMapping("/tapCustomerLists")
    @ResponseBody
    public TableDataInfo tapCustomerLists(THmCustomer tHmCustomer) {

        if (tHmCustomer.getDType() == 0) {
            startPage();
            List<THmCustomer> list = tHmFeedbackService.tapCustomerd(tHmCustomer);
            if (list.size() > 0) {
                for (THmCustomer hmCustomer : list) {
                    if (hmCustomer.getWqInt1() != null) {
                        if (hmCustomer.getHcustomerTapstate() == 0 && (hmCustomer.getWqInt1() == 0 || hmCustomer.getWqInt1() == 3 || hmCustomer.getWqInt1() == 99)) {
                            hmCustomer.setDType(2);
                        } else if (hmCustomer.getHcustomerTapstate() == 2 && (hmCustomer.getWqInt1() == 1 || hmCustomer.getWqInt1() == 2 || hmCustomer.getWqInt1() == 4 || hmCustomer.getWqInt1() == 6)) {
                            hmCustomer.setDType(3);
                        } else if (hmCustomer.getHcustomerTapstate() == 1 && (hmCustomer.getWqInt1() == 1 || hmCustomer.getWqInt1() == 2 || hmCustomer.getWqInt1() == 4 || hmCustomer.getWqInt1() == 6)) {
                            hmCustomer.setDType(1);
                        }
                    }
                }
            }
            return getDataTable(list);
        } else if (tHmCustomer.getDType() == 1) {
            startPage();
            List<THmCustomer> list = tHmFeedbackService.tapCustomerd3(tHmCustomer);
            if (list.size() > 0) {
                for (THmCustomer hmCustomer : list) {
                    if (hmCustomer.getHcustomerTapstate() == 1 && (hmCustomer.getWqInt1() == 1 || hmCustomer.getWqInt1() == 2 || hmCustomer.getWqInt1() == 4 || hmCustomer.getWqInt1() == 6)) {
                        hmCustomer.setDType(1);
                    }
                }
            }
            return getDataTable(list);
        } else if (tHmCustomer.getDType() == 2) {
            startPage();
            List<THmCustomer> list = tHmFeedbackService.tapCustomerd1(tHmCustomer);
            if (list.size() > 0) {
                for (THmCustomer hmCustomer : list) {
                    if (hmCustomer.getHcustomerTapstate() == 0 && (hmCustomer.getWqInt1() == 0 || hmCustomer.getWqInt1() == 3 || hmCustomer.getWqInt1() == 99)) {
                        hmCustomer.setDType(2);
                    }
                }
            }
            return getDataTable(list);
        } else {
            startPage();
            List<THmCustomer> list = tHmFeedbackService.tapCustomerd2(tHmCustomer);
            if (list.size() > 0) {
                for (THmCustomer hmCustomer : list) {
                    if (hmCustomer.getHcustomerTapstate() == 2 && (hmCustomer.getWqInt1() == 1 || hmCustomer.getWqInt1() == 2 || hmCustomer.getWqInt1() == 4 || hmCustomer.getWqInt1() == 6)) {
                        hmCustomer.setDType(3);
                    }
                }
            }
            return getDataTable(list);
        }
    }

    /**
     * 导出阀控用户
     */
    @PostMapping("/tapCustomerListExport")
    @ResponseBody
    public AjaxResult tapCustomerListExport(THmCustomer tHmCustomer) {
//        int count = tHmFeedbackMapper.exportCount(tHmCustomer);
//        if (count > 9999) {
//            return AjaxResult.error("导出数据不能超过9999");
//        } else {
        List<THmCustomer> list = tHmFeedbackService.tapCustomers(tHmCustomer);
        ExcelUtil<THmCustomer> util = new ExcelUtil<THmCustomer>(THmCustomer.class);
        return util.exportExcel(list, "feedback");
//        }
    }

    //跳转阀控操作页面
    @GetMapping("/top/{id}")
    public String top(@PathVariable("id") int id, ModelMap mmap) {
        THmCustomer tHmCustomer = tHmFeedbackService.tapCustomerByPriceId(id);
        mmap.put("tHmCustomer", tHmCustomer);
        return prefix + "/top";
    }

    @GetMapping("/glist")
    public String glist() {
        return prefix + "/gtoplist";
    }

    /**
     * 查询稽查反馈信息列表
     */
    @PostMapping("/glist")
    @ResponseBody
    public TableDataInfo glist(THmCustomer tHmCustomer) {
        if (tHmCustomer.getHareaId() == null) {
            tHmCustomer.setHareaId(1);
        }
        startPage();
        List<THmCustomer> list = tHmFeedbackService.tapCustomer(tHmCustomer);
        return getDataTable(list);
    }

    /**
     * 导出关阀用户
     */
    @PostMapping("/glistExport")
    @ResponseBody
    public AjaxResult glistExport(THmCustomer tHmCustomer) {
//        int count = tHmFeedbackMapper.exportCounts(tHmCustomer);
//        if (count > 9999) {
//            return AjaxResult.error("导出数据不能超过9999");
//        } else {
        List<THmCustomer> list = tHmFeedbackService.tapCustomer(tHmCustomer);
        ExcelUtil<THmCustomer> util = new ExcelUtil<THmCustomer>(THmCustomer.class);
        return util.exportExcel(list, "feedback");
//        }
    }

    @GetMapping("/dlist")
    public String dlist() {
        return prefix + "/dtoplist";
    }

    /**
     * 查询稽查反馈信息列表
     */
    @PostMapping("/dlist")
    @ResponseBody
    public TableDataInfo dlist(THmCustomer tHmCustomer) {
        if (tHmCustomer.getHareaId() == null) {
            tHmCustomer.setHareaId(1);
        }
        startPage();
        List<THmCustomer> list = tHmFeedbackService.tapCustomer(tHmCustomer);
        return getDataTable(list);
    }

    /**
     * 导出断管用户
     */
    @PostMapping("/dlistExport")
    @ResponseBody
    public AjaxResult dlistExport(THmCustomer tHmCustomer) {
        List<THmCustomer> list = tHmFeedbackService.tapCustomer(tHmCustomer);
        ExcelUtil<THmCustomer> util = new ExcelUtil<THmCustomer>(THmCustomer.class);
        return util.exportExcel(list, "feedback");
    }

    //待关阀
    @GetMapping("/gTap")
    public String gTap() {
        return prefix + "/gtap";
    }

    @PostMapping("/gTaplist")
    @ResponseBody
    public TableDataInfo gTaplist(THmCustomer tHmCustomer) {
        startPage();
        List<THmCustomer> list = tHmFeedbackService.tapCustomerd1(tHmCustomer);
        return getDataTable(list);
    }

    /**
     * 导出待关阀用户
     */
    @PostMapping("/gTaplistExport")
    @ResponseBody
    public AjaxResult gTaplistExport(THmCustomer tHmCustomer) {
        List<THmCustomer> list = tHmFeedbackService.tapCustomerd1(tHmCustomer);
        ExcelUtil<THmCustomer> util = new ExcelUtil<THmCustomer>(THmCustomer.class);
        return util.exportExcel(list, "feedback");
    }

    //待接管
    @GetMapping("/jTap")
    public String jTap() {
        return prefix + "/jtap";
    }

    @PostMapping("/jTaplist")
    @ResponseBody
    public TableDataInfo jTaplist(THmCustomer tHmCustomer) {
        startPage();
        List<THmCustomer> list = tHmFeedbackService.tapCustomerd2(tHmCustomer);
        return getDataTable(list);
    }

    /**
     * 导出待接管用户
     */
    @PostMapping("/jTaplistExport")
    @ResponseBody
    public AjaxResult jTaplistExport(THmCustomer tHmCustomer) {
        List<THmCustomer> list = tHmFeedbackService.tapCustomerd2(tHmCustomer);
        ExcelUtil<THmCustomer> util = new ExcelUtil<THmCustomer>(THmCustomer.class);
        return util.exportExcel(list, "feedback");
    }

    //待开阀
    @GetMapping("/kTap")
    public String kTap() {
        return prefix + "/ktap";
    }

    @PostMapping("/kTaplist")
    @ResponseBody
    public TableDataInfo kTaplist(THmCustomer tHmCustomer) {
        startPage();
        List<THmCustomer> list = tHmFeedbackService.tapCustomerd3(tHmCustomer);
        return getDataTable(list);
    }

    /**
     * 导出待开阀用户
     */
    @PostMapping("/kTaplistExport")
    @ResponseBody
    public AjaxResult kTaplistExport(THmCustomer tHmCustomer) {
        List<THmCustomer> list = tHmFeedbackService.tapCustomerd3(tHmCustomer);
        ExcelUtil<THmCustomer> util = new ExcelUtil<THmCustomer>(THmCustomer.class);
        return util.exportExcel(list, "feedback");
    }
}
