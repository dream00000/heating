package com.ruoyi.business.domain;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;

//缴费时的记录，用于打印
@Data
public class THmPayhistoryPrice {

    //主键
    private Long id;
    //充值外键
    private Integer payhistoryId;
    //分区
    private Integer hareaId;

    @Excel(name = "用户编号")
    private String hcustomerCode;

    @Excel(name = "用户姓名")
    private String hcustomerName;

    @Excel(name = "用户手机号")
    private String hcustomerMobile;

    @Excel(name = "用户分区")
    private String hareaName;

    @Excel(name = "详细地址")
    private String hcustomerDetailaddress;

    @Excel(name = "单价")
    private String price;

    @Excel(name = "面积")
    private String area;

    //超高面积
    private String areac;
    //类型 2实供 3停供
    private Integer type;

    @Excel(name = "系数")
    private String xishu;

    @Excel(name = "金额")
    private String total;

    @Excel(name = "单价名称")
    private String priceType;

    @Excel(name = "收费年度")
    private String nian;

    @Excel(name = "缴费类型", width = 20, readConverterExp = "0=基础,1=全热,2=续费,3=面积变更,4=部分缴费")
    private Integer hpaySfqe;
}
