package com.ruoyi.business.mapper;


import com.ruoyi.business.domain.PaymentRecord;

import java.util.List;

/**
 * 订单Mapper接口
 */
public interface PaymentRecordMapper {
    /**
     * 查询订单
     */
    public PaymentRecord selectPaymentRecordById(Long paymentId);

    /**
     * 查询订单列表
     */
    public List<PaymentRecord> selectPaymentRecordList(PaymentRecord paymentRecord);

    /**
     * 新增订单
     */
    public int insertPaymentRecord(PaymentRecord paymentRecord);

    /**
     * 修改订单
     */
    public int updatePaymentRecord(PaymentRecord paymentRecord);

    /**
     * 删除订单
     */
    public int deletePaymentRecordById(Long paymentId);

    /**
     * 批量删除订单
     */
    public int deletePaymentRecordByIds(String[] paymentIds);
}
