package com.ruoyi.business.service.impl;

import com.ruoyi.business.domain.THmArea;
import com.ruoyi.business.domain.THmCustomer;
import com.ruoyi.business.domain.THmLessen;
import com.ruoyi.business.domain.vo.LessenVO;
import com.ruoyi.business.mapper.THmAreaMapper;
import com.ruoyi.business.mapper.THmCustomerMapper;
import com.ruoyi.business.mapper.THmLessenMapper;
import com.ruoyi.business.service.ITHmLessenService;
import com.ruoyi.framework.util.ShiroUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 用热减免Service业务层处理
 *
 * @author ruoyi
 * @date 2021-08-13
 */
@Service
public class THmLessenServiceImpl implements ITHmLessenService {
    @Resource
    private THmLessenMapper tHmLessenMapper;

    @Resource
    private THmCustomerMapper tHmCustomerMapper;

    @Resource
    private THmAreaMapper tHmAreaMapper;

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * 查询用热减免
     *
     * @param id
     * @return 用热减免
     */
    @Override
    public THmLessen selectTHmLessenById(int id) {
        THmLessen tHmLessen = new THmLessen();
        THmCustomer tHmCustomer = tHmCustomerMapper.selectTHmCustomerById(id);
        tHmLessen.setHcustomerId(id);
        tHmLessen.setCustomerName(tHmCustomer.getHcustomerName());
        tHmLessen.setCustomerCode(tHmCustomer.getHcustomerCode());
        tHmLessen.setCustomerPhone(tHmCustomer.getHcustomerMobile());
        tHmLessen.setCustomerAddress(tHmCustomer.getHcustomerDetailaddress());
        tHmLessen.setCustomerArea(tHmAreaMapper.selectTHmAreaById(tHmCustomer.getHareaId()).getHareaName());
        return tHmLessen;
    }

    /**
     * 查询用热减免
     *
     * @param id
     * @return 用热减免
     */
    @Override
    public THmLessen selectTHmLessenByIds(int id) {
        return tHmLessenMapper.selectTHmLessenById(id);
    }

    /**
     * 查询用热减免列表
     *
     * @param tHmLessen 用热减免
     * @return 用热减免
     */
    @Override
    public List<THmLessen> selectTHmLessenList(THmLessen tHmLessen) {
        List<THmLessen> list = tHmLessenMapper.selectTHmLessenList(tHmLessen);
        for (THmLessen hmLessen : list) {
            THmCustomer tHmCustomer = tHmCustomerMapper.selectTHmCustomerById(hmLessen.getHcustomerId());
            hmLessen.setCustomerName(tHmCustomer.getHcustomerName());
            hmLessen.setCustomerCode(tHmCustomer.getHcustomerCode());
            hmLessen.setCustomerAddress(tHmCustomer.getHcustomerDetailaddress());
            hmLessen.setCustomerPhone(tHmCustomer.getHcustomerMobile());

            //所属分区
            THmArea tHmArea = tHmAreaMapper.selectTHmAreaById(tHmCustomer.getHareaId());
            if (tHmArea != null) {
                if (tHmArea.getHareaLevel() == 2) {
                    hmLessen.setCustomerArea(tHmArea.getHareaName());
                } else {
                    THmArea tHmArea1 = tHmAreaMapper.selectTHmAreaById(tHmArea.getHareaParentid());
                    hmLessen.setCustomerArea(tHmArea1.getHareaName());
                }
            }

        }
        return list;
    }

    @Override
    public List<LessenVO> selectTHmLessenListt(THmCustomer tHmCustomer) {
        List<LessenVO> list = tHmLessenMapper.selectTHmLessenListt(tHmCustomer);
        for (LessenVO lessenVO : list) {
            //所属分区
            THmArea tHmArea = tHmAreaMapper.selectTHmAreaById(lessenVO.getHareaId());
            if (tHmArea != null) {
                if (tHmArea.getHareaLevel() == 2) {
                    lessenVO.setHcustomerArea(tHmArea.getHareaName());
                } else {
                    THmArea tHmArea1 = tHmAreaMapper.selectTHmAreaById(tHmArea.getHareaParentid());
                    lessenVO.setHcustomerArea(tHmArea1.getHareaName());
                }
            }
        }
        return list;
    }

    /**
     * 新增用热减免
     *
     * @param tHmLessen 用热减免
     * @return 结果
     */
    @Override
    public int insertTHmLessen(THmLessen tHmLessen) {
        //新增记录
        tHmLessen.setJmAdduserid(ShiroUtils.getSysUser().getUserId());
        tHmLessen.setJmAddtime(sdf.format(new Date()));
        tHmLessen.setJmState(0);
        //更新用户优惠金额
        THmCustomer tHmCustomer = tHmCustomerMapper.selectTHmCustomerById(tHmLessen.getHcustomerId());
        tHmCustomer.setHcustomerId(tHmLessen.getHcustomerId());
        if (tHmCustomer.getHcustomerGzje() != null) {
            BigDecimal bd = new BigDecimal(tHmCustomer.getHcustomerGzje());
            BigDecimal bds = new BigDecimal(tHmLessen.getJmMoney());
            bd = bd.add(bds);
            tHmCustomer.setHcustomerGzje(bd.toString());
        } else {
            tHmCustomer.setHcustomerGzje(tHmLessen.getJmMoney());
            tHmCustomer.setBalance(tHmLessen.getJmMoney());
        }
        //更新用户余额
        if (tHmCustomer.getBalance() != null) {
            BigDecimal b1 = new BigDecimal(tHmLessen.getJmMoney());
            BigDecimal b2 = new BigDecimal(tHmCustomer.getBalance());
            BigDecimal b3 = b2.add(b1);
            tHmCustomer.setBalance(b3.toString());
        } else {
            tHmCustomer.setBalance(tHmLessen.getJmMoney());
        }
        tHmCustomerMapper.updateTHmCustomer(tHmCustomer);
        return tHmLessenMapper.insertTHmLessen(tHmLessen);
    }

    /**
     * 修改用热减免
     *
     * @param tHmLessen 用热减免
     * @return 结果
     */
    @Override
    public int updateTHmLessen(THmLessen tHmLessen) {
        //将金额从用户余额里删掉
        THmLessen tHmLessen1 = tHmLessenMapper.selectTHmLessenById(tHmLessen.getJmId());
        THmCustomer tHmCustomer = tHmCustomerMapper.selectTHmCustomerById(tHmLessen1.getHcustomerId());
        BigDecimal bd = new BigDecimal(tHmCustomer.getBalance());//用户当前余额
        tHmCustomer.setBalance((bd.subtract(new BigDecimal(tHmLessen1.getJmMoney()))).toString());
        tHmCustomerMapper.updateTHmCustomer(tHmCustomer);

        return tHmLessenMapper.updateTHmLessen(tHmLessen);
    }

}
