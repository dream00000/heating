package com.ruoyi.business.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.List;

/**
 * 用户年度账期对象 t_hm_customerpaydays
 *
 * @author ruoyi
 * @date 2021-08-13
 */
@Data
public class THmCustomerpaydays extends BaseEntity {

    private Integer wqId;

    //策略id
    private Integer hzqId;

    //用户id
    private Integer hcustomerId;

    @Excel(name = "用户编号")
    private String customerCode;

    @Excel(name = "用户姓名")
    private String customerName;

    @Excel(name = "手机号")
    private String phone;

    @Excel(name = "所属分区")
    private String areaName;

    @Excel(name = "详细地址")
    private String hcustomerDetailaddress;

    @Excel(name = "房屋面积")
    private String hcustomerHomearea;

    @Excel(name = "超高面积")
    private String hcustomerAbovearea;

    @Excel(name = "停供面积")
    private String hcustomerTgarea;

    @Excel(name = "实供面积")
    private String hcustomerActualarea;

    @Excel(name = "账期金额")
    private String hcustomerMoney;

    @Excel(name = "本期优惠金额")
    private String hcustomerGzje;

    @Excel(name = "供热开始年度")
    private String hzqGrstarttime;

    @Excel(name = "供热结束年度")
    private String hzqGrendtime;

    @Excel(name = "供热天数")
    private Integer hzqGrdays;

    private Integer wqState;

    @Excel(name = "添加时间")
    private String wqAddtime;

    /**
     * 0--》基础热
     * 1--》全额
     * 2--》续费
     * 3--》延期欠费
     * 4--》平账
     * 5--》面积变更欠费
     * 6--》部分缴费欠费
     * 99 --》正常欠费
     */
    @Excel(name = "充值类型")
    private Integer wqInt1;

    //充值前状态 用于缴费的作废
    private Integer wqInt2;

    @Excel(name = "当期金额")
    private String wqStr1;

    @Excel(name = "延期欠费金额")
    private String wqStr2;

    @Excel(name = "延期天数")
    private String wqStr3;

    @Excel(name = "延期金额")
    private String wqStr4;

    @Excel(name = "面积变更金额")
    private String wqStr5;

    @Excel(name = "分期欠费金额")
    private String wqStr6;

    //单价列表
    private List<THmCustomerpaydaysPrice> customerpriceList;

    @Excel(name = "用户余额")
    private String balance;

    //零钱计算方式
    private Integer ling;
    //费用类型，0是基础热，1是全热 5是复合
    private Integer payType;
    private String payTypeName;
    //是否调整 0否1是
    private Integer isTiao;
    //查询不同状态组合的账期
    //1的时候查 int1为3，99的
    //2的时候查 int1为0的
    //3的时候查 int1为5的
    private Integer type;
    //微信支付本账期能不能缴费 0是能 1是不能
    private Integer isNeng;
    //微信支付本账期不能缴费的原因
    private String reason;
}
