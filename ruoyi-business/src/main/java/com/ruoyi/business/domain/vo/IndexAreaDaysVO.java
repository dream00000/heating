package com.ruoyi.business.domain.vo;

import lombok.Data;

/**
 * 返回首页分区供热天数的实体类
 */
@Data
public class IndexAreaDaysVO {

    private int days;//时间
    private int hareaId;//内容
    private String hareaName;//内容
}
