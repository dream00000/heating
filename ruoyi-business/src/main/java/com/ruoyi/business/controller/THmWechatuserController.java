package com.ruoyi.business.controller;

import com.ruoyi.business.domain.THmCustomer;
import com.ruoyi.business.domain.WechatBind;
import com.ruoyi.business.domain.WechatUser;
import com.ruoyi.business.service.ITHmCustomerService;
import com.ruoyi.business.service.IWechatUserService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 微信用户Controller
 *
 * @author ruoyi
 * @date 2022-01-15
 */
@Controller
@RequestMapping("/business/wechatuser")
public class THmWechatuserController extends BaseController {
    private String prefix = "business/wechatuser";

    @Resource
    private ITHmCustomerService itHmCustomerService;

    @Resource
    private IWechatUserService iWechatUserService;

    @GetMapping()
    public String wechatuser() {
        return prefix + "/wechatuser";
    }

    /**
     * 查询微信用户列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(WechatUser wechatUser) {
        startPage();
        List<WechatUser> list = iWechatUserService.selectWechatUserList(wechatUser);
        return getDataTable(list);
    }

    /**
     * 修改微信用户
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap) {
        WechatUser wechatUser = iWechatUserService.selectWechatUserById(id);
        mmap.put("wechatUser", wechatUser);
        return prefix + "/edit";
    }

    //绑定
    @PostMapping("edit")
    @ResponseBody
    public AjaxResult bindCustomerCode(WechatUser wechatUser) {
        //根据用户code和手机号查询是否存在用户
        THmCustomer tHmCustomer = itHmCustomerService.selectTHmCustomerByHcustomerCode(wechatUser.getCode());
        if (tHmCustomer == null) {
            return AjaxResult.error("1", "查询无此用户信息");
        } else {
            if (tHmCustomer.getHcustomerMobile() == null) {
                return AjaxResult.error("1", "您在供热公司预留的电话号码格式不正确，请到供热公司修改后再绑定");
            } else if (!tHmCustomer.getHcustomerMobile().equals(wechatUser.getPhone())) {
                return AjaxResult.error("1", "填写的手机号与系统内预留信息不符");
            } else {
                //根据openId获取用户信息
                WechatUser wechatUser1 = iWechatUserService.getWechatUser(wechatUser.getOpenId());
                //绑定
                WechatBind wechatBind = new WechatBind();
                wechatBind.setCustomerId(tHmCustomer.getHcustomerId());
                wechatBind.setWechatuserId(wechatUser1.getWechatId());
                wechatBind.setStatus(0);
                return AjaxResult.success(itHmCustomerService.bindCustomer(wechatBind));
            }
        }
    }

    @GetMapping("/bind")
    public String bind() {
        return prefix + "/bind";
    }

    /**
     * 查询微信用户列表
     */
    @PostMapping("/bindList")
    @ResponseBody
    public TableDataInfo bindList(THmCustomer tHmCustomer) {
        startPage();
        List<THmCustomer> list = itHmCustomerService.bindlist(tHmCustomer);
        return getDataTable(list);
    }

    /**
     * 解绑
     */
    @Log(title = "解绑", businessType = BusinessType.DELETE)
    @PostMapping("/remove/{bindId}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("bindId") String bindId) {
        return toAjax(itHmCustomerService.delBind(Integer.parseInt(bindId)));
    }
}
