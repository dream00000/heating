package com.ruoyi.business.controller;

import com.ruoyi.business.domain.THmCustomer;
import com.ruoyi.business.domain.THmOtherpayhistory;
import com.ruoyi.business.domain.vo.OtherpayhistoryVO;
import com.ruoyi.business.service.ITHmCustomerService;
import com.ruoyi.business.service.ITHmOtherpayhistoryService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 其他收费Controller
 *
 * @author ruoyi
 * @date 2021-08-13
 */
@Controller
@RequestMapping("/business/otherPayhistory")
public class THmOtherpayhistoryController extends BaseController {
    private String prefix = "business/otherpayhistory";

    @Resource
    private ITHmOtherpayhistoryService tHmOtherpayhistoryService;

    @Resource
    private ITHmCustomerService tHmCustomerService;

    @GetMapping()
    public String customer() {
        return prefix + "/customer";
    }

    /**
     * 查询用户信息列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(THmCustomer tHmCustomer) {
        startPage();
        List<THmCustomer> list = tHmCustomerService.selectTHmCustomerList(tHmCustomer);
        return getDataTable(list);
    }

    @GetMapping("/otherpayhistoryList")
    public String otherpayhistoryList() {
        return prefix + "/otherPayhistorylist";
    }

    /**
     * 查询退费列表
     */
    @PostMapping("/listt")
    @ResponseBody
    public TableDataInfo listt(THmCustomer tHmCustomer) {
        startPage();
        List<OtherpayhistoryVO> list = tHmOtherpayhistoryService.selectTHmOtherpayhistoryListt(tHmCustomer);
        return getDataTable(list);
    }


    @GetMapping("/{id}")
    public String otherPayhistory(@PathVariable("id") int id, Model model) {
        Map<String, Object> map = new HashMap<>();
        map.put("hcustomerId", id);
        model.addAllAttributes(map);
        return prefix + "/otherPayhistory";
    }

    /**
     * 查询其他收费列表
     */
    @PostMapping("/list/{id}")
    @ResponseBody
    public TableDataInfo list(@PathVariable("id") Integer id) {
        THmOtherpayhistory tHmOtherpayhistory = new THmOtherpayhistory();
        tHmOtherpayhistory.setHcustomerId(id);
        List<THmOtherpayhistory> list = tHmOtherpayhistoryService.selectTHmOtherpayhistoryList(tHmOtherpayhistory);
        return getDataTable(list);
    }

    /**
     * 其他收费明细
     */
    @PostMapping("/list/{startTime}/{endTime}")
    @ResponseBody
    public TableDataInfo list(@PathVariable("startTime") String startTime, @PathVariable("endTime") String endTime) {
        //拼参
        Map<String, Object> map = new HashMap<>();
        map.put("startTime", startTime);
        map.put("endTime", endTime);
        //查询
        THmOtherpayhistory tHmOtherpayhistory = new THmOtherpayhistory();
        tHmOtherpayhistory.setParams(map);
        List<THmOtherpayhistory> list = tHmOtherpayhistoryService.selectTHmOtherpayhistoryList(tHmOtherpayhistory);
        return getDataTable(list);
    }

    /**
     * 导出其他收费列表
     */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(THmOtherpayhistory tHmOtherpayhistory) {
        List<THmOtherpayhistory> list = tHmOtherpayhistoryService.selectTHmOtherpayhistoryList(tHmOtherpayhistory);
        ExcelUtil<THmOtherpayhistory> util = new ExcelUtil<THmOtherpayhistory>(THmOtherpayhistory.class);
        return util.exportExcel(list, "otherpayhistory");
    }

    /**
     * 导出其他收费列表
     */
    @PostMapping("/exportt")
    @ResponseBody
    @Log(title = "导出其他收费列表", businessType = BusinessType.EXPORT)
    public AjaxResult exportt(THmCustomer tHmCustomer) {
        List<OtherpayhistoryVO> list = tHmOtherpayhistoryService.selectTHmOtherpayhistoryListt(tHmCustomer);
        ExcelUtil<OtherpayhistoryVO> util = new ExcelUtil<>(OtherpayhistoryVO.class);
        return util.exportExcel(list, "其他收费");
    }

    /**
     * 新增其他收费
     */
    @GetMapping("/add/{id}")
    public String add(@PathVariable("id") int id, ModelMap model) {
        THmOtherpayhistory tHmOtherpayhistory = tHmOtherpayhistoryService.selectTHmOtherpayhistoryById(id);
        model.put("tHmOtherpayhistory", tHmOtherpayhistory);
        return prefix + "/add";
    }

    /**
     * 新增保存其他收费
     */
    @Log(title = "新增其他收费", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(THmOtherpayhistory tHmOtherpayhistory) {
        return AjaxResult.success("成功", tHmOtherpayhistoryService.insertTHmOtherpayhistory(tHmOtherpayhistory));
    }

    /**
     * 修改其他收费
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") int id, ModelMap mmap) {
        THmOtherpayhistory tHmOtherpayhistory = tHmOtherpayhistoryService.selectTHmOtherpayhistoryById(id);
        mmap.put("tHmOtherpayhistory", tHmOtherpayhistory);
        return prefix + "/edit";
    }

    /**
     * 修改保存其他收费
     */
    @Log(title = "修改其他收费", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(THmOtherpayhistory tHmOtherpayhistory) {
        return toAjax(tHmOtherpayhistoryService.updateTHmOtherpayhistory(tHmOtherpayhistory));
    }


    /**
     * 删除其他收费
     */
    @Log(title = "删除其他收费", businessType = BusinessType.DELETE)
    @PostMapping("/remove/{id}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("id") int id) {
        THmOtherpayhistory tHmOtherpayhistory = new THmOtherpayhistory();
        tHmOtherpayhistory.setOpayId(id);
        tHmOtherpayhistory.setOpayState(1);
        return toAjax(tHmOtherpayhistoryService.updateTHmOtherpayhistory(tHmOtherpayhistory));
    }

    //待收费列表
    @GetMapping("/shou/{id}")
    public String otherPayhistorys(@PathVariable("id") int id, Model model) {
        Map<String, Object> map = new HashMap<>();
        map.put("hcustomerId", id);
        model.addAllAttributes(map);
        return prefix + "/otherPayhistorys";
    }

    /**
     * 查询其他收费列表
     */
    @PostMapping("/lists/{id}")
    @ResponseBody
    public TableDataInfo lists(@PathVariable("id") Integer id) {
        THmOtherpayhistory tHmOtherpayhistory = new THmOtherpayhistory();
        tHmOtherpayhistory.setHcustomerId(id);
        List<THmOtherpayhistory> list = tHmOtherpayhistoryService.lists(tHmOtherpayhistory);
        return getDataTable(list);
    }


    /**
     * 收费
     */
    @Log(title = "收费", businessType = BusinessType.DELETE)
    @PostMapping("/fei")
    @ResponseBody
    public AjaxResult fei(Integer id) {
        THmOtherpayhistory tHmOtherpayhistory = new THmOtherpayhistory();
        tHmOtherpayhistory.setOpayState(0);
        tHmOtherpayhistory.setOpayId(id);
        if (tHmOtherpayhistoryService.updateTHmOtherpayhistory(tHmOtherpayhistory) > 0) {
            return AjaxResult.success(id);
        } else {
            return AjaxResult.error();
        }
    }
}
