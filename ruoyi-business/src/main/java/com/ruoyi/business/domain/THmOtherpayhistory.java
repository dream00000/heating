package com.ruoyi.business.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 其他收费对象 t_hm_otherPayhistory
 *
 * @author ruoyi
 * @date 2021-08-13
 */
@Data
public class THmOtherpayhistory extends BaseEntity {

    private Integer opayId;

    /**
     * 用户id
     */
    private Integer hcustomerId;

    @Excel(name = "用户编号")
    private String hcustomerCode;

    @Excel(name = "用户姓名")
    private String hcustomerName;

    @Excel(name = "手机号")
    private String hcustomerMobile;

    @Excel(name = "详细地址")
    private String hcustomerAddress;

    @Excel(name = "所属分区")
    private String hcustomerArea;

    @Excel(name = "收费类型")
    private String opayType;
    private String opayTypes;

    //支付方式
    private Integer hpayType;

    //汉字
    @Excel(name = "账期类型")
    private String payType;

    @Excel(name = "收费金额")
    private String opayMoney;

    @Excel(name = "备注")
    private String opayMemo;

    @Excel(name = "状态")
    private Integer opayState;

    private String sysname;
    //添加人
    private Long opayAdduserid;
    //添加时间
    private String opayAddtime;
    //大写
    private String hpayMoneyd;
}
