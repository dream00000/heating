package com.ruoyi.business.controller;

import com.ruoyi.business.domain.*;
import com.ruoyi.business.domain.vo.DetailVO;
import com.ruoyi.business.domain.vo.PayhistoryVO;
import com.ruoyi.business.domain.vo.StationDetailVO;
import com.ruoyi.business.mapper.THmCustomerMapper;
import com.ruoyi.business.mapper.THmDelayMapper;
import com.ruoyi.business.mapper.THmOtherpayhistoryMapper;
import com.ruoyi.business.mapper.THmPayhistoryMapper;
import com.ruoyi.business.service.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.util.ShiroUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 充值记录Controller
 *
 * @author ruoyi
 * @date 2021-08-13
 */
@Controller
@RequestMapping("/business/payhistory")
@Api("充值缴费")
public class THmPayhistoryController extends BaseController {
    private String prefix = "business/payhistory";

    @Resource
    private ITHmPayhistoryService tHmPayhistoryService;

    @Resource
    private ITHmCustomerService tHmCustomerService;

    @Resource
    private ITHmCustomerpaydaysService itHmCustomerpaydaysService;

    @Resource
    private THmPayhistoryMapper tHmPayhistoryMapper;

    @Resource
    private THmDelayMapper tHmDelayMapper;

    @Resource
    private THmCustomerMapper tHmCustomerMapper;

    @Resource
    private ITHmOtherpayhistoryService itHmOtherpayhistoryService;

    @Resource
    private THmOtherpayhistoryMapper tHmOtherpayhistoryMapper;

    @Resource
    private ITHmPayhistoryPriceService itHmPayhistoryPriceService;

    @Resource
    private ITHmPayhistoryRecordService tHmPayhistoryRecordService;

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @GetMapping()
    public String customer() {
        return prefix + "/customer";
    }

    @GetMapping("/customerQian")
    public String customerQian(Model model) {
        Map<String, Object> map = new HashMap<>();
        map.put("type", "qian");
        model.addAllAttributes(map);
        return prefix + "/customer";
    }

    @GetMapping("/payhistorylist")
    public String payhistory() {
        return prefix + "/payhistorylist";
    }

    @GetMapping("/payhistorylists")
    public String payhistorys() {
        return prefix + "/payhistorylists";
    }

    @PostMapping("/listt")
    @ResponseBody
    public TableDataInfo listt(THmCustomer tHmCustomer) {
        startPage();
        List<PayhistoryVO> list = tHmPayhistoryService.selectTHmPayhistoryListt(tHmCustomer);
        return getDataTable(list);
    }

    /**
     * 导出充值记录列表
     */
    @PostMapping("/exportt")
    @ResponseBody
    @Log(title = "导出充值记录", businessType = BusinessType.EXPORT)
    public AjaxResult exportt(THmCustomer tHmCustomer) {
        List<PayhistoryVO> list = tHmPayhistoryService.selectTHmPayhistoryListt(tHmCustomer);
        ExcelUtil<PayhistoryVO> util = new ExcelUtil<>(PayhistoryVO.class);
        return util.exportExcel(list, "用户充值");
    }

    /**
     * 查询用户信息列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(THmCustomer tHmCustomer) {
        startPage();
        List<THmCustomer> list = tHmCustomerService.selectTHmCustomerList(tHmCustomer);
        return getDataTable(list);
    }

    @GetMapping("/{id}/{yj}")
    public String suan1(@PathVariable("id") int id, @PathVariable("yj") String yj, Model model) {
        Map<String, Object> map = new HashMap<>();
        map.put("hcustomerId", id);
        map.put("yj", yj);
        model.addAllAttributes(map);
        return prefix + "/suan1";
    }

    @GetMapping("/{id}")
    public String payhistory(@PathVariable("id") int id, Model model) {
        Map<String, Object> map = new HashMap<>();
        map.put("hcustomerId", id);
        model.addAllAttributes(map);
        return prefix + "/payhistory";
    }

    @GetMapping("/payprint/{hpayid}")
    public String payPrint(@PathVariable("hpayid") int hpayid, Model model) {
        Map<String, Object> map = new HashMap<>();
        THmPayhistory tHmPayhistory = tHmPayhistoryService.selectTHmPayhistoryByIds(hpayid);
        map.put("hpayid", hpayid);
        map.put("tHmPayhistory", tHmPayhistory);
        model.addAllAttributes(map);
        return prefix + "/payprint";
    }

    @GetMapping("/getById/{id}")
    @ResponseBody
    @ApiOperation("打印（充值缴费 根据id查详情）")
    public THmPayhistory getById(@PathVariable("id") int id) {
        return tHmPayhistoryService.selectTHmPayhistoryByIds(id);
    }

    /**
     * 查询用热减免列表
     */
    @PostMapping("/list/{id}")
    @ResponseBody
    public TableDataInfo list(@PathVariable("id") Integer id) {
        THmPayhistory tHmPayhistory = new THmPayhistory();
        tHmPayhistory.setHcustomerId(id);
        List<THmPayhistory> list = tHmPayhistoryService.selectTHmPayhistoryList(tHmPayhistory);
        return getDataTable(list);
    }

    /**
     * 充值明细
     */
    @PostMapping("/list/{startTime}/{endTime}")
    @ResponseBody
    public TableDataInfo list(@PathVariable("startTime") String startTime, @PathVariable("endTime") String endTime) {
        //拼参
        Map<String, Object> map = new HashMap<>();
        map.put("startTime", startTime);
        map.put("endTime", endTime);
        //查询
        THmPayhistory tHmPayhistory = new THmPayhistory();
        tHmPayhistory.setParams(map);
        List<THmPayhistory> list = tHmPayhistoryService.selectTHmPayhistoryList(tHmPayhistory);
        return getDataTable(list);
    }

    /**
     * 导出充值记录列表
     */
    @PostMapping("/export")
    @ResponseBody
    @Log(title = "导出充值记录", businessType = BusinessType.EXPORT)
    public AjaxResult export(THmPayhistory tHmPayhistory) {
        List<THmPayhistory> list = tHmPayhistoryService.selectTHmPayhistoryList(tHmPayhistory);
        ExcelUtil<THmPayhistory> util = new ExcelUtil<THmPayhistory>(THmPayhistory.class);
        return util.exportExcel(list, "payhistory");
    }

    /**
     * 新增充值记录
     */
    @GetMapping("/add/{id}")
    public String add(@PathVariable("id") int id, ModelMap mmap) {
        //此id为用户id
        THmPayhistory tHmPayhistory = tHmPayhistoryService.selectTHmPayhistoryByIdt(id, 1);
        mmap.put("tHmPayhistory", tHmPayhistory);
        return prefix + "/add";
    }

    /**
     * 新增保存充值记录
     */
    @Log(title = "正常充值", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(THmPayhistory tHmPayhistory) {
        if (tHmPayhistory.getIsReadOnly() == 1) {
            return AjaxResult.success("成功", tHmPayhistoryService.fenqi(tHmPayhistory));
        } else {
            return AjaxResult.success("成功", tHmPayhistoryService.insertTHmPayhistory(tHmPayhistory));
        }
    }

    /**
     * 续费充值
     */
    @GetMapping("/xufei/{id}")
    public String xufei(@PathVariable("id") int id, ModelMap mmap) {
        THmPayhistory tHmPayhistory = tHmPayhistoryService.selectTHmPayhistoryById(id, 2);
        mmap.put("tHmPayhistory", tHmPayhistory);
        return prefix + "/xu";
    }

    /**
     * 续费
     */
    @Log(title = "续费充值", businessType = BusinessType.INSERT)
    @PostMapping("/xufei")
    @ResponseBody
    public AjaxResult xufei(THmPayhistory tHmPayhistory) {
        return AjaxResult.success("成功", tHmPayhistoryService.xufei(tHmPayhistory));
    }

    /**
     * 充值作废
     */
    @Log(title = "充值作废", businessType = BusinessType.UPDATE)
    @ResponseBody
    @PostMapping("/fei")
    public AjaxResult fei(Integer hcustomerId, String order) {
        //现根据用户id和时间查到充值记录
        THmPayhistory tHmPayhistory = new THmPayhistory();
        tHmPayhistory.setHcustomerId(hcustomerId);
        tHmPayhistory.setHpayOuttradeno(order);
        List<THmPayhistory> tHmPayhistoryList = tHmPayhistoryService.selectTHmPayhistoryList(tHmPayhistory);
        if (tHmPayhistoryList.size() > 0) {
            for (THmPayhistory hmPayhistory : tHmPayhistoryList) {
                //状态置为1
                hmPayhistory.setHpayState(1);
                tHmPayhistoryService.updateTHmPayhistory(hmPayhistory);
                //账期状态回退
                THmCustomerpaydays tHmCustomerpaydays = itHmCustomerpaydaysService.selectTHmCustomerpaydaysById(hmPayhistory.getWqId());
                if (tHmCustomerpaydays.getWqInt2() != null) {
                    if (tHmCustomerpaydays.getWqInt2() == 5) {
                        tHmCustomerpaydays.setWqInt2(tHmCustomerpaydays.getWqInt1());
                        tHmCustomerpaydays.setWqInt1(5);
                    } else {
                        tHmCustomerpaydays.setWqInt1(tHmCustomerpaydays.getWqInt2());
                    }
                }
                //查这个账期有没有缴费记录
                THmPayhistory tHmPayhistory1 = new THmPayhistory();
                tHmPayhistory1.setWqId(hmPayhistory.getWqId());
                tHmPayhistory1.setHpaySfqe(4);
                List<THmPayhistory> list = tHmPayhistoryMapper.selectTHmPayhistoryListForWeChat(tHmPayhistory1);
                if (!tHmCustomerpaydays.getWqStr6().equals("0.00") || list.size() > 0) {
                    if (hmPayhistory.getHpayStr4() != null) {
                        tHmCustomerpaydays.setWqStr6(new BigDecimal(tHmCustomerpaydays.getWqStr6()).add(new BigDecimal(hmPayhistory.getHpayMoney())).add(new BigDecimal(hmPayhistory.getHpayStr4())).toString());
                    } else {
                        tHmCustomerpaydays.setWqStr6(new BigDecimal(tHmCustomerpaydays.getWqStr6()).add(new BigDecimal(hmPayhistory.getHpayMoney())).toString());
                    }
                }
                itHmCustomerpaydaysService.updateTHmCustomerpaydays(tHmCustomerpaydays);
                //更新用户余额
                THmCustomer tHmCustomer = tHmCustomerService.selectTHmCustomerById(hmPayhistory.getHcustomerId());
                THmCustomer tHmCustomer1 = new THmCustomer();
                tHmCustomer1.setHcustomerInt4(tHmCustomerpaydays.getWqInt1());
                tHmCustomer1.setHcustomerId(tHmCustomerpaydays.getHcustomerId());
                if (hmPayhistory.getHpayStr5() != null) {
                    tHmCustomer1.setBalance(new BigDecimal(tHmCustomer.getBalance()).add(new BigDecimal(hmPayhistory.getHpayStr4())).subtract(new BigDecimal(hmPayhistory.getHpayStr5())).toString());
                } else {
                    tHmCustomer1.setBalance(new BigDecimal(tHmCustomer.getBalance()).add(new BigDecimal(hmPayhistory.getHpayStr4())).toString());
                }
                tHmCustomerService.updateTHmCustomer(tHmCustomer1);

                THmDelay tHmDelay = new THmDelay();
                tHmDelay.setIsGai(0);
                tHmDelay.setWqId(hmPayhistory.getWqId());
                tHmDelayMapper.updateTHmDelay(tHmDelay);
            }

            //更新用户状态 为最后一条账期的状态
            THmCustomerpaydays tHmCustomerpaydays = new THmCustomerpaydays();
            tHmCustomerpaydays.setHcustomerId(hcustomerId);
            List<THmCustomerpaydays> tHmCustomerpaydaysList = itHmCustomerpaydaysService.selectTHmCustomerpaydaysList(tHmCustomerpaydays);
            if (tHmCustomerpaydaysList.size() > 0) {
                THmCustomer tHmCustomer1 = new THmCustomer();
                tHmCustomer1.setHcustomerInt4(tHmCustomerpaydaysList.get(0).getWqInt1());
                tHmCustomer1.setHcustomerId(tHmCustomerpaydays.getHcustomerId());
                tHmCustomer1.setIsZheng(0);
                tHmCustomerService.updateTHmCustomer(tHmCustomer1);
            }
        }
        return AjaxResult.success("充值作废成功");
    }

    /**
     * 续费作废
     */
    @Log(title = "续费作废", businessType = BusinessType.UPDATE)
    @ResponseBody
    @PostMapping("/xfei")
    public AjaxResult xfei(Integer payHistoryId) {
        THmPayhistory tHmPayhistory = tHmPayhistoryService.selectTHmPayhistoryById(payHistoryId);
        if (tHmPayhistory != null) {
            //状态置为1
            tHmPayhistory.setHpayState(1);
            tHmPayhistoryService.updateTHmPayhistory(tHmPayhistory);
            //账期状态回退
            THmCustomerpaydays tHmCustomerpaydays = itHmCustomerpaydaysService.selectTHmCustomerpaydaysById(tHmPayhistory.getWqId());
            tHmCustomerpaydays.setWqInt1(0);
            itHmCustomerpaydaysService.updateTHmCustomerpaydays(tHmCustomerpaydays);
            //更新用户余额
            THmCustomer tHmCustomer = tHmCustomerService.selectTHmCustomerById(tHmPayhistory.getHcustomerId());
            THmCustomer tHmCustomer1 = new THmCustomer();
            tHmCustomer1.setHcustomerInt4(tHmCustomerpaydays.getWqInt1());
            tHmCustomer1.setHcustomerId(tHmCustomerpaydays.getHcustomerId());
            tHmCustomer1.setBalance(new BigDecimal(tHmCustomer.getBalance()).add(new BigDecimal(tHmPayhistory.getHpayStr4())).toString());
            tHmCustomer1.setHcustomerInt4(0);
            tHmCustomer1.setIsZheng(0);
            tHmCustomerService.updateTHmCustomer(tHmCustomer1);
        }
        return AjaxResult.success("续费作废成功");
    }

    /**
     * 销售明细跳转
     */
    @GetMapping("/detail")
    public String detail() {
        return "business/report/detail";
    }

    /**
     * 销售明细
     */
    @ResponseBody
    @PostMapping("/detailList")
    public AjaxResult detail(DetailVO detailVO) {
        return AjaxResult.success("成功", tHmPayhistoryService.detail(detailVO));
    }

    /**
     * 高低热战统计跳转
     */
    @GetMapping("/stationDetail")
    public String stationDetail() {
        return "business/report/stationDetail";
    }

    /**
     * 高低热战统计跳转
     */
    @GetMapping("/stationDetails")
    public String stationDetails() {
        return "business/report/stationDetails";
    }

    /**
     * 高低热战统计跳转
     */
    @GetMapping("/stationDetailss")
    public String stationDetailss() {
        return "business/report/stationDetailss";
    }

    /**
     * 高低热区统计
     */
    @ResponseBody
    @PostMapping("/stationDetailLists")
    public AjaxResult stationDetailLists(StationDetailVO stationDetailVO) {
        return AjaxResult.success("成功", tHmCustomerService.stationDetailLists(stationDetailVO));
    }

    /**
     * 高低热区统计
     */
    @ResponseBody
    @PostMapping("/stationDetailListss")
    public AjaxResult stationDetailListss(StationDetailVO stationDetailVO) {
        return AjaxResult.success("成功", tHmCustomerService.stationDetailListss(stationDetailVO));
    }

    /**
     * 高低热区统计
     */
    @ResponseBody
    @PostMapping("/stationDetailListsss")
    public AjaxResult stationDetailListsss(StationDetailVO stationDetailVO) {
        return AjaxResult.success("成功", tHmCustomerService.stationDetailListsss(stationDetailVO));
    }

    /**
     * 延期明细跳转
     */
    @GetMapping("/yanDetail")
    public String yanDetail() {
        return "business/report/yanDetail";
    }

    /**
     * 销售明细
     */
    @ResponseBody
    @PostMapping("/yanDetailList")
    public AjaxResult yanDetailList(DetailVO detailVO) {
        return AjaxResult.success("成功", tHmPayhistoryService.yanDetail(detailVO));
    }

    /**
     * 调整充值
     */
    @GetMapping("/tiao/{id}")
    public String tiao(@PathVariable("id") int id, ModelMap mmap) {
        THmPayhistory tHmPayhistory = tHmPayhistoryService.selectTHmPayhistoryById(id);
        mmap.put("tHmPayhistory", tHmPayhistory);
        return prefix + "/edit";
    }

    /**
     * 调整充值
     */
    @Log(title = "调整充值", businessType = BusinessType.INSERT)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult edit(THmPayhistory tHmPayhistory) {
        return AjaxResult.success("成功", tHmPayhistoryService.updateTHmPayhistory(tHmPayhistory));
    }

    @GetMapping("/customert")
    public String customert() {
        return prefix + "/customert";
    }

    /**
     * 充值到余额
     */
    @GetMapping("/addBalance/{id}")
    public String addBalance(@PathVariable("id") int id, ModelMap mmap) {
        THmCustomer tHmCustomer = tHmCustomerService.selectTHmCustomerById(id);
        mmap.put("tHmCustomer", tHmCustomer);
        return prefix + "/balance";
    }

    /**
     * 充值到余额
     */
    @Log(title = "充值到余额", businessType = BusinessType.INSERT)
    @PostMapping("/balance")
    @ResponseBody
    public AjaxResult balance(THmCustomer tHmCustomer) {

        THmCustomer hmCustomer = tHmCustomerMapper.selectTHmCustomerById(tHmCustomer.getHcustomerId());
        tHmCustomer.setBalance(new BigDecimal(tHmCustomer.getBalance()).add(new BigDecimal(tHmCustomer.getXBalance())).toString());
        //新增充值记录
        THmOtherpayhistory tHmOtherpayhistory = new THmOtherpayhistory();
        tHmOtherpayhistory.setHcustomerId(hmCustomer.getHcustomerId());
        tHmOtherpayhistory.setHcustomerMobile(hmCustomer.getHcustomerMobile());
        tHmOtherpayhistory.setHcustomerAddress(hmCustomer.getHcustomerDetailaddress());
        tHmOtherpayhistory.setHcustomerName(hmCustomer.getHcustomerName());
        tHmOtherpayhistory.setHcustomerCode(hmCustomer.getHcustomerCode());
        tHmOtherpayhistory.setOpayType("充值到余额");
        tHmOtherpayhistory.setOpayMemo("用户" + hmCustomer.getHcustomerName() + "充值" + tHmCustomer.getXBalance() + "元到余额");
        tHmOtherpayhistory.setHpayType(tHmCustomer.getHpayType());
        tHmOtherpayhistory.setOpayMoney(tHmCustomer.getXBalance());
        tHmOtherpayhistory.setOpayAddtime(sdf.format(new Date()));
        tHmOtherpayhistory.setOpayAdduserid(ShiroUtils.getSysUser().getUserId());
        tHmOtherpayhistory.setOpayState(3);
        tHmOtherpayhistoryMapper.insertTHmOtherpayhistory(tHmOtherpayhistory);

        tHmCustomerService.updateTHmCustomer(tHmCustomer);
        return AjaxResult.success("成功", tHmOtherpayhistory.getOpayId());
    }

    /**
     * 充值到余额记录
     */
    @GetMapping("/balanceList/{id}")
    public String balanceList(@PathVariable("id") int id, Model model) {
        Map<String, Object> map = new HashMap<>();
        map.put("hcustomerId", id);
        model.addAllAttributes(map);
        return prefix + "/balanceList";
    }

    /**
     * 充值到余额记录列表
     */
    @PostMapping("/balanceLists/{id}")
    @ResponseBody
    public TableDataInfo balanceLists(@PathVariable("id") Integer id) {
        List<THmOtherpayhistory> list = itHmOtherpayhistoryService.selectTHmOtherpayhistoryLists(id);
        return getDataTable(list);
    }

    @GetMapping("/payprints/{opayId}")
    public String payPrints(@PathVariable("opayId") int opayId, Model model) {
        Map<String, Object> map = new HashMap<>();
        THmOtherpayhistory tHmOtherpayhistory = itHmOtherpayhistoryService.getForPrint(opayId);
        map.put("tHmPayhistory", tHmOtherpayhistory);
        model.addAllAttributes(map);
        return prefix + "/payprints";
    }

    /**
     * 余额充值作废
     */
    @Log(title = "余额充值作废", businessType = BusinessType.UPDATE)
    @ResponseBody
    @PostMapping("/feis")
    public AjaxResult feis(Integer opayId) {
        //取到充值金额
        THmOtherpayhistory tHmOtherpayhistory = tHmOtherpayhistoryMapper.selectTHmOtherpayhistoryById(opayId);
        //在余额中减掉作废金额
        THmCustomer tHmCustomer = tHmCustomerMapper.selectTHmCustomerById(tHmOtherpayhistory.getHcustomerId());
        tHmCustomer.setBalance(new BigDecimal(tHmCustomer.getBalance()).subtract(new BigDecimal(tHmOtherpayhistory.getOpayMoney())).toString());
        tHmCustomerMapper.updateTHmCustomer(tHmCustomer);
        //作废充值记录
        tHmOtherpayhistory.setOpayState(1);
        return AjaxResult.success("余额充值作废成功", itHmOtherpayhistoryService.updateTHmOtherpayhistory(tHmOtherpayhistory));
    }

    //充值统计
    @GetMapping("/gou")
    public String gou() {
        return prefix + "/gou";
    }

    @PostMapping("/gouList")
    @ResponseBody
    public TableDataInfo gouList(THmPayhistoryPrice tHmPayhistoryPrice) {
        startPage();
        List<THmPayhistoryPrice> list = itHmPayhistoryPriceService.gou(tHmPayhistoryPrice);
        return getDataTable(list);
    }

    /**
     * 导出充值记录列表
     */
    @PostMapping("/gouExport")
    @ResponseBody
    @Log(title = "导出充值构成记录", businessType = BusinessType.EXPORT)
    public AjaxResult gouExport(THmPayhistoryPrice tHmPayhistoryPrice) {
        List<THmPayhistoryPrice> list = itHmPayhistoryPriceService.gou(tHmPayhistoryPrice);
        ExcelUtil<THmPayhistoryPrice> util = new ExcelUtil<>(THmPayhistoryPrice.class);
        return util.exportExcel(list, "充值构成");
    }

    @GetMapping("/record/{id}")
    public String record(@PathVariable("id") int id, ModelMap mmap) {
        mmap.put("hpayId", id);
        return prefix + "/record";
    }

    /**
     * 查询充值调整日志列表
     */
    @PostMapping("/recordList")
    @ResponseBody
    public TableDataInfo list(THmPayhistoryRecord tHmPayhistoryRecord) {
        startPage();
        List<THmPayhistoryRecord> list = tHmPayhistoryRecordService.selectTHmPayhistoryRecordList(tHmPayhistoryRecord);
        return getDataTable(list);
    }
}
