package com.ruoyi.business.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.List;

/**
 * 用户信息对象 t_hm_customer
 *
 * @author ruoyi
 * @date 2021-08-13
 */
@Data
public class THmCustomer extends BaseEntity {

    private Integer hcustomerId;

    //所属分区
    private Integer hareaId;

    @Excel(name = "所属分区序号", type = Excel.Type.IMPORT)
    private String hareaCode;

    @Excel(name = "所属分区", type = Excel.Type.EXPORT)
    private String hareaName;

    private String hstationName;

    @Excel(name = "用户类型：1=散户,2=产权,3=补贴,4=单位", width = 55, readConverterExp = "1=散户,2=产权,3=补贴,4=单位")
    private String hcustomerType;

    @Excel(name = "用户编号", type = Excel.Type.EXPORT)
    private String hcustomerCode;

    @Excel(name = "用户姓名")
    private String hcustomerName;

    @Excel(name = "手机号码")
    private String hcustomerMobile;

    @Excel(name = "身份证号")
    private String hcustomerPersonlcard;

    @Excel(name = "常住人口")
    private Integer hcustomerPersoncount;

    @Excel(name = "详细地址")
    private String hcustomerDetailaddress;

    @Excel(name = "开户状态：0=否,1=是", width = 30, readConverterExp = "0=否,1=是", type = Excel.Type.EXPORT)
    private Integer hcustomerKhstate;

    @Excel(name = "开户日期", type = Excel.Type.EXPORT)
    private String hcustomerKhtime;

    private Long hcustomerKhuserid;

    private String hcustomerGzje;

    @Excel(name = "用户余额", type = Excel.Type.EXPORT)
    private String balance;
    private String xBalance;

    @Excel(name = "高低热区：0=高,1=低", width = 30, type = Excel.Type.IMPORT)
    private Integer isRe;

    @Excel(name = "房屋面积")
    private String hcustomerHomearea;

    @Excel(name = "超高面积")
    private String hcustomerAbovearea;

    @Excel(name = "停供面积", type = Excel.Type.EXPORT)
    private String hcustomerTgarea;

    @Excel(name = "实供面积", type = Excel.Type.EXPORT)
    private String hcustomerActualarea;

    /**
     * 合同附件组code
     */
    private String hcustomerContactcode;

    /**
     * 是否删除0否1是
     */
    private Integer hcustomerState;

    /**
     * 添加人
     */
    private Long hcustomerAdduserid;

    @Excel(name = "添加时间", type = Excel.Type.EXPORT)
    private String hcustomerAddtime;

    //是否有变更账单
    private Integer hcustomerInt3;

    /**
     * 最后一次账期的状态  0--》20%  1--》全额 2--》续费 3--》延期 4--》平账 5--》变更 99 --》欠费
     */
    private Integer hcustomerInt4;

    //是否为大客户 0否1是
    private Integer isDa;

    //导入字段
    @Excel(name = "面积", type = Excel.Type.IMPORT)
    private String areas;
    @Excel(name = "单价编号", type = Excel.Type.IMPORT)
    private String priceIds;
    @Excel(name = "类型：2=实供,3=停供", width = 30, type = Excel.Type.IMPORT)
    private String types;
    @Excel(name = "系数：（%）", type = Excel.Type.IMPORT)
    private String xishus;
    @Excel(name = "阀门状态：0=阀开,1阀关,2=断管", width = 30, type = Excel.Type.IMPORT)
    private Integer tap;
    @Excel(name = "是否立即生成账期：0=否,1=是", width = 30, type = Excel.Type.IMPORT)
    private Integer isInit;
    @Excel(name = "备注")
    private String remark;

    //是否欠费
    private Integer isQian;
    //是否允许充值 0否1是
    private Integer isChong;
    //是否允许生成 0否1是
    private Integer isSheng;
    //用户缴费状态 0是欠费 1是正常
    private Integer isZheng;

    //导入验重说明
    private String dataExplain;

    private List<THmCustomerprice> customerpriceList;//单价列表
    private List<THmEnclosure> enclosureList;//附件列表
    private List<THmCustomerpaydays> customerpaydaysList;//用户所有账期
    private List<THmPayhistory> payhistoryList;//充值记录
    private List<THmFeedback> feedbackList;//开关阀记录

    private String lou;//楼号
    private String dan;//单元号
    private String ceng;//层数
    private String hao;//房间号

    private Integer endTop;//最后一次阀门操作
    private String endTopTime;//最后一次阀门操作时间

    //阀门状态0开1关  新版只用于显示阀门状态
    private Integer hcustomerTapstate;
    //是否有余额的查询条件
    private String hcustomerStr4;
    private String hcustomerStr5;
    //阀控查询条件
    private Integer fdFmzt;
    private Integer fdType;
    //账期支付类型查询条件
    private Integer wqInt1;
    //账期年度查询
    private String hzqGrstarttime;
    //缴费类型查询
    private Integer hpaySfqe;
    //支付方式
    private Integer hpayType;
    //更模块操作员查询
    private Integer sysUserId;
    private String sysUserName;
    //稽查查询条件
    private String fdAdduserName;
    private String stTime;
    private String enTime;
    //应缴金额，用户开户或者变更面积前端传给后台
    private String total;
    //查询单价类型
    private String priceId;
    //用户的微信绑定信息
    private String openId;
    private String nickName;
    private String headimgurl;
    private Integer bindId;
    //用户退费
    private Integer tfType;
    //账期生成需要的
    //策略id
    private Integer hzqId;
    //零钱计算方式
    private Integer ling;

    //供热天数
    private Integer hzqGrdays;
    //费用类型 0基础热 1全热 5复合
    private Integer payType;
    //单价名称
    private String djName;
    //单价面积
    private String djArea;
    //单价Id
    private Integer pricesId;
    //待阀控 0全部 1是待开阀 2待关阀 3是待接管
    private Integer dType;
    //最后一次账期信息
    private THmCustomerpaydays lastPayDays;
    //缴费状态  1已缴 2未缴
    // 0对应int1的 0 1 2 6
    // 1对应int1的 3 5 6 99
    private Integer jiaoType;
    //其他欠费总金额
    private String otherTotal;
    //阀控时间
    private String topTime;
}
