package com.ruoyi.business.service.impl;

import com.ruoyi.business.domain.THmPayhistoryPrice;
import com.ruoyi.business.mapper.THmPayhistoryPriceMapper;
import com.ruoyi.business.service.ITHmPayhistoryPriceService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 充值-单价Service业务层处理
 *
 * @author ruoyi
 * @date 2022-01-10
 */
@Service
public class THmPayhistoryPriceServiceImpl implements ITHmPayhistoryPriceService {

    @Resource
    private THmPayhistoryPriceMapper tHmPayhistoryPriceMapper;

    /**
     * 查询充值-单价
     *
     * @param id
     * @return 充值-单价
     */
    @Override
    public THmPayhistoryPrice selectTHmPayhistoryPriceById(int id) {
        return tHmPayhistoryPriceMapper.selectTHmPayhistoryPriceById(id);
    }

    /**
     * 查询充值-单价列表
     *
     * @param tHmPayhistoryPrice 充值-单价
     * @return 充值-单价
     */
    @Override
    public List<THmPayhistoryPrice> selectTHmPayhistoryPriceList(THmPayhistoryPrice tHmPayhistoryPrice) {
        return tHmPayhistoryPriceMapper.selectTHmPayhistoryPriceList(tHmPayhistoryPrice);
    }

    /**
     * 新增充值-单价
     *
     * @param tHmPayhistoryPrice 充值-单价
     * @return 结果
     */
    @Override
    public int insertTHmPayhistoryPrice(THmPayhistoryPrice tHmPayhistoryPrice) {
        return tHmPayhistoryPriceMapper.insertTHmPayhistoryPrice(tHmPayhistoryPrice);
    }

    /**
     * 修改充值-单价
     *
     * @param tHmPayhistoryPrice 充值-单价
     * @return 结果
     */
    @Override
    public int updateTHmPayhistoryPrice(THmPayhistoryPrice tHmPayhistoryPrice) {
        return tHmPayhistoryPriceMapper.updateTHmPayhistoryPrice(tHmPayhistoryPrice);
    }

    @Override
    public List<THmPayhistoryPrice> gou(THmPayhistoryPrice tHmPayhistoryPrice) {
        if (tHmPayhistoryPrice.getNian() != null && !tHmPayhistoryPrice.getNian().equals("")) {
            tHmPayhistoryPrice.setNian(tHmPayhistoryPrice.getNian().split("-")[0]);
        }
        List<THmPayhistoryPrice> list = tHmPayhistoryPriceMapper.gou(tHmPayhistoryPrice);
        if (list.size() > 0) {
            for (THmPayhistoryPrice hmPayhistoryPrice : list) {
                hmPayhistoryPrice.setNian(hmPayhistoryPrice.getNian() + "-" + (Integer.parseInt(hmPayhistoryPrice.getNian()) + 1));
            }
        }
        return list;
    }
}
