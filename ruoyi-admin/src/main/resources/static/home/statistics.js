$(document).ready(function(){
    getAllHeatNum();
})

//查询头部汇总统计
function getAllHeatNum() {
    $.ajax({
        url: "/business/report/indexStatistics",
        type:"post",
        data:{},
        success:function(res){
            //console.log(res);
            if(res.code == 0){
                var result = res.data;
                $(".text-yhzs").html(isData(result.customerCount));
                $(".text-qfyh").html(isData(result.qianCount));
                $(".text-dkf").html(isData(result.kcount));
                $(".text-dgf").html(isData(result.gcount));
                $(".text-ljsk").html(isData(result.shouSum));
                $(".text-ljqf").html(isData(result.qianSum));
                $(".text-ljyh").html(isData(result.youSum));
                $(".text-ljtf").html(isData(result.tuiSum));
            }
        }
    })
}
function isData(data){
    return data == null ? 0 : data;
}