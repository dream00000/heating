package com.ruoyi.business.domain.vo;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.List;

@Data
public class DetailVO extends BaseEntity {

    private String payTotal;
    private String opayTotal;
    private String tfpayTotal;

    private List<DetailListVO> list;

    //用户分区
    private Integer areaid;
    //用户编号
    private String code;
    //用户姓名
    private String name;
    //用户电话
    private String phone;
    //用户地址
    private String address;
    //用户开关阀
    private String tap;
    //支付类型
    private String type;
    //支付方式
    private String paytype;
    //交易时间
    private String addTime;
    //操作人员
    private Long adduserid;
}
