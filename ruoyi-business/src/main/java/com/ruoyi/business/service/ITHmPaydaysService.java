package com.ruoyi.business.service;

import com.ruoyi.business.domain.THmPaydays;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 分区账期Service接口
 */
public interface ITHmPaydaysService {
    /**
     * 查询分区账期
     */
    public THmPaydays selectTHmPaydaysById(@Param("id") int id);

    /**
     * 查询分区账期列表
     */
    public List<THmPaydays> selectTHmPaydaysList(THmPaydays tHmPaydays);

    /**
     * 新增分区账期
     */
    public int insertTHmPaydays(THmPaydays tHmPaydays);

    /**
     * 修改分区账期
     */
    public int updateTHmPaydays(THmPaydays tHmPaydays);
}
