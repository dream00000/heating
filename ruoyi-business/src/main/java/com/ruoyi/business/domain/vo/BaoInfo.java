package com.ruoyi.business.domain.vo;

import lombok.Data;

import java.util.List;

@Data
public class BaoInfo {

    private String success;
    private String message;
    private String object;
    private String obj;
    private String msg;
    private List<BaoData> data;
}
