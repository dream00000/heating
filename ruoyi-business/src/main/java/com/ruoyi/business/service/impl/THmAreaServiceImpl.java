package com.ruoyi.business.service.impl;

import com.ruoyi.business.domain.THmArea;
import com.ruoyi.business.domain.THmStation;
import com.ruoyi.business.mapper.THmAreaMapper;
import com.ruoyi.business.mapper.THmStationMapper;
import com.ruoyi.business.service.ITHmAreaService;
import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.framework.util.ShiroUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 供热分区Service业务层处理
 *
 * @author ruoyi
 * @date 2021-08-13
 */
@Service("AreaService")
public class THmAreaServiceImpl implements ITHmAreaService {

    @Resource
    private THmAreaMapper tHmAreaMapper;

    @Resource
    private THmStationMapper tHmStationMapper;

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * 查询供热分区
     *
     * @param id
     * @return 供热分区
     */
    @Override
    public THmArea selectTHmAreaById(int id) {
        THmArea hmArea = tHmAreaMapper.selectTHmAreaById(id);
        if (hmArea.getHstationId() != null) {
            THmStation tHmStation = tHmStationMapper.selectTHmStationById(hmArea.getHstationId());
            if (tHmStation != null) {
                hmArea.setHstationName(tHmStation.getHstationName());
            }
        }
        return hmArea;
    }

    /**
     * 查询供热分区列表
     *
     * @param tHmArea 供热分区
     * @return 供热分区
     */
    @Override
    public List<THmArea> selectTHmAreaList(THmArea tHmArea) {
        return tHmAreaMapper.selectTHmAreaList(tHmArea);
    }

    @Override
    public List<THmArea> selectTHmAreaListt(THmArea tHmArea) {

        if (tHmArea.getHareaId() != null && tHmArea.getHareaCode().equals("")) {
            //获取这个名称及其下级的信息
            return tHmAreaMapper.getLevel23(tHmArea.getHareaId());
        } else {
            return tHmAreaMapper.selectTHmAreaListt(tHmArea);
        }
    }

    @Override
    public THmArea getMaxCode(THmArea tHmArea) {
        return tHmAreaMapper.getMaxCode(tHmArea);
    }

    @Override
    public THmArea getAreaByCode(String code) {
        return tHmAreaMapper.getAreaByCode(code);
    }

    /**
     * 新增供热分区
     *
     * @param tHmArea 供热分区
     * @return 结果
     */
    @Override
    public int insertTHmArea(THmArea tHmArea) {
        THmArea tHmArea1 = tHmAreaMapper.selectTHmAreaById(tHmArea.getHareaParentid());
        tHmArea.setParentName(tHmArea1.getHareaName());
        tHmArea.setHareaAdduserid(ShiroUtils.getSysUser().getUserId());
        tHmArea.setHareaAddtime(sdf.format(new Date()));
        tHmArea.setHareaState(0);
        //如果新增的是第三层 就查一下第二层有没有绑账期策略，如果绑了就绑一下
        //如果新增的是第三层 就查一下第二层有没有绑热站，如果绑了就帮一下
        if (tHmArea.getHareaLevel() == 3) {
            if (tHmArea1.getPaydaysId() != null) {
                tHmArea.setPaydaysId(tHmArea1.getPaydaysId());
            }
            if (tHmArea1.getHstationId() != null) {
                tHmArea.setHstationId(tHmArea1.getHstationId());
            }
        }
        tHmAreaMapper.insertTHmArea(tHmArea);

        //如果创建的是第二层 批量创建第三层
        String code = tHmArea.getHareaCode();
        int i = 1;
        if (tHmArea.getHareaLevel() == 2) {
            if (tHmArea.getAreaName() != null && !tHmArea.getAreaName().equals("")) {
                String[] areaNames = tHmArea.getAreaName().split(",");
                for (String areaName : areaNames) {
                    //创建第三层
                    THmArea tHmArea2 = new THmArea();
                    tHmArea2.setParentName(tHmArea.getHareaName());
                    tHmArea2.setHareaAdduserid(ShiroUtils.getSysUser().getUserId());
                    tHmArea2.setHareaAddtime(sdf.format(new Date()));
                    tHmArea2.setHareaState(0);
                    tHmArea2.setHareaName(areaName);
                    tHmArea2.setHareaLevel(3);
                    tHmArea2.setHareaParentid(tHmArea.getHareaId());
                    tHmArea2.setHareaOrderid(i);
                    tHmArea2.setHstationId(tHmArea.getHstationId());
                    //编号
                    if (i > 9 && i <= 99) {
                        tHmArea2.setHareaCode(code + "0" + i);
                    } else if (i > 99) {
                        tHmArea2.setHareaCode(code + i);
                    } else {
                        tHmArea2.setHareaCode(code + "00" + i);
                    }
                    tHmAreaMapper.insertTHmArea(tHmArea2);
                    i++;
                }
            }
        }
        return 1;
    }

    /**
     * 修改供热分区
     *
     * @param tHmArea 供热分区
     * @return 结果
     */
    @Override
    public int updateTHmArea(THmArea tHmArea) {
        //只有第二层能修改热站，修改的时候把第三层一块改了
        if (tHmArea.getHareaLevel() != null && tHmArea.getHareaLevel() == 2) {
            THmArea tHmArea1 = new THmArea();
            tHmArea1.setHareaParentid(tHmArea.getHareaId());
            List<THmArea> tHmAreaList = tHmAreaMapper.selectTHmAreaList(tHmArea1);
            for (THmArea hmArea : tHmAreaList) {
                hmArea.setHstationId(tHmArea.getHstationId());
                tHmAreaMapper.updateTHmArea(hmArea);
            }
        }
        return tHmAreaMapper.updateTHmArea(tHmArea);
    }

    /**
     * 设置账期
     *
     * @param tHmArea 供热分区
     * @return 结果
     */
    @Override
    public int setting(THmArea tHmArea) {
        return tHmAreaMapper.setting(tHmArea);
    }


    /**
     * 查询供热分区树列表
     *
     * @return 所有供热分区信息
     */
    @Override
    public List<Ztree> selectTHmAreaTree() {
        List<THmArea> tHmAreaList = tHmAreaMapper.selectTHmAreaLists(new THmArea());
        List<Ztree> ztrees = new ArrayList<>();
        for (THmArea tHmArea : tHmAreaList) {
            Ztree ztree = new Ztree();
            ztree.setId((long) tHmArea.getHareaId());
            ztree.setpId((long) tHmArea.getHareaParentid());
            ztree.setName(tHmArea.getHareaName());
            ztree.setTitle(tHmArea.getHareaName());
            ztree.setHareaLevel(tHmArea.getHareaLevel());
            ztree.setPayDaysId(tHmArea.getPaydaysId());
            ztrees.add(ztree);
        }
        return ztrees;
    }

    @Override
    public List<THmArea> getLevel2() {
        return tHmAreaMapper.getLevel2();
    }
}
