package com.ruoyi.business.mapper;


import com.ruoyi.business.domain.WechatUser;

import java.util.List;

/**
 * 微信用户Mapper接口
 */
public interface WechatUserMapper {

    /**
     * 查询微信用户
     */
    public WechatUser selectWechatUserById(Long wechatId);

    public WechatUser getWechatUser(String openId);

    /**
     * 查询微信用户列表
     */
    public List<WechatUser> selectWechatUserList(WechatUser wechatUser);

    public List<WechatUser> selectWechatUserListByCustomerId(String customerId);

    /**
     * 新增微信用户
     */
    public int insertWechatUser(WechatUser wechatUser);

    /**
     * 修改微信用户
     */
    public int updateWechatUser(WechatUser wechatUser);

    /**
     * 删除微信用户
     */
    public int deleteWechatUserById(Long wechatId);
}
