package com.ruoyi.business.controller;

import com.ruoyi.business.domain.THmCustomer;
import com.ruoyi.business.domain.THmDelay;
import com.ruoyi.business.service.ITHmCustomerService;
import com.ruoyi.business.service.ITHmDelayService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 延期记录Controller
 *
 * @author ruoyi
 * @date 2022-03-10
 */
@Controller
@RequestMapping("/business/delay")
public class THmDelayController extends BaseController {
    private String prefix = "business/delay";

    @Resource
    private ITHmDelayService tHmDelayService;

    @Resource
    private ITHmCustomerService tHmCustomerService;

    @GetMapping()
    public String delay() {
        return prefix + "/delay";
    }

    /**
     * 查询延期记录列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(THmDelay tHmDelay) {
        startPage();
        List<THmDelay> list = tHmDelayService.selectTHmDelayList(tHmDelay);
        return getDataTable(list);
    }

    /**
     * 导出延期记录列表
     */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(THmDelay tHmDelay) {
        List<THmDelay> list = tHmDelayService.selectTHmDelayList(tHmDelay);
        ExcelUtil<THmDelay> util = new ExcelUtil<THmDelay>(THmDelay.class);
        return util.exportExcel(list, "delay");
    }

    /**
     * 新增延期记录
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存延期记录
     */
    @Log(title = "延期记录", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(THmDelay tHmDelay) {
        return toAjax(tHmDelayService.insertTHmDelay(tHmDelay));
    }

    /**
     * 修改延期记录
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") int id, ModelMap mmap) {
        THmDelay tHmDelay = tHmDelayService.selectTHmDelayById(id);
        mmap.put("tHmDelay", tHmDelay);
        return prefix + "/edit";
    }

    /**
     * 修改保存延期记录
     */
    @Log(title = "延期记录", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(THmDelay tHmDelay) {
        return toAjax(tHmDelayService.updateTHmDelay(tHmDelay));
    }

    /**
     * 修改用户信息
     */
    @GetMapping("/bian/{id}")
    public String bian(@PathVariable("id") int id, ModelMap mmap) {
        THmCustomer tHmCustomer = tHmCustomerService.selectTHmCustomerById(id);
        mmap.put("tHmCustomer", tHmCustomer);
        return prefix + "/bian";
    }

    /**
     * 作废账期
     */
    @GetMapping("/zuo/{id}")
    @ResponseBody
    @Log(title = "作废延期记录", businessType = BusinessType.DELETE)
    public AjaxResult zuo(@PathVariable("id") Integer id) {
        tHmDelayService.deleteTHmDelay(id);
        return AjaxResult.success("作废延期记录成功");
    }
}
