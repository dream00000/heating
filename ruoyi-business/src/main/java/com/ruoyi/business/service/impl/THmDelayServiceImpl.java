package com.ruoyi.business.service.impl;

import com.ruoyi.business.domain.THmArea;
import com.ruoyi.business.domain.THmCustomerpaydays;
import com.ruoyi.business.domain.THmDelay;
import com.ruoyi.business.mapper.THmCustomerpaydaysMapper;
import com.ruoyi.business.mapper.THmDelayMapper;
import com.ruoyi.business.service.ITHmAreaService;
import com.ruoyi.business.service.ITHmDelayService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

/**
 * 延期记录Service业务层处理
 *
 * @author ruoyi
 * @date 2022-03-10
 */
@Service
public class THmDelayServiceImpl implements ITHmDelayService {
    @Resource
    private THmDelayMapper tHmDelayMapper;

    @Resource
    private ITHmAreaService itHmAreaService;

    @Resource
    private THmCustomerpaydaysMapper tHmCustomerpaydaysMapper;

    /**
     * 查询延期记录
     *
     * @param id
     * @return 延期记录
     */
    @Override
    public THmDelay selectTHmDelayById(int id) {
        return tHmDelayMapper.selectTHmDelayById(id);
    }

    /**
     * 查询延期记录列表
     *
     * @param tHmDelay 延期记录
     * @return 延期记录
     */
    @Override
    public List<THmDelay> selectTHmDelayList(THmDelay tHmDelay) {
        List<THmDelay> list = tHmDelayMapper.selectTHmDelayList(tHmDelay);
        if (list.size() > 0) {
            for (THmDelay hmDelay : list) {
                THmArea tHmArea = itHmAreaService.selectTHmAreaById(hmDelay.getHareaId());
                if (tHmArea != null) {
                    if (tHmArea.getHareaLevel() == 2) {
                        hmDelay.setCustomerArea(tHmArea.getHareaName());
                    } else {
                        THmArea tHmArea1 = itHmAreaService.selectTHmAreaById(tHmArea.getHareaParentid());
                        hmDelay.setCustomerArea(tHmArea1.getHareaName());
                    }
                }
            }
        }
        return list;
    }

    /**
     * 新增延期记录
     *
     * @param tHmDelay 延期记录
     * @return 结果
     */
    @Override
    public int insertTHmDelay(THmDelay tHmDelay) {
        return tHmDelayMapper.insertTHmDelay(tHmDelay);
    }

    /**
     * 修改延期记录
     *
     * @param tHmDelay 延期记录
     * @return 结果
     */
    @Override
    public int updateTHmDelay(THmDelay tHmDelay) {

        //查到原来的金额，在账期里减掉，再加上新的
        THmDelay hmDelay = tHmDelayMapper.selectTHmDelayById(tHmDelay.getId());
        THmCustomerpaydays tHmCustomerpaydays = tHmCustomerpaydaysMapper.selectTHmCustomerpaydaysById(tHmDelay.getWqId());
        BigDecimal bd = new BigDecimal(hmDelay.getTotal());
        THmCustomerpaydays tHmCustomerpaydays1 = new THmCustomerpaydays();
        tHmCustomerpaydays1.setWqStr4(new BigDecimal(tHmCustomerpaydays.getWqStr4()).subtract(bd).add(new BigDecimal(tHmDelay.getTotal())).toString());
        tHmCustomerpaydays1.setWqId(tHmCustomerpaydays.getWqId());
        tHmCustomerpaydaysMapper.updateTHmCustomerpaydays(tHmCustomerpaydays1);

        return tHmDelayMapper.updateTHmDelay(tHmDelay);
    }

    @Override
    public void deleteTHmDelay(int id) {
        //更新账期中的延期金额
        THmDelay tHmDelay = tHmDelayMapper.selectTHmDelayById(id);
        THmCustomerpaydays tHmCustomerpaydays = tHmCustomerpaydaysMapper.selectTHmCustomerpaydaysById(tHmDelay.getWqId());
        if (tHmCustomerpaydays != null) {
            tHmCustomerpaydays.setWqStr4(new BigDecimal(tHmCustomerpaydays.getWqStr4()).subtract(new BigDecimal(tHmDelay.getTotal())).toString());
        }
        tHmCustomerpaydaysMapper.updateTHmCustomerpaydays(tHmCustomerpaydays);

        tHmDelayMapper.deleteTHmDelay(id);
    }

}
