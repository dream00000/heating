package com.ruoyi.business.domain.vo;

import lombok.Data;

@Data
public class OrderKeFu {
    //id
    private Integer customerId;
    //年度
    private String year;
    //缴费状态
    private String payStatus;
    //用热状态
    private String tapStatus;
    //应交
    private String yTotal;
    //已交
    private String jTotal;
    //欠交
    private String qTotal;
    //缴费日期
    private String payTime;
}
