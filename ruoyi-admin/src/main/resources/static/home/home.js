/*首页*/

var colorArr = ["#78BBFF","#FF8F9C","#B793FF","#55EECF","#FFBE60","#B793FF","#55EECF","#FFBE60","#B793FF","#55EECF","#FFBE60"];
var zzoption = {
    tooltip: {
        trigger: 'axis',
        formatter: '{b0}<br />{a0}: {c0}㎡',
        axisPointer: {            // 坐标轴指示器，坐标轴触发有效
            type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
    },
    grid: { x: 90, y: 30, x2: 40, y2: 40 },
    xAxis: [
        {
            name:"热站",
            type: 'category',
            data: [],
            nameTextStyle:{
                color:"#ccc"
            },
            axisLabel: {
                color:"#666666"
            },
            axisTick: {
                alignWithLabel: true,
                show:false
            },
            axisLine:{
                lineStyle:{
                    color:"#EBEBEB"
                }
            }

        }
    ],
    yAxis: [
        {
            type: 'value',
            nameTextStyle:{
                color:"#ccc"
            },
            axisLabel: {
                formatter: '{value} ',
                color:"#666666"
            },
            axisTick: {
                alignWithLabel: true,
                show:false
            },
            axisLine:{
                lineStyle:{
                    color:"#EBEBEB"
                }
            }
        }
    ],
    series: [
        {
            name: '面积',
            type: 'bar',
            barWidth: '20',
            data: [],
            itemStyle: {
                normal:{
                    barBorderRadius:20,
                    color: function(params){
                        return colorArr[params.dataIndex];
                        // 取每条数据的 index 对应 colors 中的 index 返回这个颜色
                    }
                }
            },
        }
    ]
};
//var zzChart = echarts.init(document.getElementById('mainDay'),"macarons");
// //模拟数据
// var titleArr = ['1','2','3','4','5','6','7','8','9','10','11','12'];
// var dataShadow = [100,200,455,323,123,456,657,345,676,345,134,867];
// zzoption.xAxis[0].data = titleArr;
// zzoption.series[0].data = dataShadow;
// zzChart.setOption(zzoption);


var colorArrPie = ["#78BBFF","#FF8F9C","#B793FF","#55EECF","#FFBE60","#B793FF","#55EECF","#FFBE60","#B793FF","#55EECF","#FFBE60"];
var zzPieoption = {
    tooltip: {
        trigger: 'item'
    },
    legend: {
        top: '5%',
        left: 'center'
    },
    series: [
        {
            name: '收费占比',
            type: 'pie',
            center:['50%', '53%'],
            radius: ['40%', '70%'],
            avoidLabelOverlap: false,
            itemStyle: {
                borderRadius: 10,
                borderColor: '#fff',
                borderWidth: 2
            },
            label: {
                show: true,
                position: 'inner'
            },
            emphasis: {
                label: {
                    show: true,
                    fontSize: '40',
                    fontWeight: 'bold'
                }
            },
            labelLine: {
                show: false
            },
            data: []
        }
    ]
};
//var zzPieChart = echarts.init(document.getElementById('mainMoney'),"macarons");

$(document).ready(function(){
    getAuth();
    getAllHeatNum();
    //getAllHistoryList("充值记录");
    //getAllHistoryList("退费记录");
    //getAllHistoryList("优惠记录");
    //getAllHistoryList("稽查记录");
    //getHeatDayNum();
    //getPaytypeNum();

    $(document).on("click",".tab-box li",function (){
        var url = $(this).data("url");
        var name = $(this).data("name");
        var auth = $(this).data("auth");
        if(auth == "1"){
            $.modal.openTab(name, url);
        } else {
            $.modal.alertWarning("无访问权限");
        }
    })
})
function getAuth(){
    $.ajax({
        url: "/business/customer/indexList",
        type:"get",
        data:{},
        success:function(res){
            if(res.code == 0){
                var result = res.data;
                $(".tab-box li").each(function(){
                    var name = $(this).data("name");
                    var that = $(this);
                    result.forEach((item)=>{
                        if(item.menuName == name) {
                            that.data("auth","1");
                        }
                    })
                })
            }
        }
    })
}
//查询头部汇总统计
function getAllHeatNum() {
    $.modal.loading("加载中...")
    $.ajax({
        url: "/business/report/indexStatistics",
        type:"post",
        data:{},
        success:function(res){
            $.modal.closeLoading();
            if(res.code == 0){
                var result = res.data;
                $(".text-yhzs").html(isData(result.customerCount));
                $(".text-qfyh").html(isData(result.qianCount));
                $(".text-dkf").html(isData(result.kcount));
                $(".text-dgf").html(isData(result.gcount));
                $(".text-ljsk").html(isData(result.shouSum));
                $(".text-ljqf").html(isData(result.qianSum));
                $(".text-ljyh").html(isData(result.youSum));
                $(".text-ljtf").html(isData(result.tuiSum));
            }
        }
    })
}
function isData(data){
    return data == null ? 0 : data;
}
function isDataEmpty(data){
    return data == null ? "" : data;
}
// 查询记录信息
function getAllHistoryList(type) {
    var url = "",num = 0;
    if(type == "充值记录"){
        url = "/business/report/indexRecharge";
    } else if(type == "退费记录"){
        url ="/business/report/indexRefund";
        num = 1;
    } else if(type == "优惠记录"){
        url = "/business/report/indexDiscount";
        num = 2;
    } else if(type == "稽查记录"){
        url = "/business/report/indexCheck";
        num = 3;
    }
    $.ajax({
        url: url,
        type:"post",
        data:{
            pageSize:15,
            pageNum:1
        },
        success:function(res){
            if(res.code == 0){
                var list = res.data;
                var str = "";
                for(var i=0;i<list.length;i++) {
                    str+="<li><span>"+ isDataEmpty(list[i].addTime) +"</span><span>"+ isDataEmpty(list[i].context) +"</span></li>";
                }
                $(".home-top-right .home-top-right-item").eq(num).find(".heng-list").html(str);
            } else {
                $(".home-top-right .home-top-right-item").eq(num).find(".heng-list").html("");
            }
        }
    })
}
// 热站面积统计
function getHeatDayNum() {
    $.ajax({
        url: "/business/payhistory/stationDetailLists",
        type:"post",
        data:{
            stTime: "2022",
            endTime: "2023",
            stationId: ""
        },
        success: function(res){
            if(res.code == 0) {
                var result = res.data;
                var titleArr = [];
                var dataShadow = [];
                if(result.length>0){
                    result = result.sort(function(a,b){
                        return parseFloat(b.zzong)-parseFloat(a.zzong);
                    })
                }
                var length = result.length > 6?6:result.length;
                for (var i=0; i < length; i++) {
                    titleArr.push(result[i].stationName?(result[i].stationName.replace('换热站','\n换热站')):"");
                    dataShadow.push(result[i].zzong);
                }
                zzoption.xAxis[0].data = titleArr;
                zzoption.series[0].data = dataShadow;
                zzChart.setOption(zzoption);
            } else {
                zzoption.xAxis[0].data = [];
                zzoption.series[0].data = [];
                zzChart.setOption(zzoption);
            }
        }
    })
}
// 各单价类型收费占比
function getPaytypeNum() {
    $.ajax({
        url: "/business/report/indexProportion",
        type:"post",
        data:{
            hareaId:"",
            hcustomerCode:"",
            hcustomerName:"",
            hcustomerMobile:""
        },
        success:function(res){
            if(res.code == 0){
                var result = res.data;
                var dataShadow = [];
                for (var i=0; i < result.length; i++) {
                    dataShadow.push( { value: result[i].count, name:result[i].type });
                }
                zzPieoption.series[0].data = dataShadow;
                zzPieChart.setOption(zzPieoption);
            } else {
                zzPieoption.series[0].data = [];
                zzPieChart.setOption(zzPieoption);
            }
        }
    })
}

// $.modal.openTab("修改" + $.table._option.modalName, $.operate.editUrl(id));