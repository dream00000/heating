package com.ruoyi.business.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * 充值调整日志对象 t_hm_payhistory_record
 */
@Data
public class THmPayhistoryRecord extends BaseEntity {


    private Integer id;

    @Excel(name = "账期id")
    private Integer hpayId;

    @Excel(name = "操作人")
    private Long addUser;
    private String addUserName;

    @Excel(name = "操作时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date addTime;

    @Excel(name = "变更前支付金额")
    private String qianZtotal;

    @Excel(name = "变更后支付金额")
    private String houZtotal;

    @Excel(name = "0正常1删除")
    private Integer status;

    @Excel(name = "变更前应缴金额")
    private String qianYtotal;

    @Excel(name = "变更后应缴金额")
    private String houYtotal;

    @Excel(name = "变更前支付类型")
    private Integer qianType;

    @Excel(name = "变更后支付类型")
    private Integer houType;

    @Excel(name = "变更前缴费类型")
    private Integer qianLei;

    @Excel(name = "变更后缴费类型")
    private Integer houLei;
}
