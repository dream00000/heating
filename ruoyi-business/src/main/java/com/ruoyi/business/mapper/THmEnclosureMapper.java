package com.ruoyi.business.mapper;

import com.ruoyi.business.domain.THmEnclosure;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 附件Mapper接口
 */
public interface THmEnclosureMapper {
    /**
     * 查询附件
     */
    THmEnclosure selectTHmEnclosureById(@Param("id") int id);

    /**
     * 查询附件列表
     */
    List<THmEnclosure> selectTHmEnclosureList(THmEnclosure tHmEnclosure);

    /**
     * 新增附件
     */
    int insertTHmEnclosure(THmEnclosure tHmEnclosure);

    /**
     * 修改附件
     */
    int updateTHmEnclosure(THmEnclosure tHmEnclosure);

    /**
     * 删除
     */
    void delEnclosure(@Param("id") int id);
}
