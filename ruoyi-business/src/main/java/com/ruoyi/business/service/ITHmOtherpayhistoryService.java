package com.ruoyi.business.service;

import com.ruoyi.business.domain.THmCustomer;
import com.ruoyi.business.domain.THmOtherpayhistory;
import com.ruoyi.business.domain.vo.OtherpayhistoryVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 其他收费Service接口
 */
public interface ITHmOtherpayhistoryService {
    /**
     * 查询其他收费
     */
    public THmOtherpayhistory selectTHmOtherpayhistoryById(@Param("id") int id);

    /**
     * 查询其他收费列表
     */
    public List<THmOtherpayhistory> selectTHmOtherpayhistoryList(THmOtherpayhistory tHmOtherpayhistory);

    public List<THmOtherpayhistory> lists(THmOtherpayhistory tHmOtherpayhistory);

    public List<OtherpayhistoryVO> selectTHmOtherpayhistoryListt(THmCustomer tHmCustomer);

    public List<THmOtherpayhistory> selectTHmOtherpayhistoryLists(@Param("hcustomerId") int id);

    /**
     * 新增其他收费
     */
    public int insertTHmOtherpayhistory(THmOtherpayhistory tHmOtherpayhistory);

    /**
     * 修改其他收费
     */
    public int updateTHmOtherpayhistory(THmOtherpayhistory tHmOtherpayhistory);

    //打印专用查询
    public THmOtherpayhistory getForPrint(@Param("opayId") int opayId);
}
