package com.ruoyi.business.service.impl;

import com.ruoyi.business.domain.THmEnclosure;
import com.ruoyi.business.mapper.THmEnclosureMapper;
import com.ruoyi.business.service.ITHmEnclosureService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 附件Service业务层处理
 *
 * @author ruoyi
 * @date 2021-08-13
 */
@Service
public class THmEnclosureServiceImpl implements ITHmEnclosureService {
    @Resource
    private THmEnclosureMapper tHmEnclosureMapper;

    /**
     * 查询附件
     *
     * @param id
     * @return 附件
     */
    @Override
    public THmEnclosure selectTHmEnclosureById(int id) {
        return tHmEnclosureMapper.selectTHmEnclosureById(id);
    }

    /**
     * 查询附件列表
     *
     * @param tHmEnclosure 附件
     * @return 附件
     */
    @Override
    public List<THmEnclosure> selectTHmEnclosureList(THmEnclosure tHmEnclosure) {
        return tHmEnclosureMapper.selectTHmEnclosureList(tHmEnclosure);
    }

    /**
     * 新增附件
     *
     * @param tHmEnclosure 附件
     * @return 结果
     */
    @Override
    public int insertTHmEnclosure(THmEnclosure tHmEnclosure) {
        return tHmEnclosureMapper.insertTHmEnclosure(tHmEnclosure);
    }

    /**
     * 修改附件
     *
     * @param tHmEnclosure 附件
     * @return 结果
     */
    @Override
    public int updateTHmEnclosure(THmEnclosure tHmEnclosure) {
        return tHmEnclosureMapper.updateTHmEnclosure(tHmEnclosure);
    }

    @Override
    public void delEnclosure(int id) {
        tHmEnclosureMapper.delEnclosure(id);
    }

}
