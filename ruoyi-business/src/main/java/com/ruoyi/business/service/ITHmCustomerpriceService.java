package com.ruoyi.business.service;

import com.ruoyi.business.domain.THmCustomerprice;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户单价Service接口
 */
public interface ITHmCustomerpriceService {
    /**
     * 查询用户单价
     */
    public THmCustomerprice selectTHmCustomerpriceById(@Param("id") int id);

    /**
     * 查询用户单价列表
     */
    public List<THmCustomerprice> selectTHmCustomerpriceList(THmCustomerprice tHmCustomerprice);

    /**
     * 新增用户单价
     */
    public int insertTHmCustomerprice(THmCustomerprice tHmCustomerprice);

    /**
     * 修改用户单价
     */
    public int updateTHmCustomerprice(THmCustomerprice tHmCustomerprice);

    public int updateTHmCustomerprices(THmCustomerprice tHmCustomerprice);


    /**
     * 根据用户id删除关联的单价
     */
    public void delTHmCustomerprice(Integer customerId);

}
