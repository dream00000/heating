package com.ruoyi.business.util;

import cn.hutool.cache.Cache;
import cn.hutool.cache.CacheUtil;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.business.domain.WechatUser;
import com.ruoyi.business.domain.WxPayBean;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.ruoyi.business.util.Sign.sign;

/**
 * 微信工具类；
 */
@Component
public class WeChatUtils {

    Cache<String, String> fifoCache = CacheUtil.newFIFOCache(3);

    @Resource
    private WxPayBean wxPayBean;

    /**
     * 授权跳转；
     */
    public String weChatGetCodes() {
        return "http://" + wxPayBean.getRedirect() + "?u=http://" + wxPayBean.getDomain() + "/kddz/wechat/accredits";
    }

    /**
     * 授权跳转；
     */
    public String weChatGetCode(WxPayBean wxPayBean, String c) {
        String url = "https://open.weixin.qq.com/connect/oauth2/authorize" +
                "?appid=" + wxPayBean.getAppId() +
                "&redirect_uri=http://" + wxPayBean.getDomain() + "/kddz/wechat/getInfo" +
                "&response_type=code" +
                "&scope=snsapi_userinfo" +
                "&state=" + c + "#wechat_redirect";
        return url;
    }

    /**
     * 获取微信Openid
     */
    public WechatUser weChatGetOpenid(WxPayBean wxPayBean, String code) {
        String getOpenidUrl = "https://api.weixin.qq.com/sns/oauth2/access_token";
        Map<String, Object> params = new HashMap<>();
        params.put("appid", wxPayBean.getAppId());
        params.put("secret", wxPayBean.getAppSecret());
        params.put("code", code);
        params.put("grant_type", "authorization_code");
        String openidJSON = HttpUtil.post(getOpenidUrl, params);
        JSONObject openidObject = JSONObject.parseObject(openidJSON);
        if (StrUtil.isNotBlank(openidObject.getString("errcode")) && StrUtil.isNotBlank(openidObject.getString("errmsg"))) {
            return null;
        } else {
            WechatUser wechatUser = new WechatUser();
            wechatUser.setOpenId(openidObject.getString("openid"));
            wechatUser.setAccessToken(openidObject.getString("access_token"));
            wechatUser.setRefreshToken(openidObject.getString("refresh_token"));
            wechatUser.setAccessTokenDate(DateUtil.date());
            return wechatUser;
        }
    }

    /**
     * 刷新AccessToken
     */
    public WechatUser weChatRefreshOpenid(WxPayBean wxPayBean, WechatUser wechatUser) {
        String refreshUrl = "https://api.weixin.qq.com/sns/oauth2/refresh_token" +
                "?appid=APPID" +
                "&grant_type=refresh_token" +
                "&refresh_token=REFRESH_TOKEN";
        Map<String, Object> params = new HashMap<>();
        params.put("appid", wxPayBean.getAppId());
        params.put("grant_type", "refresh_token");
        params.put("refresh_token", wechatUser.getRefreshToken());
        String openidJSON = HttpUtil.post(refreshUrl, params);
        JSONObject openidObject = JSONObject.parseObject(openidJSON);
        if (StrUtil.isNotBlank(openidObject.getString("errcode")) && StrUtil.isNotBlank(openidObject.getString("errmsg"))) {
            return wechatUser;
        } else {
            wechatUser.setOpenId(openidObject.getString("openid"));
            wechatUser.setAccessToken(openidObject.getString("access_token"));
            wechatUser.setRefreshToken(openidObject.getString("refresh_token"));
            wechatUser.setAccessTokenDate(DateUtil.date());
            return wechatUser;
        }
    }

    /**
     * 获取微信用户信息
     */
    public WechatUser wechatGetUserInfo(WxPayBean wxPayBean, WechatUser wechatUser) {
        //对比AccessToken是否过期
        Date accessTokenDate = wechatUser.getAccessTokenDate();
        long between = DateUtil.between(accessTokenDate, DateUtil.date(), DateUnit.SECOND);
        //刷新accessToken
        if (between >= 7200) {
            wechatUser = weChatRefreshOpenid(wxPayBean, wechatUser);
        }
        String infoUrl = "https://api.weixin.qq.com/sns/userinfo";
        Map<String, Object> params = new HashMap<>();
        params.put("access_token", wechatUser.getAccessToken());
        params.put("openid", wechatUser.getOpenId());
        params.put("lang", "zh_CN");
        String infoJSON = HttpUtil.get(infoUrl, params);
        JSONObject infoObject = JSONObject.parseObject(infoJSON);
        if (StrUtil.isBlank(infoObject.getString("errcode")) && StrUtil.isBlank(infoObject.getString("errmsg"))) {
            wechatUser.setNickName(infoObject.getString("nickname"));
            wechatUser.setSex(infoObject.getString("sex"));
            wechatUser.setProvince(infoObject.getString("province"));
            wechatUser.setCity(infoObject.getString("city"));
            wechatUser.setHeadimgurl(infoObject.getString("headimgurl"));
            return wechatUser;
        } else {
            return wechatUser;
        }
    }

    public String getSDKToken(WxPayBean wxBean) {
        String token = fifoCache.get("token");
        if (StrUtil.isNotBlank(token)) {
            return token;
        } else {
            String getOpenidUrl = "https://api.weixin.qq.com/cgi-bin/token";
            Map<String, Object> params = new HashMap<>();
            params.put("appid", wxBean.getAppId());
            params.put("secret", wxBean.getAppSecret());
            params.put("grant_type", "client_credential");
            String openidJSON = HttpUtil.get(getOpenidUrl, params);
            JSONObject openidObject = JSONObject.parseObject(openidJSON);
            if (StrUtil.isNotBlank(openidObject.getString("errcode")) && StrUtil.isNotBlank(openidObject.getString("errmsg"))) {
                return "";
            } else {
                fifoCache.put("token", openidObject.getString("access_token"), DateUnit.SECOND.getMillis() * 7200);
                token = openidObject.getString("access_token");
                return token;
            }
        }
    }

    public Map<String, String> wechatGetSDKJSON(WxPayBean wxBean, String squrl) {
        String jsapi_ticket = fifoCache.get("jsapi_ticket");
        if (StrUtil.isNotBlank(jsapi_ticket)) {
            return sign(jsapi_ticket, squrl, wxBean.getAppId());
        } else {
            String tokenurl = "https://api.weixin.qq.com/cgi-bin/ticket/getticket";
            String token = getSDKToken(wxBean);
            Map<String, Object> params = new HashMap<>();
            params.put("access_token", token);
            params.put("type", "jsapi");
            String ticketJSON = HttpUtil.post(tokenurl, params);
            JSONObject ticketObject = JSONObject.parseObject(ticketJSON);
            if (StrUtil.equals("ok", ticketObject.getString("errmsg")) && StrUtil.equals("0", ticketObject.getString("errcode"))) {
                jsapi_ticket = ticketObject.getString("ticket");
                fifoCache.put("jsapi_ticket", jsapi_ticket, DateUnit.SECOND.getMillis() * 7200);
                return sign(jsapi_ticket, squrl, wxBean.getAppId());
            } else {
                return null;
            }
        }
    }
}
