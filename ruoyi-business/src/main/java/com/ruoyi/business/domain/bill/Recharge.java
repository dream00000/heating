package com.ruoyi.business.domain.bill;

import lombok.Data;

@Data
public class Recharge {
    private String type;//类型名称
    private String money;//类型金额
    private Integer count;//类型笔数
}
