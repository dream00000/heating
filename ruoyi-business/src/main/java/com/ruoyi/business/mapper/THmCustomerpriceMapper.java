package com.ruoyi.business.mapper;

import com.ruoyi.business.domain.THmCustomerprice;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户单价Mapper接口
 */
public interface THmCustomerpriceMapper {
    /**
     * 查询用户单价
     */
    THmCustomerprice selectTHmCustomerpriceById(@Param("id") int id);

    /**
     * 查询用户单价列表
     */
    List<THmCustomerprice> selectTHmCustomerpriceList(THmCustomerprice tHmCustomerprice);

    List<THmCustomerprice> getByCustomerId(@Param("customerId") int customerId);

    /**
     * 新增用户单价
     */
    int insertTHmCustomerprices(THmCustomerprice tHmCustomerprice);

    int insertTHmCustomerprice(THmCustomerprice tHmCustomerprice);

    /**
     * 修改用户单价
     */
    int updateTHmCustomerprice(THmCustomerprice tHmCustomerprice);

    /**
     * 根据用户id删除关联的单价
     */
    int delTHmCustomerprice(Integer customerId);
}
