package com.ruoyi.business.controller;

import java.math.BigDecimal;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import com.ruoyi.business.domain.*;
import com.ruoyi.business.mapper.THmDelayMapper;
import com.ruoyi.business.mapper.THmPayhistoryMapper;
import com.ruoyi.business.service.ITHmCustomerService;
import com.ruoyi.business.service.ITHmCustomerpaydaysService;
import com.ruoyi.business.service.ITHmPayhistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.business.service.ITHmAllocationService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 金额划拨Controller
 *
 * @author ruoyi
 * @date 2022-03-16
 */

@Controller
@RequestMapping("/business/allocation")
public class THmAllocationController extends BaseController
{
    private String prefix = "business/allocation";
    @Autowired
    private ITHmAllocationService tHmAllocationService;
    @Resource
    private ITHmCustomerService tHmCustomerService;
    @Resource
    private ITHmCustomerpaydaysService tHmCustomerpaydaysService;
    @Resource
    private ITHmPayhistoryService tHmPayhistoryService;
    @Resource
    private THmDelayMapper tHmDelayMapper;


    @Resource
    private ITHmCustomerpaydaysService itHmCustomerpaydaysService;

    @Resource
    private THmPayhistoryMapper tHmPayhistoryMapper;

    @GetMapping()
    public String customer() {
        return prefix + "/allocationlist";
    }



    /**
     * 查询金额划拨列表
     */

    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(THmAllocation tHmAllocation)
    {

        startPage();
        List<THmAllocation> list = tHmAllocationService.selectTHmAllocationList(tHmAllocation);
        return getDataTable(list);
    }


    @Log(title = "金额划拨", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, THmAllocation tHmAllocation)
    {
        List<THmAllocation> list = tHmAllocationService.selectTHmAllocationList(tHmAllocation);
        ExcelUtil<THmAllocation> util = new ExcelUtil<THmAllocation>(THmAllocation.class);
        //util.exportExcel(response, list, "金额划拨数据");
    }

    @GetMapping("/fcustomer")
    public String fcustomer()
    {
        return prefix + "/fcustomer";
    }

    @GetMapping("/tcustomer")
    public String tcustomer()
    {
        return prefix + "/tcustomer";
    }

    /**
     * 查询用户信息列表
     */
    @PostMapping("/customerlist")
    @ResponseBody
    public TableDataInfo customerlist(THmCustomer tHmCustomer) {
        startPage();
        List<THmCustomer> list = tHmCustomerService.selectTHmCustomerList(tHmCustomer);
        return getDataTable(list);
    }

    /**
     * 新增用户信息
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /*
    * 获取某用户的账期信息
    * */


    @PostMapping("/getwq")
    @ResponseBody
    public AjaxResult getwq(@RequestBody String cusid) {
        String s=cusid.split("=")[1];
        Long sa=Long.parseLong(s);
        List<THmCustomerpaydays> list =tHmCustomerpaydaysService.getcuswq(sa);
        return AjaxResult.success("成功", list);
    }


    /**
     * 获取金额划拨详细信息
     */

    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(tHmAllocationService.selectTHmAllocationById(id));
    }

    /**
     * 新增金额划拨
     */

    @Log(title = "金额划拨", businessType = BusinessType.INSERT)
    @PostMapping
    @ResponseBody
    public AjaxResult add(@RequestBody THmAllocation tHmAllocation)
    {
        return toAjax(tHmAllocationService.insertTHmAllocation(tHmAllocation));
    }


    /**
     * 新增保存用户信息
     */
    @Log(title = "金额划拨", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(THmAllocation tHmAllocation) {

        //THmCustomer tHmCustomer2 = tHmCustomerService.selectTHmCustomerByHcustomerCode(tHmCustomer.getHcustomerCode());
        //充值作废

        /*
        * 判断是否账期金额划拨
        * 若是账期划拨则执行以下代码
        * */

        if(tHmAllocation.getcType().equals("余额"))
        {
            int fcustomerid=tHmAllocation.getfCustomerid().intValue();
            int tcustomerid=tHmAllocation.gettCustomerid().intValue();
            // 更新划出人余额
            THmCustomer fHmCustomer = tHmCustomerService.selectTHmCustomerById(fcustomerid);
            BigDecimal bfbalance=tHmAllocation.getfBeforebalance().subtract(tHmAllocation.getfAmount());
            fHmCustomer.setBalance(bfbalance.toString());
            tHmCustomerService.updateTHmCustomer(fHmCustomer);
            tHmAllocation.setfAfterbalance(bfbalance);

            //更新划入人余额
            THmCustomer tHmCustomer = tHmCustomerService.selectTHmCustomerById(tcustomerid);
            String tbalance=tHmAllocation.gettAfterbalance().toString();
            tHmCustomer.setBalance(tbalance);
            tHmCustomerService.updateTHmCustomer(tHmCustomer);


        }
        else if(tHmAllocation.getcType().equals("账期") && tHmAllocation.getfNian()!=null && !"".equals(tHmAllocation.getfNian()))
        {
            //获取账期id
            THmCustomerpaydays hmCustomerpaydays=new THmCustomerpaydays();
            Integer fCustomerid=tHmAllocation.getfCustomerid().intValue();
            hmCustomerpaydays.setHcustomerId(fCustomerid);
            hmCustomerpaydays.setHzqGrstarttime(tHmAllocation.getfNian());
            List<THmCustomerpaydays> tHmCustomerpaydayslist=itHmCustomerpaydaysService.selectTHmCustomerpaydaysList(hmCustomerpaydays);
            if(tHmCustomerpaydayslist!=null && tHmCustomerpaydayslist.size()>0)
            {
                THmCustomerpaydays tHmCustomerpaydays = tHmCustomerpaydayslist.get(0);
                //更新划入人余额
                int tcustomerid=tHmAllocation.gettCustomerid().intValue();
                THmCustomer tHmCustomer = tHmCustomerService.selectTHmCustomerById(tcustomerid);
                String tbalance=tHmAllocation.gettAfterbalance().toString();
                tHmCustomer.setBalance(tbalance);
                tHmCustomerService.updateTHmCustomer(tHmCustomer);

                //tHmAllocation.setfNian(tHmAllocation.getWqname());
                tHmAllocation.setfWqid(tHmCustomerpaydays.getWqId());
                /*作废*/
                THmPayhistory thmPayhistory=new THmPayhistory();
                thmPayhistory.setWqId(tHmCustomerpaydays.getWqId());

                List<THmPayhistory> tHmPayhistoryList = tHmPayhistoryService.selectTHmPayhistoryList(thmPayhistory);

                if (tHmPayhistoryList.size() > 0) {
                    for (THmPayhistory tHmPayhistory : tHmPayhistoryList) {

                        /*
                         * 判断是否是续费
                         * */
                        if(tHmPayhistory.getHpaySfqe()==2)
                        {
                            //状态置为1
                            tHmPayhistory.setHpayState(1);
                            tHmPayhistoryService.updateTHmPayhistory(tHmPayhistory);
                            //账期状态回退

                            tHmCustomerpaydays.setWqInt1(0);
                            itHmCustomerpaydaysService.updateTHmCustomerpaydays(tHmCustomerpaydays);
                            //更新用户余额
                        /*
                        THmCustomer tHmCustomer = tHmCustomerService.selectTHmCustomerById(tHmPayhistory.getHcustomerId());
                        THmCustomer tHmCustomer1 = new THmCustomer();
                        tHmCustomer1.setHcustomerInt4(tHmCustomerpaydays.getWqInt1());
                        tHmCustomer1.setHcustomerId(tHmCustomerpaydays.getHcustomerId());
                        tHmCustomer1.setBalance(new BigDecimal(tHmCustomer.getBalance()).add(new BigDecimal(tHmPayhistory.getHpayStr4())).toString());
                        tHmCustomer1.setHcustomerInt4(0);
                        tHmCustomer1.setIsZheng(0);
                        tHmCustomerService.updateTHmCustomer(tHmCustomer1);

                         */
                        }
                        else
                        {
                            THmPayhistory hmPayhistory =tHmPayhistory;
                            //状态置为1
                            hmPayhistory.setHpayState(1);
                            tHmPayhistoryService.updateTHmPayhistory(hmPayhistory);
                            //账期状态回退
                            //THmCustomerpaydays tHmCustomerpaydays = itHmCustomerpaydaysService.selectTHmCustomerpaydaysById(hmPayhistory.getWqId());
                            if (tHmCustomerpaydays.getWqInt2() != null) {
                                if (tHmCustomerpaydays.getWqInt2() == 5) {
                                    tHmCustomerpaydays.setWqInt2(tHmCustomerpaydays.getWqInt1());
                                    tHmCustomerpaydays.setWqInt1(5);
                                } else {
                                    tHmCustomerpaydays.setWqInt1(tHmCustomerpaydays.getWqInt2());
                                }
                            }
                            //查这个账期有没有缴费记录
                            THmPayhistory tHmPayhistory1 = new THmPayhistory();
                            tHmPayhistory1.setWqId(hmPayhistory.getWqId());
                            tHmPayhistory1.setHpaySfqe(4);
                            List<THmPayhistory> list = tHmPayhistoryMapper.selectTHmPayhistoryListForWeChat(tHmPayhistory1);
                            if (!tHmCustomerpaydays.getWqStr6().equals("0.00") || list.size() > 0) {
                                tHmCustomerpaydays.setWqStr6(new BigDecimal(tHmCustomerpaydays.getWqStr6()).add(new BigDecimal(hmPayhistory.getHpayMoney())).toString());
                            }
                            itHmCustomerpaydaysService.updateTHmCustomerpaydays(tHmCustomerpaydays);
                            //更新用户余额
                        /*


                        THmCustomer tHmCustomer = tHmCustomerService.selectTHmCustomerById(hmPayhistory.getHcustomerId());
                        THmCustomer tHmCustomer1 = new THmCustomer();
                        tHmCustomer1.setHcustomerInt4(tHmCustomerpaydays.getWqInt1());
                        tHmCustomer1.setHcustomerId(tHmCustomerpaydays.getHcustomerId());
                        tHmCustomer1.setBalance(new BigDecimal(tHmCustomer.getBalance()).add(new BigDecimal(hmPayhistory.getHpayStr4())).toString());
                        tHmCustomerService.updateTHmCustomer(tHmCustomer1);

  * */
                            THmDelay tHmDelay = new THmDelay();
                            tHmDelay.setIsGai(0);
                            tHmDelay.setWqId(hmPayhistory.getWqId());
                            tHmDelayMapper.updateTHmDelay(tHmDelay);

                            //更新用户状态 为最后一条账期的状态
                            THmCustomerpaydays tHmCustomerpaydaysnew = new THmCustomerpaydays();
                            tHmCustomerpaydaysnew.setHcustomerId(tHmPayhistory.getHcustomerId());
                            List<THmCustomerpaydays> tHmCustomerpaydaysList = itHmCustomerpaydaysService.selectTHmCustomerpaydaysList(tHmCustomerpaydaysnew);
                            if (tHmCustomerpaydaysList.size() > 0) {
                                THmCustomer tHmCustomer1new = new THmCustomer();
                                tHmCustomer1new.setHcustomerInt4(tHmCustomerpaydaysList.get(0).getWqInt1());
                                tHmCustomer1new.setHcustomerId(tHmCustomerpaydays.getHcustomerId());
                                tHmCustomer1new.setIsZheng(0);
                                tHmCustomerService.updateTHmCustomer(tHmCustomer1new);
                            }
                        }




                    }


                }
            }

        }



        tHmAllocationService.insertTHmAllocation(tHmAllocation);
        return AjaxResult.success("成功");


    }

    /*
    * 作废
    * */



    /**
     * 修改金额划拨
     */


    @Log(title = "金额划拨", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody THmAllocation tHmAllocation)
    {
        return toAjax(tHmAllocationService.updateTHmAllocation(tHmAllocation));
    }

    /**
     * 删除金额划拨
     */

    @Log(title = "金额划拨", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tHmAllocationService.deleteTHmAllocationByIds(ids));
    }
}
