package com.ruoyi.business.controller;

import com.ruoyi.business.domain.*;
import com.ruoyi.business.domain.data.THmCustomerpaydaysPriceCopy;
import com.ruoyi.business.domain.vo.CustomerpaydaysVO;
import com.ruoyi.business.mapper.THmCustomerMapper;
import com.ruoyi.business.mapper.THmCustomerpaydaysMapper;
import com.ruoyi.business.mapper.THmCustomerpaydaysPriceMapper;
import com.ruoyi.business.service.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户年度账期Controller
 *
 * @author ruoyi
 * @date 2021-08-13
 */
@Controller
@RequestMapping("/business/customerpaydays")
public class THmCustomerpaydaysController extends BaseController {
    private String prefix = "business/customerpaydays";

    @Resource
    private ITHmCustomerpaydaysService tHmCustomerpaydaysService;

    @Resource
    private ITHmCustomerService itHmCustomerService;

    @Resource
    private THmCustomerMapper tHmCustomerMapper;

    @Resource
    private ITHmPayhistoryService itHmPayhistoryService;

    @Resource
    private ITHmCustomerpaydaysRecordService itHmCustomerpaydaysRecordService;

    @Resource
    private ITHmLessenService itHmLessenService;

    @Resource
    private ITHmCustomerpaydaysPriceService itHmCustomerpaydaysPriceService;

    @Resource
    private THmCustomerpaydaysPriceMapper tHmCustomerpaydaysPriceMapper;

    @Resource
    private THmCustomerpaydaysMapper tHmCustomerpaydaysMapper;

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @GetMapping()
    public String customerpaydays() {
        return prefix + "/customerpaydays";
    }

    @GetMapping("/customerpaydayslist")
    public String customerpaydayslist() {
        return prefix + "/customerpaydayslist";
    }

    @GetMapping("/tiao")
    public String tiao() {
        return prefix + "/customerpaydayst";
    }

    /**
     * 查询用户年度账期列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(THmCustomerpaydays tHmCustomerpaydays) {
        startPage();
        List<THmCustomerpaydays> list = tHmCustomerpaydaysService.selectTHmCustomerpaydaysList(tHmCustomerpaydays);
        return getDataTable(list);
    }

    /**
     * 查询用户年度账期列表
     */
    @PostMapping("/listt")
    @ResponseBody
    public TableDataInfo listt(THmCustomer tHmCustomer) {
        if ("fou".equals(tHmCustomer.getBalance())) {
            tHmCustomer.setHcustomerStr4("fou");
        } else if ("shi".equals(tHmCustomer.getBalance())) {
            tHmCustomer.setHcustomerStr5("shi");
        }
        startPage();
        List<CustomerpaydaysVO> list = tHmCustomerpaydaysService.selectTHmCustomerpaydaysLists(tHmCustomer);
        return getDataTable(list);
    }

    /**
     * 查询用户年度账期列表
     */
    @PostMapping("/listt/{id}")
    @ResponseBody
    public TableDataInfo zhangqi(@PathVariable("id") int id) {
        THmCustomer tHmCustomer = new THmCustomer();
        tHmCustomer.setHcustomerId(id);
        startPage();
        List<CustomerpaydaysVO> list = tHmCustomerpaydaysService.selectTHmCustomerpaydaysLists(tHmCustomer);
        return getDataTable(list);
    }

    /**
     * 导出用户年度账期列表
     */
    @PostMapping("/exportt")
    @ResponseBody
    @Log(title = "导出用户年度", businessType = BusinessType.EXPORT)
    public AjaxResult exportt(THmCustomer tHmCustomer) {
        List<CustomerpaydaysVO> list = tHmCustomerpaydaysService.selectTHmCustomerpaydaysLists(tHmCustomer);
        ExcelUtil<CustomerpaydaysVO> util = new ExcelUtil<>(CustomerpaydaysVO.class);
        return util.exportExcel(list, "用户账期");
    }

    /**
     * 导出用户年度账期列表
     */
    @PostMapping("/export")
    @ResponseBody
    @Log(title = "导出用户年度", businessType = BusinessType.EXPORT)
    public AjaxResult export(THmCustomerpaydays tHmCustomerpaydays) {
        List<THmCustomerpaydays> list = tHmCustomerpaydaysService.selectTHmCustomerpaydaysList(tHmCustomerpaydays);
        ExcelUtil<THmCustomerpaydays> util = new ExcelUtil<THmCustomerpaydays>(THmCustomerpaydays.class);
        return util.exportExcel(list, "customerpaydays");
    }

    /**
     * 新增用户年度账期
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存用户年度账期
     */
    @Log(title = "新增用户年度账期", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(THmCustomerpaydays tHmCustomerpaydays) {
        return toAjax(tHmCustomerpaydaysService.insertTHmCustomerpaydays(tHmCustomerpaydays));
    }

    /**
     * 修改用户年度账期
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") int id, ModelMap mmap) {
        THmCustomerpaydays tHmCustomerpaydays = tHmCustomerpaydaysService.selectTHmCustomerpaydaysById(id);
        mmap.put("tHmCustomerpaydays", tHmCustomerpaydays);
        return prefix + "/edit";
    }

    /**
     * 修改保存用户账期
     */
    @Log(title = "面积变更", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(@RequestBody THmCustomerpaydays tHmCustomerpaydays) {
        //先取出原金额
        THmCustomerpaydays tHmCustomerpaydays1 = tHmCustomerpaydaysService.selectTHmCustomerpaydaysById(tHmCustomerpaydays.getWqId());
        //此条如果有面积变更则需先处理前一条面积变更
        if (tHmCustomerpaydays.getWqInt1() == 5) {
            return AjaxResult.success("已有面积变更账期，请先处理后再进行面积变更");
        }
        //如果 延期 或者 欠款 则直接修改
        if (tHmCustomerpaydays.getWqInt1() == 3 || tHmCustomerpaydays.getWqInt1() == 99) {
//            tHmCustomerpaydaysService.updateTHmCustomerpaydays(tHmCustomerpaydays);
//            //修改用户单价里边的面积
//            if (tHmCustomerpaydays.getCustomerpriceList() != null) {
//                for (THmCustomerpaydaysPrice tHmCustomerpaydaysPrice : tHmCustomerpaydays.getCustomerpriceList()) {
//                    itHmCustomerpaydaysPriceService.updateTHmCustomerpaydaysPrice(tHmCustomerpaydaysPrice);
//                }
//            }
            return AjaxResult.success("账期还未进行缴费，请在复合调整中进行调整");
        }

        //如果 全额、基础热、续费、平账、部分缴费 则多退少补
        if (tHmCustomerpaydays.getWqInt1() == 0 || tHmCustomerpaydays.getWqInt1() == 1 || tHmCustomerpaydays.getWqInt1() == 2 || tHmCustomerpaydays.getWqInt1() == 4 || tHmCustomerpaydays.getWqInt1() == 6) {
            //现在金额
            BigDecimal bd = new BigDecimal(tHmCustomerpaydays.getWqStr1());
            //原来金额
            BigDecimal bd1 = new BigDecimal(tHmCustomerpaydays1.getWqStr1());
            int i = bd.compareTo(bd1);
            //修改后的金额比修改前的  小
            if (i == -1) {
                BigDecimal bds = bd1.subtract(bd);
                //生成减免记录并将金额增加到用户余额中
                THmLessen tHmLessen = new THmLessen();
                tHmLessen.setHcustomerId(tHmCustomerpaydays.getHcustomerId());
                tHmLessen.setJmMoney(bds.toString());
                tHmLessen.setJmReasion("修改用户面积减小，增加减免");
                itHmLessenService.insertTHmLessen(tHmLessen);
                //更新账期
                tHmCustomerpaydaysService.updateTHmCustomerpaydays(tHmCustomerpaydays);
            }
            //修改后的金额比修改前的  大
            if (i == 1) {
                BigDecimal bds = bd.subtract(bd1);
                tHmCustomerpaydays.setHcustomerMoney(bds.toString());
                tHmCustomerpaydays.setWqStr5(bds.toString());
                //面积变更需要保存上一个状态
                tHmCustomerpaydays.setWqInt2(tHmCustomerpaydays.getWqInt1());
                //更新账期
                tHmCustomerpaydays.setWqInt1(5);
                tHmCustomerpaydaysService.updateTHmCustomerpaydays(tHmCustomerpaydays);
                //添加用户单价
                if (tHmCustomerpaydays.getCustomerpriceList() != null) {
                    //修改用户单价里边的面积
                    for (THmCustomerpaydaysPrice tHmCustomerpaydaysPrice : tHmCustomerpaydays.getCustomerpriceList()) {
                        itHmCustomerpaydaysPriceService.updateTHmCustomerpaydaysPrice(tHmCustomerpaydaysPrice);
                    }
                }
                //更新用户状态
                THmCustomer tHmCustomer = new THmCustomer();
                tHmCustomer.setHcustomerInt4(5);
                tHmCustomer.setIsZheng(0);
                tHmCustomer.setHcustomerId(tHmCustomerpaydays.getHcustomerId());
                itHmCustomerService.updateTHmCustomer(tHmCustomer);
            }
        }
        //账期修改记录
        THmCustomerpaydaysRecord tHmCustomerpaydaysRecord = new THmCustomerpaydaysRecord();
        tHmCustomerpaydaysRecord.setQianTotal(tHmCustomerpaydays1.getWqStr1());
        tHmCustomerpaydaysRecord.setHouTotal(tHmCustomerpaydays.getWqStr1());
        tHmCustomerpaydaysRecord.setType(1);//类型为修改
        tHmCustomerpaydaysRecord.setCustomerpaydaysId(tHmCustomerpaydays.getWqId());//账期id
        itHmCustomerpaydaysRecordService.insertTHmCustomerpaydaysRecord(tHmCustomerpaydaysRecord);
        return AjaxResult.success("成功");
    }

    /**
     * 调整用户年度账期
     */
    @GetMapping("/edits/{id}")
    public String edits(@PathVariable("id") int id, ModelMap mmap) {
        THmCustomerpaydays tHmCustomerpaydays = tHmCustomerpaydaysService.selectTHmCustomerpaydaysById(id);
        mmap.put("tHmCustomerpaydays", tHmCustomerpaydays);
        return prefix + "/edits";
    }

    /**
     * 调整用户年度账期
     */
    @Log(title = "调整用户账期", businessType = BusinessType.UPDATE)
    @PostMapping("/edits")
    @ResponseBody
    public AjaxResult editsSave(THmCustomerpaydays tHmCustomerpaydays) {

        THmCustomerpaydaysRecord tHmCustomerpaydaysRecord = new THmCustomerpaydaysRecord();
        //先取出原金额
        THmCustomerpaydays tHmCustomerpaydays1 = tHmCustomerpaydaysService.selectTHmCustomerpaydaysById(tHmCustomerpaydays.getWqId());
        tHmCustomerpaydaysRecord.setQianTotal(tHmCustomerpaydays1.getWqStr1());
        tHmCustomerpaydaysRecord.setHouTotal(tHmCustomerpaydays.getWqStr1());
        tHmCustomerpaydaysRecord.setType(3);//类型为修改
        tHmCustomerpaydaysRecord.setCustomerpaydaysId(tHmCustomerpaydays.getWqId());//账期id
        itHmCustomerpaydaysRecordService.insertTHmCustomerpaydaysRecord(tHmCustomerpaydaysRecord);

        tHmCustomerpaydays.setIsTiao(1);//调整
        return AjaxResult.success(tHmCustomerpaydaysService.updateTHmCustomerpaydays(tHmCustomerpaydays));
    }

    @GetMapping("/record/{id}")
    public String record(@PathVariable("id") int id, Model model) {
        Map<String, Object> map = new HashMap<>();
        map.put("wqId", id);
        model.addAllAttributes(map);
        return prefix + "/record";
    }

    /**
     * 账期操作记录
     */
    @PostMapping("/recordList/{id}")
    @ResponseBody
    public TableDataInfo recordList(@PathVariable("id") Integer id) {
        THmCustomerpaydaysRecord tHmCustomerpaydaysRecord = new THmCustomerpaydaysRecord();
        tHmCustomerpaydaysRecord.setCustomerpaydaysId(id);
        startPage();
        List<THmCustomerpaydaysRecord> list = itHmCustomerpaydaysRecordService.selectTHmCustomerpaydaysRecordList(tHmCustomerpaydaysRecord);
        return getDataTable(list);
    }

    /**
     * 删除用户年度账期
     */
    @Log(title = "平账", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(Integer id) {

        THmCustomerpaydays tHmCustomerpaydays = tHmCustomerpaydaysService.selectTHmCustomerpaydaysById(id);

        //添加账期操作记录
        THmCustomerpaydaysRecord tHmCustomerpaydaysRecord = new THmCustomerpaydaysRecord();
        tHmCustomerpaydaysRecord.setType(2);//类型为平账
        tHmCustomerpaydaysRecord.setCustomerpaydaysId(id);//账期id
        tHmCustomerpaydaysRecord.setQianTotal(tHmCustomerpaydays.getWqStr1());
        tHmCustomerpaydaysRecord.setHouTotal("0.00");
        itHmCustomerpaydaysRecordService.insertTHmCustomerpaydaysRecord(tHmCustomerpaydaysRecord);

        if (tHmCustomerpaydays != null) {
            //更新账期状态
            tHmCustomerpaydays.setWqInt2(tHmCustomerpaydays.getWqInt1());
            tHmCustomerpaydays.setWqInt1(4);//平账
            tHmCustomerpaydaysService.updateTHmCustomerpaydays(tHmCustomerpaydays);
            //更新用户int4 最后一次账期状态
            THmCustomer tHmCustomer = new THmCustomer();
            tHmCustomer.setHcustomerInt4(4);
            //查所有账期如果有不是全额，基础热，续费，平账的账单即为0
            tHmCustomer.setIsZheng(1);
            THmCustomerpaydays tHmCustomerpaydays1 = new THmCustomerpaydays();
            tHmCustomerpaydays1.setHcustomerId(tHmCustomerpaydays.getHcustomerId());
            List<THmCustomerpaydays> tHmCustomerpaydaysList = tHmCustomerpaydaysMapper.selectTHmCustomerpaydaysList(tHmCustomerpaydays1);
            for (THmCustomerpaydays customerpaydays : tHmCustomerpaydaysList) {
                if (customerpaydays.getWqInt1() != 0 && customerpaydays.getWqInt1() != 1 && customerpaydays.getWqInt1() != 2 && customerpaydays.getWqInt1() != 4) {
                    tHmCustomer.setIsZheng(0);
                }
            }
            tHmCustomer.setHcustomerId(tHmCustomerpaydays.getHcustomerId());
            itHmCustomerService.updateTHmCustomer(tHmCustomer);
        }
        return toAjax(1);
    }

    /**
     * 生成账期
     */
    @GetMapping("/initPay/{nian}/{ling}/{hcustomerCode}")
    @ResponseBody
    @Log(title = "生成用户账期", businessType = BusinessType.INSERT)
    public AjaxResult initPay(@PathVariable("nian") String thisYear, @PathVariable("ling") Integer ling, @PathVariable("hcustomerCode") String hcustomerCode) {
        //判断今年有没有
        String nextYear = String.valueOf(Integer.parseInt(thisYear) + 1);//明年
        List<THmCustomer> tHmCustomerList = tHmCustomerMapper.getCustomerNotShengBenNian(thisYear, nextYear, hcustomerCode);
        if (tHmCustomerList.size() == 0) {
            return AjaxResult.error("该用户所选年度已生成账期");
        } else {
            tHmCustomerpaydaysService.initPay(thisYear, ling, hcustomerCode);
            return AjaxResult.success("生成账期成功");
        }
    }

    /**
     * 生成账期
     */
    @GetMapping("/initPays/{nian}/{ling}")
    @ResponseBody
    @Log(title = "生成用户账期", businessType = BusinessType.INSERT)
    public AjaxResult initPays(@PathVariable("nian") String thisYear, @PathVariable("ling") Integer ling) {
        return toAjax(tHmCustomerpaydaysService.initPays(thisYear, ling));
    }

    /**
     * 生成延期
     */
    @GetMapping("/initDelays/{nian}")
    @ResponseBody
    @Log(title = "生成用户延期", businessType = BusinessType.INSERT)
    public AjaxResult initDelays(@PathVariable("nian") String thisYear) {
        return toAjax(tHmCustomerpaydaysService.initDelays(thisYear));
    }

    @GetMapping("/jiao/{id}")
    public String jiao(@PathVariable("id") int id, Model model) {
        Map<String, Object> map = new HashMap<>();
        map.put("wqId", id);
        model.addAllAttributes(map);
        return prefix + "/jiao";
    }

    /**
     * 本次账期充值记录
     */
    @PostMapping("/jiaoList/{id}")
    @ResponseBody
    public TableDataInfo jiaoList(@PathVariable("id") int id) {
        THmPayhistory tHmPayhistory = new THmPayhistory();
        tHmPayhistory.setWqId(id);
        startPage();
        List<THmPayhistory> list = itHmPayhistoryService.selectTHmPayhistoryList(tHmPayhistory);
        return getDataTable(list);
    }

    @GetMapping("/gou/{id}")
    public String gou(@PathVariable("id") int id, Model model) {
        Map<String, Object> map = new HashMap<>();
        map.put("wqId", id);
        model.addAllAttributes(map);
        return prefix + "/gou";
    }

    /**
     * 本次账期构成
     */
    @PostMapping("/gouList/{id}")
    @ResponseBody
    public TableDataInfo gouList(@PathVariable("id") int id) throws ParseException {
        THmCustomerpaydaysPrice tHmCustomerpaydaysPrice = new THmCustomerpaydaysPrice();
        tHmCustomerpaydaysPrice.setCustomerpaydaysId(id);
        startPage();
        List<THmCustomerpaydaysPrice> list = itHmCustomerpaydaysPriceService.selectTHmCustomerpaydaysPriceList(tHmCustomerpaydaysPrice);
        //账期详情
        THmCustomerpaydays tHmCustomerpaydays = tHmCustomerpaydaysMapper.selectTHmCustomerpaydaysById(id);
        BigDecimal bd = new BigDecimal("0.00");
        BigDecimal bds = new BigDecimal("0.00");
        if (list.size() > 0) {
            for (THmCustomerpaydaysPrice price : list) {
                price.setNian(tHmCustomerpaydays.getHzqGrstarttime() + "-" + tHmCustomerpaydays.getHzqGrendtime());
                price.setTotal(new BigDecimal(price.getArea()).multiply(new BigDecimal(price.getPrice())).multiply(new BigDecimal(price.getXishu())).divide(new BigDecimal("100")).toString());
                bd = bd.add(new BigDecimal(price.getTotal()));
                bds = bds.add(new BigDecimal(price.getArea()));
            }
        }
        //优惠记录
        THmLessen tHmLessen = new THmLessen();
        tHmLessen.setWqId(id);
        List<THmLessen> tHmLessenList = itHmLessenService.selectTHmLessenList(tHmLessen);
        if (tHmLessenList.size() > 0) {
            for (THmLessen hmLessen : tHmLessenList) {
                THmCustomerpaydaysPrice tHmCustomerpaydaysPrice2 = new THmCustomerpaydaysPrice();
                tHmCustomerpaydaysPrice2.setCustomerCode(list.get(0).getCustomerCode());
                tHmCustomerpaydaysPrice2.setCustomerName(list.get(0).getCustomerName());
                tHmCustomerpaydaysPrice2.setPriceType("优惠减免");
                tHmCustomerpaydaysPrice2.setPrice("");
                tHmCustomerpaydaysPrice2.setNian("");
                tHmCustomerpaydaysPrice2.setXishu("");
                tHmCustomerpaydaysPrice2.setType(0);
                tHmCustomerpaydaysPrice2.setArea("");
                tHmCustomerpaydaysPrice2.setTotal(hmLessen.getJmMoney());
                tHmCustomerpaydaysPrice2.setAddTime(sdf.parse(hmLessen.getJmAddtime()));
                list.add(tHmCustomerpaydaysPrice2);
                bd = bd.subtract(new BigDecimal(hmLessen.getJmMoney()));
            }
        }
        //物业费记录
        List<THmCustomerpaydaysPriceCopy> tHmCustomerpaydaysPriceCopyList = tHmCustomerpaydaysPriceMapper.getCopyList(id);
        if (tHmCustomerpaydaysPriceCopyList.size() > 0) {
            for (THmCustomerpaydaysPriceCopy tHmCustomerpaydaysPriceCopy : tHmCustomerpaydaysPriceCopyList) {
                THmCustomerpaydaysPrice tHmCustomerpaydaysPrice3 = new THmCustomerpaydaysPrice();
                tHmCustomerpaydaysPrice3.setCustomerCode(list.get(0).getCustomerCode());
                tHmCustomerpaydaysPrice3.setCustomerName(list.get(0).getCustomerName());
                tHmCustomerpaydaysPrice3.setPriceType(tHmCustomerpaydaysPriceCopy.getPriceType());
                tHmCustomerpaydaysPrice3.setPrice("");
                tHmCustomerpaydaysPrice3.setNian("");
                tHmCustomerpaydaysPrice3.setXishu("");
                tHmCustomerpaydaysPrice3.setType(0);
                tHmCustomerpaydaysPrice3.setArea("");
                tHmCustomerpaydaysPrice3.setTotal(tHmCustomerpaydaysPriceCopy.getPrice());
                tHmCustomerpaydaysPrice3.setAddTime(sdf.parse(tHmCustomerpaydaysPriceCopy.getAddTime()));
                list.add(tHmCustomerpaydaysPrice3);
                bd = bd.add(new BigDecimal(tHmCustomerpaydaysPriceCopy.getPrice()));
            }
        }
        //合计
        THmCustomerpaydaysPrice tHmCustomerpaydaysPrice1 = new THmCustomerpaydaysPrice();
        tHmCustomerpaydaysPrice1.setArea(bds.toString());
        tHmCustomerpaydaysPrice1.setCustomerName("");
        tHmCustomerpaydaysPrice1.setPriceType("");
        tHmCustomerpaydaysPrice1.setPrice("");
        tHmCustomerpaydaysPrice1.setXishu("");
        tHmCustomerpaydaysPrice1.setType(0);
        tHmCustomerpaydaysPrice1.setNian("");
        tHmCustomerpaydaysPrice1.setCustomerCode("合计");
        tHmCustomerpaydaysPrice1.setTotal(bd.toString());
        tHmCustomerpaydaysPrice1.setAddTime(new Date());
        list.add(tHmCustomerpaydaysPrice1);
        return getDataTable(list);
    }

    /**
     * 作废账期
     */
    @GetMapping("/zuo/{nian}/{hcustomerCode}")
    @ResponseBody
    @Log(title = "作废账期", businessType = BusinessType.DELETE)
    public AjaxResult zuo(@PathVariable("nian") String thisYear, @PathVariable("hcustomerCode") String hcustomerCode) {

        THmCustomer tHmCustomer = itHmCustomerService.selectTHmCustomerByHcustomerCode(hcustomerCode);
        //查账期
        //查缴费记录 没有缴费记录的就
        THmCustomerpaydays tHmCustomerpaydays = new THmCustomerpaydays();
        tHmCustomerpaydays.setHzqGrstarttime(thisYear);
        tHmCustomerpaydays.setHcustomerId(tHmCustomer.getHcustomerId());
        List<THmCustomerpaydays> list = tHmCustomerpaydaysMapper.selectTHmCustomerpaydaysList(tHmCustomerpaydays);
        if (list.size() > 0) {
            THmPayhistory tHmPayhistory = new THmPayhistory();
            tHmPayhistory.setWqId(list.get(0).getWqId());
            List<THmPayhistory> list1 = itHmPayhistoryService.selectTHmPayhistoryList(tHmPayhistory);
            if (list1.size() > 0) {
                return AjaxResult.success("本年度账期已缴费不能作废");
            } else {
                return toAjax(tHmCustomerpaydaysMapper.zuo(list.get(0).getWqId()));
            }
        } else {
            return AjaxResult.success("本年度没有账期");
        }
    }

    /**
     * 余额充值作废
     */
    @Log(title = "平账作废", businessType = BusinessType.UPDATE)
    @ResponseBody
    @PostMapping("/recordFei")
    public AjaxResult recordFei(Long id) {

        THmCustomerpaydaysRecord tHmCustomerpaydaysRecord = new THmCustomerpaydaysRecord();
        tHmCustomerpaydaysRecord.setStatus(1);
        tHmCustomerpaydaysRecord.setId(id);
        itHmCustomerpaydaysRecordService.updateTHmCustomerpaydaysRecord(tHmCustomerpaydaysRecord);

        tHmCustomerpaydaysRecord = itHmCustomerpaydaysRecordService.selectTHmCustomerpaydaysRecordById(id);

        THmCustomerpaydays tHmCustomerpaydays = tHmCustomerpaydaysMapper.selectTHmCustomerpaydaysById(tHmCustomerpaydaysRecord.getCustomerpaydaysId());
        if (tHmCustomerpaydays != null) {
            //更新账期状态
            tHmCustomerpaydays.setWqInt1(tHmCustomerpaydays.getWqInt2());
            tHmCustomerpaydaysService.updateTHmCustomerpaydays(tHmCustomerpaydays);
            //更新用户int4 最后一次账期状态
            THmCustomer tHmCustomer = new THmCustomer();
            tHmCustomer.setHcustomerInt4(4);
            //查所有账期如果有不是全额，基础热，续费，平账的账单即为0
            tHmCustomer.setIsZheng(1);
            THmCustomerpaydays tHmCustomerpaydays1 = new THmCustomerpaydays();
            tHmCustomerpaydays1.setHcustomerId(tHmCustomerpaydays.getHcustomerId());
            List<THmCustomerpaydays> tHmCustomerpaydaysList = tHmCustomerpaydaysMapper.selectTHmCustomerpaydaysList(tHmCustomerpaydays1);
            for (THmCustomerpaydays customerpaydays : tHmCustomerpaydaysList) {
                if (customerpaydays.getWqInt1() != 0 && customerpaydays.getWqInt1() != 1 && customerpaydays.getWqInt1() != 2 && customerpaydays.getWqInt1() != 4) {
                    tHmCustomer.setIsZheng(0);
                }
            }
            tHmCustomer.setHcustomerId(tHmCustomerpaydays.getHcustomerId());
            itHmCustomerService.updateTHmCustomer(tHmCustomer);
        }
        return AjaxResult.success("平账作废成功", 1);
    }

    @GetMapping("/lists")
    public String lists() {
        return prefix + "/jiaolist";
    }

    /**
     * 查询用户年度账期列表
     */
    @PostMapping("/jiaoList")
    @ResponseBody
    public TableDataInfo jiaoList(THmCustomer tHmCustomer) {
        startPage();
        List<CustomerpaydaysVO> list = tHmCustomerpaydaysService.jiaoList(tHmCustomer);
        return getDataTable(list);
    }

    /**
     * 导出用户年度账期列表
     */
    @PostMapping("/jiaoExport")
    @ResponseBody
    @Log(title = "账期缴费记录", businessType = BusinessType.EXPORT)
    public AjaxResult jiaoExport(THmCustomer tHmCustomer) {
        List<CustomerpaydaysVO> list = tHmCustomerpaydaysService.jiaoList(tHmCustomer);
        ExcelUtil<CustomerpaydaysVO> util = new ExcelUtil<>(CustomerpaydaysVO.class);
        return util.exportExcel(list, "用户账期");
    }
}
