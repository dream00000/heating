package com.ruoyi.business.mapper;

import com.ruoyi.business.domain.THmDelay;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 延期记录Mapper接口
 *
 * @author ruoyi
 * @date 2022-03-10
 */
public interface THmDelayMapper {
    /**
     * 查询延期记录
     *
     * @param id
     * @return 延期记录
     */
    THmDelay selectTHmDelayById(@Param("id") int id);

    List<THmDelay> getByWqId(@Param("wqId") int wqId);

    /**
     * 查询延期记录列表
     *
     * @param tHmDelay 延期记录
     * @return 延期记录集合
     */
    List<THmDelay> selectTHmDelayList(THmDelay tHmDelay);

    /**
     * 新增延期记录
     *
     * @param tHmDelay 延期记录
     * @return 结果
     */
    int insertTHmDelay(THmDelay tHmDelay);

    /**
     * 修改延期记录
     *
     * @param tHmDelay 延期记录
     * @return 结果
     */
    int updateTHmDelay(THmDelay tHmDelay);

    void deleteTHmDelay(@Param("id") int id);
}
