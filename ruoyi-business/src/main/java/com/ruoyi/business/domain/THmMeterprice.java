package com.ruoyi.business.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 用热单价对象 t_hm_meterprice
 *
 * @author ruoyi
 * @date 2021-08-13
 */
@Data
public class THmMeterprice extends BaseEntity {

    private Integer rjId;

    /**
     * 单价id，对应属性表
     */
    @Excel(name = "单价id，对应属性表")
    private Integer rjDjid;

    /**
     * 用热量上限
     */
    @Excel(name = "用热量上限")
    private String rjSx;

    /**
     * 用热量下限
     */
    @Excel(name = "用热量下限")
    private String rjXx;

    /**
     * 每平米单价（元）
     */
    @Excel(name = "每平米单价", readConverterExp = "元=")
    private String rjPrice;

    /**
     * 是否删除0否1是
     */
    @Excel(name = "是否删除0否1是")
    private Integer rjState;

    /**
     * 添加人id
     */
    @Excel(name = "添加人id")
    private Long rjAdduserid;

    /**
     * 添加时间
     */
    @Excel(name = "添加时间")
    private String rjAddtime;

    private String rjDjname;

    private String djid;
}
