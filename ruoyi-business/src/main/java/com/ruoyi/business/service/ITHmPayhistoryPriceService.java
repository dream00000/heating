package com.ruoyi.business.service;

import com.ruoyi.business.domain.THmPayhistoryPrice;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 充值-单价Service接口
 */
public interface ITHmPayhistoryPriceService {
    /**
     * 查询充值-单价
     */
    public THmPayhistoryPrice selectTHmPayhistoryPriceById(@Param("id") int id);

    /**
     * 查询充值-单价列表
     */
    public List<THmPayhistoryPrice> selectTHmPayhistoryPriceList(THmPayhistoryPrice tHmPayhistoryPrice);

    /**
     * 新增充值-单价
     */
    public int insertTHmPayhistoryPrice(THmPayhistoryPrice tHmPayhistoryPrice);

    /**
     * 修改充值-单价
     */
    public int updateTHmPayhistoryPrice(THmPayhistoryPrice tHmPayhistoryPrice);

    public List<THmPayhistoryPrice> gou(THmPayhistoryPrice tHmPayhistoryPrice);
}
