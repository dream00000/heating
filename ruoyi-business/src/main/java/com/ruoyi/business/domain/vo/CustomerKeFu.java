package com.ruoyi.business.domain.vo;

import lombok.Data;

@Data
public class CustomerKeFu {
    //id
    private Integer customerId;
    //客户姓名
    private String customerName;
    //客户编号
    private String customerCode;
    //换热站
    private String customerStation;
    //小区
    private String customerArea;
    //楼
    private String lou;
    //单元
    private String dan;
    //门牌号
    private String hao;
    //收费面积
    private String homeArea;
    //房屋性质
    private String type;
    //联系电话
    private String customerMobile;
}
