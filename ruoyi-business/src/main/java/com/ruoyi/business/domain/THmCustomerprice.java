package com.ruoyi.business.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 用户单价对象 t_hm_customerprice
 *
 * @author ruoyi
 * @date 2021-09-23
 */
@Data
public class THmCustomerprice extends BaseEntity {

    private Integer id;

    /**
     * 用户外键
     */
    @Excel(name = "用户外键")
    private Integer customerId;

    /**
     * 单价外键
     */
    @Excel(name = "单价外键")
    private Integer priceId;

    /**
     * 状态
     */
    @Excel(name = "状态")
    private Integer state;

    /**
     * 面积
     */
    @Excel(name = "面积")
    private String area;

    /**
     * 说明
     */
    @Excel(name = "说明")
    private String remark;

    /**
     * 总价
     */
    @Excel(name = "总价")
    private String total;

    /**
     * 阀门状态
     */
    @Excel(name = "阀门状态,", readConverterExp = "0=阀开,1=阀关,2=断管")
    private Integer tap;

    private String name;
    private String price;
    private String shiTotal;
    private Integer type;
    //系数
    private String xishu;
    //阀控时间
    private String topTime;
}
