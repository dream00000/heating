package com.ruoyi.business.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 分区账期对象 t_hm_paydays
 *
 * @author ruoyi
 * @date 2021-08-13
 */
@Data
public class THmPaydays extends BaseEntity {

    /**
     * 账期ID
     */
    @Excel(name = "账期ID")
    private Integer hzqId;

    /**
     * 账期年度yyyy-yyyy
     */
    @Excel(name = "账期策略名称")
    private String hzqDate;

    /**
     * 开始缴费日期
     */
    @Excel(name = "计费开始日期")
    private String hzqStartime;

    /**
     * 计划结束日期
     */
    @Excel(name = "账单生成日期")
    private String hzqEndtime;

    /**
     * 供热开始时间
     */
    @Excel(name = "供热开始时间")
    private String hzqGrstarttime;

    /**
     * 供热结束时间
     */
    @Excel(name = "供热结束时间")
    private String hzqGrendtime;

    /**
     * 供热天数
     */
    @Excel(name = "供热天数")
    private Integer hzqGrdays;

    /**
     * 微信消息推送提前x天
     */
    @Excel(name = "微信消息推送提前x天")
    private Integer hzqMsgday;

    /**
     * 发送次数
     */
    @Excel(name = "发送次数")
    private Integer hzqMsgsentcount;

    /**
     * 是否删除0否1是
     */
    @Excel(name = "是否删除0否1是")
    private Integer hzqState;

    /**
     * 添加时间
     */
    @Excel(name = "添加时间")
    private String hzqAddtime;

    /**
     * 添加人
     */
    @Excel(name = "添加人")
    private Long hzqAdduserid;

    /**
     * 备用字段
     */
    @Excel(name = "备用字段")
    private Integer hzqInt1;

    /**
     * 备用字段
     */
    @Excel(name = "备用字段")
    private Integer hzqInt2;

    /**
     * 备用字段
     */
    @Excel(name = "备用字段")
    private Integer hzqInt3;

    /**
     * 备用字段
     */
    @Excel(name = "备用字段")
    private Integer hzqInt4;

    /**
     * 备用字段
     */
    @Excel(name = "实际供热开始时间")
    private String hzqStr1;

    /**
     * 备用字段
     */
    @Excel(name = "实际供热结束时间")
    private String hzqStr2;

    /**
     * 备用字段
     */
    @Excel(name = "实际供热天数")
    private String hzqStr3;

    /**
     * 备用字段
     */
    @Excel(name = "备用字段")
    private String hzqStr4;

    private String paydays;

    //零钱计算方式
    private Integer ling;

    private Integer[] menuIds;
}
