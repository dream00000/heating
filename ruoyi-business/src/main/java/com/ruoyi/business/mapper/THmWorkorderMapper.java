package com.ruoyi.business.mapper;

import com.ruoyi.business.domain.THmWorkorder;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 客服工单Mapper接口
 *
 * @author ruoyi
 * @date 2022-02-26
 */
public interface THmWorkorderMapper {
    /**
     * 查询客服工单
     *
     * @param id
     * @return 客服工单
     */
    THmWorkorder selectTHmWorkorderById(@Param("id") int id);

    /**
     * 查询客服工单列表
     *
     * @param tHmWorkorder 客服工单
     * @return 客服工单集合
     */
    List<THmWorkorder> selectTHmWorkorderList(THmWorkorder tHmWorkorder);

    int insertTHmWorkorder(THmWorkorder tHmWorkorder);
}
