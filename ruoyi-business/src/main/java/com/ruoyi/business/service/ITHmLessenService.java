package com.ruoyi.business.service;

import com.ruoyi.business.domain.vo.LessenVO;
import com.ruoyi.business.domain.THmCustomer;
import com.ruoyi.business.domain.THmLessen;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用热减免Service接口
 *
 * @author ruoyi
 * @date 2021-08-13
 */
public interface ITHmLessenService {
    /**
     * 查询用热减免
     *
     * @param id
     * @return 用热减免
     */
    public THmLessen selectTHmLessenById(@Param("id") int id);

    public THmLessen selectTHmLessenByIds(@Param("id") int id);

    /**
     * 查询用热减免列表
     *
     * @param tHmLessen 用热减免
     * @return 用热减免集合
     */
    public List<THmLessen> selectTHmLessenList(THmLessen tHmLessen);

    public List<LessenVO> selectTHmLessenListt(THmCustomer tHmCustomer);

    /**
     * 新增用热减免
     *
     * @param tHmLessen 用热减免
     * @return 结果
     */
    public int insertTHmLessen(THmLessen tHmLessen);

    /**
     * 修改用热减免
     *
     * @param tHmLessen 用热减免
     * @return 结果
     */
    public int updateTHmLessen(THmLessen tHmLessen);

}
