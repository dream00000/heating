package com.ruoyi.business.service;

import com.ruoyi.business.domain.THmStation;
import com.ruoyi.common.core.domain.Ztree;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 热源站Service接口
 */
public interface ITHmStationService {
    /**
     * 查询热源站
     */
    public THmStation selectTHmStationById(@Param("id") Long id);

    //获取parentId下边的id最大的区域 -- 用于计算code
    public THmStation getMaxCode(THmStation tHmStation);

    /**
     * 查询热源站列表
     */
    public List<THmStation> selectTHmStationList(THmStation tHmStation);

    /**
     * 新增热源站
     */
    public int insertTHmStation(THmStation tHmStation);

    /**
     * 修改热源站
     */
    public int updateTHmStation(THmStation tHmStation);


    /**
     * 查询热源站树列表
     */
    public List<Ztree> selectTHmStationTree();
}
