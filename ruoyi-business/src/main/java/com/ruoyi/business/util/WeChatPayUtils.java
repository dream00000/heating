package com.ruoyi.business.util;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.ijpay.core.enums.SignType;
import com.ijpay.core.kit.WxPayKit;
import com.ijpay.wxpay.WxPayApi;
import com.ijpay.wxpay.model.UnifiedOrderModel;
import com.ruoyi.business.domain.PaymentRecord;
import com.ruoyi.business.domain.WechatUser;
import com.ruoyi.business.domain.WxPayBean;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;


@Component
public class WeChatPayUtils {
    //生成微信支付信息
    public Map<String, String> getWeChatPayJSON(WxPayBean wxPayBean, WechatUser wechatUser, PaymentRecord paymentRecord, String IP, String notify) {
        Map<String, String> returnMap = new HashMap<>();
        String wechatmoney = new BigDecimal(paymentRecord.getPaymentPayMoney()).multiply(new BigDecimal(100)).divide(new BigDecimal(1), 0, RoundingMode.DOWN).toString();
        Map<String, String> params = UnifiedOrderModel
                .builder()
                .appid(wxPayBean.getAppId())
                .mch_id(wxPayBean.getMchId())
                .nonce_str(WxPayKit.generateStr())
                .body("供热缴费")
                .attach(paymentRecord.getPaymentId().toString())
                .out_trade_no(paymentRecord.getPaymentOrder())
                .total_fee(wechatmoney)
                .spbill_create_ip(IP)
                .notify_url(wxPayBean.getDomain() + notify)
                .trade_type("JSAPI")
                .openid(paymentRecord.getPaymentCreateOpenId())
                .build()
                .createSign(wxPayBean.getPartnerKey(), SignType.HMACSHA256);
        String xmlResult = WxPayApi.pushOrder(false, params);
        Map<String, String> resultMap = WxPayKit.xmlToMap(xmlResult);
        String returnCode = resultMap.get("return_code");
        String returnMsg = resultMap.get("return_msg");
        if (!WxPayKit.codeIsOk(returnCode)) {
            returnMap.put("code", "0");
            returnMap.put("data", returnMsg);
            return returnMap;
        }
        String resultCode = resultMap.get("result_code");
        if (!WxPayKit.codeIsOk(resultCode)) {
            returnMap.put("code", "0");
            returnMap.put("data", returnMsg);
            return returnMap;
        }
        // 以下字段在 return_code 和 result_code 都为 SUCCESS 的时候有返回
        String prepayId = resultMap.get("prepay_id");
        Map<String, String> packageParams = WxPayKit.prepayIdCreateSign(prepayId, wxPayBean.getAppId(),
                wxPayBean.getPartnerKey(), SignType.HMACSHA256);
        returnMap.put("code", "1");
        returnMap.put("data", JSON.toJSONString(packageParams));

        return returnMap;
    }

    //校验微信支付是否成功
    public Map<String, Object> paymentRecordIsPay(WxPayBean wxPayBean, WechatUser wechatUser, PaymentRecord paymentRecord) {
        Map<String, Object> returnMap = new HashMap<>();
        String wechatmoney = new BigDecimal(paymentRecord.getPaymentPayMoney()).multiply(new BigDecimal(100)).divide(new BigDecimal(1), 0, RoundingMode.DOWN).toString();
        Map<String, String> params = UnifiedOrderModel
                .builder()
                .appid(wxPayBean.getAppId())
                .mch_id(wxPayBean.getMchId())
                .out_trade_no(paymentRecord.getPaymentOrder())
                .nonce_str(WxPayKit.generateStr())
                .build()
                .createSign(wxPayBean.getPartnerKey(), SignType.HMACSHA256);
        String xmlResult = WxPayApi.orderQuery(false, params);
        Map<String, String> resultMap = WxPayKit.xmlToMap(xmlResult);
        String returnCode = resultMap.get("return_code");
        String returnMsg = resultMap.get("return_msg");
        String resultCode = resultMap.get("result_code");
        String tradeState = resultMap.get("trade_state");
        String attach = resultMap.get("attach");
        if (WxPayKit.codeIsOk(returnCode) && WxPayKit.codeIsOk(resultCode) && WxPayKit.codeIsOk(tradeState) && StrUtil.equals(wechatmoney, resultMap.get("total_fee")) && StrUtil.equals(wechatUser.getOpenId(), resultMap.get("openid")) && (StrUtil.equals(attach, paymentRecord.getPaymentId().toString()))) {

            paymentRecord.setPaymentPayStuts("1");
            paymentRecord.setPaymentPayDate(cn.hutool.core.date.DateUtil.date());
            paymentRecord.setPaymentPayOpenId(resultMap.get("openid"));
            paymentRecord.setPaymentPayType("1");
            paymentRecord.setPaymentWechatOrder(resultMap.get("transaction_id"));
            paymentRecord.setPaymentWechatDate(DateUtil.parse(resultMap.get("time_end"), "yyyyMMddHHmmss"));

            returnMap.put("msg", returnMsg);
            returnMap.put("stuts", true);
            returnMap.put("paymentRecord", paymentRecord);

            return returnMap;
        } else {
            returnMap.put("msg", returnMsg);
            returnMap.put("stuts", false);
            returnMap.put("paymentRecord", paymentRecord);
            return returnMap;
        }
    }

    //验证微信通知有消息；
    public boolean WxVerifyNotify(WxPayBean wxPayBean, Map<String, String> params) {
        if (WxPayKit.verifyNotify(params, wxPayBean.getPartnerKey(), SignType.HMACSHA256)) {
            return WxPayKit.codeIsOk(params.get("return_code"));
        }
        return false;
    }
}
