package com.ruoyi.business.controller;

import com.ruoyi.business.domain.THmCustomer;
import com.ruoyi.business.domain.THmCustomerpaydays;
import com.ruoyi.business.domain.THmLessen;
import com.ruoyi.business.domain.vo.LessenVO;
import com.ruoyi.business.service.ITHmCustomerService;
import com.ruoyi.business.service.ITHmCustomerpaydaysService;
import com.ruoyi.business.service.ITHmLessenService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用热减免Controller
 *
 * @author ruoyi
 * @date 2021-08-13
 */
@Controller
@RequestMapping("/business/lessen")
public class THmLessenController extends BaseController {
    private String prefix = "business/lessen";

    @Resource
    private ITHmLessenService tHmLessenService;

    @Resource
    private ITHmCustomerService tHmCustomerService;

    @Resource
    private ITHmCustomerpaydaysService itHmCustomerpaydaysService;

    @GetMapping()
    public String customer() {
        return prefix + "/customer";
    }

    /**
     * 查询用户信息列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(THmCustomer tHmCustomer) {
        startPage();
        List<THmCustomer> list = tHmCustomerService.selectTHmCustomerList(tHmCustomer);
        return getDataTable(list);
    }

    @GetMapping("/lessenList")
    public String lessenList() {
        return prefix + "/lessenlist";
    }

    /**
     * 查询退费列表
     */
    @PostMapping("/listt")
    @ResponseBody
    public TableDataInfo listt(THmCustomer tHmCustomer) {
        startPage();
        List<LessenVO> list = tHmLessenService.selectTHmLessenListt(tHmCustomer);
        return getDataTable(list);
    }

    @GetMapping("/{id}")
    public String lessen(@PathVariable("id") int id, Model model) {
        Map<String, Object> map = new HashMap<>();
        map.put("hcustomerId", id);
        model.addAllAttributes(map);
        return prefix + "/lessen";
    }

    @GetMapping("zhangqi/{id}")
    public String zhangqi(@PathVariable("id") int id, Model model) {
        Map<String, Object> map = new HashMap<>();
        map.put("hcustomerId", id);
        model.addAllAttributes(map);
        return prefix + "/customerpaydays";
    }

    /**
     * 查询用热减免列表
     */
    @PostMapping("/list/{id}")
    @ResponseBody
    public TableDataInfo list(@PathVariable("id") Integer id) {
        THmLessen tHmLessen = new THmLessen();
        tHmLessen.setHcustomerId(id);
        List<THmLessen> list = tHmLessenService.selectTHmLessenList(tHmLessen);
        return getDataTable(list);
    }

    /**
     * 导出用热减免列表
     */
    @PostMapping("/export")
    @ResponseBody
    @Log(title = "导出减免信息", businessType = BusinessType.EXPORT)
    public AjaxResult export(THmLessen tHmLessen) {
        List<THmLessen> list = tHmLessenService.selectTHmLessenList(tHmLessen);
        ExcelUtil<THmLessen> util = new ExcelUtil<THmLessen>(THmLessen.class);
        return util.exportExcel(list, "lessen");
    }

    /**
     * 导出用热减免列表
     */
    @PostMapping("/exportt")
    @ResponseBody
    @Log(title = "导出减免信息", businessType = BusinessType.EXPORT)
    public AjaxResult exportt(THmCustomer tHmCustomer) {
        List<LessenVO> list = tHmLessenService.selectTHmLessenListt(tHmCustomer);
        ExcelUtil<LessenVO> util = new ExcelUtil<>(LessenVO.class);
        return util.exportExcel(list, "减免");
    }

    /**
     * 新增用热减免
     */
    @GetMapping("/add/{id}")
    public String add(@PathVariable("id") int id, ModelMap mmap) {
        THmCustomerpaydays tHmCustomerpaydays = itHmCustomerpaydaysService.selectTHmCustomerpaydaysById(id);
        THmLessen tHmLessen = tHmLessenService.selectTHmLessenById(tHmCustomerpaydays.getHcustomerId());
        tHmLessen.setJmYear(tHmCustomerpaydays.getHzqGrstarttime());
        tHmLessen.setQian(tHmCustomerpaydays.getWqStr1());
        tHmLessen.setWqId(tHmCustomerpaydays.getWqId());
        mmap.put("tHmLessen", tHmLessen);
        return prefix + "/add";
    }

    /**
     * 新增保存用热减免
     */
    @Log(title = "新增用热减免", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(THmLessen tHmLessen) {
        return AjaxResult.success("优惠减免成功", tHmLessenService.insertTHmLessen(tHmLessen));
    }

    /**
     * 修改用热减免
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") int id, ModelMap mmap) {
        THmLessen tHmLessen = tHmLessenService.selectTHmLessenById(id);
        mmap.put("tHmLessen", tHmLessen);
        return prefix + "/edit";
    }

    /**
     * 修改保存用热减免
     */
    @Log(title = "修改用热减免", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(THmLessen tHmLessen) {
        return toAjax(tHmLessenService.updateTHmLessen(tHmLessen));
    }


    /**
     * 删除用热减免
     */
    @Log(title = "删除用热减免", businessType = BusinessType.DELETE)
    @PostMapping("/remove/{id}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("id") int id) {
        THmLessen tHmLessen = new THmLessen();
        tHmLessen.setJmId(id);
        tHmLessen.setJmState(1);
        return toAjax(tHmLessenService.updateTHmLessen(tHmLessen));
    }
}
