package com.ruoyi.business.service;

import com.ruoyi.business.domain.THmEnclosure;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 附件Service接口
 */
public interface ITHmEnclosureService {
    /**
     * 查询附件
     */
    public THmEnclosure selectTHmEnclosureById(@Param("id") int id);

    /**
     * 查询附件列表
     */
    public List<THmEnclosure> selectTHmEnclosureList(THmEnclosure tHmEnclosure);

    /**
     * 新增附件
     */
    public int insertTHmEnclosure(THmEnclosure tHmEnclosure);

    /**
     * 修改附件
     */
    public int updateTHmEnclosure(THmEnclosure tHmEnclosure);

    /**
     * 删除
     */
    public void delEnclosure(@Param("id") int id);
}
