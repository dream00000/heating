package com.ruoyi.business.mapper;

import com.ruoyi.business.domain.THmCustomerpaydaysRecord;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 账期操作日志Mapper接口
 */
public interface THmCustomerpaydaysRecordMapper {
    /**
     * 查询账期操作日志
     */
    THmCustomerpaydaysRecord selectTHmCustomerpaydaysRecordById(@Param("id") Long id);

    THmCustomerpaydaysRecord getRecordByWqId(@Param("customerpaydaysId") int customerpaydaysId);

    /**
     * 查询账期操作日志列表
     */
    List<THmCustomerpaydaysRecord> selectTHmCustomerpaydaysRecordList(THmCustomerpaydaysRecord tHmCustomerpaydaysRecord);

    /**
     * 新增账期操作日志
     */
    int insertTHmCustomerpaydaysRecord(THmCustomerpaydaysRecord tHmCustomerpaydaysRecord);

    /**
     * 修改账期操作日志
     */
    int updateTHmCustomerpaydaysRecord(THmCustomerpaydaysRecord tHmCustomerpaydaysRecord);
}
