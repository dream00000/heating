package com.ruoyi.business.domain;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 客服工单对象 t_hm_workorder
 *
 * @author ruoyi
 * @date 2022-02-26
 */
@Data
public class THmNameList extends BaseEntity {
    private static final long serialVersionUID = 1L;

    //主键
    private Integer id;

    //用户id
    private Integer customerId;

    //标题
    private String title;

    //类型 0黑名单 1白名单
    private Integer type;

    //内容
    private String content;

    //添加人
    private Long sysUser;
    private String addUser;

    //添加时间
    private String addTime;

    //显示以及查询
    private Integer hareaId;
    private String customerCode;
    private String customerName;
    private String customerMobile;
    private String customerPersonlcard;
    private String areaName;
    private String customerDetailaddress;
}
