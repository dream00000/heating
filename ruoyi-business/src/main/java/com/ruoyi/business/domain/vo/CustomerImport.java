package com.ruoyi.business.domain.vo;

import com.ruoyi.business.domain.THmCustomer;
import lombok.Data;

import java.util.List;

//导入数据验证返回
@Data
public class CustomerImport {

    //数据状态 0：去全部合格 1：有不合格
    private Integer dataStatus;

    private List<THmCustomer> list;
}
