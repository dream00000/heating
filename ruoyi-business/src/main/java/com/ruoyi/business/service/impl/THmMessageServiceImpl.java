package com.ruoyi.business.service.impl;

import com.ruoyi.business.domain.THmMessage;
import com.ruoyi.business.mapper.THmMessageMapper;
import com.ruoyi.business.service.ITHmMessageService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 推送消息记录Service业务层处理
 *
 * @author ruoyi
 * @date 2022-03-12
 */
@Service
public class THmMessageServiceImpl implements ITHmMessageService {
    @Resource
    private THmMessageMapper tHmMessageMapper;

    /**
     * 查询推送消息记录
     *
     * @param id
     * @return 推送消息记录
     */
    @Override
    public THmMessage selectTHmMessageById(int id) {
        return tHmMessageMapper.selectTHmMessageById(id);
    }

    /**
     * 查询推送消息记录列表
     *
     * @param tHmMessage 推送消息记录
     * @return 推送消息记录
     */
    @Override
    public List<THmMessage> selectTHmMessageList(THmMessage tHmMessage) {
        return tHmMessageMapper.selectTHmMessageList(tHmMessage);
    }

    /**
     * 新增推送消息记录
     *
     * @param tHmMessage 推送消息记录
     * @return 结果
     */
    @Override
    public int insertTHmMessage(THmMessage tHmMessage) {
        return tHmMessageMapper.insertTHmMessage(tHmMessage);
    }

    /**
     * 修改推送消息记录
     *
     * @param tHmMessage 推送消息记录
     * @return 结果
     */
    @Override
    public int updateTHmMessage(THmMessage tHmMessage) {
        return tHmMessageMapper.updateTHmMessage(tHmMessage);
    }

}
