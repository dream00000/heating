package com.ruoyi.business.service;

import com.ruoyi.business.domain.THmDelay;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 延期记录Service接口
 *
 * @author ruoyi
 * @date 2022-03-10
 */
public interface ITHmDelayService {
    /**
     * 查询延期记录
     *
     * @param id
     * @return 延期记录
     */
    public THmDelay selectTHmDelayById(@Param("id") int id);

    /**
     * 查询延期记录列表
     *
     * @param tHmDelay 延期记录
     * @return 延期记录集合
     */
    public List<THmDelay> selectTHmDelayList(THmDelay tHmDelay);

    /**
     * 新增延期记录
     *
     * @param tHmDelay 延期记录
     * @return 结果
     */
    public int insertTHmDelay(THmDelay tHmDelay);

    /**
     * 修改延期记录
     *
     * @param tHmDelay 延期记录
     * @return 结果
     */
    public int updateTHmDelay(THmDelay tHmDelay);

    public void deleteTHmDelay(@Param("id") int id);
}
