package com.ruoyi.business.domain.vo;

import lombok.Data;

import java.util.List;

@Data
public class BaoInfoList {

    private Integer count;
    private Integer total;
    private String message;
    private String code;
    private List<BaoDataList> data;
}
