package com.ruoyi.business.controller;

import com.ruoyi.business.domain.*;
import com.ruoyi.business.mapper.THmCustomerMapper;
import com.ruoyi.business.mapper.THmCustomerpaydaysMapper;
import com.ruoyi.business.mapper.THmCustomerpriceMapper;
import com.ruoyi.business.service.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.config.Global;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.service.ISysDictDataService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 用户信息Controller
 *
 * @author ruoyi
 * @date 2021-08-13
 */
@Controller
@RequestMapping("/business/open")
public class THmOpenController extends BaseController {
    private String prefix = "business/open";

    @Resource
    private ITHmCustomerService tHmCustomerService;


    @Resource
    private THmCustomerMapper tHmCustomerMapper;

    @Resource
    private ITHmFeedbackService itHmFeedbackService;

    @Resource
    private ITHmCustomerpriceService itHmCustomerpriceService;

    @Resource
    private ITHmCustomerpaydaysService itHmCustomerpaydaysService;

    @Resource
    private ITHmMeterpriceService itHmMeterpriceService;

    @Resource
    private ITHmEnclosureService itHmEnclosureService;

    @Resource
    private ITHmCustomerpaydaysPriceService itHmCustomerpaydaysPriceService;

    @Resource
    private THmCustomerpaydaysMapper tHmCustomerpaydaysMapper;

    @Resource
    private THmCustomerpriceMapper tHmCustomerpriceMapper;

    @Resource
    private ISysDictDataService iSysDictDataService;

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @GetMapping()
    public String customer() {
        return prefix + "/customer";
    }

    @GetMapping("/opens")
    public String customers() {
        return prefix + "/customers";
    }

    /**
     * 查询用户信息列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(THmCustomer tHmCustomer) {
        if ("fou".equals(tHmCustomer.getBalance())) {
            tHmCustomer.setHcustomerStr4("fou");
        } else if ("shi".equals(tHmCustomer.getBalance())) {
            tHmCustomer.setHcustomerStr5("shi");
        }
        startPage();
        List<THmCustomer> list = tHmCustomerService.selectTHmCustomerList(tHmCustomer);
        return getDataTable(list);
    }

    /**
     * 查询用户信息列表
     */
    @PostMapping("/areaList")
    @ResponseBody
    public TableDataInfo areaList(THmCustomer tHmCustomer) {
        startPage();
        List<THmCustomer> list = tHmCustomerService.selectTHmCustomerList(tHmCustomer);
        return getDataTable(list);
    }

    /**
     * 导出用户信息列表
     */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(THmCustomer tHmCustomer) {
//        int count = tHmCustomerMapper.exportCount(tHmCustomer);
//        if (count > 9999) {
//            return AjaxResult.error("导出数据不能超过9999");
//        } else {
        List<THmCustomer> list = tHmCustomerService.selectTHmCustomerList(tHmCustomer);
        ExcelUtil<THmCustomer> util = new ExcelUtil<THmCustomer>(THmCustomer.class);
        return util.exportExcel(list, "customer");
//        }
    }

    /**
     * 修改用户信息
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") int id, ModelMap mmap) {
        THmCustomer tHmCustomer = tHmCustomerService.selectTHmCustomerById(id);
        mmap.put("tHmCustomer", tHmCustomer);
        return prefix + "/edit";
    }

    /**
     * 修改用户信息
     */
    @GetMapping("/bian/{id}")
    public String bian(@PathVariable("id") int id, ModelMap mmap) {
        THmCustomer tHmCustomer = tHmCustomerService.selectTHmCustomerById(id);
        mmap.put("tHmCustomer", tHmCustomer);
        return prefix + "/bian";
    }

    /**
     * 查看单价列表
     */
    @GetMapping("/dan/{id}")
    public String dan(@PathVariable("id") int id, ModelMap mmap) {
        THmCustomer tHmCustomer = tHmCustomerService.selectTHmCustomerById(id);
        mmap.put("tHmCustomer", tHmCustomer);
        return prefix + "/dan";
    }

    /**
     * 阀控管理的阀控操作跳转
     */
    @GetMapping("/top/{id}")
    public String top(@PathVariable("id") int id, ModelMap mmap) {
        THmCustomer tHmCustomer = tHmCustomerService.selectTHmCustomerById(id);
        mmap.put("tHmCustomer", tHmCustomer);
        return "business/feedback" + "/top";
    }

    /**
     * 图片上传
     */
    @GetMapping("/img")
    public String img(int id, ModelMap mmap) {
        THmCustomer tHmCustomer = tHmCustomerService.selectTHmCustomerById(id);
        if (tHmCustomer.getHcustomerContactcode() != null) {
            THmEnclosure tHmEnclosure = new THmEnclosure();
            tHmEnclosure.setEnFjcode(tHmCustomer.getHcustomerContactcode());
            List<THmEnclosure> tHmEnclosureList = itHmEnclosureService.selectTHmEnclosureList(tHmEnclosure);
            tHmCustomer.setEnclosureList(tHmEnclosureList);
        }
        mmap.put("tHmCustomer", tHmCustomer);
        return prefix + "/img";
    }

    /**
     * 图片上传
     */
    @GetMapping("/imgs")
    public String imgs(int id, ModelMap mmap) {
        THmCustomer tHmCustomer = tHmCustomerService.selectTHmCustomerById(id);
        if (tHmCustomer.getHcustomerContactcode() != null) {
            THmEnclosure tHmEnclosure = new THmEnclosure();
            tHmEnclosure.setEnFjcode(tHmCustomer.getHcustomerContactcode());
            List<THmEnclosure> tHmEnclosureList = itHmEnclosureService.selectTHmEnclosureList(tHmEnclosure);
            tHmCustomer.setEnclosureList(tHmEnclosureList);
        }
        mmap.put("tHmCustomer", tHmCustomer);
        return prefix + "/imgs";
    }

    /**
     * 删除图片上传
     */
    @GetMapping("/delImg")
    @ResponseBody
    @Log(title = "删除照片", businessType = BusinessType.DELETE)
    public AjaxResult delImg(int id) {
        itHmEnclosureService.delEnclosure(id);
        return success();
    }

    /**
     * 修改保存用户信息
     */
    @Log(title = "拍照", businessType = BusinessType.UPDATE)
    @PostMapping("/edits")
    @ResponseBody
    public AjaxResult editSaves(THmCustomer tHmCustomer, @RequestParam("file") MultipartFile file) {
        try {
            if (!file.isEmpty()) {
                String url = FileUploadUtils.upload(Global.getAvatarPath(), file);
                //获取用户
                THmCustomer hmCustomer = tHmCustomerService.selectTHmCustomerById(tHmCustomer.getHcustomerId());
                THmEnclosure tHmEnclosure = new THmEnclosure();
                if (hmCustomer.getHcustomerContactcode() == null) {
                    tHmCustomer.setHcustomerContactcode(String.valueOf(System.currentTimeMillis()));
                    tHmCustomerService.updateTHmCustomer(tHmCustomer);
                    tHmEnclosure.setEnFjcode(tHmCustomer.getHcustomerContactcode());
                    tHmEnclosure.setEnFjurl(url);
                    tHmEnclosure.setEnAddtime(sdf.format(new Date()));
                    tHmEnclosure.setEnState(0);
                    tHmEnclosure.setEnZcstate(1);//正式
                    itHmEnclosureService.insertTHmEnclosure(tHmEnclosure);
                } else {
                    tHmEnclosure.setEnFjcode(hmCustomer.getHcustomerContactcode());
                    tHmEnclosure.setEnFjurl(url);
                    tHmEnclosure.setEnAddtime(sdf.format(new Date()));
                    tHmEnclosure.setEnState(0);
                    tHmEnclosure.setEnZcstate(1);//正式
                    itHmEnclosureService.insertTHmEnclosure(tHmEnclosure);
                }
                return AjaxResult.success("成功", tHmEnclosure.getEnId());
            }
            return error();
        } catch (Exception e) {
            return error(e.getMessage());
        }
    }

    /**
     * 自动生成编号，返回最大编号
     */
    @PostMapping("/getCode")
    @ResponseBody
    public AjaxResult getCode() {
        Integer i = tHmCustomerService.getMaxCode();
        if (i == null) {
            i = 0;
        }
        return AjaxResult.success("成功", String.format("%07d", i + 1));
    }

    /**
     * 用户修改房屋面积
     */
    @Log(title = "开户", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult edit(@RequestBody THmCustomer tHmCustomer) {
        tHmCustomer.setHcustomerKhuserid(ShiroUtils.getSysUser().getUserId());
        tHmCustomer.setHcustomerKhtime(sdf.format(new Date()));
        tHmCustomer.setHcustomerKhstate(1);
        return toAjax(tHmCustomerService.updateTHmCustomer(tHmCustomer));
    }

    /**
     * 用户修改房屋面积
     */
    @Log(title = "复合调整", businessType = BusinessType.UPDATE)
    @PostMapping("/editm")
    @ResponseBody
    public AjaxResult editm(@RequestBody THmCustomer tHmCustomer) {
        //获取当前用户
        //取出用户最后一条账期
        THmCustomerpaydays tHmCustomerpaydays = tHmCustomerpaydaysMapper.getLast(tHmCustomer.getHcustomerId());
        //第一步先把基本信息修改了
        //添加用户单价
        if (tHmCustomer.getCustomerpriceList() != null) {
            //先将用户关联的单价删除
            itHmCustomerpriceService.delTHmCustomerprice(tHmCustomer.getHcustomerId());
            for (THmCustomerprice tHmCustomerprice : tHmCustomer.getCustomerpriceList()) {
                BigDecimal bd = new BigDecimal(tHmCustomerprice.getArea());//面积
                THmMeterprice tHmMeterprice = itHmMeterpriceService.selectTHmMeterpriceById(tHmCustomerprice.getPriceId());
                BigDecimal bds = new BigDecimal(tHmMeterprice.getRjPrice());//单价
                tHmCustomerprice.setCustomerId(tHmCustomer.getHcustomerId());//用户外键
                tHmCustomerprice.setTotal(bd.multiply(bds).multiply(new BigDecimal(tHmCustomerprice.getXishu())).divide(new BigDecimal("100")).toString());
                tHmCustomerprice.setState(0);
                tHmCustomerprice.setRemark(tHmCustomerprice.getXishu());

                //再重新增加
                if (tHmCustomerprice.getId() != null) {
                    tHmCustomerpriceMapper.insertTHmCustomerprices(tHmCustomerprice);
                } else {
                    itHmCustomerpriceService.insertTHmCustomerprice(tHmCustomerprice);

                    THmFeedback tHmFeedback = new THmFeedback();
                    tHmFeedback.setFdType(1);//开关阀操作
                    if (tHmCustomerprice.getTap() == 1) {
                        tHmFeedback.setFdFmzt(0);
                    } else if (tHmCustomerprice.getTap() == 0) {
                        tHmFeedback.setFdFmzt(1);
                    } else if (tHmCustomerprice.getTap() == 2) {
                        tHmFeedback.setFdFmzt(2);
                    }
                    tHmFeedback.setHcustomerId(tHmCustomer.getHcustomerId());
                    tHmFeedback.setFdAddtime(sdf.format(new Date()));
                    tHmFeedback.setFdAdduserName(ShiroUtils.getSysUser().getUserName());
                    tHmFeedback.setFdState(0);
                    tHmFeedback.setFdTitle("开关阀操作");
                    tHmFeedback.setCustomerpriceId(Long.valueOf(tHmCustomerprice.getId()));
                    tHmFeedback.setFdContent("复合调整时" + ShiroUtils.getSysUser().getUserName() + "进行了阀控操作");
                    itHmFeedbackService.insertTHmFeedback(tHmFeedback);
                }
            }
        }
        tHmCustomerService.updateTHmCustomer(tHmCustomer);
        //第二步再把账期修改了
        if (tHmCustomerpaydays != null) {
            //有账期 查一下账期状态
            //缴了费的账期调整的时候只能调整系数和金额，面积不能动
            if (tHmCustomerpaydays.getWqInt1() == 0 || tHmCustomerpaydays.getWqInt1() == 1 || tHmCustomerpaydays.getWqInt1() == 2 || tHmCustomerpaydays.getWqInt1() == 4 || tHmCustomerpaydays.getWqInt1() == 5 || tHmCustomerpaydays.getWqInt1() == 6) {
                //修改账期
                THmCustomerpaydays hmCustomerpaydays = new THmCustomerpaydays();
                hmCustomerpaydays.setWqId(tHmCustomerpaydays.getWqId());
                hmCustomerpaydays.setWqStr1(tHmCustomer.getTotal());
                hmCustomerpaydays.setPayType(tHmCustomer.getPayType());
                itHmCustomerpaydaysService.updateTHmCustomerpaydays(hmCustomerpaydays);

                //修改本次账期相关的历史记录
//                itHmCustomerpaydaysPriceService.delTHmCustomerpaydaysPrice(tHmCustomerpaydays.getWqId());
//                for (THmCustomerprice tHmCustomerprice : tHmCustomer.getCustomerpriceList()) {
//                    THmCustomerpaydaysPrice tHmCustomerpaydaysPrice = new THmCustomerpaydaysPrice();
//                    tHmCustomerpaydaysPrice.setAddTime(new Date());
//                    tHmCustomerpaydaysPrice.setAddUser(ShiroUtils.getSysUser().getUserId());
//                    THmMeterprice tHmMeterprice1 = itHmMeterpriceService.selectTHmMeterpriceById(tHmCustomerprice.getPriceId());
//                    tHmCustomerpaydaysPrice.setPrice(tHmMeterprice1.getRjPrice());
//                    tHmCustomerpaydaysPrice.setCustomerpaydaysId(tHmCustomerpaydays.getWqId());
//                    tHmCustomerpaydaysPrice.setPriceType(iSysDictDataService.selectDictLabel("data_price", tHmMeterprice1.getRjDjid().toString()));
//                    tHmCustomerpaydaysPrice.setType(tHmCustomerprice.getType());
//                    tHmCustomerpaydaysPrice.setXishu(tHmCustomerprice.getRemark());
//                    itHmCustomerpaydaysPriceService.insertTHmCustomerpaydaysPrice(tHmCustomerpaydaysPrice);
//                }
            } else {
                //修改账期
                THmCustomerpaydays hmCustomerpaydays = new THmCustomerpaydays();
                hmCustomerpaydays.setWqId(tHmCustomerpaydays.getWqId());
                hmCustomerpaydays.setHcustomerActualarea(tHmCustomer.getHcustomerActualarea());
                hmCustomerpaydays.setHcustomerAbovearea(tHmCustomer.getHcustomerAbovearea());
                hmCustomerpaydays.setHcustomerHomearea(tHmCustomer.getHcustomerHomearea());
                hmCustomerpaydays.setHcustomerTgarea(tHmCustomer.getHcustomerTgarea());
                hmCustomerpaydays.setWqStr1(tHmCustomer.getTotal());
                hmCustomerpaydays.setPayType(tHmCustomer.getPayType());
                itHmCustomerpaydaysService.updateTHmCustomerpaydays(hmCustomerpaydays);

                //修改本次账期相关的历史记录
                itHmCustomerpaydaysPriceService.delTHmCustomerpaydaysPrice(tHmCustomerpaydays.getWqId());
                for (THmCustomerprice tHmCustomerprice : tHmCustomer.getCustomerpriceList()) {
                    THmCustomerpaydaysPrice tHmCustomerpaydaysPrice = new THmCustomerpaydaysPrice();
                    tHmCustomerpaydaysPrice.setAddTime(new Date());
                    tHmCustomerpaydaysPrice.setAddUser(ShiroUtils.getSysUser().getUserId());
                    THmMeterprice tHmMeterprice1 = itHmMeterpriceService.selectTHmMeterpriceById(tHmCustomerprice.getPriceId());
                    tHmCustomerpaydaysPrice.setPrice(tHmMeterprice1.getRjPrice());
                    tHmCustomerpaydaysPrice.setCustomerpaydaysId(tHmCustomerpaydays.getWqId());
                    tHmCustomerpaydaysPrice.setPriceType(iSysDictDataService.selectDictLabel("data_price", tHmMeterprice1.getRjDjid().toString()));
                    tHmCustomerpaydaysPrice.setArea(tHmCustomerprice.getArea());
                    tHmCustomerpaydaysPrice.setType(tHmCustomerprice.getType());
                    tHmCustomerpaydaysPrice.setXishu(tHmCustomerprice.getRemark());
                    itHmCustomerpaydaysPriceService.insertTHmCustomerpaydaysPrice(tHmCustomerpaydaysPrice);
                }
            }
        }
        THmCustomer tHmCustomer2 = new THmCustomer();
        tHmCustomer2.setHcustomerId(tHmCustomer.getHcustomerId());
        tHmCustomer2.setPayType(tHmCustomer.getPayType());
        return toAjax(tHmCustomerService.updateTHmCustomer(tHmCustomer2));
    }

    @GetMapping("/sheng")
    public String sheng() {
        return prefix + "/customert";
    }
}
