package com.ruoyi.business.service;

import com.ruoyi.business.domain.THmCustomer;
import com.ruoyi.business.domain.THmPayhistory;
import com.ruoyi.business.domain.vo.DetailListVO;
import com.ruoyi.business.domain.vo.DetailVO;
import com.ruoyi.business.domain.vo.PayhistoryVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 充值记录Service接口
 */
public interface ITHmPayhistoryService {

    public THmPayhistory selectTHmPayhistoryById(@Param("id") int id);

    /**
     * 查询充值记录
     */
    public THmPayhistory selectTHmPayhistoryByIdt(@Param("id") int id, Integer type);

    public THmPayhistory selectTHmPayhistoryById(@Param("id") int id, Integer type);

    public THmPayhistory selectTHmPayhistoryByIds(@Param("id") int id);

    /**
     * 查询充值记录列表
     */
    public List<THmPayhistory> selectTHmPayhistoryList(THmPayhistory tHmPayhistory);

    public List<THmPayhistory> selectTHmPayhistoryListForWeChat(THmPayhistory tHmPayhistory);

    public List<PayhistoryVO> selectTHmPayhistoryListt(THmCustomer tHmCustomer);

    /**
     * 新增充值记录
     */
    public int insertTHmPayhistory(THmPayhistory tHmPayhistory);

    //分期充值
    public int fenqi(THmPayhistory tHmPayhistory);

    public int insertTHmPayhistoryForWxChat(THmPayhistory tHmPayhistory);

    public int xufei(THmPayhistory tHmPayhistory);

    /**
     * 修改充值记录
     */
    public int updateTHmPayhistory(THmPayhistory tHmPayhistory);

    //销售明细
    public DetailVO detail(DetailVO detailVO);

    //延期金额缴纳明细
    public DetailVO yanDetail(DetailVO detailVO);
}
