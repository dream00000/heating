package com.ruoyi.business.domain.vo;

import lombok.Data;

/**
 * 返回首页统计数据的实体类
 */
@Data
public class IndexStatisticsVO {

    private int customerCount;//用户总数
    private int qianCount;//欠费总数
    private int gCount;//待关阀总数
    private int kCount;//待开阀总数
    private String shouSum;//收款总金额
    private String qianSum;//欠费总金额
    private String youSum;//优惠总金额
    private String tuiSum;//退费总金额
}
