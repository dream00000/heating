package com.ruoyi.business.service;

import com.ruoyi.business.domain.THmCustomerpaydaysPrice;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 账期-单价Service接口
 */
public interface ITHmCustomerpaydaysPriceService {
    /**
     * 查询账期-单价
     */
    public THmCustomerpaydaysPrice selectTHmCustomerpaydaysPriceById(@Param("id") int id);

    /**
     * 查询账期-单价列表
     */
    public List<THmCustomerpaydaysPrice> selectTHmCustomerpaydaysPriceList(THmCustomerpaydaysPrice tHmCustomerpaydaysPrice);

    /**
     * 新增账期-单价
     */
    public int insertTHmCustomerpaydaysPrice(THmCustomerpaydaysPrice tHmCustomerpaydaysPrice);

    /**
     * 修改账期-单价
     */
    public int updateTHmCustomerpaydaysPrice(THmCustomerpaydaysPrice tHmCustomerpaydaysPrice);

    public void delTHmCustomerpaydaysPrice(@Param("id") int id);
}
