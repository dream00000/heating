package com.ruoyi.business.controller;

import com.ruoyi.business.domain.THmCustomerprice;
import com.ruoyi.business.service.ITHmCustomerpriceService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户单价Controller
 *
 * @author ruoyi
 * @date 2021-09-23
 */
@Controller
@RequestMapping("/business/customerprice")
public class THmCustomerpriceController extends BaseController {
    private String prefix = "business/customerprice";

    @Resource
    private ITHmCustomerpriceService tHmCustomerpriceService;

    @GetMapping()
    public String customerprice() {
        return prefix + "/customerprice";
    }

    /**
     * 查询用户单价列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(THmCustomerprice tHmCustomerprice) {
        startPage();
        List<THmCustomerprice> list = tHmCustomerpriceService.selectTHmCustomerpriceList(tHmCustomerprice);
        return getDataTable(list);
    }

    /**
     * 新增用户单价
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存用户单价
     */
    @Log(title = "新增用户单价", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(THmCustomerprice tHmCustomerprice) {
        return toAjax(tHmCustomerpriceService.insertTHmCustomerprice(tHmCustomerprice));
    }

    /**
     * 修改用户单价
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") int id, ModelMap mmap) {
        THmCustomerprice tHmCustomerprice = tHmCustomerpriceService.selectTHmCustomerpriceById(id);
        mmap.put("tHmCustomerprice", tHmCustomerprice);
        return prefix + "/edit";
    }

    /**
     * 修改保存用户单价
     */
    @Log(title = "修改用户单价", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(THmCustomerprice tHmCustomerprice) {
        return toAjax(tHmCustomerpriceService.updateTHmCustomerprice(tHmCustomerprice));
    }


    /**
     * 删除用户单价
     */
    @Log(title = "删除用户单价", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(@PathVariable("id") int id) {
        THmCustomerprice tHmCustomerprice = new THmCustomerprice();
        return toAjax(tHmCustomerpriceService.updateTHmCustomerprice(tHmCustomerprice));
    }
}
