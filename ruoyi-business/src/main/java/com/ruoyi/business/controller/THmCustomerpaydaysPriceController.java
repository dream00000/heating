package com.ruoyi.business.controller;

import com.ruoyi.business.domain.THmCustomerpaydaysPrice;
import com.ruoyi.business.service.ITHmCustomerpaydaysPriceService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 账期-单价Controller
 *
 * @author ruoyi
 * @date 2021-12-30
 */
@Controller
@RequestMapping("/business/price")
public class THmCustomerpaydaysPriceController extends BaseController {
    private String prefix = "business/price";

    @Resource
    private ITHmCustomerpaydaysPriceService tHmCustomerpaydaysPriceService;

    @GetMapping()
    public String price() {
        return prefix + "/price";
    }

    /**
     * 查询账期-单价列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(THmCustomerpaydaysPrice tHmCustomerpaydaysPrice) {
        startPage();
        List<THmCustomerpaydaysPrice> list = tHmCustomerpaydaysPriceService.selectTHmCustomerpaydaysPriceList(tHmCustomerpaydaysPrice);
        return getDataTable(list);
    }

    /**
     * 导出账期-单价列表
     */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(THmCustomerpaydaysPrice tHmCustomerpaydaysPrice) {
        List<THmCustomerpaydaysPrice> list = tHmCustomerpaydaysPriceService.selectTHmCustomerpaydaysPriceList(tHmCustomerpaydaysPrice);
        ExcelUtil<THmCustomerpaydaysPrice> util = new ExcelUtil<THmCustomerpaydaysPrice>(THmCustomerpaydaysPrice.class);
        return util.exportExcel(list, "price");
    }

    /**
     * 新增账期-单价
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存账期-单价
     */
    @Log(title = "账期-单价", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(THmCustomerpaydaysPrice tHmCustomerpaydaysPrice) {
        return toAjax(tHmCustomerpaydaysPriceService.insertTHmCustomerpaydaysPrice(tHmCustomerpaydaysPrice));
    }

    /**
     * 修改账期-单价
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") int id, ModelMap mmap) {
        THmCustomerpaydaysPrice tHmCustomerpaydaysPrice = tHmCustomerpaydaysPriceService.selectTHmCustomerpaydaysPriceById(id);
        mmap.put("tHmCustomerpaydaysPrice", tHmCustomerpaydaysPrice);
        return prefix + "/edit";
    }

    /**
     * 修改保存账期-单价
     */
    @Log(title = "账期-单价", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(THmCustomerpaydaysPrice tHmCustomerpaydaysPrice) {
        return toAjax(tHmCustomerpaydaysPriceService.updateTHmCustomerpaydaysPrice(tHmCustomerpaydaysPrice));
    }


    /**
     * 删除账期-单价
     */
    @Log(title = "账期-单价", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(@PathVariable("id") int id) {
        THmCustomerpaydaysPrice tHmCustomerpaydaysPrice = new THmCustomerpaydaysPrice();
        return toAjax(tHmCustomerpaydaysPriceService.updateTHmCustomerpaydaysPrice(tHmCustomerpaydaysPrice));
    }
}
