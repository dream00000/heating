package com.ruoyi.business.service.impl;

import com.ruoyi.business.domain.vo.RefundVO;
import com.ruoyi.business.domain.THmArea;
import com.ruoyi.business.domain.THmCustomer;
import com.ruoyi.business.domain.THmRefund;
import com.ruoyi.business.mapper.THmAreaMapper;
import com.ruoyi.business.mapper.THmCustomerMapper;
import com.ruoyi.business.mapper.THmCustomerpaydaysMapper;
import com.ruoyi.business.mapper.THmRefundMapper;
import com.ruoyi.business.service.ITHmRefundService;
import com.ruoyi.business.util.MoneyToCNFormat;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.mapper.SysUserMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 退费Service业务层处理
 *
 * @author ruoyi
 * @date 2021-08-13
 */
@Service
public class THmRefundServiceImpl implements ITHmRefundService {
    @Resource
    private THmRefundMapper tHmRefundMapper;

    @Resource
    private THmCustomerMapper tHmCustomerMapper;

    @Resource
    private THmAreaMapper tHmAreaMapper;

    @Resource
    private THmCustomerpaydaysMapper tHmCustomerpaydaysMapper;

    @Resource
    private SysUserMapper sysUserMapper;

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * 查询退费
     *
     * @param id
     * @return 退费
     */
    @Override
    public THmRefund selectTHmRefundById(int id) {
        THmRefund tHmRefund = new THmRefund();
        THmCustomer tHmCustomer = tHmCustomerMapper.selectTHmCustomerById(id);
        tHmRefund.setHcustomerId(id);
        tHmRefund.setCustomerName(tHmCustomer.getHcustomerName());
        tHmRefund.setCustomerCode(tHmCustomer.getHcustomerCode());
        tHmRefund.setCustomerPhone(tHmCustomer.getHcustomerMobile());
        tHmRefund.setAddress(tHmCustomer.getHcustomerDetailaddress());

        THmArea tHmArea = tHmAreaMapper.selectTHmAreaById(tHmCustomer.getHareaId());
        if (tHmArea != null) {
            if (tHmArea.getHareaLevel() == 2) {
                tHmRefund.setCustomerArea(tHmArea.getHareaName());
            } else {
                THmArea tHmArea1 = tHmAreaMapper.selectTHmAreaById(tHmArea.getHareaParentid());
                tHmRefund.setCustomerArea(tHmArea1.getHareaName());
            }
        }

        tHmRefund.setBalance(tHmCustomer.getBalance());

        return tHmRefund;
    }

    /**
     * 查询退费
     *
     * @param id
     * @return 退费
     */
    @Override
    public THmRefund selectTHmRefundByIds(int id) {

        THmRefund tHmRefund = tHmRefundMapper.selectTHmRefundById(id);
        if(tHmRefund != null){
            THmCustomer tHmCustomer = tHmCustomerMapper.selectTHmCustomerById(tHmRefund.getHcustomerId());
            if(tHmCustomer != null){
                tHmRefund.setCustomerName(tHmCustomer.getHcustomerName());
                tHmRefund.setCustomerCode(tHmCustomer.getHcustomerCode());
                tHmRefund.setCustomerPhone(tHmCustomer.getHcustomerMobile());
                tHmRefund.setAddress(tHmCustomer.getHcustomerDetailaddress());

                THmArea tHmArea = tHmAreaMapper.selectTHmAreaById(tHmCustomer.getHareaId());
                if (tHmArea != null) {
                    if (tHmArea.getHareaLevel() == 2) {
                        tHmRefund.setCustomerArea(tHmArea.getHareaName());
                    } else {
                        THmArea tHmArea1 = tHmAreaMapper.selectTHmAreaById(tHmArea.getHareaParentid());
                        tHmRefund.setCustomerArea(tHmArea1.getHareaName());
                    }
                }
            }

            //大写金额
            tHmRefund.setTfMoneyd(MoneyToCNFormat.formatToCN(Double.parseDouble(tHmRefund.getTfMoney())));
            //收款人，开票人
            SysUser sysUser = sysUserMapper.selectUserById(tHmRefund.getTfAdduserid());
            tHmRefund.setSysname(sysUser.getUserName());
        }

        return tHmRefund;
    }

    /**
     * 查询退费列表
     *
     * @param tHmRefund 退费
     * @return 退费
     */
    @Override
    public List<THmRefund> selectTHmRefundList(THmRefund tHmRefund) {
        List<THmRefund> list = tHmRefundMapper.selectTHmRefundList(tHmRefund);
        for (THmRefund hmRefund : list) {
            THmCustomer tHmCustomer = tHmCustomerMapper.selectTHmCustomerById(hmRefund.getHcustomerId());
            hmRefund.setCustomerName(tHmCustomer.getHcustomerName());
            hmRefund.setCustomerCode(tHmCustomer.getHcustomerCode());
            hmRefund.setAddress(tHmCustomer.getHcustomerDetailaddress());
            hmRefund.setCustomerPhone(tHmCustomer.getHcustomerMobile());
            hmRefund.setCustomerArea(tHmAreaMapper.selectTHmAreaById(tHmCustomer.getHareaId()).getHareaName());
            //操作人员
            SysUser sysUser = sysUserMapper.selectUserById(hmRefund.getTfAdduserid());
            hmRefund.setSysname(sysUser.getUserName());
        }
        return list;
    }

    @Override
    public List<RefundVO> selectTHmRefundListt(THmCustomer tHmCustomer) {
        List<RefundVO> list = tHmRefundMapper.selectTHmRefundListt(tHmCustomer);
        for (RefundVO refundVO : list) {
            //所属分区
            THmArea tHmArea = tHmAreaMapper.selectTHmAreaById(refundVO.getHareaId());
            if (tHmArea != null) {
                if (tHmArea.getHareaLevel() == 2) {
                    refundVO.setHcustomerArea(tHmArea.getHareaName());
                } else {
                    THmArea tHmArea1 = tHmAreaMapper.selectTHmAreaById(tHmArea.getHareaParentid());
                    refundVO.setHcustomerArea(tHmArea1.getHareaName());
                }
            }
        }
        return list;
    }

    /**
     * 新增退费
     *
     * @param tHmRefund 退费
     * @return 结果
     */
    @Override
    public int insertTHmRefund(THmRefund tHmRefund) {
        tHmRefund.setTfAdduserid(ShiroUtils.getSysUser().getUserId());
        tHmRefund.setTfAddtime(sdf.format(new Date()));
        tHmRefund.setTfState(0);
        //更新用户余额
        THmCustomer tHmCustomer = tHmCustomerMapper.selectTHmCustomerById(tHmRefund.getHcustomerId());
        if (tHmCustomer.getBalance() != null) {
            BigDecimal b1 = new BigDecimal(tHmRefund.getTfMoney());
            BigDecimal b2 = new BigDecimal(tHmCustomer.getBalance());
            BigDecimal b3 = b2.subtract(b1);
            tHmCustomer.setBalance(b3.toString());
        } else {
            tHmCustomer.setBalance(tHmRefund.getTfMoney());
        }
        tHmCustomerMapper.updateTHmCustomer(tHmCustomer);
        return tHmRefundMapper.insertTHmRefund(tHmRefund);
    }

    /**
     * 修改退费
     *
     * @param tHmRefund 退费
     * @return 结果
     */
    @Override
    public int updateTHmRefund(THmRefund tHmRefund) {
        //如果tfType是1则从用户的余额减去
        THmRefund tHmRefund1 = tHmRefundMapper.selectTHmRefundById(tHmRefund.getTfId());
        THmCustomer tHmCustomer = tHmCustomerMapper.selectTHmCustomerById(tHmRefund1.getHcustomerId());
        BigDecimal bd = new BigDecimal(tHmCustomer.getBalance());//用户当前余额
        tHmCustomer.setBalance((bd.add(new BigDecimal(tHmRefund1.getTfMoney()))).toString());
        tHmCustomerMapper.updateTHmCustomer(tHmCustomer);

        return tHmRefundMapper.updateTHmRefund(tHmRefund);
    }

}
