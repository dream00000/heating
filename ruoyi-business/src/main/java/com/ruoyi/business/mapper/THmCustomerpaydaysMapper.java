package com.ruoyi.business.mapper;

import com.ruoyi.business.domain.THmCustomer;
import com.ruoyi.business.domain.THmCustomerpaydays;
import com.ruoyi.business.domain.vo.CustomerpaydaysVO;
import com.ruoyi.business.domain.vo.OrderKeFu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户年度账期Mapper接口
 */
public interface THmCustomerpaydaysMapper {
    /**
     * 查询用户年度账期
     */
    THmCustomerpaydays selectTHmCustomerpaydaysById(@Param("id") int id);

    /**
     * 查询用户年度账期列表
     */
    List<THmCustomerpaydays> selectTHmCustomerpaydaysList(THmCustomerpaydays tHmCustomerpaydays);

    List<THmCustomerpaydays> selectTHmCustomerpaydaysListm(THmCustomerpaydays tHmCustomerpaydays);

    List<CustomerpaydaysVO> selectTHmCustomerpaydaysLists(THmCustomer tHmCustomer);

    List<CustomerpaydaysVO> jiaoList(THmCustomer tHmCustomer);

    int exportCount(THmCustomer tHmCustomer);

    List<THmCustomerpaydays> getcuswq(Long hcustomerId);

    /**
     * 新增用户年度账期
     */
    int insertTHmCustomerpaydays(THmCustomerpaydays tHmCustomerpaydays);

    /**
     * 修改用户年度账期
     */
    int updateTHmCustomerpaydays(THmCustomerpaydays tHmCustomerpaydays);

    //首页饼图统计
    List<THmCustomerpaydays> indexProportion(THmCustomerpaydays tHmCustomerpaydays);

    //获取 账期 的开始年度
    List<String> startTime();

    //根据年度和用户id获取账期
    THmCustomerpaydays getByNianAndCustomerId(THmCustomerpaydays tHmCustomerpaydays);

    //获取最后一次账期
    THmCustomerpaydays getLast(@Param("hcustomerId") int hcustomerId);

    int getZheng(@Param("hcustomerId") int hcustomerId);

    int zuo(@Param("wqId") int wqId);

    THmCustomerpaydays getLastYan(@Param("hcustomerId") int hcustomerId);

    THmCustomerpaydays getByCustomerIdAndYear(@Param("hcustomerId") int hcustomerId, @Param("hzqGrstarttime") String hzqGrstarttime);

    List<OrderKeFu> orderKeFuList(@Param("type") int type);
}
