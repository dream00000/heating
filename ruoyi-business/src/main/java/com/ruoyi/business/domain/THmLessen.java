package com.ruoyi.business.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 用热减免对象 t_hm_lessen
 *
 * @author ruoyi
 * @date 2021-08-13
 */
@Data
public class THmLessen extends BaseEntity {

    private Integer jmId;

    /**
     * 减免用户
     */
    @Excel(name = "减免用户")
    private Integer hcustomerId;

    /**
     * 减免年份
     */
    @Excel(name = "减免年份")
    private String jmYear;

    /**
     * 减免原因
     */
    @Excel(name = "减免原因")
    private String jmReasion;

    /**
     * 减免金额
     */
    @Excel(name = "减免金额")
    private String jmMoney;

    /**
     * 是否删除0否1是
     */
    @Excel(name = "是否删除0否1是")
    private Integer jmState;

    /**
     * 添加人
     */
    @Excel(name = "添加人")
    private String jmAddtime;

    /**
     * 添加时间
     */
    @Excel(name = "添加时间")
    private Long jmAdduserid;

    private String qian;

    private String customerName;
    private String customerCode;
    private String customerPhone;
    private String customerCard;
    private String customerArea;
    private String customerAddress;

    private Integer wqId;
}
