package com.ruoyi.business.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * 微信用户对象 wechat_user
 */
@Data
public class WechatUser extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Integer wechatId;

    /**
     * 用户唯一Id
     */
    @Excel(name = "OpenId")
    private String openId;

    /**
     * 用户名称
     */
    @Excel(name = "用户名称")
    private String nickName;

    /**
     * 用户城市
     */
    @Excel(name = "用户城市")
    private String city;

    /**
     * 用户详细信息
     */
    @Excel(name = "用户详细信息")
    private String province;

    /**
     * 用户性别
     */
    @Excel(name = "用户性别")
    private String sex;

    /**
     * 用户头像
     */
    @Excel(name = "用户头像")
    private String headimgurl;

    /**
     * access_token
     */
    @Excel(name = "access_token")
    private String accessToken;

    /**
     * refresh_token
     */
    @Excel(name = "refresh_token")
    private String refreshToken;

    /**
     * 获取token时间
     */
    @Excel(name = "获取token时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date accessTokenDate;

    private String code;
    private String phone;
}
