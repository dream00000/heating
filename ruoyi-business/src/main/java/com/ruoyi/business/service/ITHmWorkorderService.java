package com.ruoyi.business.service;

import com.ruoyi.business.domain.THmWorkorder;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 客服工单Service接口
 *
 * @author ruoyi
 * @date 2022-02-26
 */
public interface ITHmWorkorderService {
    /**
     * 查询客服工单
     *
     * @param id
     * @return 客服工单
     */
    public THmWorkorder selectTHmWorkorderById(@Param("id") int id);

    /**
     * 查询客服工单列表
     *
     * @param tHmWorkorder 客服工单
     * @return 客服工单集合
     */
    public List<THmWorkorder> selectTHmWorkorderList(THmWorkorder tHmWorkorder);

    public int insertTHmWorkorder(THmWorkorder tHmWorkorder);
}
