package com.ruoyi.business.controller;

import com.ruoyi.business.domain.*;
import com.ruoyi.business.service.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.SysMenu;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.service.ISysMenuService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 用户信息Controller
 *
 * @author ruoyi
 * @date 2021-08-13
 */
@Controller
@RequestMapping("/business/customer")
public class THmCustomerController extends BaseController {
    private String prefix = "business/customer";

    @Resource
    private ITHmCustomerService tHmCustomerService;

    @Resource
    private ITHmMeterpriceService itHmMeterpriceService;

    @Resource
    private ITHmCustomerpriceService itHmCustomerpriceService;

    @Resource
    private ITHmFeedbackService itHmFeedbackService;

    @Resource
    private ITHmEnclosureService itHmEnclosureService;

    @Resource
    private ISysMenuService menuService;

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @GetMapping()
    public String customer() {
        return prefix + "/customer";
    }

    @GetMapping("/customerlist")
    public String customerlist() {
        return prefix + "/customerlist";
    }

    /**
     * 查询用户信息列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(THmCustomer tHmCustomer) {
        startPage();
        List<THmCustomer> list = tHmCustomerService.selectTHmCustomerList(tHmCustomer);
        return getDataTable(list);
    }

    /**
     * 查询用户信息列表
     */
    @PostMapping("/areaList")
    @ResponseBody
    public TableDataInfo areaList(THmCustomer tHmCustomer) {
        startPage();
        List<THmCustomer> list = tHmCustomerService.checkAddress(tHmCustomer);
        return getDataTable(list);
    }

    /**
     * 导出用户信息列表
     */
    @PostMapping("/export")
    @ResponseBody
    @Log(title = "导出用户信息", businessType = BusinessType.EXPORT)
    public AjaxResult export(THmCustomer tHmCustomer) {
        List<THmCustomer> list = tHmCustomerService.selectTHmCustomerList(tHmCustomer);
        ExcelUtil<THmCustomer> util = new ExcelUtil<>(THmCustomer.class);
        return util.exportExcel(list, "用户信息");
    }

    @PostMapping("/importCheck")
    @ResponseBody
    public AjaxResult importCheck(MultipartFile file) throws Exception {
        ExcelUtil<THmCustomer> util = new ExcelUtil<>(THmCustomer.class);
        List<THmCustomer> userList = util.importExcel(file.getInputStream());
        return AjaxResult.success(tHmCustomerService.importCustomerCheck(userList));
    }

    @PostMapping("/importData")
    @ResponseBody
    public AjaxResult importData(MultipartFile file) throws Exception {
        ExcelUtil<THmCustomer> util = new ExcelUtil<>(THmCustomer.class);
        List<THmCustomer> userList = util.importExcel(file.getInputStream());
        String message = tHmCustomerService.importCustomer(userList);
        return AjaxResult.success(message);
    }

    @GetMapping("/importTemplate")
    @ResponseBody
    public AjaxResult importTemplate() {
        ExcelUtil<THmCustomer> util = new ExcelUtil<>(THmCustomer.class);
        return util.importTemplateExcel("用户数据导入");
    }

    /**
     * 新增用户信息
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存用户信息
     */
    @Log(title = "新增用户信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@RequestBody THmCustomer tHmCustomer) {

        THmCustomer tHmCustomer2 = tHmCustomerService.selectTHmCustomerByHcustomerCode(tHmCustomer.getHcustomerCode());
        if (tHmCustomer2 != null) {
            return AjaxResult.warn("用户编号已被使用，请重新获取用户编号");
        }

        tHmCustomer.setHcustomerKhuserid(ShiroUtils.getSysUser().getUserId());
        tHmCustomer.setHcustomerKhtime(sdf.format(new Date()));
        tHmCustomer.setHcustomerKhstate(1);

        tHmCustomerService.insertTHmCustomer(tHmCustomer);
        //第一步先把户开了
        //添加用户单价
        if (tHmCustomer.getCustomerpriceList() != null) {
            //先将用户关联的单价删除
            for (THmCustomerprice tHmCustomerprice : tHmCustomer.getCustomerpriceList()) {
                BigDecimal bd = new BigDecimal(tHmCustomerprice.getArea());//面积
                THmMeterprice tHmMeterprice = itHmMeterpriceService.selectTHmMeterpriceById(tHmCustomerprice.getPriceId());
                BigDecimal bds = new BigDecimal(tHmMeterprice.getRjPrice());//单价
                tHmCustomerprice.setCustomerId(tHmCustomer.getHcustomerId());//用户外键
                tHmCustomerprice.setTotal(bd.multiply(bds).multiply(new BigDecimal(tHmCustomerprice.getXishu())).divide(new BigDecimal("100")).toString());
                tHmCustomerprice.setState(0);
                tHmCustomerprice.setTap(1);
                tHmCustomerprice.setRemark(tHmCustomerprice.getXishu());
                //再重新增加
                itHmCustomerpriceService.insertTHmCustomerprice(tHmCustomerprice);

                THmFeedback tHmFeedback = new THmFeedback();
                tHmFeedback.setFdType(1);//开关阀操作
                tHmFeedback.setFdFmzt(0);
                tHmFeedback.setHcustomerId(tHmCustomer.getHcustomerId());
                tHmFeedback.setFdAddtime(sdf.format(new Date()));
                tHmFeedback.setFdAdduserName(ShiroUtils.getSysUser().getUserName());
                tHmFeedback.setFdState(0);
                tHmFeedback.setFdTitle("开关阀操作");
                tHmFeedback.setCustomerpriceId(Long.valueOf(tHmCustomerprice.getId()));
                tHmFeedback.setFdContent("新增用户时" + ShiroUtils.getSysUser().getUserName() + "进行了阀控操作");
                itHmFeedbackService.insertTHmFeedback(tHmFeedback);
            }
        }

        return AjaxResult.success("成功");
    }

    /**
     * 修改用户信息
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") int id, ModelMap mmap) {
        THmCustomer tHmCustomer = tHmCustomerService.selectTHmCustomerById(id);
        mmap.put("tHmCustomer", tHmCustomer);
        return prefix + "/edit";
    }

    /**
     * 修改保存用户信息
     */
    @Log(title = "修改用户信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(@RequestBody THmCustomer tHmCustomer) {
        return toAjax(tHmCustomerService.updateTHmCustomer(tHmCustomer));
    }

    /**
     * 阀控操作
     */
    @Log(title = "阀控操作", businessType = BusinessType.UPDATE)
    @PostMapping("/edits")
    @ResponseBody
    public AjaxResult editsSave(THmCustomer tHmCustomer) {
        THmCustomerprice tHmCustomerprice = new THmCustomerprice();
        tHmCustomerprice.setTap(tHmCustomer.getHcustomerTapstate());
        tHmCustomerprice.setId(tHmCustomer.getPricesId());
        tHmCustomerprice.setCustomerId(tHmCustomer.getHcustomerId());
        tHmCustomerprice.setTopTime(tHmCustomer.getTopTime());
        return toAjax(itHmCustomerpriceService.updateTHmCustomerprices(tHmCustomerprice));
    }


    /**
     * 删除用户信息
     */
    @Log(title = "删除用户信息", businessType = BusinessType.DELETE)
    @PostMapping("/remove/{id}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("id") int id) {
        THmCustomer tHmCustomer = new THmCustomer();
        tHmCustomer.setHcustomerId(id);
        tHmCustomer.setHcustomerState(1);
        return toAjax(tHmCustomerService.updateTHmCustomer(tHmCustomer));
    }

    /**
     * 小房子页
     */
    @GetMapping("/customerHouse")
    public String customerHouse() {
        return prefix + "/customerHouse";
    }

    /**
     * 大数据页
     */
    @GetMapping("/survey")
    public String survey() {
        return "business/home/survey";
    }

    /**
     * 导入数据页
     */
    @GetMapping("/importExcel")
    public String importExcel() {
        return prefix + "/importExcel";
    }

    @GetMapping("/img/{id}")
    public String img(@PathVariable("id") int id, Model model) {
        Map<String, Object> map = new HashMap<>();
        map.put("id", id);
        model.addAllAttributes(map);
        return prefix + "/imgs";
    }

    @PostMapping("/imgs/{id}")
    @ResponseBody
    public TableDataInfo imgsList(@PathVariable("id") int id) {
        THmCustomer tHmCustomer = tHmCustomerService.selectTHmCustomerById(id);
        THmEnclosure tHmEnclosure = new THmEnclosure();
        List<THmEnclosure> tHmEnclosureList = new ArrayList<>();
        if (tHmCustomer.getHcustomerContactcode() != null) {
            tHmEnclosure.setEnFjcode(tHmCustomer.getHcustomerContactcode());
            tHmEnclosureList = itHmEnclosureService.selectTHmEnclosureList(tHmEnclosure);
        }
        return getDataTable(tHmEnclosureList);
    }

    @GetMapping("/indexList")
    @ResponseBody
    public AjaxResult indexList() {
        // 取身份信息
        SysUser user = ShiroUtils.getSysUser();
        // 根据用户id取出菜单
        List<SysMenu> list = menuService.selectMenusByUsers(user);
        System.out.println(list);
        return AjaxResult.success(list);
    }
}