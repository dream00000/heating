package com.ruoyi.business.controller;

import com.ruoyi.business.domain.THmArea;
import com.ruoyi.business.domain.THmCustomer;
import com.ruoyi.business.service.ITHmAreaService;
import com.ruoyi.business.service.ITHmCustomerService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 供热分区Controller
 *
 * @author ruoyi
 * @date 2021-08-13
 */
@Controller
@RequestMapping("/business/area")
public class THmAreaController extends BaseController {

    private String prefix = "business/area";

    @Resource
    private ITHmAreaService tHmAreaService;

    @Resource
    private ITHmCustomerService itHmCustomerService;

    @GetMapping()
    public String area() {
        return prefix + "/area";
    }

    /**
     * 查询供热分区树列表
     */
    @PostMapping("/list")
    @ResponseBody
    public List<THmArea> list(THmArea tHmArea) {
        return tHmAreaService.selectTHmAreaListt(tHmArea);
    }

    /**
     * 获取parentId下边的id最大的区域 -- 用于计算code
     */
    @PostMapping("/getMaxCode")
    @ResponseBody
    public AjaxResult getMaxCode(THmArea tHmArea) {
        return AjaxResult.success("成功", tHmAreaService.getMaxCode(tHmArea));
    }

    /**
     * 查询供热分区树列表
     */
    @PostMapping("/getById")
    @ResponseBody
    public AjaxResult getById(Integer id) {
        return AjaxResult.success("成功", tHmAreaService.selectTHmAreaById(id));
    }

    /**
     * 导出供热分区列表
     */
    @PostMapping("/export")
    @ResponseBody
    @Log(title = "导出分区信息", businessType = BusinessType.EXPORT)
    public AjaxResult export(THmArea tHmArea) {
        List<THmArea> list = tHmAreaService.selectTHmAreaList(tHmArea);
        ExcelUtil<THmArea> util = new ExcelUtil<THmArea>(THmArea.class);
        return util.exportExcel(list, "area");
    }

    /**
     * 新增供热分区
     */
    @GetMapping(value = {"/add/{id}"})
    public String add(@PathVariable("id") int id, ModelMap mmap) {
        THmArea tHmArea = tHmAreaService.selectTHmAreaById(id);
        mmap.put("tHmArea", tHmArea);
        return prefix + "/add";
    }

    /**
     * 新增保存供热分区
     */
    @PostMapping("/add")
    @ResponseBody
    @Log(title = "新增分区", businessType = BusinessType.INSERT)
    public AjaxResult addSave(THmArea tHmArea) {
        return toAjax(tHmAreaService.insertTHmArea(tHmArea));
    }

    /**
     * 修改供热分区
     */
    @GetMapping("/edit/{id}")

    public String edit(@PathVariable("id") int id, ModelMap mmap) {
        THmArea tHmArea = tHmAreaService.selectTHmAreaById(id);
        mmap.put("tHmArea", tHmArea);
        return prefix + "/edit";
    }

    /**
     * 修改保存供热分区
     */
    @Log(title = "修改分区", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(THmArea tHmArea) {
        return toAjax(tHmAreaService.updateTHmArea(tHmArea));
    }

    /**
     * 设置账期
     */
    @Log(title = "设置账期", businessType = BusinessType.UPDATE)
    @PostMapping("/setting")
    @ResponseBody
    public AjaxResult setting(THmArea tHmArea) {
        return toAjax(tHmAreaService.setting(tHmArea));
    }

    /**
     * 选择供热分区树
     */
    @GetMapping(value = {"/selectAreaTree/{id}"})
    public String selectAreaTree(@PathVariable("id") int id, ModelMap mmap) {
        if (id == 0) {
            id = 1;
        }
        mmap.put("tHmArea", tHmAreaService.selectTHmAreaById(id));
        return prefix + "/tree";
    }

    /**
     * 加载供热分区树列表
     */
    @GetMapping("/treeData")
    @ResponseBody
    public List<Ztree> treeData() {
        List<Ztree> ztrees = tHmAreaService.selectTHmAreaTree();
        return ztrees;
    }

    /**
     * 删除供热分区
     */
    @Log(title = "供热分区", businessType = BusinessType.DELETE)
    @GetMapping("/remove")
    @ResponseBody
    public AjaxResult remove(Integer id) {
        THmCustomer tHmCustomer = new THmCustomer();
        tHmCustomer.setHareaId(id);
        List<THmCustomer> list = itHmCustomerService.selectTHmCustomerList(tHmCustomer);
        if (list.size() > 0) {
            return AjaxResult.error("删除失败，该分区下有用户存在");
        } else {
            THmArea tHmArea = new THmArea();
            tHmArea.setHareaId(id);
            tHmArea.setHareaState(1);
            return toAjax(tHmAreaService.updateTHmArea(tHmArea));
        }
    }

    /**
     * 跳转设置账期
     */
    @GetMapping("/paydays/{id}")
    public String paydays(@PathVariable("id") int id, ModelMap mmap) {
        THmArea tHmArea = tHmAreaService.selectTHmAreaById(id);
        mmap.put("tHmArea", tHmArea);
        return prefix + "/paydays";
    }
}
