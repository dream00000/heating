package com.ruoyi.business.controller;

import com.ruoyi.business.domain.*;
import com.ruoyi.business.domain.bill.BusinessList;
import com.ruoyi.business.domain.report.IndexProportion;
import com.ruoyi.business.domain.vo.IndexAreaDaysVO;
import com.ruoyi.business.domain.vo.IndexRecordVO;
import com.ruoyi.business.domain.vo.ReportVO;
import com.ruoyi.business.mapper.*;
import com.ruoyi.business.service.*;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 业务查询
 *
 * @author lihao
 * @date 2021-09-02
 */
@Api("报表")
@Controller
@RequestMapping("/business/report")
public class THmReportController extends BaseController {
    private String prefix = "business/report";

    @Resource
    private THmReportService tHmReportService;

    @Resource
    private THmPayhistoryMapper tHmPayhistoryMapper;

    @Resource
    private THmLessenMapper tHmLessenMapper;

    @Resource
    private THmRefundMapper refundMapper;

    @Resource
    private THmFeedbackMapper tHmFeedbackMapper;

    @Resource
    private THmCustomerMapper tHmCustomerMapper;

    @Resource
    private ITHmCustomerService itHmCustomerService;

    @GetMapping()
    public String report() {
        return prefix + "/report";
    }

    @GetMapping("/reportBill")
    public String reportBill() {
        return prefix + "/reportBill";
    }

    @GetMapping("/statistics")
    public String statisticsstatistics() {
        return prefix + "/statistics";
    }

    //首页的数据统计
    @PostMapping("/indexStatistics")
    @ResponseBody
    @ApiOperation("首页的数据统计")
    public AjaxResult indexStatistics() {
        return AjaxResult.success("成功", tHmReportService.indexStatistics());
    }

    //首页的充值记录
    @PostMapping("/indexRecharge")
    @ResponseBody
    @ApiOperation("首页的充值记录")
    public AjaxResult indexRecharge(THmPayhistory tHmPayhistory) {
        List<IndexRecordVO> indexRecordVOList = new ArrayList<>();
        startPage();
        List<THmPayhistory> list = tHmPayhistoryMapper.selectTHmPayhistoryList(tHmPayhistory);
        if (list.size() > 0) {
            for (THmPayhistory hmPayhistory : list) {
                THmCustomer tHmCustomer = tHmCustomerMapper.selectTHmCustomerById(hmPayhistory.getHcustomerId());
                IndexRecordVO indexRecordVO = new IndexRecordVO();
                indexRecordVO.setAddTime(hmPayhistory.getHpayAddtime());
                indexRecordVO.setContext(tHmCustomer.getHcustomerName() + "充值了" + hmPayhistory.getHpayMoney() + "元");
                indexRecordVOList.add(indexRecordVO);
            }
            return AjaxResult.success("成功", indexRecordVOList);
        } else {
            return AjaxResult.warn("无数据", "");
        }
    }

    //首页的优惠记录
    @PostMapping("/indexDiscount")
    @ResponseBody
    @ApiOperation("首页的优惠记录")
    public AjaxResult indexDiscount(THmLessen tHmLessen) {
        List<IndexRecordVO> indexRecordVOList = new ArrayList<>();
        startPage();
        List<THmLessen> list = tHmLessenMapper.selectTHmLessenList(tHmLessen);
        if (list.size() > 0) {
            for (THmLessen hmLessen : list) {
                THmCustomer tHmCustomer = tHmCustomerMapper.selectTHmCustomerById(hmLessen.getHcustomerId());
                IndexRecordVO indexRecordVO = new IndexRecordVO();
                indexRecordVO.setAddTime(hmLessen.getJmAddtime());
                indexRecordVO.setContext(tHmCustomer.getHcustomerName() + "优惠了" + hmLessen.getJmMoney() + "元");
                indexRecordVOList.add(indexRecordVO);
            }
            return AjaxResult.success("成功", indexRecordVOList);
        } else {
            return AjaxResult.warn("无数据", "");
        }
    }

    //首页的退费记录
    @PostMapping("/indexRefund")
    @ResponseBody
    @ApiOperation("首页的退费记录")
    public AjaxResult indexRefund(THmRefund tHmRefund) {
        List<IndexRecordVO> indexRecordVOList = new ArrayList<>();
        startPage();
        List<THmRefund> list = refundMapper.selectTHmRefundList(tHmRefund);
        if (list.size() > 0) {
            for (THmRefund hmRefund : list) {
                THmCustomer tHmCustomer = tHmCustomerMapper.selectTHmCustomerById(hmRefund.getHcustomerId());
                IndexRecordVO indexRecordVO = new IndexRecordVO();
                indexRecordVO.setAddTime(hmRefund.getTfAddtime());
                indexRecordVO.setContext(tHmCustomer.getHcustomerName() + "退费" + hmRefund.getTfMoney() + "元");
                indexRecordVOList.add(indexRecordVO);
            }
            return AjaxResult.success("成功", indexRecordVOList);
        } else {
            return AjaxResult.warn("无数据", "");
        }
    }

    //首页的稽查记录
    @PostMapping("/indexCheck")
    @ResponseBody
    @ApiOperation("首页的稽查记录")
    public AjaxResult indexCheck(THmFeedback tHmFeedback) {
        List<IndexRecordVO> indexRecordVOList = new ArrayList<>();
        startPage();
        List<THmFeedback> list = tHmFeedbackMapper.selectTHmFeedbackList(tHmFeedback);
        if (list.size() > 0) {
            for (THmFeedback hmFeedback : list) {
                THmCustomer tHmCustomer = tHmCustomerMapper.selectTHmCustomerById(hmFeedback.getHcustomerId());
                IndexRecordVO indexRecordVO = new IndexRecordVO();
                indexRecordVO.setAddTime(hmFeedback.getFdAddtime());
                indexRecordVO.setContext(hmFeedback.getFdAdduserName() + "上传了" +tHmCustomer.getHcustomerName() + "的稽查信息");
                indexRecordVOList.add(indexRecordVO);
            }
            return AjaxResult.success("成功", indexRecordVOList);
        } else {
            return AjaxResult.warn("无数据", "");
        }
    }

    //首页的单价类型收费占比
    @PostMapping("/indexProportion")
    @ResponseBody
    @ApiOperation("首页的单价类型收费占比")
    public AjaxResult indexProportion() {
        List<IndexProportion> list = tHmReportService.indexProportion();
        if (list.size() > 0) {
            return AjaxResult.success("成功", list);
        } else {
            return AjaxResult.warn("无数据", "");
        }
    }

    //营业账单
    @PostMapping("/lists")
    @ResponseBody
    public AjaxResult lists(BusinessList businessList) throws ParseException {
        if(businessList.getSysName() == null){
            businessList.setSysName("");
        }
        BusinessList businessList1 = tHmReportService.reportBill(businessList);
        return AjaxResult.success("成功", businessList1);
    }

    //用户信息(房子)
    @PostMapping("/homeList")
    @ResponseBody
    @ApiOperation("用户信息(房子)")
    public TableDataInfo homeList(THmCustomer tHmCustomer) {
        startPage();
        List<THmCustomer> list = itHmCustomerService.selectTHmCustomerList(tHmCustomer);
        return getDataTable(list);
    }

    @GetMapping("/jiao/{startTime}/{endTime}")
    public String jiao(@PathVariable("startTime") String startTime, @PathVariable("endTime") String endTime, Model model) {
        Map<String, Object> map = new HashMap<>();
        map.put("startTime", startTime);
        map.put("endTime", endTime);
        model.addAllAttributes(map);
        return prefix + "/jiao";
    }

    @GetMapping("/tui/{startTime}/{endTime}")
    public String tui(@PathVariable("startTime") String startTime, @PathVariable("endTime") String endTime, Model model) {
        Map<String, Object> map = new HashMap<>();
        map.put("startTime", startTime);
        map.put("endTime", endTime);
        model.addAllAttributes(map);
        return prefix + "/tui";
    }

    @GetMapping("/qi/{startTime}/{endTime}")
    public String qi(@PathVariable("startTime") String startTime, @PathVariable("endTime") String endTime, Model model) {
        Map<String, Object> map = new HashMap<>();
        map.put("startTime", startTime);
        map.put("endTime", endTime);
        model.addAllAttributes(map);
        return prefix + "/qi";
    }
}
