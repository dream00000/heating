package com.ruoyi.business.service.impl;

import com.ruoyi.business.domain.THmStation;
import com.ruoyi.business.mapper.THmStationMapper;
import com.ruoyi.business.service.ITHmStationService;
import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.framework.util.ShiroUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 热源站Service业务层处理
 *
 * @author ruoyi
 * @date 2021-12-25
 */
@Service
public class THmStationServiceImpl implements ITHmStationService {

    @Resource
    private THmStationMapper tHmStationMapper;

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * 查询热源站
     *
     * @param id
     * @return 热源站
     */
    @Override
    public THmStation selectTHmStationById(Long id) {
        return tHmStationMapper.selectTHmStationById(id);
    }

    @Override
    public THmStation getMaxCode(THmStation tHmStation) {
        return tHmStationMapper.getMaxCode(tHmStation);
    }

    /**
     * 查询热源站列表
     *
     * @param tHmStation 热源站
     * @return 热源站
     */
    @Override
    public List<THmStation> selectTHmStationList(THmStation tHmStation) {
        List<THmStation> tHmStationList = tHmStationMapper.selectTHmStationList(tHmStation);
        if (tHmStationList.size() > 0) {
            for (THmStation hmStation : tHmStationList) {
                if (hmStation.getHstationParentid() != 0) {
                    THmStation hmStation1 = tHmStationMapper.selectTHmStationById(hmStation.getHstationParentid());
                    if (hmStation1 != null) {
                        hmStation.setParentName(hmStation1.getHstationName());
                    }
                }
            }
        }
        return tHmStationList;
    }

    /**
     * 新增热源站
     *
     * @param tHmStation 热源站
     * @return 结果
     */
    @Override
    public int insertTHmStation(THmStation tHmStation) {
        THmStation tHmStation1 = tHmStationMapper.selectTHmStationById(tHmStation.getHstationParentid());
        tHmStation.setParentName(tHmStation1.getHstationName());
        tHmStation.setHstationAdduserid(ShiroUtils.getSysUser().getUserId());
        tHmStation.setHstationAddtime(sdf.format(new Date()));
        tHmStation.setHstationState(0);
        return tHmStationMapper.insertTHmStation(tHmStation);
    }

    /**
     * 修改热源站
     *
     * @param tHmStation 热源站
     * @return 结果
     */
    @Override
    public int updateTHmStation(THmStation tHmStation) {
        return tHmStationMapper.updateTHmStation(tHmStation);
    }


    /**
     * 查询热源站树列表
     *
     * @return 所有热源站信息
     */
    @Override
    public List<Ztree> selectTHmStationTree() {
        List<THmStation> tHmStationList = tHmStationMapper.selectTHmStationList(new THmStation());
        List<Ztree> ztrees = new ArrayList<Ztree>();
        for (THmStation tHmStation : tHmStationList) {
            Ztree ztree = new Ztree();
            ztree.setId(tHmStation.getHstationId());
            ztree.setpId(tHmStation.getHstationParentid());
            ztree.setName(tHmStation.getHstationName());
            ztree.setTitle(tHmStation.getHstationName());
            ztree.setHstationLevel(tHmStation.getHstationLevel());
            ztrees.add(ztree);
        }
        return ztrees;
    }
}
