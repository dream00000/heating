package com.ruoyi.business.service;

import com.ruoyi.business.domain.THmMeterprice;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用热单价Service接口
 */
public interface ITHmMeterpriceService {
    /**
     * 查询用热单价
     */
    public THmMeterprice selectTHmMeterpriceById(@Param("id") int id);

    /**
     * 查询用热单价列表
     */
    public List<THmMeterprice> selectTHmMeterpriceList(THmMeterprice tHmMeterprice);

    /**
     * 新增用热单价
     */
    public int insertTHmMeterprice(THmMeterprice tHmMeterprice);

    /**
     * 修改用热单价
     */
    public int updateTHmMeterprice(THmMeterprice tHmMeterprice);
}
