package com.ruoyi.business.domain.vo;

import lombok.Data;

@Data
public class StationDetailVO {

    //开始年
    private String stTime;
    //结束年
    private String endTime;
    //点选的热源站的id
    private String stationId;
    private String priceId;

    private String parentName;
    private String stationName;

    private String dShi;
    private String dTing;
    private String dHu;
    private String dZong;

    private String gShi;
    private String gTing;
    private String gHu;
    private String gZong;

    private String zShi;
    private String zTing;
    private String zHu;
    private String zZong;

    private String zzShi;
    private String zzTing;
    private String zzHu;
    private String zzZong;
}
