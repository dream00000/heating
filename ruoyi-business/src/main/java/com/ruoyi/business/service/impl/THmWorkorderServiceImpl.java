package com.ruoyi.business.service.impl;

import com.ruoyi.business.domain.THmWorkorder;
import com.ruoyi.business.mapper.THmWorkorderMapper;
import com.ruoyi.business.service.ITHmWorkorderService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 客服工单Service业务层处理
 *
 * @author ruoyi
 * @date 2022-02-26
 */
@Service
public class THmWorkorderServiceImpl implements ITHmWorkorderService {
    @Resource
    private THmWorkorderMapper tHmWorkorderMapper;

    /**
     * 查询客服工单
     *
     * @param id
     * @return 客服工单
     */
    @Override
    public THmWorkorder selectTHmWorkorderById(int id) {
        return tHmWorkorderMapper.selectTHmWorkorderById(id);
    }

    /**
     * 查询客服工单列表
     *
     * @param tHmWorkorder 客服工单
     * @return 客服工单
     */
    @Override
    public List<THmWorkorder> selectTHmWorkorderList(THmWorkorder tHmWorkorder) {
        return tHmWorkorderMapper.selectTHmWorkorderList(tHmWorkorder);
    }

    @Override
    public int insertTHmWorkorder(THmWorkorder tHmWorkorder) {
        return tHmWorkorderMapper.insertTHmWorkorder(tHmWorkorder);
    }

}
