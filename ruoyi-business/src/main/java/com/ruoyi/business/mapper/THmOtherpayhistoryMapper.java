package com.ruoyi.business.mapper;

import com.ruoyi.business.domain.THmCustomer;
import com.ruoyi.business.domain.THmOtherpayhistory;
import com.ruoyi.business.domain.vo.OtherpayhistoryVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 其他收费Mapper接口
 */
public interface THmOtherpayhistoryMapper {
    /**
     * 查询其他收费
     */
    THmOtherpayhistory selectTHmOtherpayhistoryById(@Param("id") int id);

    /**
     * 查询其他收费列表
     */
    List<THmOtherpayhistory> selectTHmOtherpayhistoryList(THmOtherpayhistory tHmOtherpayhistory);

    List<THmOtherpayhistory> lists(THmOtherpayhistory tHmOtherpayhistory);

    List<OtherpayhistoryVO> selectTHmOtherpayhistoryListt(THmCustomer tHmCustomer);

    List<THmOtherpayhistory> selectTHmOtherpayhistoryLists(@Param("hcustomerId") int id);

    /**
     * 新增其他收费
     */
    int insertTHmOtherpayhistory(THmOtherpayhistory tHmOtherpayhistory);

    /**
     * 修改其他收费
     */
    int updateTHmOtherpayhistory(THmOtherpayhistory tHmOtherpayhistory);

    int reportBillCount(THmOtherpayhistory tHmOtherpayhistory);

    String reportBillSum(THmOtherpayhistory tHmOtherpayhistory);

    /**
     * 业务查询 其他收费总金额
     */
    String payTotal(THmOtherpayhistory tHmOtherpayhistory);

    String total(Integer hcustomerId);
}
