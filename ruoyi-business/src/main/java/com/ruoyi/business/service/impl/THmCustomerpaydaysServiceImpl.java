package com.ruoyi.business.service.impl;

import com.ruoyi.business.domain.*;
import com.ruoyi.business.domain.vo.CustomerpaydaysVO;
import com.ruoyi.business.domain.vo.OrderKeFu;
import com.ruoyi.business.mapper.*;
import com.ruoyi.business.service.ITHmCustomerpaydaysService;
import com.ruoyi.business.util.LingUtil;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.service.ISysDictDataService;
import org.apache.shiro.util.ThreadContext;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 用户年度账期Service业务层处理
 *
 * @author ruoyi
 * @date 2021-08-13
 */
@Service("CustomerpaydaysService")
@EnableScheduling
public class THmCustomerpaydaysServiceImpl implements ITHmCustomerpaydaysService {

    @Resource
    private THmCustomerpaydaysMapper tHmCustomerpaydaysMapper;

    @Resource
    private THmPaydaysMapper tHmPaydaysMapper;

    @Resource
    private THmAreaMapper tHmAreaMapper;

    @Resource
    private THmCustomerMapper tHmCustomerMapper;

    @Resource
    private THmCustomerpriceMapper tHmCustomerpriceMapper;

    @Resource
    private THmMeterpriceMapper tHmMeterpriceMapper;

    @Resource
    private THmCustomerpaydaysPriceMapper tHmCustomerpaydaysPriceMapper;

    @Resource
    private ISysDictDataService iSysDictDataService;

    @Resource
    private THmMessageMapper tHmMessageMapper;

    @Resource
    WxPayBean wxPayBean;

    SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * 查询用户年度账期
     */
    @Override
    public THmCustomerpaydays selectTHmCustomerpaydaysById(int id) {
        THmCustomerpaydays tHmCustomerpaydays = tHmCustomerpaydaysMapper.selectTHmCustomerpaydaysById(id);
        THmCustomer tHmCustomer = tHmCustomerMapper.selectTHmCustomerById(tHmCustomerpaydays.getHcustomerId());
        if (tHmCustomer != null) {
            tHmCustomerpaydays.setCustomerCode(tHmCustomer.getHcustomerCode());
            tHmCustomerpaydays.setHcustomerDetailaddress(tHmCustomer.getHcustomerDetailaddress());
            tHmCustomerpaydays.setCustomerName(tHmCustomer.getHcustomerName());
            tHmCustomerpaydays.setPhone(tHmCustomer.getHcustomerMobile());
            tHmCustomerpaydays.setBalance(tHmCustomer.getBalance());
            THmArea tHmArea = tHmAreaMapper.selectTHmAreaById(tHmCustomer.getHareaId());
            if (tHmArea != null) {
                if (tHmArea.getHareaLevel() == 2) {
                    tHmCustomerpaydays.setAreaName(tHmArea.getHareaName());
                } else {
                    THmArea tHmArea1 = tHmAreaMapper.selectTHmAreaById(tHmArea.getHareaParentid());
                    tHmCustomerpaydays.setAreaName(tHmArea1.getHareaName());
                }
            }
            THmCustomerpaydaysPrice tHmCustomerpaydaysPrice = new THmCustomerpaydaysPrice();
            tHmCustomerpaydaysPrice.setCustomerpaydaysId(id);
            List<THmCustomerpaydaysPrice> tHmCustomerpriceList = tHmCustomerpaydaysPriceMapper.selectTHmCustomerpaydaysPriceList(tHmCustomerpaydaysPrice);
            tHmCustomerpaydays.setCustomerpriceList(tHmCustomerpriceList);
        }
        return tHmCustomerpaydays;
    }

    /**
     * 查询用户年度账期列表
     */
    @Override
    public List<THmCustomerpaydays> selectTHmCustomerpaydaysList(THmCustomerpaydays tHmCustomerpaydays) {
        List<THmCustomerpaydays> list = tHmCustomerpaydaysMapper.selectTHmCustomerpaydaysList(tHmCustomerpaydays);
        for (THmCustomerpaydays hmCustomerpaydays : list) {
            THmCustomer tHmCustomer = tHmCustomerMapper.selectTHmCustomerById(hmCustomerpaydays.getHcustomerId());
            hmCustomerpaydays.setCustomerName(tHmCustomer.getHcustomerName());
            hmCustomerpaydays.setPhone(tHmCustomer.getHcustomerMobile());
            hmCustomerpaydays.setCustomerCode(tHmCustomer.getHcustomerCode());
            hmCustomerpaydays.setHcustomerDetailaddress(tHmCustomer.getHcustomerDetailaddress());
            THmArea tHmArea = tHmAreaMapper.selectTHmAreaById(tHmCustomer.getHareaId());
            if (tHmArea != null) {
                if (tHmArea.getHareaLevel() == 2) {
                    hmCustomerpaydays.setAreaName(tHmArea.getHareaName());
                } else {
                    THmArea tHmArea1 = tHmAreaMapper.selectTHmAreaById(tHmArea.getHareaParentid());
                    hmCustomerpaydays.setAreaName(tHmArea1.getHareaName());
                }
            }
        }
        return list;
    }

    @Override
    public List<THmCustomerpaydays> getcuswq(Long hcustomerId) {
        return tHmCustomerpaydaysMapper.getcuswq(hcustomerId);
    }

    @Override
    public List<THmCustomerpaydays> selectTHmCustomerpaydaysListm(THmCustomerpaydays tHmCustomerpaydays) {
        return tHmCustomerpaydaysMapper.selectTHmCustomerpaydaysListm(tHmCustomerpaydays);
    }

    @Override
    public List<CustomerpaydaysVO> selectTHmCustomerpaydaysLists(THmCustomer tHmCustomer) {
        if (tHmCustomer.getHzqGrstarttime() != null && !tHmCustomer.getHzqGrstarttime().equals("")) {
            tHmCustomer.setHzqGrstarttime(tHmCustomer.getHzqGrstarttime().split("-")[0]);
        }
        List<CustomerpaydaysVO> list = tHmCustomerpaydaysMapper.selectTHmCustomerpaydaysLists(tHmCustomer);
        for (CustomerpaydaysVO customerpaydaysVO : list) {
            //用户年度
            customerpaydaysVO.setNiandu(customerpaydaysVO.getHzqGrstarttime() + "-" + customerpaydaysVO.getHzqGrendtime());
            if (customerpaydaysVO.getWqInt1() == 3) {
                if (customerpaydaysVO.getWqStr4() != null) {
                    customerpaydaysVO.setWqStr2(new BigDecimal(customerpaydaysVO.getWqStr2()).add(new BigDecimal(customerpaydaysVO.getWqStr4())).toString());
                }
            } else if (customerpaydaysVO.getWqInt1() == 5) {
                customerpaydaysVO.setWqStr2(customerpaydaysVO.getWqStr5());
            } else if (customerpaydaysVO.getWqInt1() == 99) {
                customerpaydaysVO.setWqStr2(customerpaydaysVO.getWqStr1());
            }
        }
        return list;
    }

    //账期缴没缴费
    @Override
    public List<CustomerpaydaysVO> jiaoList(THmCustomer tHmCustomer) {
        if (tHmCustomer.getHzqGrstarttime() != null && !tHmCustomer.getHzqGrstarttime().equals("")) {
            tHmCustomer.setHzqGrstarttime(tHmCustomer.getHzqGrstarttime().split("-")[0]);
        }
        List<CustomerpaydaysVO> list = tHmCustomerpaydaysMapper.jiaoList(tHmCustomer);
        if (list.size() > 0) {
            for (CustomerpaydaysVO customerpaydaysVO : list) {
                customerpaydaysVO.setNiandu(customerpaydaysVO.getHzqGrstarttime() + "-" + customerpaydaysVO.getHzqGrendtime());
                if (customerpaydaysVO.getWqInt1() == 0 || customerpaydaysVO.getWqInt1() == 1 || customerpaydaysVO.getWqInt1() == 2) {
                    customerpaydaysVO.setJiaoType(1);
                } else {
                    customerpaydaysVO.setJiaoType(2);
                    if (customerpaydaysVO.getWqInt1() == 6) {
                        customerpaydaysVO.setWqStr1(customerpaydaysVO.getWqStr6());
                    }
                }
            }
        }
        return list;
    }

    @Override
    public int exportCount(THmCustomer tHmCustomer) {
        if (tHmCustomer.getHzqGrstarttime() != null && !tHmCustomer.getHzqGrstarttime().equals("")) {
            tHmCustomer.setHzqGrstarttime(tHmCustomer.getHzqGrstarttime().split("-")[0]);
        }
        return tHmCustomerpaydaysMapper.exportCount(tHmCustomer);
    }

    /**
     * 新增用户年度账期
     */
    @Override
    public int insertTHmCustomerpaydays(THmCustomerpaydays tHmCustomerpaydays) {
        return tHmCustomerpaydaysMapper.insertTHmCustomerpaydays(tHmCustomerpaydays);
    }

    /**
     * 修改用户年度账期
     */
    @Override
    public int updateTHmCustomerpaydays(THmCustomerpaydays tHmCustomerpaydays) {
        return tHmCustomerpaydaysMapper.updateTHmCustomerpaydays(tHmCustomerpaydays);
    }

    //生成账期  -- 单个
    @Override
    public int initPay(String thisYear, Integer ling, String hcustomerCode) {

        String nextYear = String.valueOf(Integer.parseInt(thisYear) + 1);//明年

        //查出所有的用户
        List<THmCustomer> tHmCustomerList = tHmCustomerMapper.getCustomerNotShengBenNian(thisYear, nextYear, hcustomerCode);
        for (THmCustomer hmCustomer : tHmCustomerList) {
            //生成用户年度账期
            THmCustomerpaydays tHmCustomerpaydays = new THmCustomerpaydays();
            tHmCustomerpaydays.setHcustomerId(hmCustomer.getHcustomerId());//用户id
            tHmCustomerpaydays.setHzqGrstarttime(thisYear);//开始年度
            tHmCustomerpaydays.setHzqGrendtime(nextYear);//结束年度
            //账期策略id
            tHmCustomerpaydays.setHzqId(hmCustomer.getHzqId());
            //零钱计算方式
            tHmCustomerpaydays.setLing(ling);
            //获取用户单价
            THmCustomerprice tHmCustomerprice = new THmCustomerprice();
            tHmCustomerprice.setCustomerId(hmCustomer.getHcustomerId());
            List<THmCustomerprice> tHmCustomerpriceList = tHmCustomerpriceMapper.selectTHmCustomerpriceList(tHmCustomerprice);
            //当期的计算总金额
            BigDecimal bds = new BigDecimal("0.00");
            //计算当期金额
            if (tHmCustomerpriceList.size() > 0) {
                for (THmCustomerprice hmCustomerprice : tHmCustomerpriceList) {
                    //计算当期账期金额
                    BigDecimal bd = new BigDecimal(hmCustomerprice.getTotal());
                    bds = bds.add(bd);
                }
            }

            THmMessage tHmMessage = new THmMessage();

            switch (ling) {
                //取整舍
                case 1:
                    tHmCustomerpaydays.setWqStr1(LingUtil.zhengShe(bds.toString()));
                    break;
                //取整入
                case 2:
                    tHmCustomerpaydays.setWqStr1(LingUtil.zhengRu(bds.toString()));
                    break;
                //按原价
                case 3:
                    tHmCustomerpaydays.setWqStr1(LingUtil.erRu(bds.toString()));
                    break;
                //保留一位小数舍
                case 4:
                    tHmCustomerpaydays.setWqStr1(LingUtil.yiShe(bds.toString()));
                    break;
                //保留一位小数入
                case 5:
                    tHmCustomerpaydays.setWqStr1(LingUtil.yiRu(bds.toString()));
                    break;
                //保留两位小数入
                case 6:
                    tHmCustomerpaydays.setWqStr1(LingUtil.erRu(bds.toString()));
                    break;
            }
            //查询历史账期是否有延期的，有延期的生成欠费费用
            THmCustomerpaydays hmCustomerpaydays1 = tHmCustomerpaydaysMapper.getLastYan(hmCustomer.getHcustomerId());
            if (hmCustomerpaydays1 != null) {
                //处理上一期
                THmCustomerpaydaysPrice price = new THmCustomerpaydaysPrice();
                price.setCustomerpaydaysId(hmCustomerpaydays1.getWqId());
                List<THmCustomerpaydaysPrice> list = tHmCustomerpaydaysPriceMapper.selectTHmCustomerpaydaysPriceList(price);
                BigDecimal bd = new BigDecimal("0.00");
                for (THmCustomerpaydaysPrice price1 : list) {
                    if (price1.getType() == 3) {
                        price1.setTotal(new BigDecimal(price1.getArea()).multiply(new BigDecimal(price1.getPrice())).multiply(new BigDecimal(price1.getXishu())).divide(new BigDecimal("100")).toString());
                    } else {
                        price1.setTotal(new BigDecimal(price1.getArea()).multiply(new BigDecimal(price1.getPrice())).multiply(new BigDecimal(price1.getXishu())).divide(new BigDecimal("100")).multiply(new BigDecimal("0.2")).toString());
                    }
                    bd = bd.add(new BigDecimal(price1.getTotal()));
                }
                //应缴金额和当期欠款金额
                switch (hmCustomerpaydays1.getLing()) {
                    //取整舍
                    case 1:
                        hmCustomerpaydays1.setWqStr1(LingUtil.zhengShe(bd.toString()));
                        hmCustomerpaydays1.setWqStr2(LingUtil.zhengShe(bd.toString()));
                        break;
                    //取整入
                    case 2:
                        hmCustomerpaydays1.setWqStr1(LingUtil.zhengRu(bd.toString()));
                        hmCustomerpaydays1.setWqStr2(LingUtil.zhengRu(bd.toString()));
                        break;
                    //按原价
                    case 3:
                        hmCustomerpaydays1.setWqStr1(LingUtil.erRu(bd.toString()));
                        hmCustomerpaydays1.setWqStr2(LingUtil.erRu(bd.toString()));
                        break;
                    //保留一位小数舍
                    case 4:
                        hmCustomerpaydays1.setWqStr1(LingUtil.yiShe(bd.toString()));
                        hmCustomerpaydays1.setWqStr2(LingUtil.yiShe(bd.toString()));
                        break;
                    //保留一位小数入
                    case 5:
                        hmCustomerpaydays1.setWqStr1(LingUtil.yiRu(bd.toString()));
                        hmCustomerpaydays1.setWqStr2(LingUtil.yiRu(bd.toString()));
                        break;
                    //保留两位小数入
                    case 6:
                        hmCustomerpaydays1.setWqStr1(LingUtil.erRu(bd.toString()));
                        hmCustomerpaydays1.setWqStr2(LingUtil.erRu(bd.toString()));
                        break;
                }
                hmCustomerpaydays1.setPayType(0);
                tHmCustomerpaydaysMapper.updateTHmCustomerpaydays(hmCustomerpaydays1);
            }
            //应付金额
            BigDecimal bdd = new BigDecimal("0.00");
            if (tHmCustomerpriceList.size() > 0) {
                for (THmCustomerprice hmCustomerprice : tHmCustomerpriceList) {
                    //计算当期账期金额
                    BigDecimal bd = new BigDecimal(hmCustomerprice.getTotal());
                    bdd = bdd.add(bd);
                }
            }
            tHmCustomerpaydays.setHcustomerMoney(bdd.toString());
            //用户的房屋信息
            tHmCustomerpaydays.setHcustomerHomearea(hmCustomer.getHcustomerHomearea());
            tHmCustomerpaydays.setHcustomerAbovearea(hmCustomer.getHcustomerAbovearea());
            tHmCustomerpaydays.setHcustomerActualarea(hmCustomer.getHcustomerActualarea());
            tHmCustomerpaydays.setHcustomerTgarea(hmCustomer.getHcustomerTgarea());
            //计划供热天数
            tHmCustomerpaydays.setHzqGrdays(hmCustomer.getHzqGrdays());
            //账期int1
            Calendar cal = Calendar.getInstance();
            int day = cal.get(Calendar.DATE);
            int month = cal.get(Calendar.MONTH) + 1;
            int year = cal.get(Calendar.YEAR);
            //年度在今年之前的为3
            if (year - Integer.parseInt(thisYear) > 0) {
                tHmCustomerpaydays.setWqInt1(3);
            } else if (year - Integer.parseInt(thisYear) == 0) {
                THmPaydays tHmPaydays = tHmPaydaysMapper.selectTHmPaydaysById(hmCustomer.getHzqId());
                //年度是今年的 判断今日月份与供热开始月份
                String[] paydays = tHmPaydays.getHzqGrstarttime().split("-");
                //年度是今年的 判断今日日期
                if (month - Integer.parseInt(paydays[0]) > 0) {
                    tHmCustomerpaydays.setWqInt1(3);
                } else if (month - Integer.parseInt(paydays[0]) == 0) {
                    if (day - Integer.parseInt(paydays[1]) > 0) {
                        tHmCustomerpaydays.setWqInt1(3);
                    } else {
                        tHmCustomerpaydays.setWqInt1(99);
                    }
                } else if (month - Integer.parseInt(paydays[0]) < 0) {
                    tHmCustomerpaydays.setWqInt1(99);
                }
            } else if (year - Integer.parseInt(thisYear) < 0) {
                tHmCustomerpaydays.setWqInt1(99);
            }
            //生成时间
            tHmCustomerpaydays.setWqAddtime(sdf.format(new Date()));
            //状态
            tHmCustomerpaydays.setWqState(0);
            //优惠金额
            tHmCustomerpaydays.setHcustomerGzje("0.00");
            tHmCustomerpaydays.setWqStr2("0.00");
            tHmCustomerpaydays.setWqStr3("0");
            tHmCustomerpaydays.setWqStr4("0.00");
            tHmCustomerpaydays.setWqStr5("0.00");
            tHmCustomerpaydays.setWqStr6("0.00");
            tHmCustomerpaydays.setIsTiao(0);//是否被调整过
            tHmCustomerpaydays.setPayType(hmCustomer.getPayType());
            tHmCustomerpaydaysMapper.insertTHmCustomerpaydays(tHmCustomerpaydays);
            //生成消息记录表
            THmCustomer tHmCustomer = new THmCustomer();
            tHmCustomer.setHcustomerId(hmCustomer.getHcustomerId());
            List<THmCustomer> wechatBindList = tHmCustomerMapper.bindlist(tHmCustomer);
            if (wechatBindList.size() > 0) {
                tHmMessage.setCustomerId(hmCustomer.getHcustomerId());
                THmPaydays hmPaydays = tHmPaydaysMapper.selectTHmPaydaysById(hmCustomer.getHzqId());
                tHmMessage.setFtime(hmPaydays.getHzqEndtime());
                tHmMessage.setJtime("10月20日之前");
                tHmMessage.setNian(thisYear + "-" + nextYear);
                tHmMessage.setTemplateId(wxPayBean.getTemplateId());
                tHmMessage.setTotal(tHmCustomerpaydays.getWqStr1());
                tHmMessage.setType(0);
                tHmMessage.setWqId(tHmCustomerpaydays.getWqId());
                for (THmCustomer customer : wechatBindList) {
                    tHmMessage.setOpenId(customer.getOpenId());
                    tHmMessageMapper.insertTHmMessage(tHmMessage);
                }
            }
            //保存账期单价
            if (tHmCustomerpriceList.size() > 0) {
                for (THmCustomerprice hmCustomerprice : tHmCustomerpriceList) {
                    THmCustomerpaydaysPrice tHmCustomerpaydaysPrice = new THmCustomerpaydaysPrice();
                    tHmCustomerpaydaysPrice.setAddTime(new Date());
                    tHmCustomerpaydaysPrice.setAddUser(ShiroUtils.getSysUser().getUserId());
                    THmMeterprice tHmMeterprice1 = tHmMeterpriceMapper.selectTHmMeterpriceById(hmCustomerprice.getPriceId());
                    tHmCustomerpaydaysPrice.setPrice(tHmMeterprice1.getRjPrice());
                    tHmCustomerpaydaysPrice.setPriceType(iSysDictDataService.selectDictLabel("data_price", tHmMeterprice1.getRjDjid().toString()));
                    tHmCustomerpaydaysPrice.setArea(hmCustomerprice.getArea());
                    tHmCustomerpaydaysPrice.setType(hmCustomerprice.getType());
                    tHmCustomerpaydaysPrice.setXishu(hmCustomerprice.getRemark());
                    tHmCustomerpaydaysPrice.setCustomerpaydaysId(tHmCustomerpaydays.getWqId());
                    tHmCustomerpaydaysPriceMapper.insertTHmCustomerpaydaysPrice(tHmCustomerpaydaysPrice);
                }
            }
            //更新用户int4 最后一次账期状态
            THmCustomer tHmCustomer1 = new THmCustomer();
            tHmCustomer1.setHcustomerInt4(99);
            tHmCustomer1.setHcustomerId(hmCustomer.getHcustomerId());
            tHmCustomer1.setIsZheng(0);
            tHmCustomerMapper.updateTHmCustomer(tHmCustomer1);
        }
        return 1;
    }

    //生成账期 -- 全部
    @Override
    public int initPays(String thisYear, Integer ling) {

        String nextYear = String.valueOf(Integer.parseInt(thisYear) + 1);//明年

        //查出所有的用户
        List<THmCustomer> tHmCustomerList = tHmCustomerMapper.getCustomerNotShengBenNians(thisYear, nextYear);
        for (THmCustomer hmCustomer : tHmCustomerList) {
            //生成用户年度账期
            THmCustomerpaydays tHmCustomerpaydays = new THmCustomerpaydays();
            tHmCustomerpaydays.setHcustomerId(hmCustomer.getHcustomerId());//用户id
            tHmCustomerpaydays.setHzqGrstarttime(thisYear);//开始年度
            tHmCustomerpaydays.setHzqGrendtime(nextYear);//结束年度
            //账期策略id
            tHmCustomerpaydays.setHzqId(hmCustomer.getHzqId());
            //零钱计算方式
            tHmCustomerpaydays.setLing(ling);
            //获取用户单价
            THmCustomerprice tHmCustomerprice = new THmCustomerprice();
            tHmCustomerprice.setCustomerId(hmCustomer.getHcustomerId());
            List<THmCustomerprice> tHmCustomerpriceList = tHmCustomerpriceMapper.selectTHmCustomerpriceList(tHmCustomerprice);
            //当期的计算总金额
            BigDecimal bds = new BigDecimal("0.00");
            //计算当期金额
            if (tHmCustomerpriceList.size() > 0) {
                for (THmCustomerprice hmCustomerprice : tHmCustomerpriceList) {
                    //计算当期账期金额
                    BigDecimal bd = new BigDecimal(hmCustomerprice.getTotal());
                    bds = bds.add(bd);
                }
            }
            switch (ling) {
                //取整舍
                case 1:
                    tHmCustomerpaydays.setWqStr1(LingUtil.zhengShe(bds.toString()));
                    break;
                //取整入
                case 2:
                    tHmCustomerpaydays.setWqStr1(LingUtil.zhengRu(bds.toString()));
                    break;
                //按原价
                case 3:
                    tHmCustomerpaydays.setWqStr1(LingUtil.erRu(bds.toString()));
                    break;
                //保留一位小数舍
                case 4:
                    tHmCustomerpaydays.setWqStr1(LingUtil.yiShe(bds.toString()));
                    break;
                //保留一位小数入
                case 5:
                    tHmCustomerpaydays.setWqStr1(LingUtil.yiRu(bds.toString()));
                    break;
                //保留两位小数入
                case 6:
                    tHmCustomerpaydays.setWqStr1(LingUtil.erRu(bds.toString()));
                    break;
            }
            //查询历史账期是否有延期的，有延期的生成欠费费用
            THmCustomerpaydays hmCustomerpaydays1 = tHmCustomerpaydaysMapper.getLastYan(hmCustomer.getHcustomerId());
            if (hmCustomerpaydays1 != null) {
                //处理上一期
                THmCustomerpaydaysPrice price = new THmCustomerpaydaysPrice();
                price.setCustomerpaydaysId(hmCustomerpaydays1.getWqId());
                List<THmCustomerpaydaysPrice> list = tHmCustomerpaydaysPriceMapper.selectTHmCustomerpaydaysPriceList(price);
                BigDecimal bd = new BigDecimal("0.00");
                for (THmCustomerpaydaysPrice price1 : list) {
                    if (price1.getType() == 3) {
                        price1.setTotal(new BigDecimal(price1.getArea()).multiply(new BigDecimal(price1.getPrice())).multiply(new BigDecimal(price1.getXishu())).divide(new BigDecimal("100")).toString());
                    } else {
                        price1.setTotal(new BigDecimal(price1.getArea()).multiply(new BigDecimal(price1.getPrice())).multiply(new BigDecimal(price1.getXishu())).divide(new BigDecimal("100")).multiply(new BigDecimal("0.2")).toString());
                    }
                    bd = bd.add(new BigDecimal(price1.getTotal()));
                }
                //应缴金额和当期欠款金额
                switch (hmCustomerpaydays1.getLing()) {
                    //取整舍
                    case 1:
                        hmCustomerpaydays1.setWqStr1(LingUtil.zhengShe(bd.toString()));
                        hmCustomerpaydays1.setWqStr2(LingUtil.zhengShe(bd.toString()));
                        break;
                    //取整入
                    case 2:
                        hmCustomerpaydays1.setWqStr1(LingUtil.zhengRu(bd.toString()));
                        hmCustomerpaydays1.setWqStr2(LingUtil.zhengRu(bd.toString()));
                        break;
                    //按原价
                    case 3:
                        hmCustomerpaydays1.setWqStr1(LingUtil.erRu(bd.toString()));
                        hmCustomerpaydays1.setWqStr2(LingUtil.erRu(bd.toString()));
                        break;
                    //保留一位小数舍
                    case 4:
                        hmCustomerpaydays1.setWqStr1(LingUtil.yiShe(bd.toString()));
                        hmCustomerpaydays1.setWqStr2(LingUtil.yiShe(bd.toString()));
                        break;
                    //保留一位小数入
                    case 5:
                        hmCustomerpaydays1.setWqStr1(LingUtil.yiRu(bd.toString()));
                        hmCustomerpaydays1.setWqStr2(LingUtil.yiRu(bd.toString()));
                        break;
                    //保留两位小数入
                    case 6:
                        hmCustomerpaydays1.setWqStr1(LingUtil.erRu(bd.toString()));
                        hmCustomerpaydays1.setWqStr2(LingUtil.erRu(bd.toString()));
                        break;
                }
                hmCustomerpaydays1.setPayType(0);
                tHmCustomerpaydaysMapper.updateTHmCustomerpaydays(hmCustomerpaydays1);
            }
            //应付金额
            BigDecimal bdd = new BigDecimal("0.00");
            if (tHmCustomerpriceList.size() > 0) {
                for (THmCustomerprice hmCustomerprice : tHmCustomerpriceList) {
                    //计算当期账期金额
                    BigDecimal bd = new BigDecimal(hmCustomerprice.getTotal());
                    bdd = bdd.add(bd);
                }
            }
            tHmCustomerpaydays.setHcustomerMoney(bdd.toString());
            //用户的房屋信息
            tHmCustomerpaydays.setHcustomerHomearea(hmCustomer.getHcustomerHomearea());
            tHmCustomerpaydays.setHcustomerAbovearea(hmCustomer.getHcustomerAbovearea());
            tHmCustomerpaydays.setHcustomerActualarea(hmCustomer.getHcustomerActualarea());
            tHmCustomerpaydays.setHcustomerTgarea(hmCustomer.getHcustomerTgarea());
            //计划供热天数
            tHmCustomerpaydays.setHzqGrdays(hmCustomer.getHzqGrdays());
            //账期int1
            Calendar cal = Calendar.getInstance();
            int day = cal.get(Calendar.DATE);
            int month = cal.get(Calendar.MONTH) + 1;
            int year = cal.get(Calendar.YEAR);
            //年度在今年之前的为3
            if (year - Integer.parseInt(thisYear) > 0) {
                tHmCustomerpaydays.setWqInt1(3);
            } else if (year - Integer.parseInt(thisYear) == 0) {
                THmPaydays tHmPaydays = tHmPaydaysMapper.selectTHmPaydaysById(hmCustomer.getHzqId());
                //年度是今年的 判断今日月份与供热开始月份
                String[] paydays = tHmPaydays.getHzqGrstarttime().split("-");
                //年度是今年的 判断今日日期
                if (month - Integer.parseInt(paydays[0]) > 0) {
                    tHmCustomerpaydays.setWqInt1(3);
                } else if (month - Integer.parseInt(paydays[0]) == 0) {
                    if (day - Integer.parseInt(paydays[1]) > 0) {
                        tHmCustomerpaydays.setWqInt1(3);
                    } else {
                        tHmCustomerpaydays.setWqInt1(99);
                    }
                } else if (month - Integer.parseInt(paydays[0]) < 0) {
                    tHmCustomerpaydays.setWqInt1(99);
                }
            } else if (year - Integer.parseInt(thisYear) < 0) {
                tHmCustomerpaydays.setWqInt1(99);
            }
            //生成时间
            tHmCustomerpaydays.setWqAddtime(sdf.format(new Date()));
            //状态
            tHmCustomerpaydays.setWqState(0);
            //优惠金额
            tHmCustomerpaydays.setHcustomerGzje("0.00");
            tHmCustomerpaydays.setWqStr2("0.00");
            tHmCustomerpaydays.setWqStr3("0");
            tHmCustomerpaydays.setWqStr4("0.00");
            tHmCustomerpaydays.setWqStr5("0.00");
            tHmCustomerpaydays.setWqStr6("0.00");
            tHmCustomerpaydays.setIsTiao(0);//是否被调整过
            tHmCustomerpaydays.setPayType(hmCustomer.getPayType());
            tHmCustomerpaydaysMapper.insertTHmCustomerpaydays(tHmCustomerpaydays);
            //生成消息记录表
            THmCustomer tHmCustomer = new THmCustomer();
            tHmCustomer.setHcustomerId(hmCustomer.getHcustomerId());
            List<THmCustomer> wechatBindList = tHmCustomerMapper.bindlist(tHmCustomer);
            if (wechatBindList.size() > 0) {
                THmMessage tHmMessage = new THmMessage();
                tHmMessage.setCustomerId(hmCustomer.getHcustomerId());
                THmPaydays hmPaydays = tHmPaydaysMapper.selectTHmPaydaysById(hmCustomer.getHzqId());
                tHmMessage.setFtime(hmPaydays.getHzqEndtime());
                tHmMessage.setJtime("10月20日之前");
                tHmMessage.setNian(thisYear + "-" + nextYear);
                tHmMessage.setTemplateId(wxPayBean.getTemplateId());
                tHmMessage.setTotal(tHmCustomerpaydays.getWqStr1());
                tHmMessage.setType(0);
                tHmMessage.setWqId(tHmCustomerpaydays.getWqId());
                for (THmCustomer customer : wechatBindList) {
                    tHmMessage.setOpenId(customer.getOpenId());
                    tHmMessageMapper.insertTHmMessage(tHmMessage);
                }
            }
            //保存账期单价
            if (tHmCustomerpriceList.size() > 0) {
                for (THmCustomerprice hmCustomerprice : tHmCustomerpriceList) {
                    THmCustomerpaydaysPrice tHmCustomerpaydaysPrice = new THmCustomerpaydaysPrice();
                    tHmCustomerpaydaysPrice.setAddTime(new Date());
                    tHmCustomerpaydaysPrice.setAddUser(ShiroUtils.getSysUser().getUserId());
                    THmMeterprice tHmMeterprice1 = tHmMeterpriceMapper.selectTHmMeterpriceById(hmCustomerprice.getPriceId());
                    tHmCustomerpaydaysPrice.setPrice(tHmMeterprice1.getRjPrice());
                    tHmCustomerpaydaysPrice.setPriceType(iSysDictDataService.selectDictLabel("data_price", tHmMeterprice1.getRjDjid().toString()));
                    tHmCustomerpaydaysPrice.setArea(hmCustomerprice.getArea());
                    tHmCustomerpaydaysPrice.setType(hmCustomerprice.getType());
                    tHmCustomerpaydaysPrice.setXishu(hmCustomerprice.getRemark());
                    tHmCustomerpaydaysPrice.setCustomerpaydaysId(tHmCustomerpaydays.getWqId());
                    tHmCustomerpaydaysPriceMapper.insertTHmCustomerpaydaysPrice(tHmCustomerpaydaysPrice);
                }
            }
            //更新用户int4 最后一次账期状态
            THmCustomer tHmCustomer1 = new THmCustomer();
            tHmCustomer1.setHcustomerInt4(99);
            tHmCustomer1.setHcustomerId(hmCustomer.getHcustomerId());
            tHmCustomer1.setIsZheng(0);
            tHmCustomerMapper.updateTHmCustomer(tHmCustomer1);
        }
        return 1;
    }

    //生成账期 定时任务
    @Scheduled(cron = "0 0 1 * * ?")
    public void initPays() {
        DefaultWebSecurityManager manager = new DefaultWebSecurityManager();
        ThreadContext.bind(manager);
        System.out.println("生成账期方法启动");
        Calendar date = Calendar.getInstance();
        String thisYear = String.valueOf(date.get(Calendar.YEAR));//今年
        String nextYear = String.valueOf(Integer.parseInt(thisYear) + 1);//明年
        //查出所有的用户
        List<THmCustomer> tHmCustomerList = tHmCustomerMapper.getCustomerNotShengBenNians(thisYear, nextYear);
        for (THmCustomer hmCustomer : tHmCustomerList) {
            //获取分区绑定的账期
            if (hmCustomer.getHzqId() != null) {
                THmPaydays hmPaydays = tHmPaydaysMapper.selectTHmPaydaysById(hmCustomer.getHzqId());
                //校验账期的生成日是否为今天
                if ((sd.format(new Date())).equals(thisYear + "-" + hmPaydays.getHzqEndtime())) {
                    System.out.println("生成账期同一天");
                    //生成用户年度账期
                    //先根据开始年度和结束年度差有没有记录
                    THmCustomerpaydays tHmCustomerpaydays = new THmCustomerpaydays();
                    tHmCustomerpaydays.setHcustomerId(hmCustomer.getHcustomerId());//用户id
                    tHmCustomerpaydays.setHzqGrstarttime(thisYear);//开始年度
                    tHmCustomerpaydays.setHzqGrendtime(nextYear);//结束年度
                    //如果没有生成用户账期
                    System.out.println("生成账期进入本年度没有账期的循环");
                    //账期id
                    tHmCustomerpaydays.setHzqId(hmPaydays.getHzqId());
                    //获取用户单价
                    THmCustomerprice tHmCustomerprice = new THmCustomerprice();
                    tHmCustomerprice.setCustomerId(hmCustomer.getHcustomerId());
                    List<THmCustomerprice> tHmCustomerpriceList = tHmCustomerpriceMapper.selectTHmCustomerpriceList(tHmCustomerprice);
                    //当期的计算总金额
                    BigDecimal bds = new BigDecimal("0.00");
                    //计算当期金额
                    if (tHmCustomerpriceList.size() > 0) {
                        for (THmCustomerprice hmCustomerprice : tHmCustomerpriceList) {
                            //计算当期账期金额
                            BigDecimal bd = new BigDecimal(hmCustomerprice.getTotal());
                            bds = bds.add(bd);
                        }
                    }
                    switch (hmCustomer.getLing()) {
                        //取整舍
                        case 1:
                            tHmCustomerpaydays.setWqStr1(LingUtil.zhengShe(bds.toString()));
                            break;
                        //取整入
                        case 2:
                            tHmCustomerpaydays.setWqStr1(LingUtil.zhengRu(bds.toString()));
                            break;
                        //按原价
                        case 3:
                            tHmCustomerpaydays.setWqStr1(LingUtil.erRu(bds.toString()));
                            break;
                        //保留一位小数舍
                        case 4:
                            tHmCustomerpaydays.setWqStr1(LingUtil.yiShe(bds.toString()));
                            break;
                        //保留一位小数入
                        case 5:
                            tHmCustomerpaydays.setWqStr1(LingUtil.yiRu(bds.toString()));
                            break;
                        //保留两位小数入
                        case 6:
                            tHmCustomerpaydays.setWqStr1(LingUtil.erRu(bds.toString()));
                            break;
                    }
                    //查询历史账期是否有延期的，有延期的生成欠费费用
                    THmCustomerpaydays hmCustomerpaydays1 = tHmCustomerpaydaysMapper.getLastYan(hmCustomer.getHcustomerId());
                    if (hmCustomerpaydays1 != null) {
                        //处理上一期
                        THmCustomerpaydaysPrice price = new THmCustomerpaydaysPrice();
                        price.setCustomerpaydaysId(hmCustomerpaydays1.getWqId());
                        List<THmCustomerpaydaysPrice> list = tHmCustomerpaydaysPriceMapper.selectTHmCustomerpaydaysPriceList(price);
                        BigDecimal bd = new BigDecimal("0.00");
                        for (THmCustomerpaydaysPrice price1 : list) {
                            if (price1.getType() == 3) {
                                price1.setTotal(new BigDecimal(price1.getArea()).multiply(new BigDecimal(price1.getPrice())).multiply(new BigDecimal(price1.getXishu())).divide(new BigDecimal("100")).toString());
                            } else {
                                price1.setTotal(new BigDecimal(price1.getArea()).multiply(new BigDecimal(price1.getPrice())).multiply(new BigDecimal(price1.getXishu())).divide(new BigDecimal("100")).multiply(new BigDecimal("0.2")).toString());
                            }
                            bd = bd.add(new BigDecimal(price1.getTotal()));
                        }
                        //应缴金额和当期欠款金额
                        switch (hmCustomerpaydays1.getLing()) {
                            //取整舍
                            case 1:
                                hmCustomerpaydays1.setWqStr1(LingUtil.zhengShe(bd.toString()));
                                hmCustomerpaydays1.setWqStr2(LingUtil.zhengShe(bd.toString()));
                                break;
                            //取整入
                            case 2:
                                hmCustomerpaydays1.setWqStr1(LingUtil.zhengRu(bd.toString()));
                                hmCustomerpaydays1.setWqStr2(LingUtil.zhengRu(bd.toString()));
                                break;
                            //按原价
                            case 3:
                                hmCustomerpaydays1.setWqStr1(LingUtil.erRu(bd.toString()));
                                hmCustomerpaydays1.setWqStr2(LingUtil.erRu(bd.toString()));
                                break;
                            //保留一位小数舍
                            case 4:
                                hmCustomerpaydays1.setWqStr1(LingUtil.yiShe(bd.toString()));
                                hmCustomerpaydays1.setWqStr2(LingUtil.yiShe(bd.toString()));
                                break;
                            //保留一位小数入
                            case 5:
                                hmCustomerpaydays1.setWqStr1(LingUtil.yiRu(bd.toString()));
                                hmCustomerpaydays1.setWqStr2(LingUtil.yiRu(bd.toString()));
                                break;
                            //保留两位小数入
                            case 6:
                                hmCustomerpaydays1.setWqStr1(LingUtil.erRu(bd.toString()));
                                hmCustomerpaydays1.setWqStr2(LingUtil.erRu(bd.toString()));
                                break;
                        }
                        hmCustomerpaydays1.setPayType(0);
                        tHmCustomerpaydaysMapper.updateTHmCustomerpaydays(hmCustomerpaydays1);
                        System.out.println("生成账期上期欠费费用");
                    }
                    //应付金额
                    BigDecimal bdd = new BigDecimal("0.00");
                    if (tHmCustomerpriceList.size() > 0) {
                        for (THmCustomerprice hmCustomerprice : tHmCustomerpriceList) {
                            //计算当期账期金额
                            BigDecimal bd = new BigDecimal(hmCustomerprice.getTotal());
                            bdd = bdd.add(bd);
                        }
                    }
                    tHmCustomerpaydays.setHcustomerMoney(bdd.toString());

                    //用户的房屋信息
                    tHmCustomerpaydays.setHcustomerHomearea(hmCustomer.getHcustomerHomearea());
                    tHmCustomerpaydays.setHcustomerAbovearea(hmCustomer.getHcustomerAbovearea());
                    tHmCustomerpaydays.setHcustomerActualarea(hmCustomer.getHcustomerActualarea());
                    tHmCustomerpaydays.setHcustomerTgarea(hmCustomer.getHcustomerTgarea());
                    //计划供热天数
                    tHmCustomerpaydays.setHzqGrdays(hmPaydays.getHzqGrdays());
                    //优惠金额
                    tHmCustomerpaydays.setHcustomerGzje(hmCustomer.getHcustomerGzje());
                    //生成时间
                    tHmCustomerpaydays.setWqAddtime(sdf.format(new Date()));
                    //状态
                    tHmCustomerpaydays.setWqState(0);
                    tHmCustomerpaydays.setWqInt1(99);
                    tHmCustomerpaydays.setWqStr2("0.00");
                    tHmCustomerpaydays.setWqStr3("0");
                    tHmCustomerpaydays.setWqStr4("0.00");
                    tHmCustomerpaydays.setWqStr5("0.00");
                    tHmCustomerpaydays.setWqStr6("0.00");
                    //是否被调整过
                    tHmCustomerpaydays.setIsTiao(0);
                    tHmCustomerpaydays.setPayType(hmCustomer.getPayType());
                    tHmCustomerpaydaysMapper.insertTHmCustomerpaydays(tHmCustomerpaydays);
                    System.out.println("生成本年度账期");
                    //生成消息记录表
                    THmCustomer tHmCustomer = new THmCustomer();
                    tHmCustomer.setHcustomerId(hmCustomer.getHcustomerId());
                    List<THmCustomer> wechatBindList = tHmCustomerMapper.bindlist(tHmCustomer);
                    if (wechatBindList.size() > 0) {
                        THmMessage tHmMessage = new THmMessage();
                        tHmMessage.setCustomerId(hmCustomer.getHcustomerId());
                        tHmMessage.setFtime(hmPaydays.getHzqEndtime());
                        tHmMessage.setJtime("10月20日之前");
                        tHmMessage.setNian(thisYear + "-" + nextYear);
                        tHmMessage.setTemplateId(wxPayBean.getTemplateId());
                        tHmMessage.setTotal(tHmCustomerpaydays.getWqStr1());
                        tHmMessage.setType(0);
                        tHmMessage.setWqId(tHmCustomerpaydays.getWqId());
                        for (THmCustomer customer : wechatBindList) {
                            tHmMessage.setOpenId(customer.getOpenId());
                            tHmMessageMapper.insertTHmMessage(tHmMessage);
                        }
                    }
                    //保存账期单价
                    if (tHmCustomerpriceList.size() > 0) {
                        for (THmCustomerprice hmCustomerprice : tHmCustomerpriceList) {
                            THmCustomerpaydaysPrice tHmCustomerpaydaysPrice = new THmCustomerpaydaysPrice();
                            tHmCustomerpaydaysPrice.setAddTime(new Date());
                            tHmCustomerpaydaysPrice.setAddUser(1L);
                            THmMeterprice tHmMeterprice1 = tHmMeterpriceMapper.selectTHmMeterpriceById(hmCustomerprice.getPriceId());
                            tHmCustomerpaydaysPrice.setPrice(tHmMeterprice1.getRjPrice());
                            tHmCustomerpaydaysPrice.setPriceType(iSysDictDataService.selectDictLabel("data_price", tHmMeterprice1.getRjDjid().toString()));
                            tHmCustomerpaydaysPrice.setArea(hmCustomerprice.getArea());
                            tHmCustomerpaydaysPrice.setType(hmCustomerprice.getType());
                            tHmCustomerpaydaysPrice.setXishu(hmCustomerprice.getRemark());
                            tHmCustomerpaydaysPrice.setCustomerpaydaysId(tHmCustomerpaydays.getWqId());
                            tHmCustomerpaydaysPriceMapper.insertTHmCustomerpaydaysPrice(tHmCustomerpaydaysPrice);
                            System.out.println("生成本年度账期价格");
                        }
                    }
                    //更新用户int4 最后一次账期状态
                    THmCustomer tHmCustomer1 = new THmCustomer();
                    tHmCustomer1.setHcustomerInt4(99);
                    tHmCustomer1.setIsZheng(0);
                    tHmCustomer1.setHcustomerId(hmCustomer.getHcustomerId());
                    tHmCustomerMapper.updateTHmCustomer(tHmCustomer1);
                    System.out.println("更新用户状态");
                }
            }
        }
    }

    //生成延期 -- 全部
    @Override
    public int initDelays(String niandu) {
        String nextYear = String.valueOf(Integer.parseInt(niandu) + 1);
        List<THmPaydays> tHmPaydaysList = tHmPaydaysMapper.selectTHmPaydaysList(new THmPaydays());
        for (THmPaydays hmPaydays : tHmPaydaysList) {
            //比较账期策略的开始缴费日期和今天是否为同一天
            //同一天
            //取出用这个账期的所有分区
            THmArea tHmArea = new THmArea();
            tHmArea.setPaydaysId(hmPaydays.getHzqId());
            List<THmArea> tHmAreaList = tHmAreaMapper.selectTHmAreaList(tHmArea);
            for (THmArea hmArea : tHmAreaList) {
                //取出分区下所有用户
                THmCustomer tHmCustomer = new THmCustomer();
                tHmCustomer.setHareaId(hmArea.getHareaId());
                List<THmCustomer> tHmCustomerList = tHmCustomerMapper.selectTHmCustomerList(tHmCustomer);
                for (THmCustomer hmCustomer : tHmCustomerList) {
                    //取出用户本年度账期
                    THmCustomerpaydays tHmCustomerpaydays = new THmCustomerpaydays();
                    tHmCustomerpaydays.setHcustomerId(hmCustomer.getHcustomerId());//用户id
                    tHmCustomerpaydays.setHzqGrstarttime(niandu);//开始年度
                    tHmCustomerpaydays.setHzqGrendtime(nextYear);//结束年度
                    List<THmCustomerpaydays> tHmCustomerpaydaysList = tHmCustomerpaydaysMapper.selectTHmCustomerpaydaysList(tHmCustomerpaydays);
                    //本年有账期
                    if (tHmCustomerpaydaysList.size() > 0) {
                        THmCustomerpaydays tHmCustomerpaydays1 = tHmCustomerpaydaysList.get(0);
                        //且最后一条为99的 更新欠费中为延期
                        if (tHmCustomerpaydays1.getWqInt1() == 99 && "0.00".equals(tHmCustomerpaydays1.getWqStr6())) {
                            tHmCustomerpaydays1.setWqInt1(3);
                            tHmCustomerpaydaysMapper.updateTHmCustomerpaydays(tHmCustomerpaydays1);
                            //更新用户int4 最后一次账期状态
                            hmCustomer.setHcustomerInt4(3);
                            tHmCustomerMapper.updateTHmCustomer(hmCustomer);
                        }
                    }
                }
            }
        }
        return 1;
    }

    //生成延期 定时任务
    @Scheduled(cron = "0 0 3 * * ?")
    public void initDelays() throws ParseException {
        DefaultWebSecurityManager manager = new DefaultWebSecurityManager();
        ThreadContext.bind(manager);
        System.out.println("生成延期方法进入");
        Calendar dates = Calendar.getInstance();
        String niandu = String.valueOf(dates.get(Calendar.YEAR));//今年
        String nextYear = String.valueOf(Integer.parseInt(niandu) + 1);
        Date date = new Date();
        List<THmPaydays> tHmPaydaysList = tHmPaydaysMapper.selectTHmPaydaysList(new THmPaydays());
        for (THmPaydays hmPaydays : tHmPaydaysList) {
            System.out.println("生成延期循环账期策略");
            String thisYear = niandu + "-" + hmPaydays.getHzqGrstarttime();
            Date date1 = sd.parse(thisYear);
            Date date2 = sd.parse(sd.format(date));
            //比较账期策略的开始缴费日期和今天是否为同一天
            System.out.println("date1 :" + date1);
            System.out.println("date2 :" + date2);
            //同一天
            if (date1.equals(date2)) {
                System.out.println("延期同一天");
                //取出用这个账期的所有分区
                THmArea tHmArea = new THmArea();
                tHmArea.setPaydaysId(hmPaydays.getHzqId());
                List<THmArea> tHmAreaList = tHmAreaMapper.selectTHmAreaList(tHmArea);
                for (THmArea hmArea : tHmAreaList) {
                    //取出分区下所有用户
                    THmCustomer tHmCustomer = new THmCustomer();
                    tHmCustomer.setHareaId(hmArea.getHareaId());
                    List<THmCustomer> tHmCustomerList = tHmCustomerMapper.selectTHmCustomerList(tHmCustomer);
                    for (THmCustomer hmCustomer : tHmCustomerList) {
                        //取出用户本年度账期
                        THmCustomerpaydays tHmCustomerpaydays = new THmCustomerpaydays();
                        tHmCustomerpaydays.setHcustomerId(hmCustomer.getHcustomerId());//用户id
                        tHmCustomerpaydays.setHzqGrstarttime(niandu);//开始年度
                        tHmCustomerpaydays.setHzqGrendtime(nextYear);//结束年度
                        List<THmCustomerpaydays> tHmCustomerpaydaysList = tHmCustomerpaydaysMapper.selectTHmCustomerpaydaysList(tHmCustomerpaydays);
                        //本年有账期
                        if (tHmCustomerpaydaysList.size() > 0) {
                            THmCustomerpaydays tHmCustomerpaydays1 = tHmCustomerpaydaysList.get(0);
                            //且最后一条为99的 更新欠费中为延期
                            if (tHmCustomerpaydays1.getWqInt1() == 99 && "0.00".equals(tHmCustomerpaydays1.getWqStr6())) {
                                tHmCustomerpaydays1.setWqInt1(3);
                                tHmCustomerpaydaysMapper.updateTHmCustomerpaydays(tHmCustomerpaydays1);
                                //更新用户int4 最后一次账期状态
                                hmCustomer.setHcustomerInt4(3);
                                tHmCustomerMapper.updateTHmCustomer(hmCustomer);
                            }
                        }
                    }
                }
            }
        }
    }

    //获取账期年度
    @Override
    public List<String> startTime() {
        List<String> list = tHmCustomerpaydaysMapper.startTime();
        List<String> listt = new ArrayList<>();
        String ss = "";
        if (list.size() > 0) {
            for (String s : list) {
                ss = s + "-" + (Integer.parseInt(s) + 1);
                listt.add(ss);
            }
        }
        return listt;
    }

    //客服系统
    @Override
    public List<OrderKeFu> orderKeFuList(String payStatus) {
//        THmCustomerpaydays hmCustomerpaydays = new THmCustomerpaydays();

        int type = 0;
        if (payStatus != null) {
            if ("欠费".equals(payStatus)) {
                type = 5;
            } else if ("已缴费".equals(payStatus)) {
                type = 4;
            }
        }
        List<OrderKeFu> orderKeFuList = tHmCustomerpaydaysMapper.orderKeFuList(type);

        if (orderKeFuList.size() > 0) {
            for (OrderKeFu orderKeFu : orderKeFuList) {

                if (orderKeFu.getJTotal() == null) {
                    orderKeFu.setQTotal(orderKeFu.getJTotal());
                    orderKeFu.setJTotal("0.0");
                    orderKeFu.setPayTime("");
                }

                switch (orderKeFu.getPayStatus()) {
                    case "0":
                        orderKeFu.setPayStatus("已缴费");
                        orderKeFu.setQTotal("0.0");
                        break;
                    case "1":
                        orderKeFu.setPayStatus("已缴费");
                        orderKeFu.setQTotal("0.0");
                        break;
                    case "2":
                        orderKeFu.setPayStatus("已缴费");
                        orderKeFu.setQTotal("0.0");
                        break;
                    case "3":
                        orderKeFu.setPayStatus("欠费");
                        orderKeFu.setQTotal(orderKeFu.getYTotal());
                        break;
                    case "4":
                        orderKeFu.setPayStatus("已缴费");
                        orderKeFu.setQTotal("0.0");
                        break;
                    case "6":
                        orderKeFu.setPayStatus("欠费");
                        orderKeFu.setQTotal(new BigDecimal(orderKeFu.getYTotal()).subtract(new BigDecimal(orderKeFu.getJTotal())).toString());
                        break;
                    case "99":
                        orderKeFu.setPayStatus("欠费");
                        orderKeFu.setQTotal(new BigDecimal(orderKeFu.getYTotal()).subtract(new BigDecimal(orderKeFu.getJTotal())).toString());
                        break;
                }

                if (orderKeFu.getTapStatus() == null) {
                    orderKeFu.setTapStatus("");
                } else {
                    switch (orderKeFu.getTapStatus()) {
                        case "0":
                            orderKeFu.setTapStatus("阀开");
                            break;
                        case "1":
                            orderKeFu.setTapStatus("阀关");
                            break;
                        case "2":
                            orderKeFu.setTapStatus("断管");
                            break;
                    }
                }
            }
        }

//        List<THmCustomerpaydays> tHmCustomerpaydaysList = tHmCustomerpaydaysMapper.selectTHmCustomerpaydaysList(hmCustomerpaydays);
//        List<OrderKeFu> orderKeFuList = new ArrayList<>();
//        if (tHmCustomerpaydaysList.size() > 0) {
//            for (THmCustomerpaydays tHmCustomerpaydays : tHmCustomerpaydaysList) {
//                OrderKeFu orderKeFu = new OrderKeFu();
//                orderKeFu.setYear(tHmCustomerpaydays.getHzqGrstarttime() + "-" + tHmCustomerpaydays.getHzqGrendtime());
//                orderKeFu.setCustomerId(tHmCustomerpaydays.getHcustomerId());
//                orderKeFu.setQTotal(tHmCustomerpaydays.getWqStr6());
//                switch (tHmCustomerpaydays.getWqInt1()) {
//                    case 0:
//                        orderKeFu.setPayStatus("已缴费");
//                        break;
//                    case 1:
//                        orderKeFu.setPayStatus("已缴费");
//                        break;
//                    case 2:
//                        orderKeFu.setPayStatus("已缴费");
//                        break;
//                    case 3:
//                        orderKeFu.setPayStatus("欠费");
//                        break;
//                    case 4:
//                        orderKeFu.setPayStatus("已缴费");
//                        break;
//                    case 5:
//                        orderKeFu.setPayStatus("欠费");
//                        break;
//                    case 6:
//                        orderKeFu.setPayStatus("欠费");
//                        break;
//                    case 99:
//                        orderKeFu.setPayStatus("欠费");
//                        break;
//                }
//                THmPayhistory tHmPayhistory = new THmPayhistory();
//                tHmPayhistory.setWqId(tHmCustomerpaydays.getWqId());
//                List<THmPayhistory> tHmPayhistoryList = tHmPayhistoryMapper.selectTHmPayhistoryList(tHmPayhistory);
//                if (tHmPayhistoryList.size() > 0) {
//                    orderKeFu.setYTotal(tHmPayhistoryList.get(0).getHpayYjmoney());
//                    orderKeFu.setJTotal(tHmPayhistoryList.get(0).getHpayMoney());
//                    orderKeFu.setPayTime(tHmPayhistoryList.get(0).getHpayAddtime());
//                }
//                THmCustomer tHmCustomer = tHmCustomerMapper.selectTHmCustomerById(tHmCustomerpaydays.getHcustomerId());
//                if (tHmCustomer != null) {
//                    switch (tHmCustomer.getHcustomerTapstate()) {
//                        case 0:
//                            orderKeFu.setTapStatus("阀关");
//                            break;
//                        case 1:
//                            orderKeFu.setTapStatus("阀开");
//                            break;
//                        case 2:
//                            orderKeFu.setTapStatus("断管");
//                            break;
//                    }
//                }
//                orderKeFuList.add(orderKeFu);
//            }
//        }
        return orderKeFuList;
    }
}
