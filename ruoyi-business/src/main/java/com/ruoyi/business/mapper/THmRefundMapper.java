package com.ruoyi.business.mapper;

import com.ruoyi.business.domain.THmCustomer;
import com.ruoyi.business.domain.THmRefund;
import com.ruoyi.business.domain.vo.RefundVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 退费Mapper接口
 */
public interface THmRefundMapper {
    /**
     * 查询退费
     */
    THmRefund selectTHmRefundById(@Param("id") int id);

    /**
     * 查询退费列表
     */
    List<THmRefund> selectTHmRefundList(THmRefund tHmRefund);

    List<RefundVO> selectTHmRefundListt(THmCustomer tHmCustomer);

    /**
     * 新增退费
     */
    int insertTHmRefund(THmRefund tHmRefund);

    /**
     * 修改退费
     */
    int updateTHmRefund(THmRefund tHmRefund);

    /**
     * 业务查询 退费
     */
    String refundTotal(THmRefund tHmRefund);

    int reportBillCount(THmRefund tHmRefund);

    String reportBillSum(THmRefund tHmRefund);
}
