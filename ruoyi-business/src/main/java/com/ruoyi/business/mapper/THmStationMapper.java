package com.ruoyi.business.mapper;

import com.ruoyi.business.domain.THmStation;
import com.ruoyi.business.domain.vo.THmStationDetail;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 热源站Mapper接口
 */
public interface THmStationMapper {
    /**
     * 查询热源站
     */
    THmStation selectTHmStationById(@Param("id") Long id);

    //获取parentId下边的id最大的区域 -- 用于计算code
    THmStation getMaxCode(THmStation tHmStation);

    /**
     * 查询热源站列表
     */
    List<THmStation> selectTHmStationList(THmStation tHmStation);

    List<THmStationDetail> selectTHmStationLists(@Param("startTime") String startTime, @Param("endTime") String endTime, @Param("hstationId") String hstationId);

    List<THmStationDetail> selectTHmStationListss(@Param("startTime") String startTime, @Param("endTime") String endTime, @Param("hareaId") String hareaId);

    List<THmStationDetail> selectTHmStationListsss(@Param("priceId") String priceId);

    //获取本热区下的高低热区的用户数
    int getCountForIsRe(@Param("stationId") Integer stationId, @Param("isRe") Integer isRe);

    /**
     * 新增热源站
     */
    int insertTHmStation(THmStation tHmStation);

    /**
     * 修改热源站
     */
    int updateTHmStation(THmStation tHmStation);
}
