package com.ruoyi.business.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 稽查反馈信息对象 t_hm_feedback
 *
 * @author ruoyi
 * @date 2021-08-13
 */
@Data
public class THmFeedback extends BaseEntity {

    private Integer fdId;

    @Excel(name = "用户编号")
    private String hcustomerCode;

    private Integer hcustomerId;

    @Excel(name = "用户姓名")
    private String hcustomerName;

    @Excel(name = "手机号码")
    private String hcustomerPhone;

    @Excel(name = "身份证号")
    private String hcustomerPersonlcard;

    @Excel(name = "所属分区")
    private String hcustomerArea;

    @Excel(name = "详细地址")
    private String hcustomerAddress;

    @Excel(name = "操作人员")
    private String fdAdduserName;

    @Excel(name = "阀门状态", readConverterExp = "0=阀开,1=阀关,2=断管")
    private String hcustomerTapstate;

    /**
     * 反馈类型0普通反馈1开关阀
     */
    @Excel(name = "反馈类型", readConverterExp = "0=普通反馈,1=开关阀")
    private Integer fdType;

    @Excel(name = "阀门操作", readConverterExp = "0=关阀,1=开阀,2=断管")
    private Integer fdFmzt;
    private String fdFmztName;

    /**
     * 反馈标题
     */
    @Excel(name = "反馈标题")
    private String fdTitle;

    /**
     * 反馈内容
     */
    @Excel(name = "反馈内容")
    private String fdContent;

    //所属单价类型
    private Long customerpriceId;
    /**
     * 附件组code
     */
    private String fdFjcode;

    /**
     * 是否删除0否1是
     */
    private Integer fdState;

    /**
     * 添加人
     */
    private Long fdAdduserid;

    /**
     * 添加时间
     */
    @Excel(name = "添加时间")
    private String fdAddtime;

    private String stTime;
    private String enTime;

    private Integer pricesId;
}
