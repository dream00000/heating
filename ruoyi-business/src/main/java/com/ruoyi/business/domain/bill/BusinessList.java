package com.ruoyi.business.domain.bill;

import lombok.Data;

import java.util.List;

@Data
public class BusinessList {
    private String startTime;//查询开始时间
    private String endTime;//查询结束时间
    private String sysName;//查询操作员名

    private String totalMoney;//总计金额
    private Integer totalCount;//总计笔数

    private List<TopUser> topUserList;//操作员
}