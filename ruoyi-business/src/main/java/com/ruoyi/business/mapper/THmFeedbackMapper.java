package com.ruoyi.business.mapper;

import com.ruoyi.business.domain.THmCustomer;
import com.ruoyi.business.domain.THmFeedback;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 稽查反馈信息Mapper接口
 */
public interface THmFeedbackMapper {
    /**
     * 查询稽查反馈信息
     */
    THmFeedback selectTHmFeedbackById(@Param("id") int id);

    /**
     * 查询稽查反馈信息列表
     */
    List<THmFeedback> selectTHmFeedbackList(THmFeedback tHmFeedback);

    List<THmFeedback> selectTHmFeedbackListt(THmCustomer tHmCustomer);
    int exportCountt(THmCustomer tHmCustomer);

    List<THmCustomer> selectTHmFeedbackLists(THmCustomer tHmCustomer);

    /**
     * 新增稽查反馈信息
     */
    int insertTHmFeedback(THmFeedback tHmFeedback);

    /**
     * 修改稽查反馈信息
     */
    int updateTHmFeedback(THmFeedback tHmFeedback);

    //新版阀控 阀门管理列表
    List<THmCustomer> tapCustomers(THmCustomer tHmCustomer);
    int exportCount(THmCustomer tHmCustomer);

    List<THmCustomer> tapCustomer(THmCustomer tHmCustomer);
    int exportCounts(THmCustomer tHmCustomer);

    List<THmCustomer> tapCustomerd(THmCustomer tHmCustomer);
    List<THmCustomer> tapCustomerd1(THmCustomer tHmCustomer);
    List<THmCustomer> tapCustomerd2(THmCustomer tHmCustomer);
    List<THmCustomer> tapCustomerd3(THmCustomer tHmCustomer);

    List<THmCustomer> tapCustomerdd(THmCustomer tHmCustomer);

    //根据单价id查用户
    THmCustomer tapCustomerByPriceId(@Param("pricesId") int id);

    THmFeedback getLast(@Param("hcustomerId") int hcustomerId);
}
