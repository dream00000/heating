package com.ruoyi.business.service.impl;

import com.ruoyi.business.domain.*;
import com.ruoyi.business.domain.vo.*;
import com.ruoyi.business.mapper.*;
import com.ruoyi.business.service.*;
import com.ruoyi.business.util.CheckUtil;
import com.ruoyi.business.util.LingUtil;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.mapper.SysUserMapper;
import com.ruoyi.system.service.ISysDictDataService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 用户信息Service业务层处理
 *
 * @author ruoyi
 * @date 2021-08-13
 */
@Service("CustomerService")
public class THmCustomerServiceImpl implements ITHmCustomerService {

    @Resource
    private THmCustomerMapper tHmCustomerMapper;

    @Resource
    private SysUserMapper sysUserMapper;

    @Resource
    private ITHmAreaService itHmAreaService;

    @Resource
    private THmAreaMapper tHmAreaMapper;

    @Resource
    private ITHmCustomerpriceService itHmCustomerpriceService;

    @Resource
    private ITHmCustomerpaydaysService itHmCustomerpaydaysService;

    @Resource
    private ITHmPayhistoryService itHmPayhistoryService;

    @Resource
    private ITHmFeedbackService itHmFeedbackService;

    @Resource
    private THmMeterpriceMapper tHmMeterpriceMapper;

    @Resource
    private THmCustomerpriceMapper tHmCustomerpriceMapper;

    @Resource
    private THmPaydaysMapper tHmPaydaysMapper;

    @Resource
    private THmCustomerpaydaysMapper tHmCustomerpaydaysMapper;

    @Resource
    private THmCustomerpaydaysPriceMapper tHmCustomerpaydaysPriceMapper;

    @Resource
    private THmStationMapper tHmStationMapper;

    @Resource
    private ISysDictDataService iSysDictDataService;

    @Resource
    private THmOtherpayhistoryMapper tHmOtherpayhistoryMapper;

    @Resource
    private WechatUserMapper wechatUserMapper;

    @Resource
    private THmMessageMapper tHmMessageMapper;

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    public THmCustomer selectTHmCustomerByIdForWeChat(int id) {
        return tHmCustomerMapper.selectTHmCustomerById(id);
    }

    /**
     * 查询用户信息
     */
    @Override
    public THmCustomer selectTHmCustomerById(int id) {
        THmCustomer tHmCustomer = tHmCustomerMapper.selectTHmCustomerById(id);
        THmArea tHmArea = itHmAreaService.selectTHmAreaById(tHmCustomer.getHareaId());
        if (tHmArea != null) {
            if (tHmArea.getHareaLevel() == 2) {
                tHmCustomer.setHareaName(tHmArea.getHareaName());
            } else {
                THmArea tHmArea1 = tHmAreaMapper.selectTHmAreaById(tHmArea.getHareaParentid());
                tHmCustomer.setHareaName(tHmArea1.getHareaName());
            }

            THmPaydays tHmPaydays = tHmPaydaysMapper.selectTHmPaydaysById(tHmArea.getPaydaysId());
            if (tHmPaydays != null) {
                tHmCustomer.setLing(tHmPaydays.getLing());
            }
        }
        //单价列表
        THmCustomerprice tHmCustomerprice = new THmCustomerprice();
        tHmCustomerprice.setCustomerId(tHmCustomer.getHcustomerId());
        List<THmCustomerprice> list = itHmCustomerpriceService.selectTHmCustomerpriceList(tHmCustomerprice);
        for (THmCustomerprice hmCustomerprice : list) {
            hmCustomerprice.setXishu(hmCustomerprice.getRemark());
        }
        tHmCustomer.setCustomerpriceList(list);
        //充值记录
        THmPayhistory tHmPayhistory = new THmPayhistory();
        tHmPayhistory.setHcustomerId(tHmCustomer.getHcustomerId());
        List<THmPayhistory> tHmPayhistoryList = itHmPayhistoryService.selectTHmPayhistoryList(tHmPayhistory);
        if (tHmPayhistoryList.size() > 0) {
            for (THmPayhistory hmPayhistory : tHmPayhistoryList) {
                THmCustomerpaydays tHmCustomerpaydays = itHmCustomerpaydaysService.selectTHmCustomerpaydaysById(hmPayhistory.getWqId());
                hmPayhistory.setNiandu(tHmCustomerpaydays.getHzqGrstarttime() + "-" + tHmCustomerpaydays.getHzqGrendtime());
                if (hmPayhistory.getHpaySfqe() == 0) {
                    hmPayhistory.setHpaySfqeName("已缴基础热");
                } else if (hmPayhistory.getHpaySfqe() == 1) {
                    hmPayhistory.setHpaySfqeName("已缴全热");
                } else if (hmPayhistory.getHpaySfqe() == 2) {
                    hmPayhistory.setHpaySfqeName("已缴续费");
                } else if (hmPayhistory.getHpaySfqe() == 3) {
                    hmPayhistory.setHpaySfqeName("已缴面积变更");
                } else if (hmPayhistory.getHpaySfqe() == 4) {
                    hmPayhistory.setHpaySfqeName("部分缴费");
                }
            }
        }
        tHmCustomer.setPayhistoryList(tHmPayhistoryList);
        //阀控记录
        THmFeedback tHmFeedback = new THmFeedback();
        tHmFeedback.setFdType(1);
        tHmFeedback.setHcustomerId(tHmCustomer.getHcustomerId());
        List<THmFeedback> tHmFeedbackList = itHmFeedbackService.selectTHmFeedbackList(tHmFeedback);
        for (THmFeedback hmFeedback : tHmFeedbackList) {
            //0=开阀,1=关阀,2=断管
            if (hmFeedback.getFdFmzt() == 0) {
                hmFeedback.setFdFmztName("关阀");
            } else if (hmFeedback.getFdFmzt() == 1) {
                hmFeedback.setFdFmztName("开阀");
            } else if (hmFeedback.getFdFmzt() == 2) {
                hmFeedback.setFdFmztName("断管");
            }
        }
        tHmCustomer.setFeedbackList(tHmFeedbackList);

        //最后一次账期信息
        THmCustomerpaydays tHmCustomerpaydays = tHmCustomerpaydaysMapper.getLast(tHmCustomer.getHcustomerId());
        if (tHmCustomerpaydays != null) {
            tHmCustomer.setLastPayDays(tHmCustomerpaydays);
        }
        return tHmCustomer;
    }

    @Override
    public THmCustomer selectTHmCustomerByHcustomerCode(String hcustomerCode) {
        THmCustomer tHmCustomer = tHmCustomerMapper.selectTHmCustomerByHcustomerCode(hcustomerCode);
        if (tHmCustomer != null) {
            if (tHmCustomer.getHareaId() != null) {
                THmArea tHmArea = itHmAreaService.selectTHmAreaById(tHmCustomer.getHareaId());
                if (tHmArea != null) {
                    if (tHmArea.getHareaLevel() == 2) {
                        tHmCustomer.setHareaName(tHmArea.getHareaName());
                    } else {
                        THmArea tHmArea1 = tHmAreaMapper.selectTHmAreaById(tHmArea.getHareaParentid());
                        tHmCustomer.setHareaName(tHmArea1.getHareaName());
                    }
                }
            }
        }

        return tHmCustomer;
    }

    /**
     * 查询用户信息列表
     */
    @Override
    public List<THmCustomer> selectTHmCustomerList(THmCustomer tHmCustomer) {
        List<THmCustomer> list = tHmCustomerMapper.selectTHmCustomerList(tHmCustomer);
        for (THmCustomer hmCustomer : list) {
            //获取其他费用总额
            String total = tHmOtherpayhistoryMapper.total(hmCustomer.getHcustomerId());
            hmCustomer.setOtherTotal(total);
        }
        return list;
    }

    //获取最大的用户编号
    @Override
    public Integer getMaxCode() {
        return tHmCustomerMapper.getMaxCode();
    }

    @Override
    public List<THmCustomer> getTHmCustomerListForWechart(THmCustomer tHmCustomer) {
        List<THmCustomer> list = tHmCustomerMapper.getTHmCustomerListForWechart(tHmCustomer);
        for (THmCustomer customer : list) {
            THmArea tHmArea = itHmAreaService.selectTHmAreaById(customer.getHareaId());
            if (tHmArea != null) {
                THmArea tHmArea1 = itHmAreaService.selectTHmAreaById(tHmArea.getHareaParentid());
                customer.setHareaName(tHmArea1.getHareaName());
            }
        }
        return list;
    }

    //导入验证
    @Override
    public CustomerImport importCustomerCheck(List<THmCustomer> userList) {

        //删除附表信息
        tHmCustomerMapper.delTHmCustomerCopy();

        CustomerImport customerImport = new CustomerImport();
        customerImport.setDataStatus(0);

        for (THmCustomer tHmCustomer : userList) {
            tHmCustomer.setDataExplain("信息提示：");
            if (tHmCustomer.getHareaCode() == null || tHmCustomer.getHareaCode().equals("")) {
                tHmCustomer.setDataExplain(tHmCustomer.getDataExplain() + "区域不能为空，");
                customerImport.setDataStatus(1);
            } else {
                //区域表号是否存在
                THmArea tHmArea = itHmAreaService.getAreaByCode(tHmCustomer.getHareaCode());
                if (tHmArea != null) {
                    if (tHmArea.getPaydaysId() == null) {
                        tHmCustomer.setDataExplain(tHmCustomer.getDataExplain() + "所选区域没有绑定账期策略，");
                        customerImport.setDataStatus(1);
                    }
                    if (tHmArea.getHareaLevel() == 2) {
                        THmArea tHmArea1 = new THmArea();
                        tHmArea1.setHareaParentid(tHmArea.getHareaId());
                        List<THmArea> tHmAreaList = tHmAreaMapper.selectTHmAreaList(tHmArea1);
                        if (tHmAreaList.size() > 0) {
                            tHmCustomer.setDataExplain(tHmCustomer.getDataExplain() + "区域需选择楼号，");
                            customerImport.setDataStatus(1);
                        }
                        if (tHmCustomer.getHcustomerDetailaddress() != null && !tHmCustomer.getHcustomerDetailaddress().equals("")) {
                            // 数据库验证 详细地址判断是否重复
                            THmCustomer tHmCustomer1 = new THmCustomer();
                            tHmCustomer1.setHareaId(tHmCustomer.getHareaId());
                            tHmCustomer1.setHcustomerDetailaddress(tHmCustomer.getHcustomerDetailaddress());
                            List<THmCustomer> tHmCustomerList1 = tHmCustomerMapper.checkAddress(tHmCustomer1);
                            if (tHmCustomerList1.size() > 0) {
                                tHmCustomer.setDataExplain(tHmCustomer.getDataExplain() + "详细地址重复，");
                                customerImport.setDataStatus(1);
                            }
                            //execl验证详细地址判断是否重复
                            THmCustomer tHmCustomer2 = new THmCustomer();
                            tHmCustomer2.setHareaId(tHmArea.getHareaId());
                            tHmCustomer2.setHcustomerDetailaddress(tHmCustomer.getHcustomerDetailaddress());
                            List<THmCustomer> tHmCustomerList2 = tHmCustomerMapper.selectTHmCustomerListCopy(tHmCustomer2);
                            if (tHmCustomerList2.size() > 1) {
                                tHmCustomer.setDataExplain(tHmCustomer.getDataExplain() + "execl详细地址重复，");
                                customerImport.setDataStatus(1);
                            }
                        } else {
                            tHmCustomer.setDataExplain(tHmCustomer.getDataExplain() + "详细地址不能为空，");
                            customerImport.setDataStatus(1);
                        }
                    } else if (tHmArea.getHareaLevel() == 1) {
                        tHmCustomer.setDataExplain(tHmCustomer.getDataExplain() + "不能选择顶层区域，");
                        customerImport.setDataStatus(1);
                    } else {
                        //添加到附表
                        tHmCustomer.setHareaId(tHmArea.getHareaId());
                        tHmCustomerMapper.insertTHmCustomerCopy(tHmCustomer);

                        if (tHmCustomer.getHcustomerDetailaddress() != null && !tHmCustomer.getHcustomerDetailaddress().equals("")) {
                            // 数据库验证 详细地址判断是否重复
                            THmCustomer tHmCustomer1 = new THmCustomer();
                            tHmCustomer1.setHareaId(tHmCustomer.getHareaId());
                            tHmCustomer1.setHcustomerDetailaddress(tHmCustomer.getHcustomerDetailaddress());
                            List<THmCustomer> tHmCustomerList1 = tHmCustomerMapper.checkAddress(tHmCustomer1);
                            if (tHmCustomerList1.size() > 0) {
                                tHmCustomer.setDataExplain(tHmCustomer.getDataExplain() + "详细地址重复，");
                                customerImport.setDataStatus(1);
                            }
                            //execl验证详细地址判断是否重复
                            THmCustomer tHmCustomer2 = new THmCustomer();
                            tHmCustomer2.setHareaId(tHmArea.getHareaId());
                            tHmCustomer2.setHcustomerDetailaddress(tHmCustomer.getHcustomerDetailaddress());
                            List<THmCustomer> tHmCustomerList2 = tHmCustomerMapper.selectTHmCustomerListCopy(tHmCustomer2);
                            if (tHmCustomerList2.size() > 1) {
                                tHmCustomer.setDataExplain(tHmCustomer.getDataExplain() + "execl详细地址重复，");
                                customerImport.setDataStatus(1);
                            }
                        } else {
                            tHmCustomer.setDataExplain(tHmCustomer.getDataExplain() + "详细地址不能为空，");
                            customerImport.setDataStatus(1);
                        }
                    }
                } else {
                    tHmCustomer.setDataExplain(tHmCustomer.getDataExplain() + "区域不存在，");
                    customerImport.setDataStatus(1);
                }
            }

            //手机号验证
            if (tHmCustomer.getHcustomerMobile() != null && !tHmCustomer.getHcustomerMobile().equals("")) {
                if (!CheckUtil.isMobile(tHmCustomer.getHcustomerMobile())) {
                    tHmCustomer.setDataExplain(tHmCustomer.getDataExplain() + "手机号格式错误，");
                    customerImport.setDataStatus(1);
                }
            }
            //身份证验证
            if (tHmCustomer.getHcustomerPersonlcard() != null && !tHmCustomer.getHcustomerPersonlcard().equals("")) {
                if (!CheckUtil.isIdCard(tHmCustomer.getHcustomerPersonlcard())) {
                    tHmCustomer.setDataExplain(tHmCustomer.getDataExplain() + "身份证号格式错误，");
                    customerImport.setDataStatus(1);
                }
            }
            if (!String.valueOf(tHmCustomer.getTap()).matches("[0-2]+")) {
                tHmCustomer.setDataExplain(tHmCustomer.getDataExplain() + "阀门状态格式错误，");
                customerImport.setDataStatus(1);
            }
            //高低热区
            if (!String.valueOf(tHmCustomer.getIsRe()).matches("[0-1]+")) {
                tHmCustomer.setDataExplain(tHmCustomer.getDataExplain() + "高低热区格式错误，");
                customerImport.setDataStatus(1);
            }
            //是否生成账期
            if (!String.valueOf(tHmCustomer.getIsInit()).matches("[0-1]+")) {
                tHmCustomer.setDataExplain(tHmCustomer.getDataExplain() + "是否生成账期格式错误，");
                customerImport.setDataStatus(1);
            }
            //用户类型
            if (!String.valueOf(tHmCustomer.getHcustomerType()).matches("[1-4]+")) {
                tHmCustomer.setDataExplain(tHmCustomer.getDataExplain() + "用户类型格式错误，");
                customerImport.setDataStatus(1);
            }
            //实供面积计算
            if (tHmCustomer.getHcustomerHomearea() == null || tHmCustomer.getHcustomerHomearea().equals("")) {
                tHmCustomer.setDataExplain(tHmCustomer.getDataExplain() + "房屋面积不能为空，");
                customerImport.setDataStatus(1);
            } else {
                if (!isNumber(tHmCustomer.getHcustomerHomearea())) {
                    tHmCustomer.setDataExplain(tHmCustomer.getDataExplain() + "房屋面积只能为数字，");
                    customerImport.setDataStatus(1);
                }
                if (!isNumber(tHmCustomer.getHcustomerAbovearea())) {
                    tHmCustomer.setDataExplain(tHmCustomer.getDataExplain() + "房屋面积只能为数字，");
                    customerImport.setDataStatus(1);
                }
                if (isNumber(tHmCustomer.getHcustomerHomearea()) && isNumber(tHmCustomer.getHcustomerAbovearea())) {
                    BigDecimal fang = new BigDecimal(tHmCustomer.getHcustomerHomearea());
                    //房屋面积不为空的时候
                    if (tHmCustomer.getHcustomerAbovearea() != null && !tHmCustomer.getHcustomerAbovearea().equals("")) {
                        fang = fang.add(new BigDecimal(tHmCustomer.getHcustomerAbovearea()));
                    }
                    BigDecimal areaH = new BigDecimal("0.00");
                    if (tHmCustomer.getAreas() != null && !tHmCustomer.getAreas().equals("")) {
                        String[] areas = tHmCustomer.getAreas().split(",");
                        for (String area : areas) {
                            if (!isNumber(area)) {
                                tHmCustomer.setDataExplain(tHmCustomer.getDataExplain() + "面积格式错误，");
                                customerImport.setDataStatus(1);
                            } else {
                                areaH = areaH.add(new BigDecimal(area));
                            }
                        }
                    }
                    if (fang.compareTo(areaH) != 0) {
                        tHmCustomer.setDataExplain(tHmCustomer.getDataExplain() + "房屋面积+超高面积与面积不相等，");
                        customerImport.setDataStatus(1);
                    }
                }
            }
            //单价和面积长度验证
            if (tHmCustomer.getPriceIds() == null || tHmCustomer.getAreas() == null || tHmCustomer.getTypes() == null || tHmCustomer.getXishus() == null) {
                tHmCustomer.setDataExplain(tHmCustomer.getDataExplain() + "单价、面积、类型、系数错误，");
                customerImport.setDataStatus(1);
            } else {
                String[] priceIds = tHmCustomer.getPriceIds().split(",");
                String[] areas = tHmCustomer.getAreas().split(",");
                String[] types = tHmCustomer.getTypes().split(",");
                String[] xishus = tHmCustomer.getXishus().split(",");
                if ((priceIds.length != areas.length) || (priceIds.length != types.length) || (priceIds.length != xishus.length)) {
                    tHmCustomer.setDataExplain(tHmCustomer.getDataExplain() + "单价、面积、类型、系数不对应，");
                    customerImport.setDataStatus(1);
                } else {
                    //单价是否存在
                    for (String s : priceIds) {
                        if (s.matches("[0-9]+")) {
                            THmMeterprice tHmMeterprice = tHmMeterpriceMapper.selectTHmMeterpriceById(Integer.parseInt(s));
                            if (tHmMeterprice == null) {
                                tHmCustomer.setDataExplain(tHmCustomer.getDataExplain() + "单价错误，");
                                customerImport.setDataStatus(1);
                            }
                        } else {
                            tHmCustomer.setDataExplain(tHmCustomer.getDataExplain() + "单价格式错误，");
                            customerImport.setDataStatus(1);
                        }
                    }
                    //类型
                    for (String s : types) {
                        if (!s.matches("[2-3]+")) {
                            tHmCustomer.setDataExplain(tHmCustomer.getDataExplain() + "类型格式错误，");
                            customerImport.setDataStatus(1);
                        }
                    }
                    //系数
                    for (String s : xishus) {
                        if (!s.matches("[0-200]+")) {
                            tHmCustomer.setDataExplain(tHmCustomer.getDataExplain() + "系数格式错误，");
                            customerImport.setDataStatus(1);
                        }
                    }
                }
            }
        }
        customerImport.setList(userList);
        return customerImport;
    }

    //导入
    @Override
    public String importCustomer(List<THmCustomer> userList) {

        for (THmCustomer tHmCustomer : userList) {
            THmArea tHmArea = itHmAreaService.getAreaByCode(tHmCustomer.getHareaCode());
            //所属分区
            tHmCustomer.setHareaId(tHmArea.getHareaId());
            //用户编号
            //获取最大编号
            Integer ii = tHmCustomerMapper.getMaxCode();
            if (ii == null) {
                ii = 0;
            }
            tHmCustomer.setHcustomerCode(String.format("%07d", ii + 1));

            //历史欠费
            tHmCustomer.setIsQian(1);
            //是否允许生成账期
            tHmCustomer.setIsSheng(1);
            //是否允许直接充值
            tHmCustomer.setIsChong(1);
            //用户欠费状态
            tHmCustomer.setIsZheng(1);
            //默认不为大客户
            tHmCustomer.setIsDa(0);

            //是否删除
            tHmCustomer.setHcustomerState(0);
            //添加人
            tHmCustomer.setHcustomerAdduserid(ShiroUtils.getSysUser().getUserId());
            //添加时间
            tHmCustomer.setHcustomerAddtime(sdf.format(new Date()));
            //余额
            tHmCustomer.setBalance("0.00");
            //楼，单元，层，号
//            String[] address = tHmCustomer.getHcustomerDetailaddress().split("-");
//            tHmCustomer.setLou(address[0]);
//            tHmCustomer.setDan(address[1]);
//            if (address[2].length() == 3) {
//                tHmCustomer.setCeng(address[2].substring(0, 1));
//            } else if (address[2].length() == 4) {
//                tHmCustomer.setCeng(address[2].substring(0, 2));
//            }
//            tHmCustomer.setHao(address[2]);

            //实供面积
            BigDecimal fang = new BigDecimal(tHmCustomer.getHcustomerHomearea());
            BigDecimal tg = new BigDecimal("0.00");

            //单价
            String[] priceIds = tHmCustomer.getPriceIds().split(",");
            String[] areas = tHmCustomer.getAreas().split(",");
            String[] types = tHmCustomer.getTypes().split(",");
            String[] xishus = tHmCustomer.getXishus().split(",");

            //费用类型
            if (!Arrays.asList(types).contains("2")) {
                //type不包含2 证明都是停供
                tHmCustomer.setPayType(0);
            } else if (!Arrays.asList(types).contains("3")) {
                //type不包含3 证明都是实供
                tHmCustomer.setPayType(1);
            } else {
                //两者都包含
                tHmCustomer.setPayType(5);
            }

            for (int i = 0; i < types.length; i++) {
                if ("3".equals(types[i])) {
                    tg = tg.add(new BigDecimal(areas[i]));
                }
            }
            tHmCustomer.setHcustomerTgarea(tg.toString());

            if (tHmCustomer.getHcustomerAbovearea() != null && !tHmCustomer.getHcustomerAbovearea().equals("")) {
                fang = fang.add(new BigDecimal(tHmCustomer.getHcustomerAbovearea()));
            } else {
                tHmCustomer.setHcustomerAbovearea("0.00");
            }
            if (tHmCustomer.getHcustomerTgarea() != null && !tHmCustomer.getHcustomerTgarea().equals("")) {
                fang = fang.subtract(new BigDecimal(tHmCustomer.getHcustomerTgarea()));
            } else {
                tHmCustomer.setHcustomerTgarea("0.00");
            }
            tHmCustomer.setHcustomerActualarea(fang.toString());

            tHmCustomerMapper.insertTHmCustomer(tHmCustomer);

            for (int i = 0; i < priceIds.length; i++) {
                THmCustomerprice tHmCustomerprice1 = new THmCustomerprice();
                tHmCustomerprice1.setPriceId(Integer.parseInt(priceIds[i]));
                tHmCustomerprice1.setArea(areas[i]);
                tHmCustomerprice1.setCustomerId(tHmCustomer.getHcustomerId());
                tHmCustomerprice1.setState(0);
                tHmCustomerprice1.setType(Integer.valueOf(types[i]));
                tHmCustomerprice1.setRemark(xishus[i]);
                THmMeterprice tHmMeterprice1 = tHmMeterpriceMapper.selectTHmMeterpriceById(Integer.parseInt(priceIds[i]));
                tHmCustomerprice1.setTotal(new BigDecimal(areas[i]).multiply(new BigDecimal(tHmMeterprice1.getRjPrice())).multiply(new BigDecimal(xishus[i])).divide(new BigDecimal("100")).toString());
                tHmCustomerprice1.setTap(tHmCustomer.getTap());
                tHmCustomerpriceMapper.insertTHmCustomerprice(tHmCustomerprice1);

                THmFeedback tHmFeedback = new THmFeedback();
                tHmFeedback.setFdType(1);//开关阀操作
                if (tHmCustomer.getTap() == 1) {
                    tHmFeedback.setFdFmzt(0);
                } else if (tHmCustomer.getTap() == 0) {
                    tHmFeedback.setFdFmzt(1);
                } else {
                    tHmFeedback.setFdFmzt(2);
                }
                tHmFeedback.setHcustomerId(tHmCustomer.getHcustomerId());
                tHmFeedback.setFdAddtime(sdf.format(new Date()));
                tHmFeedback.setFdAdduserName(ShiroUtils.getSysUser().getUserName());
                tHmFeedback.setFdState(0);
                tHmFeedback.setFdTitle("开关阀操作");
                tHmFeedback.setCustomerpriceId(Long.valueOf(tHmCustomerprice1.getId()));
                tHmFeedback.setFdContent("新增用户时" + ShiroUtils.getSysUser().getUserName() + "进行了阀控操作");
                itHmFeedbackService.insertTHmFeedback(tHmFeedback);
            }

            if (tHmCustomer.getIsInit() == 1) {
                //生成账期
                THmCustomerpaydays tHmCustomerpaydays = new THmCustomerpaydays();
                //用户id
                tHmCustomerpaydays.setHcustomerId(tHmCustomer.getHcustomerId());

                Calendar date = Calendar.getInstance();
                String thisYear = String.valueOf(date.get(Calendar.YEAR));//今年
                String nextYear = String.valueOf(Integer.parseInt(thisYear) + 1);//明年

                tHmCustomerpaydays.setHzqGrstarttime(thisYear);//开始年度
                tHmCustomerpaydays.setHzqGrendtime(nextYear);//结束年度

                //获取分区
                THmArea tHmAreat = tHmAreaMapper.selectTHmAreaById(tHmCustomer.getHareaId());
                //获取分区绑定的账期
                if (tHmAreat.getPaydaysId() != null) {
                    THmPaydays hmPaydays = tHmPaydaysMapper.selectTHmPaydaysById(tHmAreat.getPaydaysId());
                    //账期id
                    tHmCustomerpaydays.setHzqId(hmPaydays.getHzqId());
                    //获取用户单价
                    THmCustomerprice tHmCustomerprice1 = new THmCustomerprice();
                    tHmCustomerprice1.setCustomerId(tHmCustomer.getHcustomerId());
                    List<THmCustomerprice> tHmCustomerpriceList = tHmCustomerpriceMapper.selectTHmCustomerpriceList(tHmCustomerprice1);
                    //当期的计算总金额
                    BigDecimal bds = new BigDecimal("0.00");
                    //计算当期金额
                    if (tHmCustomerpriceList.size() > 0) {
                        for (THmCustomerprice hmCustomerprice : tHmCustomerpriceList) {
                            //计算当期账期金额
                            bds = bds.add(new BigDecimal(hmCustomerprice.getTotal()));
                        }
                    }

                    tHmCustomerpaydays.setLing(hmPaydays.getLing());

                    switch (hmPaydays.getLing()) {
                        //取整舍
                        case 1:
                            tHmCustomerpaydays.setWqStr1(LingUtil.zhengShe(bds.toString()));
                            break;
                        //取整入
                        case 2:
                            tHmCustomerpaydays.setWqStr1(LingUtil.zhengRu(bds.toString()));
                            break;
                        //按原价
                        case 3:
                            tHmCustomerpaydays.setWqStr1(LingUtil.erRu(bds.toString()));
                            break;
                        //保留一位小数舍
                        case 4:
                            tHmCustomerpaydays.setWqStr1(LingUtil.yiShe(bds.toString()));
                            break;
                        //保留一位小数入
                        case 5:
                            tHmCustomerpaydays.setWqStr1(LingUtil.yiRu(bds.toString()));
                            break;
                        //保留两位小数入
                        case 6:
                            tHmCustomerpaydays.setWqStr1(LingUtil.erRu(bds.toString()));
                            break;
                    }

                    //应付金额
                    BigDecimal bdd = new BigDecimal("0.00");
                    if (tHmCustomerpriceList.size() > 0) {
                        for (THmCustomerprice hmCustomerprice : tHmCustomerpriceList) {
                            //计算当期账期金额
                            BigDecimal bd = new BigDecimal(hmCustomerprice.getTotal());
                            bdd = bdd.add(bd);
                        }
                    }
                    tHmCustomerpaydays.setHcustomerMoney(bdd.toString());

                    tHmCustomerpaydays.setWqStr2("0.00");

                    //用户的房屋信息
                    tHmCustomerpaydays.setHcustomerHomearea(tHmCustomer.getHcustomerHomearea());
                    tHmCustomerpaydays.setHcustomerAbovearea(tHmCustomer.getHcustomerAbovearea());
                    tHmCustomerpaydays.setHcustomerActualarea(tHmCustomer.getHcustomerActualarea());
                    tHmCustomerpaydays.setHcustomerTgarea(tHmCustomer.getHcustomerTgarea());

                    tHmCustomerpaydays.setHzqGrdays(hmPaydays.getHzqGrdays());//计划供热天数
                    tHmCustomerpaydays.setWqAddtime(sdf.format(new Date()));//生成时间
                    tHmCustomerpaydays.setWqState(0);//状态
                    tHmCustomerpaydays.setWqInt1(99);
                    tHmCustomerpaydays.setWqStr3("0");
                    tHmCustomerpaydays.setWqStr5("0.00");
                    tHmCustomerpaydays.setIsTiao(0);//是否被调整过
                    tHmCustomerpaydays.setPayType(tHmCustomer.getPayType());
                    tHmCustomerpaydays.setWqStr4("0.00");
                    tHmCustomerpaydays.setWqStr6("0.00");
                    tHmCustomerpaydaysMapper.insertTHmCustomerpaydays(tHmCustomerpaydays);

                    //保存账期-单价记录
                    for (THmCustomerprice hmCustomerprice : tHmCustomerpriceList) {
                        THmCustomerpaydaysPrice tHmCustomerpaydaysPrice = new THmCustomerpaydaysPrice();
                        tHmCustomerpaydaysPrice.setAddTime(new Date());
                        tHmCustomerpaydaysPrice.setAddUser(ShiroUtils.getSysUser().getUserId());
                        THmMeterprice tHmMeterprice1 = tHmMeterpriceMapper.selectTHmMeterpriceById(hmCustomerprice.getPriceId());
                        tHmCustomerpaydaysPrice.setPrice(tHmMeterprice1.getRjPrice());
                        tHmCustomerpaydaysPrice.setPriceType(iSysDictDataService.selectDictLabel("data_price", tHmMeterprice1.getRjDjid().toString()));
                        tHmCustomerpaydaysPrice.setArea(hmCustomerprice.getArea());
                        tHmCustomerpaydaysPrice.setType(hmCustomerprice.getType());
                        tHmCustomerpaydaysPrice.setXishu(hmCustomerprice.getRemark());
                        tHmCustomerpaydaysPrice.setCustomerpaydaysId(tHmCustomerpaydays.getWqId());
                        tHmCustomerpaydaysPriceMapper.insertTHmCustomerpaydaysPrice(tHmCustomerpaydaysPrice);
                    }

                    //更新用户状态
                    THmCustomer tHmCustomer2 = new THmCustomer();
                    tHmCustomer2.setHcustomerInt4(99);
                    tHmCustomer2.setHcustomerId(tHmCustomer.getHcustomerId());
                    tHmCustomer2.setIsZheng(0);
                    tHmCustomerMapper.updateTHmCustomer(tHmCustomer2);
                }
            }
        }
        return "成功";
    }

    /**
     * 新增用户信息
     */
    @Override
    public int insertTHmCustomer(THmCustomer tHmCustomer) {
        tHmCustomer.setBalance("0.00");
        tHmCustomer.setHcustomerAdduserid(ShiroUtils.getSysUser().getUserId());
        tHmCustomer.setHcustomerAddtime(sdf.format(new Date()));
        tHmCustomer.setHcustomerState(0);
        return tHmCustomerMapper.insertTHmCustomer(tHmCustomer);
    }

    /**
     * 修改用户信息
     */
    @Override
    public int updateTHmCustomer(THmCustomer tHmCustomer) {
        return tHmCustomerMapper.updateTHmCustomer(tHmCustomer);
    }

    /**
     * 修改用户信息
     */
    @Override
    public int updateTHmCustomerForWeChat(THmCustomer tHmCustomer) {
        return tHmCustomerMapper.updateTHmCustomer(tHmCustomer);
    }

    @Override
    public List<SysUser> getUserList() {
        SysUser user = new SysUser();
        return sysUserMapper.selectUserList(user);
    }

    @Override
    public List<ReportVO> getSales(ReportVO reportVO) {
        return tHmCustomerMapper.getSales(reportVO);
    }

    @Override
    public THmCustomer getCustomerByCodeAndPhone(String code, String phone) {
        return tHmCustomerMapper.getCustomerByCodeAndPhone(code, phone);
    }

    @Override
    public int bindCustomer(WechatBind wechatBind) {
        return tHmCustomerMapper.bindCustomer(wechatBind);
    }

    //微信支付获取绑定的用户列表
    @Override
    public List<WechatBind> getWechatBindlist(Integer wechatuserId) {
        List<WechatBind> wechatBindList = tHmCustomerMapper.getWechatBindlist(wechatuserId);
        if (wechatBindList.size() > 0) {
            for (WechatBind wechatBind : wechatBindList) {
                THmCustomer tHmCustomer = tHmCustomerMapper.selectTHmCustomerById(wechatBind.getCustomerId());
                wechatBind.setCustomerCode(tHmCustomer.getHcustomerCode());
                wechatBind.setCustomerName(tHmCustomer.getHcustomerName());
                wechatBind.setCustomerPhone(tHmCustomer.getHcustomerMobile());
                wechatBind.setStatus(tHmCustomer.getHcustomerState());
                THmArea tHmArea = tHmAreaMapper.selectTHmAreaById(tHmCustomer.getHareaId());
                if (tHmArea != null) {
                    if (tHmArea.getHareaLevel() == 2) {
                        wechatBind.setCustomerArea(tHmArea.getHareaName());
                    } else {
                        THmArea tHmArea1 = tHmAreaMapper.selectTHmAreaById(tHmArea.getHareaParentid());
                        wechatBind.setCustomerArea(tHmArea1.getHareaName());
                    }
                }
                wechatBind.setCustomerAddress(tHmCustomer.getHcustomerDetailaddress());
            }
        }
        wechatBindList = wechatBindList.stream().filter(o -> o.getStatus() != 1).collect(Collectors.toList());
        return wechatBindList;
    }

    @Override
    public int delBind(Integer bindId) {
        //解绑的时候将改用户生成的消息模板一块删除了
        WechatBind wechatBind = tHmCustomerMapper.getWechatBind(bindId);
        if (wechatBind != null) {
            //查用户openid
            WechatUser wechatUser = wechatUserMapper.selectWechatUserById((long) wechatBind.getWechatuserId());
            //删除openid和用户id为条件的消息
            tHmMessageMapper.deleteTHmMessage(wechatBind.getCustomerId(), wechatUser.getOpenId());
        }
        return tHmCustomerMapper.delBind(bindId);
    }

    @Override
    public List<THmCustomer> bindlist(THmCustomer tHmCustomer) {
        List<THmCustomer> tHmCustomerList = tHmCustomerMapper.bindlist(tHmCustomer);
        return tHmCustomerList;
    }

    //客服返回用户
    @Override
    public List<CustomerKeFu> selectTHmCustomerListForKefu() {

        List<THmCustomer> tHmCustomerList = tHmCustomerMapper.selectTHmCustomerListForKefu();

        List<CustomerKeFu> customerKeFusList = new ArrayList<>();

        if (tHmCustomerList.size() > 0) {
            for (THmCustomer hmCustomer : tHmCustomerList) {

                CustomerKeFu customerKeFu = new CustomerKeFu();

                if (hmCustomer.getHcustomerId() == null) {
                    customerKeFu.setCustomerId(0);
                } else {
                    customerKeFu.setCustomerId(hmCustomer.getHcustomerId());
                }

                if (hmCustomer.getHcustomerCode() == null) {
                    customerKeFu.setCustomerCode("");
                } else {
                    customerKeFu.setCustomerCode(hmCustomer.getHcustomerCode());
                }

                if (hmCustomer.getHcustomerName() == null) {
                    customerKeFu.setCustomerName("无");
                } else {
                    customerKeFu.setCustomerName(hmCustomer.getHcustomerName());
                }

                if (hmCustomer.getLou() == null) {
                    customerKeFu.setLou("");
                } else {
                    customerKeFu.setLou(hmCustomer.getLou());
                }

                if (hmCustomer.getDan() == null) {
                    customerKeFu.setDan("");
                } else {
                    customerKeFu.setDan(hmCustomer.getDan());
                }

                if (hmCustomer.getHao() == null) {
                    customerKeFu.setHao("");
                } else {
                    customerKeFu.setHao(hmCustomer.getHao());
                }

                if (hmCustomer.getHcustomerMobile() == null) {
                    customerKeFu.setCustomerMobile("无");
                } else {
                    customerKeFu.setCustomerMobile(hmCustomer.getHcustomerMobile());
                }

                if (hmCustomer.getHcustomerHomearea() == null) {
                    customerKeFu.setHomeArea("");
                } else {
                    customerKeFu.setHomeArea(hmCustomer.getHcustomerHomearea());
                }

                if (hmCustomer.getHareaName() == null) {
                    customerKeFu.setCustomerArea("");
                } else {
                    customerKeFu.setCustomerArea(hmCustomer.getHareaName());
                }

                if (hmCustomer.getHstationName() == null) {
                    customerKeFu.setCustomerStation("");
                } else {
                    customerKeFu.setCustomerStation(hmCustomer.getHstationName());
                }

                if (hmCustomer.getDjName() == null) {
                    customerKeFu.setType("");
                } else {
                    customerKeFu.setType(hmCustomer.getDjName());
                }
                customerKeFusList.add(customerKeFu);
            }
        }
        return customerKeFusList;
    }

    @Override
    public List<THmCustomer> checkAddress(THmCustomer tHmCustomer) {
        return tHmCustomerMapper.checkAddress(tHmCustomer);
    }

    //供热面积统计 热站
    @Override
    public List<StationDetailVO> stationDetailLists(StationDetailVO stationDetailVO) {

        List<THmStationDetail> list = tHmStationMapper.selectTHmStationLists(stationDetailVO.getStTime(), stationDetailVO.getEndTime(), stationDetailVO.getStationId());
        List<StationDetailVO> stationDetailVOList = new ArrayList<>();

        //0高 1低
        for (THmStationDetail tHmStationDetail : list) {

            StationDetailVO stationDetailVO1 = new StationDetailVO();
            stationDetailVO1.setStationName(tHmStationDetail.getHstationName());
            stationDetailVO1.setDShi(tHmStationDetail.getSAreaA());
            stationDetailVO1.setDTing(tHmStationDetail.getTAreaA());
            stationDetailVO1.setDZong(new BigDecimal(tHmStationDetail.getSAreaA()).add(new BigDecimal(tHmStationDetail.getTAreaA())).toString());

            stationDetailVO1.setGShi(tHmStationDetail.getSAreaB());
            stationDetailVO1.setGTing(tHmStationDetail.getTAreaB());
            stationDetailVO1.setGZong(new BigDecimal(tHmStationDetail.getSAreaB()).add(new BigDecimal(tHmStationDetail.getTAreaB())).toString());

            stationDetailVO1.setZShi(new BigDecimal(tHmStationDetail.getSAreaA()).add(new BigDecimal(tHmStationDetail.getSAreaB())).toString());
            stationDetailVO1.setZTing(new BigDecimal(tHmStationDetail.getTAreaA()).add(new BigDecimal(tHmStationDetail.getTAreaB())).toString());
            stationDetailVO1.setZZong(new BigDecimal(stationDetailVO1.getZShi()).add(new BigDecimal(stationDetailVO1.getZTing())).toString());

            if (tHmStationDetail.getGHu() == null) {
                tHmStationDetail.setGHu("0");
            }
            if (tHmStationDetail.getDHu() == null) {
                tHmStationDetail.setDHu("0");
            }
            stationDetailVO1.setGHu(tHmStationDetail.getGHu());
            stationDetailVO1.setDHu(tHmStationDetail.getDHu());
            stationDetailVO1.setZHu(String.valueOf(Integer.parseInt(tHmStationDetail.getGHu()) + Integer.parseInt(tHmStationDetail.getDHu())));

            stationDetailVOList.add(stationDetailVO1);
        }

        if (stationDetailVOList.size() > 0) {
            StationDetailVO stationDetailVO1 = new StationDetailVO();
            BigDecimal zzShi = new BigDecimal("0.00");
            BigDecimal zzTing = new BigDecimal("0.00");
            BigDecimal zzHu = new BigDecimal("0");
            BigDecimal zzZong = new BigDecimal("0.00");
            stationDetailVO1.setStationName("总计");
            for (StationDetailVO vo : stationDetailVOList) {
                zzShi = zzShi.add(new BigDecimal(vo.getZShi()));
                zzTing = zzTing.add(new BigDecimal(vo.getZTing()));
                zzHu = zzHu.add(new BigDecimal(vo.getZHu()));
                zzZong = zzZong.add(new BigDecimal(vo.getZZong()));
            }
            stationDetailVO1.setZShi(zzShi.toString());
            stationDetailVO1.setZTing(zzTing.toString());
            stationDetailVO1.setZHu(zzHu.toString());
            stationDetailVO1.setZZong(zzZong.toString());
            stationDetailVOList.add(stationDetailVO1);
        }

        return stationDetailVOList;
    }

    //供热面积统计 分区
    @Override
    public List<StationDetailVO> stationDetailListss(StationDetailVO stationDetailVO) {

        List<THmStationDetail> list = tHmStationMapper.selectTHmStationListss(stationDetailVO.getStTime(), stationDetailVO.getEndTime(), stationDetailVO.getStationId());

        List<StationDetailVO> stationDetailVOList = new ArrayList<>();

        //0高 1低
        for (THmStationDetail tHmStationDetail : list) {

            StationDetailVO stationDetailVO1 = new StationDetailVO();
            stationDetailVO1.setParentName(tHmStationDetail.getParentName());
            stationDetailVO1.setStationName(tHmStationDetail.getHstationName());
            stationDetailVO1.setDShi(tHmStationDetail.getSAreaA());
            stationDetailVO1.setDTing(tHmStationDetail.getTAreaA());
            stationDetailVO1.setDZong(new BigDecimal(tHmStationDetail.getSAreaA()).add(new BigDecimal(tHmStationDetail.getTAreaA())).toString());

            stationDetailVO1.setGShi(tHmStationDetail.getSAreaB());
            stationDetailVO1.setGTing(tHmStationDetail.getTAreaB());
            stationDetailVO1.setGZong(new BigDecimal(tHmStationDetail.getSAreaB()).add(new BigDecimal(tHmStationDetail.getTAreaB())).toString());

            stationDetailVO1.setZShi(new BigDecimal(tHmStationDetail.getSAreaA()).add(new BigDecimal(tHmStationDetail.getSAreaB())).toString());
            stationDetailVO1.setZTing(new BigDecimal(tHmStationDetail.getTAreaA()).add(new BigDecimal(tHmStationDetail.getTAreaB())).toString());
            stationDetailVO1.setZZong(new BigDecimal(stationDetailVO1.getZShi()).add(new BigDecimal(stationDetailVO1.getZTing())).toString());

            if (tHmStationDetail.getGHu() == null) {
                tHmStationDetail.setGHu("0");
            }
            if (tHmStationDetail.getDHu() == null) {
                tHmStationDetail.setDHu("0");
            }
            stationDetailVO1.setGHu(tHmStationDetail.getGHu());
            stationDetailVO1.setDHu(tHmStationDetail.getDHu());
            stationDetailVO1.setZHu(String.valueOf(Integer.parseInt(tHmStationDetail.getGHu()) + Integer.parseInt(tHmStationDetail.getDHu())));

            stationDetailVOList.add(stationDetailVO1);
        }

        if (stationDetailVOList.size() > 0) {
            StationDetailVO stationDetailVO1 = new StationDetailVO();
            BigDecimal zzShi = new BigDecimal("0.00");
            BigDecimal zzTing = new BigDecimal("0.00");
            BigDecimal zzHu = new BigDecimal("0");
            BigDecimal zzZong = new BigDecimal("0.00");
            stationDetailVO1.setStationName("总计");
            for (StationDetailVO vo : stationDetailVOList) {
                zzShi = zzShi.add(new BigDecimal(vo.getZShi()));
                zzTing = zzTing.add(new BigDecimal(vo.getZTing()));
                zzHu = zzHu.add(new BigDecimal(vo.getZHu()));
                zzZong = zzZong.add(new BigDecimal(vo.getZZong()));
            }
            stationDetailVO1.setZShi(zzShi.toString());
            stationDetailVO1.setZTing(zzTing.toString());
            stationDetailVO1.setZHu(zzHu.toString());
            stationDetailVO1.setZZong(zzZong.toString());
            stationDetailVOList.add(stationDetailVO1);
        }

        return stationDetailVOList;
    }

    //单价 统计面积
    @Override
    public List<StationDetailVO> stationDetailListsss(StationDetailVO stationDetailVO) {

        List<THmStationDetail> list = tHmStationMapper.selectTHmStationListsss("");

        List<StationDetailVO> stationDetailVOList = new ArrayList<>();

        //0高 1低
        for (THmStationDetail tHmStationDetail : list) {

            StationDetailVO stationDetailVO1 = new StationDetailVO();
            stationDetailVO1.setStationName(tHmStationDetail.getHstationName());
            stationDetailVO1.setDShi(tHmStationDetail.getSAreaA());
            stationDetailVO1.setDTing(tHmStationDetail.getTAreaA());
            stationDetailVO1.setDZong(new BigDecimal(tHmStationDetail.getSAreaA()).add(new BigDecimal(tHmStationDetail.getTAreaA())).toString());

            stationDetailVO1.setGShi(tHmStationDetail.getSAreaB());
            stationDetailVO1.setGTing(tHmStationDetail.getTAreaB());
            stationDetailVO1.setGZong(new BigDecimal(tHmStationDetail.getSAreaB()).add(new BigDecimal(tHmStationDetail.getTAreaB())).toString());

            stationDetailVO1.setZShi(new BigDecimal(tHmStationDetail.getSAreaA()).add(new BigDecimal(tHmStationDetail.getSAreaB())).toString());
            stationDetailVO1.setZTing(new BigDecimal(tHmStationDetail.getTAreaA()).add(new BigDecimal(tHmStationDetail.getTAreaB())).toString());
            stationDetailVO1.setZZong(new BigDecimal(stationDetailVO1.getZShi()).add(new BigDecimal(stationDetailVO1.getZTing())).toString());

            if (tHmStationDetail.getGHu() == null) {
                tHmStationDetail.setGHu("0");
            }
            if (tHmStationDetail.getDHu() == null) {
                tHmStationDetail.setDHu("0");
            }
            stationDetailVO1.setGHu(tHmStationDetail.getGHu());
            stationDetailVO1.setDHu(tHmStationDetail.getDHu());
            stationDetailVO1.setZHu(String.valueOf(Integer.parseInt(tHmStationDetail.getGHu()) + Integer.parseInt(tHmStationDetail.getDHu())));

            stationDetailVOList.add(stationDetailVO1);
        }

        if (stationDetailVOList.size() > 0) {
            StationDetailVO stationDetailVO1 = new StationDetailVO();
            BigDecimal zzShi = new BigDecimal("0.00");
            BigDecimal zzTing = new BigDecimal("0.00");
            BigDecimal zzHu = new BigDecimal("0");
            BigDecimal zzZong = new BigDecimal("0.00");
            stationDetailVO1.setStationName("总计");
            for (StationDetailVO vo : stationDetailVOList) {
                zzShi = zzShi.add(new BigDecimal(vo.getZShi()));
                zzTing = zzTing.add(new BigDecimal(vo.getZTing()));
                zzHu = zzHu.add(new BigDecimal(vo.getZHu()));
                zzZong = zzZong.add(new BigDecimal(vo.getZZong()));
            }
            stationDetailVO1.setZShi(zzShi.toString());
            stationDetailVO1.setZTing(zzTing.toString());
            stationDetailVO1.setZZong(zzZong.toString());
            stationDetailVOList.add(stationDetailVO1);
        }

        return stationDetailVOList;
    }

    public static boolean isNumber(String number) {
        int index = number.indexOf(".");
        if (index < 0) {
            return StringUtils.isNumeric(number);
        } else {
            String num1 = number.substring(0, index);
            String num2 = number.substring(index + 1);
            return StringUtils.isNumeric(num1) && StringUtils.isNumeric(num2);
        }
    }
}
