package com.ruoyi.business.domain.vo;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

@Data
public class ReportVO extends BaseEntity {

    private Integer hcustomerId;

    private Integer hareaId;

    @Excel(name = "所属分区")
    private String hareaName;

    /**
     * 用户编号
     */
    @Excel(name = "用户编号")
    private String hcustomerCode;

    /**
     * 用户姓名
     */
    @Excel(name = "用户姓名")
    private String hcustomerName;

    /**
     * 手机号
     */
    @Excel(name = "手机号码")
    private String hcustomerMobile;

    /**
     * 详细地址
     */
    @Excel(name = "详细地址")
    private String hcustomerDetailaddress;

    /**
     * 开户状态
     */
    @Excel(name = "开户状态", readConverterExp = "0=否,1=是")
    private Integer hcustomerKhstate;

    /**
     * 开户日期
     */
    @Excel(name = "开户日期")
    private String hcustomerKhtime;

    private String hpayMoney;

    private String tfMoney;

    private String opayMoney;

    private String hpayName;

    private String tfName;

    private String opayName;
}
