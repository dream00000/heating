package com.ruoyi.business.domain.vo;

import com.ruoyi.business.domain.THmCustomerprice;
import lombok.Data;

import java.util.List;

@Data
public class CustomerVO {
    private Integer hcustomerId;
    private Integer hareaId;

    private String hcustomerName;
    private String hcustomerCode;
    private String hcustomerMobile;
    private Integer hcustomerPersoncount;
    private String hcustomerPersonlcard;
    private String hcustomerDetailaddress;

    private String hcustomerHomearea;
    private String hcustomerTgarea;
    private String hcustomerActualarea;
    
    private List<THmCustomerprice> list;
}
