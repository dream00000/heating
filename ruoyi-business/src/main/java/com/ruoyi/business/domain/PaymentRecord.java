package com.ruoyi.business.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * 订单对象 payment_record
 */
@Data
public class PaymentRecord extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long paymentId;

    /**
     * 订单号
     */
    @Excel(name = "订单号")
    private String paymentOrder;

    /**
     * 订单生成时间
     */
    @Excel(name = "订单生成时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date paymentCreateDate;

    /**
     * 订单生成人
     */
    @Excel(name = "订单生成人")
    private String paymentCreateOpenId;

    /**
     * 订单支付金额
     */
    @Excel(name = "订单支付金额")
    private String paymentPayMoney;

    /**
     * 订单支付时间
     */
    @Excel(name = "订单支付时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date paymentPayDate;

    /**
     * 是否支付成功
     */
    @Excel(name = "是否支付成功")
    private String paymentPayStuts;

    /**
     * 微信支付人
     */
    @Excel(name = "微信支付人")
    private String paymentPayOpenId;

    /**
     * 微信订单号
     */
    @Excel(name = "微信订单号")
    private String paymentWechatOrder;

    /**
     * 微信缴费时间
     */
    @Excel(name = "微信缴费时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date paymentWechatDate;

    /**
     * 充值订单编号
     */
    @Excel(name = "充值订单编号")
    private Integer paymentSellelerecordId;

    /**
     * 生成充值订单时间
     */
    @Excel(name = "生成充值订单时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date paymentSellelerecordDate;

    /**
     * 删除标记
     */
    private String delFlag;

    private String bankCode;
    private Integer customerId;

    /**
     * 订单支付类型
     */
    @Excel(name = "订单支付类型")
    private String paymentPayType;

    private WechatUser wechatUser;

    private THmCustomer tHmCustomer;

    private THmPayhistory tHmPayhistory;

    private THmCustomerpaydays tHmCustomerpaydays;
}
