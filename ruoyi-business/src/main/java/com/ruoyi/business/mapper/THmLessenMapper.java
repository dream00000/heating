package com.ruoyi.business.mapper;

import com.ruoyi.business.domain.THmCustomer;
import com.ruoyi.business.domain.THmLessen;
import com.ruoyi.business.domain.vo.LessenVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用热减免Mapper接口
 */
public interface THmLessenMapper {
    /**
     * 查询用热减免
     */
    THmLessen selectTHmLessenById(@Param("id") int id);

    /**
     * 查询用热减免列表
     */
    List<THmLessen> selectTHmLessenList(THmLessen tHmLessen);

    List<LessenVO> selectTHmLessenListt(THmCustomer tHmCustomer);

    /**
     * 新增用热减免
     */
    int insertTHmLessen(THmLessen tHmLessen);

    /**
     * 修改用热减免
     */
    int updateTHmLessen(THmLessen tHmLessen);
}
