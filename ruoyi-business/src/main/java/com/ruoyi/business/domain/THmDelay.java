package com.ruoyi.business.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 延期记录对象 t_hm_delay
 *
 * @author ruoyi
 * @date 2022-03-10
 */
@Data
public class THmDelay extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Integer id;

    /**
     * 账期id
     */
    private Integer wqId;

    /**
     * 账期年度
     */
    @Excel(name = "账期年度")
    private String nian;

    /**
     * 用户id
     */
    private Integer customerId;

    /**
     * 延期金额
     */
    @Excel(name = "延期金额")
    private String total;

    /**
     * 延期天数
     */
    @Excel(name = "延期天数")
    private String days;

    /**
     * 延期总金额
     */
    @Excel(name = "延期总金额")
    private String ztotal;

    /**
     * 延期开始时间
     */
    @Excel(name = "延期开始时间")
    private String startDate;

    /**
     * 延期结束时间
     */
    @Excel(name = "延期结束时间")
    private String endDate;

    /**
     * 阀控id
     */
    private Integer fbId;

    /**
     * 单价id
     */
    private Integer priceId;

    /**
     * 单价金额
     */
    private String dtotal;

    /**
     * 单价系数
     */
    private String xishu;

    /**
     * 是否能修改
     */
    private Integer isGai;

    private String addTime;

    //----查询条件------
    private Integer hareaId;
    private Integer payType;
    private String customerCode;
    private String customerName;
    private String customerPhone;
    private String customerArea;
    private String customerAddress;
    private String priceName;
    private String price;

    private String hzqGrdays;

    private Integer ling;
}
