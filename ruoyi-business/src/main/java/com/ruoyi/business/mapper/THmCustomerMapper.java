package com.ruoyi.business.mapper;

import com.ruoyi.business.domain.THmCustomer;
import com.ruoyi.business.domain.THmCustomerpaydays;
import com.ruoyi.business.domain.WechatBind;
import com.ruoyi.business.domain.data.Sheet1;
import com.ruoyi.business.domain.vo.ReportVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户信息Mapper接口
 */
public interface THmCustomerMapper {
    /**
     * 查询用户信息
     */
    THmCustomer selectTHmCustomerById(@Param("id") int id);

    THmCustomer selectTHmCustomerByHcustomerCode(@Param("hcustomerCode") String hcustomerCode);

    /**
     * 查询用户信息列表
     */
    List<THmCustomer> selectTHmCustomerList(THmCustomer tHmCustomer);

    int exportCount(THmCustomer tHmCustomer);

    //获取本年度没有生成账期的用信息
    List<THmCustomer> getCustomerNotShengBenNian(@Param("hzqGrstarttime") String hzqGrstarttime, @Param("hzqGrendtime") String hzqGrendtime, @Param("hcustomerCode") String hcustomerCode);

    List<THmCustomer> getCustomerNotShengBenNians(@Param("hzqGrstarttime") String hzqGrstarttime, @Param("hzqGrendtime") String hzqGrendtime);

    Integer getMaxCode();

    List<THmCustomer> listByArea(THmCustomer tHmCustomer);

    List<THmCustomer> checkAddress(THmCustomer tHmCustomer);

    List<THmCustomer> selectTHmCustomerListCopy(THmCustomer tHmCustomer);

    List<THmCustomer> getTHmCustomerListForWechart(THmCustomer tHmCustomer);

    /**
     * 新增用户信息
     */
    int insertTHmCustomer(THmCustomer tHmCustomer);

    int insertTHmCustomerCopy(THmCustomer tHmCustomer);

    /**
     * 修改用户信息
     */
    int updateTHmCustomer(THmCustomer tHmCustomer);

    /**
     * 获取开户数
     */
    int openNum(THmCustomer tHmCustomer);

    int getCustomerCount();

    int getCustomerCountByZheng();

    int getQianCount(@Param("thisYear") String thisYear, @Param("nextYear") String nextYear);

    int getGCount();

    int getKCount();

    String getShouSum();

    String getShouSums();

    List<THmCustomerpaydays> getQianSum(@Param("thisYear") String thisYear, @Param("nextYear") String nextYear);

    String getYouSum();

    String getTuiSum();

    List<ReportVO> getSales(ReportVO reportVO);

    void delTHmCustomerCopy();

    //微信支付绑定用户
    THmCustomer getCustomerByCodeAndPhone(@Param("code") String code, @Param("phone") String phone);

    int bindCustomer(WechatBind wechatBind);

    //微信支付获取绑定的用户列表
    List<WechatBind> getWechatBindlist(@Param("wechatuserId") Integer wechatuserId);

    WechatBind getWechatBind(@Param("bindId") Integer bindId);

    //解除绑定
    int delBind(@Param("bindId") Integer bindId);

    //绑定用户
    List<THmCustomer> bindlist(THmCustomer tHmCustomer);

    List<THmCustomer> selectTHmCustomerListForKefu();

    List<Sheet1> sheet1List();
}
