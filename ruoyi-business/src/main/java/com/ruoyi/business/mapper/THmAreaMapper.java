package com.ruoyi.business.mapper;

import com.ruoyi.business.domain.THmArea;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 供热分区Mapper接口
 */
public interface THmAreaMapper {
    /**
     * 查询供热分区
     */
    THmArea selectTHmAreaById(@Param("id") int id);

    /**
     * 查询供热分区列表
     */
    List<THmArea> selectTHmAreaList(THmArea tHmArea);

    List<THmArea> selectTHmAreaListt(THmArea tHmArea);

    List<THmArea> selectTHmAreaLists(THmArea tHmArea);

    //获取parentId下边的id最大的区域 -- 用于计算code
    THmArea getMaxCode(THmArea tHmArea);

    //根据code获得区域
    THmArea getAreaByCode(String code);

    /**
     * 新增供热分区
     */
    int insertTHmArea(THmArea tHmArea);

    /**
     * 修改供热分区
     */
    int updateTHmArea(THmArea tHmArea);

    int setting(THmArea tHmArea);

    List<THmArea> getLevel2();

    List<THmArea> getLevel23(@Param("hareaId") int hareaId);
}
