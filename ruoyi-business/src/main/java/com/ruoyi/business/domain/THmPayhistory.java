package com.ruoyi.business.domain;

import com.ruoyi.business.domain.data.THmCustomerpaydaysPriceCopy;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.List;

/**
 * 充值记录对象 t_hm_payhistory
 */
@Data
public class THmPayhistory extends BaseEntity {

    private Integer hpayId;

    private Integer hcustomerId;

    /**
     * 用户账期id
     */
    private Integer wqId;
    private String[] wqIds;

    @Excel(name = "用户编号")
    private String customerCode;

    @Excel(name = "用户姓名")
    private String customerName;

    @Excel(name = "用户手机号")
    private String customerPhone;

    @Excel(name = "用户分区")
    private String customerArea;

    @Excel(name = "详细地址")
    private String hcustomerDetailaddress;

    @Excel(name = "支付方式")
    private String payType;
    private Integer hpayType;

    @Excel(name = "应缴金额")
    private String hpayYjmoney;

    @Excel(name = "支付金额")
    private String hpayMoney;

    /**
     * 0--》基础
     * 1--》全额
     * 2--》续费
     * 3--》面积变更
     * 4--》分期
     * 5--》复合
     */
    private Integer hpaySfqe;
    private String hpaySfqeName;

    /**
     * 商户订单号
     */
    @Excel(name = "商户订单号")
    private String hpayOuttradeno;

    /**
     * 第三方交易订单号
     */
    @Excel(name = "第三方交易订单号")
    private String hpayTradeno;

    /**
     * 交易时间
     */
    @Excel(name = "交易时间")
    private String hpayTradetime;

    /**
     * 交易人
     */
    private Long hpayTradeuserid;

    /**
     * 是否删除0否1是
     */
    private Integer hpayState;

    /**
     * 当期金额
     */
    private String hpayStr1;

    /**
     * 已缴金额
     */
    private String hpayStr2;

    /**
     * 延期金额
     */
    private String hpayStr3;

    /**
     * 减免金额
     */
    private String hpayStr4;

    /**
     * 延期金额
     */
    private String hpayStr5;

    @Excel(name = "添加时间")
    private String hpayAddtime;

    /**
     * 添加人
     */
    private Long hpayAdduserid;
    private String sysname;

    private List<THmPayhistoryPrice> tHmCustomerpriceList;
    private List<THmCustomerpaydaysPriceCopy> tHmCustomerpaydaysPriceCopyList;
    //历史欠费账期
    private List<THmCustomerpaydays> customerpaydaysList;

    //打印字段
    private String hpayMoneyd;//大写
    private String balance;
    private String mainji;
    private String chaogao;
    private String tinggong;
    private String niandu;

    private PaymentRecord paymentRecord;

    //零钱计算方式
    private String ling;
    //充值金额是否为更改
    private Integer isReadOnly;
    // 0是不用 1是用
    private Integer isChangeYue;

    //开始年度
    private String hpayGrstartYear;
}
