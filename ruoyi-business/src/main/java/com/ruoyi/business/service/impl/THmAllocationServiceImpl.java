package com.ruoyi.business.service.impl;

import java.util.Date;
import java.util.List;

import com.ruoyi.framework.util.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.business.mapper.THmAllocationMapper;
import com.ruoyi.business.domain.THmAllocation;
import com.ruoyi.business.service.ITHmAllocationService;

import javax.annotation.Resource;

/**
 * 金额划拨Service业务层处理
 *
 * @author ruoyi
 * @date 2022-03-16
 */
@Service
public class THmAllocationServiceImpl implements ITHmAllocationService
{
    @Resource
    private THmAllocationMapper tHmAllocationMapper;

    /**
     * 查询金额划拨
     *
     * @param id 金额划拨主键
     * @return 金额划拨
     */
    @Override
    public THmAllocation selectTHmAllocationById(Long id)
    {
        return tHmAllocationMapper.selectTHmAllocationById(id);
    }

    /**
     * 查询金额划拨列表
     *
     * @param tHmAllocation 金额划拨
     * @return 金额划拨
     */
    @Override
    public List<THmAllocation> selectTHmAllocationList(THmAllocation tHmAllocation)
    {
        return tHmAllocationMapper.selectTHmAllocationList(tHmAllocation);
    }

    /**
     * 新增金额划拨
     *
     * @param tHmAllocation 金额划拨
     * @return 结果
     */
    @Override
    public int insertTHmAllocation(THmAllocation tHmAllocation)
    {

        tHmAllocation.setAdduserid(ShiroUtils.getSysUser().getUserId());
        tHmAllocation.setAddtime(new Date());
        return tHmAllocationMapper.insertTHmAllocation(tHmAllocation);
    }

    /**
     * 修改金额划拨
     *
     * @param tHmAllocation 金额划拨
     * @return 结果
     */
    @Override
    public int updateTHmAllocation(THmAllocation tHmAllocation)
    {
        return tHmAllocationMapper.updateTHmAllocation(tHmAllocation);
    }

    /**
     * 批量删除金额划拨
     *
     * @param ids 需要删除的金额划拨主键
     * @return 结果
     */
    @Override
    public int deleteTHmAllocationByIds(Long[] ids)
    {
        return tHmAllocationMapper.deleteTHmAllocationByIds(ids);
    }

    /**
     * 删除金额划拨信息
     *
     * @param id 金额划拨主键
     * @return 结果
     */
    @Override
    public int deleteTHmAllocationById(Long id)
    {
        return tHmAllocationMapper.deleteTHmAllocationById(id);
    }
}
