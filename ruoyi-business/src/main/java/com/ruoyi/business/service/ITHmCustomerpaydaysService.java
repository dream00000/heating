package com.ruoyi.business.service;

import com.ruoyi.business.domain.THmCustomer;
import com.ruoyi.business.domain.THmCustomerpaydays;
import com.ruoyi.business.domain.vo.CustomerpaydaysVO;
import com.ruoyi.business.domain.vo.OrderKeFu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户年度账期Service接口
 */
public interface ITHmCustomerpaydaysService {
    /**
     * 查询用户年度账期
     */
    public THmCustomerpaydays selectTHmCustomerpaydaysById(@Param("id") int id);

    /**
     * 查询用户年度账期列表
     */
    public List<THmCustomerpaydays> selectTHmCustomerpaydaysList(THmCustomerpaydays tHmCustomerpaydays);

    public List<THmCustomerpaydays> selectTHmCustomerpaydaysListm(THmCustomerpaydays tHmCustomerpaydays);

    public List<OrderKeFu> orderKeFuList(String payStatus);

    public List<CustomerpaydaysVO> selectTHmCustomerpaydaysLists(THmCustomer tHmCustomer);

    public List<CustomerpaydaysVO> jiaoList(THmCustomer tHmCustomer);

    public int exportCount(THmCustomer tHmCustomer);

    public List<THmCustomerpaydays> getcuswq(Long hcustomerId);

    /**
     * 新增用户年度账期
     */
    public int insertTHmCustomerpaydays(THmCustomerpaydays tHmCustomerpaydays);

    /**
     * 修改用户年度账期
     */
    public int updateTHmCustomerpaydays(THmCustomerpaydays tHmCustomerpaydays);

    public int initPay(String thisYear, Integer ling, String hcustomerCode);

    public int initPays(String thisYear, Integer ling);

    public int initDelays(String thisYear);

    //获取 账期 的开始年度
    public List<String> startTime();
}
