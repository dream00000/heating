package com.ruoyi.business.mapper;

import com.ruoyi.business.domain.THmNameList;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 黑白名单
 */
public interface THmNameListMapper {

    THmNameList getById(@Param("id") int id);

    List<THmNameList> selectTHmNameList(THmNameList tHmNameList);

    int insertTHmNameList(THmNameList tHmNameList);

    List<THmNameList> getByCustomerId(@Param("customerId") int customerId);
}
