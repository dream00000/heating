package com.ruoyi.business.domain.vo;

import lombok.Data;

@Data
public class ReportBills {

    private Long userid;
    private String paytype;
    private String ctype;
    private Integer cou;
    private String hpaySum;
}
