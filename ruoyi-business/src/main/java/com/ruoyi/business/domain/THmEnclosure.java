package com.ruoyi.business.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 附件对象 t_hm_enclosure
 *
 * @author ruoyi
 * @date 2021-08-13
 */
@Data
public class THmEnclosure extends BaseEntity {

    private Integer enId;

    /**
     * 附件组code
     */
    @Excel(name = "附件组code")
    private String enFjcode;

    /**
     * 附件url
     */
    @Excel(name = "附件url")
    private String enFjurl;

    /**
     * 附件暂存状态0暂存1正式
     */
    @Excel(name = "附件暂存状态0暂存1正式")
    private Integer enZcstate;

    /**
     * 是否删除0否1是
     */
    @Excel(name = "是否删除0否1是")
    private Integer enState;

    /**
     * 添加人
     */
    @Excel(name = "添加人")
    private Long enAdduserid;

    /**
     * 添加时间
     */
    @Excel(name = "添加时间")
    private String enAddtime;
}
