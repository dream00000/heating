package com.ruoyi.business.controller;

import com.ruoyi.business.domain.THmArea;
import com.ruoyi.business.domain.THmStation;
import com.ruoyi.business.mapper.THmAreaMapper;
import com.ruoyi.business.service.ITHmStationService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 热源站Controller
 *
 * @author ruoyi
 * @date 2021-12-25
 */
@Controller
@RequestMapping("/business/station")
public class THmStationController extends BaseController {
    private String prefix = "business/station";

    @Resource
    private ITHmStationService tHmStationService;

    @Resource
    private THmAreaMapper tHmAreaMapper;


    @GetMapping()
    public String station() {
        return prefix + "/station";
    }

    /**
     * 查询热源站树列表
     */
    @PostMapping("/list")
    @ResponseBody
    public List<THmStation> list(THmStation tHmStation) {
        return tHmStationService.selectTHmStationList(tHmStation);
    }

    /**
     * 获取parentId下边的id最大的区域 -- 用于计算code
     */
    @PostMapping("/getMaxCode")
    @ResponseBody
    public AjaxResult getMaxCode(THmStation tHmStation) {
        return AjaxResult.success("成功", tHmStationService.getMaxCode(tHmStation));
    }

    /**
     * 查询供热分区树列表
     */
    @PostMapping("/getById")
    @ResponseBody
    public AjaxResult getById(Long id) {
        return AjaxResult.success("成功", tHmStationService.selectTHmStationById(id));
    }

    /**
     * 导出热源站列表
     */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(THmStation tHmStation) {
        List<THmStation> list = tHmStationService.selectTHmStationList(tHmStation);
        ExcelUtil<THmStation> util = new ExcelUtil<>(THmStation.class);
        return util.exportExcel(list, "station");
    }

    /**
     * 新增供热分区
     */
    @GetMapping(value = {"/add/{id}"})
    public String add(@PathVariable("id") Long id, ModelMap mmap) {
        THmStation tHmStation = tHmStationService.selectTHmStationById(id);
        mmap.put("tHmStation", tHmStation);
        return prefix + "/add";
    }

    /**
     * 新增保存热源站
     */
    @Log(title = "热源站", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(THmStation tHmStation) {
        return toAjax(tHmStationService.insertTHmStation(tHmStation));
    }

    /**
     * 修改热源站
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap) {
        THmStation tHmStation = tHmStationService.selectTHmStationById(id);
        mmap.put("tHmStation", tHmStation);
        return prefix + "/edit";
    }

    /**
     * 修改保存热源站
     */
    @Log(title = "热源站", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(THmStation tHmStation) {
        return toAjax(tHmStationService.updateTHmStation(tHmStation));
    }


    /**
     * 选择热源站树
     */
    @GetMapping(value = {"/selectStationTree/{id}"})
    public String selectStationTree(@PathVariable("id") Long id, ModelMap mmap) {
        if (id == 0L) {
            id = 1L;
        }
        mmap.put("tHmStation", tHmStationService.selectTHmStationById(id));
        return prefix + "/tree";
    }

    /**
     * 加载热源站树列表
     */
    @GetMapping("/treeData")
    @ResponseBody
    public List<Ztree> treeData() {
        List<Ztree> ztrees = tHmStationService.selectTHmStationTree();
        return ztrees;
    }

    /**
     * 删除热源站
     */
    @Log(title = "热源站", businessType = BusinessType.DELETE)
    @GetMapping("/remove/{id}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("id") Long id) {

        THmArea tHmArea = new THmArea();
        tHmArea.setHstationId(id);
        List<THmArea> tHmAreaList = tHmAreaMapper.selectTHmAreaList(tHmArea);

        if (tHmAreaList.size() > 0) {
            return AjaxResult.error("删除失败，该热站下有分区存在");
        } else {
            THmStation tHmStation = new THmStation();
            tHmStation.setHstationState(1);
            tHmStation.setHstationId(id);
            return toAjax(tHmStationService.updateTHmStation(tHmStation));
        }
    }
}
