package com.ruoyi.business.controller;

import com.ruoyi.business.domain.THmMessage;
import com.ruoyi.business.service.ITHmMessageService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 推送消息记录Controller
 *
 * @author ruoyi
 * @date 2022-03-12
 */
@Controller
@RequestMapping("/business/message")
public class THmMessageController extends BaseController {
    private String prefix = "business/message";

    @Resource
    private ITHmMessageService tHmMessageService;

    @GetMapping()
    public String message() {
        return prefix + "/message";
    }

    /**
     * 查询推送消息记录列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(THmMessage tHmMessage) {
        startPage();
        List<THmMessage> list = tHmMessageService.selectTHmMessageList(tHmMessage);
        return getDataTable(list);
    }

    /**
     * 导出推送消息记录列表
     */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(THmMessage tHmMessage) {
        List<THmMessage> list = tHmMessageService.selectTHmMessageList(tHmMessage);
        ExcelUtil<THmMessage> util = new ExcelUtil<THmMessage>(THmMessage.class);
        return util.exportExcel(list, "message");
    }

    /**
     * 新增推送消息记录
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存推送消息记录
     */
    @Log(title = "推送消息记录", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(THmMessage tHmMessage) {
        return toAjax(tHmMessageService.insertTHmMessage(tHmMessage));
    }

    /**
     * 修改推送消息记录
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") int id, ModelMap mmap) {
        THmMessage tHmMessage = tHmMessageService.selectTHmMessageById(id);
        mmap.put("tHmMessage", tHmMessage);
        return prefix + "/edit";
    }

    /**
     * 修改保存推送消息记录
     */
    @Log(title = "推送消息记录", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(THmMessage tHmMessage) {
        return toAjax(tHmMessageService.updateTHmMessage(tHmMessage));
    }


    /**
     * 删除推送消息记录
     */
    @Log(title = "推送消息记录", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(@PathVariable("id") int id) {
        THmMessage tHmMessage = new THmMessage();
        return toAjax(tHmMessageService.updateTHmMessage(tHmMessage));
    }
}
