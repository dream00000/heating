package com.ruoyi.business.mapper;


import com.ruoyi.business.domain.data.WuYe;

import java.util.List;

public interface WuYeMapper {

    public List<WuYe> select();
}
