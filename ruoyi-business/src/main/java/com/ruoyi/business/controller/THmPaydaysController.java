package com.ruoyi.business.controller;

import com.ruoyi.business.domain.THmArea;
import com.ruoyi.business.domain.THmPaydays;
import com.ruoyi.business.mapper.THmAreaMapper;
import com.ruoyi.business.service.ITHmAreaService;
import com.ruoyi.business.service.ITHmPaydaysService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 分区账期Controller
 *
 * @author ruoyi
 * @date 2021-08-13
 */
@Controller
@RequestMapping("/business/paydays")
public class THmPaydaysController extends BaseController {
    private String prefix = "business/paydays";

    @Resource
    private ITHmPaydaysService tHmPaydaysService;

    @Resource
    private ITHmAreaService itHmAreaService;

    @Resource
    private THmAreaMapper tHmAreaMapper;

    @GetMapping()
    public String paydays() {
        return prefix + "/paydays";
    }

    /**
     * 查询分区账期列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(THmPaydays tHmPaydays) {
        startPage();
        List<THmPaydays> list = tHmPaydaysService.selectTHmPaydaysList(tHmPaydays);
        return getDataTable(list);
    }

    /**
     * 查询账期策略详情
     */
    @PostMapping("/getById")
    @ResponseBody
    public AjaxResult getById(Integer id) {
        return AjaxResult.success("成功", tHmPaydaysService.selectTHmPaydaysById(id));
    }

    /**
     * 导出分区账期列表
     */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(THmPaydays tHmPaydays) {
        List<THmPaydays> list = tHmPaydaysService.selectTHmPaydaysList(tHmPaydays);
        ExcelUtil<THmPaydays> util = new ExcelUtil<THmPaydays>(THmPaydays.class);
        return util.exportExcel(list, "paydays");
    }

    /**
     * 新增分区账期
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存分区账期
     */
    @Log(title = "新增账期策略", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(THmPaydays tHmPaydays) {
        return toAjax(tHmPaydaysService.insertTHmPaydays(tHmPaydays));
    }

    /**
     * 修改分区账期
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") int id, ModelMap mmap) {
        THmPaydays tHmPaydays = tHmPaydaysService.selectTHmPaydaysById(id);
        mmap.put("tHmPaydays", tHmPaydays);
        return prefix + "/edit";
    }

    /**
     * 修改保存分区账期
     */
    @Log(title = "修改账期策略", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(THmPaydays tHmPaydays) {
        return toAjax(tHmPaydaysService.updateTHmPaydays(tHmPaydays));
    }


    /**
     * 删除分区账期
     */
    @Log(title = "删除账期策略", businessType = BusinessType.DELETE)
    @PostMapping("/remove/{id}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("id") int id) {
        THmArea tHmArea = new THmArea();
        tHmArea.setPaydaysId(id);
        List<THmArea> list = itHmAreaService.selectTHmAreaList(tHmArea);

        if (list.size() > 0) {
            return AjaxResult.success("删除失败，该账期策略下有绑定的分区");
        } else {
            THmPaydays tHmPaydays = new THmPaydays();
            tHmPaydays.setHzqId(id);
            tHmPaydays.setHzqState(1);
            return toAjax(tHmPaydaysService.updateTHmPaydays(tHmPaydays));
        }
    }

    @GetMapping("/qu/{id}")
    public String qu(@PathVariable("id") int id, Model model) {
        Map<String, Object> map = new HashMap<>();
        map.put("hzqId", id);
        model.addAllAttributes(map);
        return prefix + "/qu";
    }

    /**
     * 查询分区账期列表
     */
    @PostMapping("/quList/{id}")
    @ResponseBody
    public TableDataInfo quList(@PathVariable("id") int id) {
        THmArea tHmArea = new THmArea();
        tHmArea.setPaydaysId(id);
        startPage();
        List<THmArea> list = itHmAreaService.selectTHmAreaList(tHmArea);
        return getDataTable(list);
    }

    @GetMapping("/bang/{id}")
    public String bang(@PathVariable("id") int id, Model model) {
        THmPaydays tHmPaydays = new THmPaydays();
        tHmPaydays.setHzqId(id);
        Map<String, Object> map = new HashMap<>();
        map.put("tHmPaydays", tHmPaydays);
        model.addAllAttributes(map);
        return prefix + "/bang";
    }

    /**
     * 修改保存分区账期
     */
    @Log(title = "绑定分区", businessType = BusinessType.UPDATE)
    @PostMapping("/bangList")
    @ResponseBody
    public AjaxResult bangList(THmPaydays tHmPaydays) {
        if (tHmPaydays.getMenuIds() != null) {
            for (Integer areaId : tHmPaydays.getMenuIds()) {
                THmArea tHmArea = new THmArea();
                tHmArea.setPaydaysId(tHmPaydays.getHzqId());
                tHmArea.setHareaId(areaId);
                tHmAreaMapper.updateTHmArea(tHmArea);
            }
        }
        return toAjax(1);
    }
}
