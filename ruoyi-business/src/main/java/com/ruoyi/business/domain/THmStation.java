package com.ruoyi.business.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.TreeEntity;
import lombok.Data;

/**
 * 热源站对象 t_hm_station
 *
 * @author ruoyi
 * @date 2021-12-25
 */
@Data
public class THmStation extends TreeEntity {

    private Long hstationId;

    /**
     * 区域编号
     */
    @Excel(name = "区域编号")
    private String hstationCode;

    /**
     * 区域名称
     */
    @Excel(name = "区域名称")
    private String hstationName;

    /**
     * 上级id
     */
    private Long hstationParentid;

    /**
     * 排序id
     */
    private Integer hstationOrderid;

    /**
     * 是否删除0否1是
     */
    private Integer hstationState;

    /**
     * 添加人id
     */
    private Long hstationAdduserid;

    /**
     * 添加时间
     */
    @Excel(name = "添加时间")
    private String hstationAddtime;

    /**
     * 上级名称
     */
    @Excel(name = "上级名称")
    private String parentName;

    /**
     * 层级
     */
    @Excel(name = "层级")
    private Integer hstationLevel;
}
