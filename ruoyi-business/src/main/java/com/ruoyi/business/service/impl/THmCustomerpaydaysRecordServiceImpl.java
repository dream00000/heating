package com.ruoyi.business.service.impl;

import com.ruoyi.business.domain.THmCustomerpaydaysRecord;
import com.ruoyi.business.mapper.THmCustomerpaydaysRecordMapper;
import com.ruoyi.business.service.ITHmCustomerpaydaysRecordService;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.mapper.SysUserMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 账期操作日志Service业务层处理
 *
 * @author ruoyi
 * @date 2021-12-29
 */
@Service
public class THmCustomerpaydaysRecordServiceImpl implements ITHmCustomerpaydaysRecordService {

    @Resource
    private THmCustomerpaydaysRecordMapper tHmCustomerpaydaysRecordMapper;

    @Resource
    private SysUserMapper sysUserMapper;

    /**
     * 查询账期操作日志
     *
     * @param id
     * @return 账期操作日志
     */
    @Override
    public THmCustomerpaydaysRecord selectTHmCustomerpaydaysRecordById(Long id) {
        return tHmCustomerpaydaysRecordMapper.selectTHmCustomerpaydaysRecordById(id);
    }

    /**
     * 查询账期操作日志列表
     *
     * @param tHmCustomerpaydaysRecord 账期操作日志
     * @return 账期操作日志
     */
    @Override
    public List<THmCustomerpaydaysRecord> selectTHmCustomerpaydaysRecordList(THmCustomerpaydaysRecord tHmCustomerpaydaysRecord) {

        List<THmCustomerpaydaysRecord> tHmCustomerpaydaysRecordList = tHmCustomerpaydaysRecordMapper.selectTHmCustomerpaydaysRecordList(tHmCustomerpaydaysRecord);
        if (tHmCustomerpaydaysRecordList.size() > 0) {
            for (THmCustomerpaydaysRecord hmCustomerpaydaysRecord : tHmCustomerpaydaysRecordList) {
                SysUser sysUser = sysUserMapper.selectUserById(hmCustomerpaydaysRecord.getAddUser());
                hmCustomerpaydaysRecord.setAddUserName(sysUser.getUserName());
            }
        }

        return tHmCustomerpaydaysRecordList;
    }

    /**
     * 新增账期操作日志
     *
     * @param tHmCustomerpaydaysRecord 账期操作日志
     * @return 结果
     */
    @Override
    public int insertTHmCustomerpaydaysRecord(THmCustomerpaydaysRecord tHmCustomerpaydaysRecord) {
        tHmCustomerpaydaysRecord.setAddTime(new Date());
        tHmCustomerpaydaysRecord.setStatus(0);
        tHmCustomerpaydaysRecord.setAddUser(ShiroUtils.getSysUser().getUserId());
        return tHmCustomerpaydaysRecordMapper.insertTHmCustomerpaydaysRecord(tHmCustomerpaydaysRecord);
    }

    /**
     * 修改账期操作日志
     *
     * @param tHmCustomerpaydaysRecord 账期操作日志
     * @return 结果
     */
    @Override
    public int updateTHmCustomerpaydaysRecord(THmCustomerpaydaysRecord tHmCustomerpaydaysRecord) {
        return tHmCustomerpaydaysRecordMapper.updateTHmCustomerpaydaysRecord(tHmCustomerpaydaysRecord);
    }

}
