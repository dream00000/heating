package com.ruoyi.business.service.impl;

import com.ruoyi.business.domain.THmArea;
import com.ruoyi.business.domain.THmCustomer;
import com.ruoyi.business.domain.THmNameList;
import com.ruoyi.business.mapper.THmCustomerMapper;
import com.ruoyi.business.mapper.THmNameListMapper;
import com.ruoyi.business.service.ITHmAreaService;
import com.ruoyi.business.service.THmNameListService;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.mapper.SysUserMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 黑白名单
 */
@Service
public class THmNameListServiceImpl implements THmNameListService {

    @Resource
    private THmNameListMapper tHmNameListMapper;

    @Resource
    private ITHmAreaService itHmAreaService;

    @Resource
    private THmCustomerMapper tHmCustomerMapper;

    @Resource
    private SysUserMapper sysUserMapper;

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    public THmNameList getById(int id) {
        return tHmNameListMapper.getById(id);
    }

    @Override
    public List<THmNameList> selectTHmNameList(THmNameList tHmNameList) {

        List<THmNameList> list = tHmNameListMapper.getByCustomerId(tHmNameList.getCustomerId());

        if (list.size() > 0) {
            for (THmNameList hmNameList : list) {
                THmArea tHmArea = itHmAreaService.selectTHmAreaById(hmNameList.getHareaId());
                if (tHmArea != null) {
                    if (tHmArea.getHareaLevel() == 2) {
                        hmNameList.setAreaName(tHmArea.getHareaName());
                    } else {
                        THmArea tHmArea1 = itHmAreaService.selectTHmAreaById(tHmArea.getHareaParentid());
                        hmNameList.setAreaName(tHmArea1.getHareaName());
                    }
                }

                //操作人员
                SysUser sysUser = sysUserMapper.selectUserById(hmNameList.getSysUser());
                hmNameList.setAddUser(sysUser.getUserName());
            }
        }
        return list;
    }

    @Override
    public int insertTHmNameList(THmNameList tHmNameList) {

        if (tHmNameList.getType() == 1) {
            THmCustomer tHmCustomer = tHmCustomerMapper.selectTHmCustomerById(tHmNameList.getCustomerId());
            if (tHmCustomer != null) {
                tHmCustomer.setIsChong(0);
                tHmCustomerMapper.updateTHmCustomer(tHmCustomer);
            }
        }

        tHmNameList.setAddTime(sdf.format(new Date()));
        tHmNameList.setSysUser(ShiroUtils.getSysUser().getUserId());

        return tHmNameListMapper.insertTHmNameList(tHmNameList);
    }

    @Override
    public List<THmNameList> getByCustomerId(int customerId) {

        List<THmNameList> list = tHmNameListMapper.getByCustomerId(customerId);

        if (list.size() > 0) {
            for (THmNameList hmNameList : list) {
                //操作人员
                SysUser sysUser = sysUserMapper.selectUserById(hmNameList.getSysUser());
                hmNameList.setAddUser(sysUser.getUserName());
            }
        }
        return list;
    }
}
