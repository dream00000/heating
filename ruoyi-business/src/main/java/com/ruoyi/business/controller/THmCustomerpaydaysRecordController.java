package com.ruoyi.business.controller;

import com.ruoyi.business.domain.THmCustomerpaydaysRecord;
import com.ruoyi.business.service.ITHmCustomerpaydaysRecordService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 账期操作日志Controller
 *
 * @author ruoyi
 * @date 2021-12-29
 */
@Controller
@RequestMapping("/business/record")
public class THmCustomerpaydaysRecordController extends BaseController {
    private String prefix = "business/record";

    @Resource
    private ITHmCustomerpaydaysRecordService tHmCustomerpaydaysRecordService;

    @GetMapping()
    public String record() {
        return prefix + "/record";
    }

    /**
     * 查询账期操作日志列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(THmCustomerpaydaysRecord tHmCustomerpaydaysRecord) {
        startPage();
        List<THmCustomerpaydaysRecord> list = tHmCustomerpaydaysRecordService.selectTHmCustomerpaydaysRecordList(tHmCustomerpaydaysRecord);
        return getDataTable(list);
    }

    /**
     * 导出账期操作日志列表
     */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(THmCustomerpaydaysRecord tHmCustomerpaydaysRecord) {
        List<THmCustomerpaydaysRecord> list = tHmCustomerpaydaysRecordService.selectTHmCustomerpaydaysRecordList(tHmCustomerpaydaysRecord);
        ExcelUtil<THmCustomerpaydaysRecord> util = new ExcelUtil<THmCustomerpaydaysRecord>(THmCustomerpaydaysRecord.class);
        return util.exportExcel(list, "record");
    }

    /**
     * 新增账期操作日志
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存账期操作日志
     */
    @Log(title = "账期操作日志", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(THmCustomerpaydaysRecord tHmCustomerpaydaysRecord) {
        return toAjax(tHmCustomerpaydaysRecordService.insertTHmCustomerpaydaysRecord(tHmCustomerpaydaysRecord));
    }

    /**
     * 修改账期操作日志
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap) {
        THmCustomerpaydaysRecord tHmCustomerpaydaysRecord = tHmCustomerpaydaysRecordService.selectTHmCustomerpaydaysRecordById(id);
        mmap.put("tHmCustomerpaydaysRecord", tHmCustomerpaydaysRecord);
        return prefix + "/edit";
    }

    /**
     * 修改保存账期操作日志
     */
    @Log(title = "账期操作日志", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(THmCustomerpaydaysRecord tHmCustomerpaydaysRecord) {
        return toAjax(tHmCustomerpaydaysRecordService.updateTHmCustomerpaydaysRecord(tHmCustomerpaydaysRecord));
    }


    /**
     * 删除账期操作日志
     */
    @Log(title = "账期操作日志", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(@PathVariable("id") int id) {
        THmCustomerpaydaysRecord tHmCustomerpaydaysRecord = new THmCustomerpaydaysRecord();
        return toAjax(tHmCustomerpaydaysRecordService.updateTHmCustomerpaydaysRecord(tHmCustomerpaydaysRecord));
    }
}
