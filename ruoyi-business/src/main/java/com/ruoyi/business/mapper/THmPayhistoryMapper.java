package com.ruoyi.business.mapper;

import com.ruoyi.business.domain.THmCustomer;
import com.ruoyi.business.domain.THmPayhistory;
import com.ruoyi.business.domain.vo.DetailListVO;
import com.ruoyi.business.domain.vo.DetailVO;
import com.ruoyi.business.domain.vo.PayhistoryVO;
import com.ruoyi.business.domain.vo.ReportBills;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 充值记录Mapper接口
 */
public interface THmPayhistoryMapper {
    /**
     * 查询充值记录
     */
    THmPayhistory selectTHmPayhistoryById(@Param("id") int id);

    /**
     * 查询充值记录列表
     */
    List<THmPayhistory> selectTHmPayhistoryList(THmPayhistory tHmPayhistory);

    List<THmPayhistory> selectTHmPayhistoryListForWeChat(THmPayhistory tHmPayhistory);

    THmPayhistory payHistoryForWX(@Param("hpayTradeno") String hpayTradeno);

    List<PayhistoryVO> selectTHmPayhistoryListt(THmCustomer tHmCustomer);

    int exportCount(THmCustomer tHmCustomer);

    int reportBillCount(THmPayhistory tHmPayhistory);

    String reportBillSum(THmPayhistory tHmPayhistory);

    /**
     * 新增充值记录
     */
    int insertTHmPayhistory(THmPayhistory tHmPayhistory);

    /**
     * 修改充值记录
     */
    int updateTHmPayhistory(THmPayhistory tHmPayhistory);

    /**
     * 业务查询 总支付户数
     */
    int payNum(THmPayhistory tHmPayhistory);

    /**
     * 业务查询 总金额
     */
    String payTotal(THmPayhistory tHmPayhistory);

    //销售明细
    List<DetailListVO> detail(DetailVO detailVO);

    //延期金额缴纳明细
    List<DetailListVO> yanDetail(DetailVO detailVO);

    List<ReportBills> reportBills(@Param("startTime") String startTime, @Param("endTime") String endTime, @Param("addUser") String addUser);

    THmPayhistory getByWqIdForXuFei(@Param("wqId") int wqId);

    List<THmPayhistory> getByWqId(@Param("wqId") int wqId);

    List<THmPayhistory> getByCustomerIdAndNian(@Param("hcustomerId") int hcustomerId,@Param("hpayGrstartYear") String hpayGrstartYear);
}
