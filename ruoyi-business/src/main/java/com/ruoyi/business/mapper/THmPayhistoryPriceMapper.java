package com.ruoyi.business.mapper;

import com.ruoyi.business.domain.THmPayhistoryPrice;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 充值-单价Mapper接口
 */
public interface THmPayhistoryPriceMapper {
    /**
     * 查询充值-单价
     */
    THmPayhistoryPrice selectTHmPayhistoryPriceById(@Param("id") int id);

    /**
     * 查询充值-单价列表
     */
    List<THmPayhistoryPrice> selectTHmPayhistoryPriceList(THmPayhistoryPrice tHmPayhistoryPrice);

    /**
     * 新增充值-单价
     */
    int insertTHmPayhistoryPrice(THmPayhistoryPrice tHmPayhistoryPrice);

    /**
     * 修改充值-单价
     */
    int updateTHmPayhistoryPrice(THmPayhistoryPrice tHmPayhistoryPrice);

    List<THmPayhistoryPrice> gou(THmPayhistoryPrice tHmPayhistoryPrice);
}
