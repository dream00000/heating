package com.ruoyi.business.service;

import com.ruoyi.business.domain.bill.BusinessList;
import com.ruoyi.business.domain.report.IndexProportion;
import com.ruoyi.business.domain.vo.IndexAreaDaysVO;
import com.ruoyi.business.domain.vo.IndexStatisticsVO;

import java.util.List;

/**
 * 业务查询
 *
 * @author ruoyi
 * @date 2021-09-02
 */
public interface THmReportService {

    IndexStatisticsVO indexStatistics();

    List<IndexProportion> indexProportion();

    BusinessList reportBill(BusinessList businessList);
}
