package com.ruoyi.business.service.impl;

import com.ruoyi.business.domain.THmPayhistoryRecord;
import com.ruoyi.business.mapper.THmPayhistoryRecordMapper;
import com.ruoyi.business.service.ITHmPayhistoryRecordService;
import com.ruoyi.framework.util.ShiroUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 充值调整日志Service业务层处理
 *
 * @author ruoyi
 * @date 2022-04-11
 */
@Service
public class THmPayhistoryRecordServiceImpl implements ITHmPayhistoryRecordService {
    @Resource
    private THmPayhistoryRecordMapper tHmPayhistoryRecordMapper;

    /**
     * 查询充值调整日志
     *
     * @param id
     * @return 充值调整日志
     */
    @Override
    public THmPayhistoryRecord selectTHmPayhistoryRecordById(int id) {
        return tHmPayhistoryRecordMapper.selectTHmPayhistoryRecordById(id);
    }

    /**
     * 查询充值调整日志列表
     *
     * @param tHmPayhistoryRecord 充值调整日志
     * @return 充值调整日志
     */
    @Override
    public List<THmPayhistoryRecord> selectTHmPayhistoryRecordList(THmPayhistoryRecord tHmPayhistoryRecord) {
        return tHmPayhistoryRecordMapper.selectTHmPayhistoryRecordList(tHmPayhistoryRecord);
    }

    /**
     * 新增充值调整日志
     *
     * @param tHmPayhistoryRecord 充值调整日志
     * @return 结果
     */
    @Override
    public int insertTHmPayhistoryRecord(THmPayhistoryRecord tHmPayhistoryRecord) {
        tHmPayhistoryRecord.setAddTime(new Date());
        tHmPayhistoryRecord.setAddUser(ShiroUtils.getSysUser().getUserId());
        tHmPayhistoryRecord.setStatus(0);
        return tHmPayhistoryRecordMapper.insertTHmPayhistoryRecord(tHmPayhistoryRecord);
    }

    /**
     * 修改充值调整日志
     *
     * @param tHmPayhistoryRecord 充值调整日志
     * @return 结果
     */
    @Override
    public int updateTHmPayhistoryRecord(THmPayhistoryRecord tHmPayhistoryRecord) {
        return tHmPayhistoryRecordMapper.updateTHmPayhistoryRecord(tHmPayhistoryRecord);
    }

}
