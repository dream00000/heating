package com.ruoyi.business.controller;

import com.ruoyi.business.domain.THmCustomer;
import com.ruoyi.business.domain.THmOtherpayhistory;
import com.ruoyi.business.domain.THmRefund;
import com.ruoyi.business.domain.vo.RefundVO;
import com.ruoyi.business.service.ITHmCustomerService;
import com.ruoyi.business.service.ITHmRefundService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 退费Controller
 *
 * @author ruoyi
 * @date 2021-08-13
 */
@Controller
@RequestMapping("/business/refund")
@Api("退费")
public class THmRefundController extends BaseController {
    private String prefix = "business/refund";

    @Resource
    private ITHmRefundService tHmRefundService;

    @Resource
    private ITHmCustomerService tHmCustomerService;

    @GetMapping()
    public String customer() {
        return prefix + "/customer";
    }

    /**
     * 查询用户信息列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(THmCustomer tHmCustomer) {
        if ("fou".equals(tHmCustomer.getBalance())) {
            tHmCustomer.setHcustomerStr4("fou");
        } else if ("shi".equals(tHmCustomer.getBalance())) {
            tHmCustomer.setHcustomerStr5("shi");
        }
        startPage();
        List<THmCustomer> list = tHmCustomerService.selectTHmCustomerList(tHmCustomer);
        return getDataTable(list);
    }

    @GetMapping("/refundList")
    public String refundList() {
        return prefix + "/refundlist";
    }

    /**
     * 查询退费列表
     */
    @PostMapping("/listt")
    @ResponseBody
    public TableDataInfo listt(THmCustomer tHmCustomer) {
        startPage();
        List<RefundVO> list = tHmRefundService.selectTHmRefundListt(tHmCustomer);
        return getDataTable(list);
    }

    /**
     * 导出退费列表
     */
    @PostMapping("/exportt")
    @ResponseBody
    public AjaxResult exportt(THmCustomer tHmCustomer) {
        List<RefundVO> list = tHmRefundService.selectTHmRefundListt(tHmCustomer);
        ExcelUtil<RefundVO> util = new ExcelUtil<>(RefundVO.class);
        return util.exportExcel(list, "用户退费");
    }

    @GetMapping("/{id}")
    public String refund(@PathVariable("id") int id, Model model) {
        Map<String, Object> map = new HashMap<>();
        map.put("hcustomerId", id);
        model.addAllAttributes(map);
        return prefix + "/refund";
    }

    @GetMapping("/getById/{id}")
    @ResponseBody
    @ApiOperation("打印（退费 根据id查详情）")
    public THmRefund getById(@PathVariable("id") int id) {
        return tHmRefundService.selectTHmRefundByIds(id);
    }

    /**
     * 查询退费列表
     */
    @PostMapping("/list/{id}")
    @ResponseBody
    public TableDataInfo list(@PathVariable("id") Integer id) {
        THmRefund tHmRefund = new THmRefund();
        tHmRefund.setHcustomerId(id);
        List<THmRefund> list = tHmRefundService.selectTHmRefundList(tHmRefund);
        return getDataTable(list);
    }

    /**
     * 退费明细
     */
    @PostMapping("/list/{startTime}/{endTime}")
    @ResponseBody
    public TableDataInfo list(@PathVariable("startTime") String startTime, @PathVariable("endTime") String endTime) {
        //拼参
        Map<String, Object> map = new HashMap<>();
        map.put("startTime", startTime);
        map.put("endTime", endTime);
        //查询
        THmRefund tHmRefund = new THmRefund();
        tHmRefund.setParams(map);
        List<THmRefund> list = tHmRefundService.selectTHmRefundList(tHmRefund);
        return getDataTable(list);
    }

    /**
     * 导出退费列表
     */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(THmRefund tHmRefund) {
        List<THmRefund> list = tHmRefundService.selectTHmRefundList(tHmRefund);
        ExcelUtil<THmRefund> util = new ExcelUtil<THmRefund>(THmRefund.class);
        return util.exportExcel(list, "refund");
    }

    /**
     * 新增退费
     */
    @GetMapping("/add/{id}")
    public String add(@PathVariable("id") int id, ModelMap mmap) {
        THmRefund tHmRefund = tHmRefundService.selectTHmRefundById(id);
        mmap.put("tHmRefund", tHmRefund);
        return prefix + "/add";
    }

    /**
     * 新增保存退费
     */
    @Log(title = "退费", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(THmRefund tHmRefund) {
        return toAjax(tHmRefundService.insertTHmRefund(tHmRefund));
    }

    /**
     * 修改退费
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") int id, ModelMap mmap) {
        THmRefund tHmRefund = tHmRefundService.selectTHmRefundById(id);
        mmap.put("tHmRefund", tHmRefund);
        return prefix + "/edit";
    }

    /**
     * 修改保存退费
     */
    @Log(title = "退费", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(THmRefund tHmRefund) {
        return toAjax(tHmRefundService.updateTHmRefund(tHmRefund));
    }


    /**
     * 删除退费
     */
    @Log(title = "退费", businessType = BusinessType.DELETE)
    @PostMapping("/remove/{id}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("id") int id) {
        THmRefund tHmRefund = new THmRefund();
        tHmRefund.setTfId(id);
        tHmRefund.setTfState(1);
        return toAjax(tHmRefundService.updateTHmRefund(tHmRefund));
    }

    @GetMapping("/payprint/{id}")
    public String payPrints(@PathVariable("id") int id, Model model) {
        Map<String, Object> map = new HashMap<>();
        THmRefund tHmRefund =  tHmRefundService.selectTHmRefundByIds(id);
        map.put("tHmRefund", tHmRefund);
        model.addAllAttributes(map);
        return prefix + "/payprints";
    }
}
