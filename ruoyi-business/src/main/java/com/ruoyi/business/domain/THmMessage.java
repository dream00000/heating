package com.ruoyi.business.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 推送消息记录对象 t_hm_message
 *
 * @author ruoyi
 * @date 2022-03-12
 */
@Data
public class THmMessage extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Integer id;

    /**
     * 用户id
     */
    @Excel(name = "用户id")
    private Integer customerId;

    private String customerName;
    private String customerCode;

    /**
     * 用户openid
     */
    @Excel(name = "用户openid")
    private String openId;

    /**
     * 账期年度
     */
    @Excel(name = "账期年度")
    private String nian;

    /**
     * 模板id
     */
    @Excel(name = "模板id")
    private String templateId;

    /**
     * 账期金额
     */
    @Excel(name = "账期金额")
    private String total;

    /**
     * 缴纳时间
     */
    @Excel(name = "缴纳时间")
    private String jtime;

    /**
     * 类型 0未发送1已发送
     */
    @Excel(name = "类型 0未发送1已发送")
    private Integer type;

    /**
     * 账期id
     */
    @Excel(name = "账期id")
    private Integer wqId;

    /**
     * 发送时间
     */
    @Excel(name = "发送时间")
    private String ftime;

    /**
     * 备用字段
     */
    @Excel(name = "备用字段")
    private String str1;

    /**
     * 备用字段
     */
    @Excel(name = "备用字段")
    private String str2;

    /**
     * 备用字段
     */
    @Excel(name = "备用字段")
    private String str3;

    /**
     * 备用字段
     */
    @Excel(name = "备用字段")
    private String str4;
}
