package com.ruoyi.business.domain.vo;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

@Data
public class DetailListVO extends BaseEntity {
    //用户分区
    private Integer areaid;
    private String areaName;
    //用户编号
    private String code;
    //用户姓名
    private String name;
    //用户电话
    private String phone;
    //用户地址
    private String address;
    //用户开关阀
    private String tap;
    //支付类型
    private String type;
    private String typeName;
    //支付方式
    private String paytype;
    private String paytypeName;
    //交易时间
    private String addTime;
    //操作人员
    private Long adduserid;
    private String addUserName;
    //充值金额
    private String pay;
    //其他收费
    private String opay;
    //退费金额
    private String tfpay;

    private Integer wqId;
    private String wqStr4;
    private Integer ling;
}
