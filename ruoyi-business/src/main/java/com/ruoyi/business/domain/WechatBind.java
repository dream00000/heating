package com.ruoyi.business.domain;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 微信绑定用户
 */
@Data
public class WechatBind extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long bindId;

    //微信用户id
    private Integer wechatuserId;

    //用户id
    private Integer customerId;

    //状态0正常1删除
    private Integer status;

    //返现
    private String customerCode;
    private String customerPhone;
    private String customerAddress;
    private String customerName;
    private String customerArea;
}
