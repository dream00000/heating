package com.ruoyi.business.domain.data;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * 账期-单价对象 t_hm_customerpaydays_price
 */
@Data
public class THmCustomerpaydaysPriceCopy extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 账期id
     */
    private Integer customerpaydaysId;

    /**
     * 金额
     */
    private String price;

    /**
     * 操作人
     */
    private Long addUser;

    /**
     * 操作时间
     */
    private String addTime;

    //单价类别
    private String priceType;

}
