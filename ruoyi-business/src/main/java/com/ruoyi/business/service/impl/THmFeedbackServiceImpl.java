package com.ruoyi.business.service.impl;

import com.ruoyi.business.domain.*;
import com.ruoyi.business.mapper.THmAreaMapper;
import com.ruoyi.business.mapper.THmCustomerMapper;
import com.ruoyi.business.mapper.THmFeedbackMapper;
import com.ruoyi.business.service.ITHmCustomerpaydaysService;
import com.ruoyi.business.service.ITHmCustomerpriceService;
import com.ruoyi.business.service.ITHmFeedbackService;
import com.ruoyi.business.service.ITHmPayhistoryService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 稽查反馈信息Service业务层处理
 *
 * @author ruoyi
 * @date 2021-08-13
 */
@Service
public class THmFeedbackServiceImpl implements ITHmFeedbackService {
    @Resource
    private THmFeedbackMapper tHmFeedbackMapper;

    @Resource
    private THmCustomerMapper tHmCustomerMapper;

    @Resource
    private THmAreaMapper tHmAreaMapper;

    @Resource
    private ITHmCustomerpaydaysService itHmCustomerpaydaysService;

    @Resource
    private ITHmPayhistoryService itHmPayhistoryService;

    @Resource
    private ITHmCustomerpriceService itHmCustomerpriceService;

    /**
     * 查询稽查反馈信息
     */
    @Override
    public THmFeedback selectTHmFeedbackById(int id) {
        return tHmFeedbackMapper.selectTHmFeedbackById(id);
    }

    /**
     * 查询稽查反馈信息列表
     */
    @Override
    public List<THmFeedback> selectTHmFeedbackList(THmFeedback tHmFeedback) {
        List<THmFeedback> list = tHmFeedbackMapper.selectTHmFeedbackList(tHmFeedback);
        if (list.size() > 0) {
            for (THmFeedback hmFeedback : list) {
                THmCustomer tHmCustomer = tHmCustomerMapper.selectTHmCustomerById(hmFeedback.getHcustomerId());
                if (tHmCustomer != null) {
                    hmFeedback.setHcustomerId(tHmCustomer.getHcustomerId());
                    hmFeedback.setHcustomerName(tHmCustomer.getHcustomerName());
                    hmFeedback.setHcustomerAddress(tHmCustomer.getHcustomerDetailaddress());
                    hmFeedback.setHcustomerPhone(tHmCustomer.getHcustomerMobile());
                    THmArea tHmArea = tHmAreaMapper.selectTHmAreaById(tHmCustomer.getHareaId());
                    if (tHmArea != null) {
                        if (tHmArea.getHareaLevel() == 2) {
                            hmFeedback.setHcustomerArea(tHmArea.getHareaName());
                        } else {
                            THmArea tHmArea1 = tHmAreaMapper.selectTHmAreaById(tHmArea.getHareaParentid());
                            hmFeedback.setHcustomerArea(tHmArea1.getHareaName());
                        }
                    }
                }
            }
        }
        return list;
    }

    @Override
    public List<THmFeedback> selectTHmFeedbackListt(THmCustomer tHmCustomer) {
        List<THmFeedback> tHmFeedbackList = tHmFeedbackMapper.selectTHmFeedbackListt(tHmCustomer);
        if (tHmFeedbackList.size() > 0) {
            for (THmFeedback tHmFeedback : tHmFeedbackList) {
                THmCustomer tHmCustomer1 = tHmCustomerMapper.selectTHmCustomerById(tHmFeedback.getHcustomerId());
                if (tHmCustomer1 != null) {
                    THmArea tHmArea = tHmAreaMapper.selectTHmAreaById(tHmCustomer1.getHareaId());
                    if (tHmArea != null) {
                        if (tHmArea.getHareaLevel() == 2) {
                            tHmFeedback.setHcustomerArea(tHmArea.getHareaName());
                        } else {
                            THmArea tHmArea1 = tHmAreaMapper.selectTHmAreaById(tHmArea.getHareaParentid());
                            tHmFeedback.setHcustomerArea(tHmArea1.getHareaName());
                        }
                    }
                }
            }
        }

        return tHmFeedbackList;
    }

    @Override
    public List<THmCustomer> selectTHmFeedbackLists(THmCustomer tHmCustomer) {
        List<THmCustomer> customerList = tHmFeedbackMapper.tapCustomer(tHmCustomer);
        if (customerList.size() > 0) {
            for (THmCustomer hmCustomer : customerList) {
                THmArea tHmArea = tHmAreaMapper.selectTHmAreaById(hmCustomer.getHareaId());
                if (tHmArea != null) {
                    if (tHmArea.getHareaLevel() == 2) {
                        hmCustomer.setHareaName(tHmArea.getHareaName());
                    } else {
                        THmArea tHmArea1 = tHmAreaMapper.selectTHmAreaById(tHmArea.getHareaParentid());
                        hmCustomer.setHareaName(tHmArea1.getHareaName());
                    }
                }
            }
        }
        return customerList;
    }

    /**
     * 新增稽查反馈信息
     */
    @Override
    public int insertTHmFeedback(THmFeedback tHmFeedback) {
        return tHmFeedbackMapper.insertTHmFeedback(tHmFeedback);
    }

    /**
     * 修改稽查反馈信息
     */
    @Override
    public int updateTHmFeedback(THmFeedback tHmFeedback) {
        return tHmFeedbackMapper.updateTHmFeedback(tHmFeedback);
    }

    @Override
    public List<THmCustomer> tapCustomers(THmCustomer tHmCustomer) {
        List<THmCustomer> customerList = tHmFeedbackMapper.tapCustomers(tHmCustomer);
        if (customerList.size() > 0) {
            for (THmCustomer hmCustomer : customerList) {
                THmArea tHmArea = tHmAreaMapper.selectTHmAreaById(hmCustomer.getHareaId());
                if (tHmArea != null) {
                    if (tHmArea.getHareaLevel() == 2) {
                        hmCustomer.setHareaName(tHmArea.getHareaName());
                    } else {
                        THmArea tHmArea1 = tHmAreaMapper.selectTHmAreaById(tHmArea.getHareaParentid());
                        hmCustomer.setHareaName(tHmArea1.getHareaName());
                    }
                }
            }
        }
        return customerList;
    }

    //新版阀控 阀门管理列表
    @Override
    public List<THmCustomer> tapCustomer(THmCustomer tHmCustomer) {
        List<THmCustomer> customerList = tHmFeedbackMapper.tapCustomer(tHmCustomer);
        if (customerList.size() > 0) {
            for (THmCustomer hmCustomer : customerList) {
                THmArea tHmArea = tHmAreaMapper.selectTHmAreaById(hmCustomer.getHareaId());
                if (tHmArea != null) {
                    if (tHmArea.getHareaLevel() == 2) {
                        hmCustomer.setHareaName(tHmArea.getHareaName());
                    } else {
                        THmArea tHmArea1 = tHmAreaMapper.selectTHmAreaById(tHmArea.getHareaParentid());
                        hmCustomer.setHareaName(tHmArea1.getHareaName());
                    }
                }
            }
        }
        return customerList;
    }

    @Override
    public List<THmCustomer> tapCustomerd(THmCustomer tHmCustomer) {
        List<THmCustomer> customerList = tHmFeedbackMapper.tapCustomerd(tHmCustomer);
        if (customerList.size() > 0) {
            for (THmCustomer hmCustomer : customerList) {
                THmArea tHmArea = tHmAreaMapper.selectTHmAreaById(hmCustomer.getHareaId());
                if (tHmArea != null) {
                    if (tHmArea.getHareaLevel() == 2) {
                        hmCustomer.setHareaName(tHmArea.getHareaName());
                    } else {
                        THmArea tHmArea1 = tHmAreaMapper.selectTHmAreaById(tHmArea.getHareaParentid());
                        hmCustomer.setHareaName(tHmArea1.getHareaName());
                    }
                }
            }
        }
        return customerList;
    }

    @Override
    public List<THmCustomer> tapCustomerd1(THmCustomer tHmCustomer) {
        List<THmCustomer> customerList = tHmFeedbackMapper.tapCustomerd1(tHmCustomer);
        if (customerList.size() > 0) {
            for (THmCustomer hmCustomer : customerList) {
                THmArea tHmArea = tHmAreaMapper.selectTHmAreaById(hmCustomer.getHareaId());
                if (tHmArea != null) {
                    if (tHmArea.getHareaLevel() == 2) {
                        hmCustomer.setHareaName(tHmArea.getHareaName());
                    } else {
                        THmArea tHmArea1 = tHmAreaMapper.selectTHmAreaById(tHmArea.getHareaParentid());
                        hmCustomer.setHareaName(tHmArea1.getHareaName());
                    }
                }
            }
        }
        return customerList;
    }

    @Override
    public List<THmCustomer> tapCustomerd2(THmCustomer tHmCustomer) {
        List<THmCustomer> customerList = tHmFeedbackMapper.tapCustomerd2(tHmCustomer);
        if (customerList.size() > 0) {
            for (THmCustomer hmCustomer : customerList) {
                THmArea tHmArea = tHmAreaMapper.selectTHmAreaById(hmCustomer.getHareaId());
                if (tHmArea != null) {
                    if (tHmArea.getHareaLevel() == 2) {
                        hmCustomer.setHareaName(tHmArea.getHareaName());
                    } else {
                        THmArea tHmArea1 = tHmAreaMapper.selectTHmAreaById(tHmArea.getHareaParentid());
                        hmCustomer.setHareaName(tHmArea1.getHareaName());
                    }
                }
            }
        }
        return customerList;
    }

    @Override
    public List<THmCustomer> tapCustomerd3(THmCustomer tHmCustomer) {
        List<THmCustomer> customerList = tHmFeedbackMapper.tapCustomerd3(tHmCustomer);
        if (customerList.size() > 0) {
            for (THmCustomer hmCustomer : customerList) {
                THmArea tHmArea = tHmAreaMapper.selectTHmAreaById(hmCustomer.getHareaId());
                if (tHmArea != null) {
                    if (tHmArea.getHareaLevel() == 2) {
                        hmCustomer.setHareaName(tHmArea.getHareaName());
                    } else {
                        THmArea tHmArea1 = tHmAreaMapper.selectTHmAreaById(tHmArea.getHareaParentid());
                        hmCustomer.setHareaName(tHmArea1.getHareaName());
                    }
                }
            }
        }
        return customerList;
    }

    @Override
    public THmCustomer tapCustomerByPriceId(int id) {
        THmCustomer tHmCustomer = tHmFeedbackMapper.tapCustomerByPriceId(id);
        THmArea tHmArea = tHmAreaMapper.selectTHmAreaById(tHmCustomer.getHareaId());
        if (tHmArea != null) {
            if (tHmArea.getHareaLevel() == 2) {
                tHmCustomer.setHareaName(tHmArea.getHareaName());
            } else {
                THmArea tHmArea1 = tHmAreaMapper.selectTHmAreaById(tHmArea.getHareaParentid());
                tHmCustomer.setHareaName(tHmArea1.getHareaName());
            }
        }
        //单价列表
        THmCustomerprice tHmCustomerprice = new THmCustomerprice();
        tHmCustomerprice.setCustomerId(tHmCustomer.getHcustomerId());
        List<THmCustomerprice> list = itHmCustomerpriceService.selectTHmCustomerpriceList(tHmCustomerprice);
        for (THmCustomerprice hmCustomerprice : list) {
            hmCustomerprice.setXishu(hmCustomerprice.getRemark());
        }
        tHmCustomer.setCustomerpriceList(list);
        //充值记录
        THmPayhistory tHmPayhistory = new THmPayhistory();
        tHmPayhistory.setHcustomerId(tHmCustomer.getHcustomerId());
        List<THmPayhistory> tHmPayhistoryList = itHmPayhistoryService.selectTHmPayhistoryList(tHmPayhistory);
        for (THmPayhistory hmPayhistory : tHmPayhistoryList) {
            THmCustomerpaydays tHmCustomerpaydays = itHmCustomerpaydaysService.selectTHmCustomerpaydaysById(hmPayhistory.getWqId());
            hmPayhistory.setNiandu(tHmCustomerpaydays.getHzqGrstarttime() + "-" + tHmCustomerpaydays.getHzqGrendtime());
            if (hmPayhistory.getHpaySfqe() == 0) {
                hmPayhistory.setHpaySfqeName("已缴基础热");
            } else if (hmPayhistory.getHpaySfqe() == 1) {
                hmPayhistory.setHpaySfqeName("已缴全热");
            } else if (hmPayhistory.getHpaySfqe() == 2) {
                hmPayhistory.setHpaySfqeName("已缴续费");
            } else if (hmPayhistory.getHpaySfqe() == 3) {
                hmPayhistory.setHpaySfqeName("已缴面积变更");
            } else if (hmPayhistory.getHpaySfqe() == 4) {
                hmPayhistory.setHpaySfqeName("部分缴费");
            }
        }
        tHmCustomer.setPayhistoryList(tHmPayhistoryList);
        //阀控记录
        THmFeedback tHmFeedback = new THmFeedback();
        tHmFeedback.setFdType(1);
        tHmFeedback.setHcustomerId(tHmCustomer.getHcustomerId());
        tHmFeedback.setCustomerpriceId((long) id);
        List<THmFeedback> tHmFeedbackList = tHmFeedbackMapper.selectTHmFeedbackList(tHmFeedback);
        for (THmFeedback hmFeedback : tHmFeedbackList) {
            //0=开阀,1=关阀,2=断管
            if (hmFeedback.getFdFmzt() == 0) {
                hmFeedback.setFdFmztName("关阀");
            } else if (hmFeedback.getFdFmzt() == 1) {
                hmFeedback.setFdFmztName("开阀");
            } else if (hmFeedback.getFdFmzt() == 2) {
                hmFeedback.setFdFmztName("断管");
            }
        }
        tHmCustomer.setFeedbackList(tHmFeedbackList);

        return tHmCustomer;
    }
}
