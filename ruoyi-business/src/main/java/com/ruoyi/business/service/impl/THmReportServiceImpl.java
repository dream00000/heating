package com.ruoyi.business.service.impl;

import com.ruoyi.business.domain.THmCustomer;
import com.ruoyi.business.domain.THmCustomerpaydays;
import com.ruoyi.business.domain.bill.*;
import com.ruoyi.business.domain.report.IndexProportion;
import com.ruoyi.business.domain.vo.IndexStatisticsVO;
import com.ruoyi.business.domain.vo.ReportBills;
import com.ruoyi.business.mapper.THmCustomerMapper;
import com.ruoyi.business.mapper.THmCustomerpaydaysMapper;
import com.ruoyi.business.mapper.THmFeedbackMapper;
import com.ruoyi.business.mapper.THmPayhistoryMapper;
import com.ruoyi.business.service.THmReportService;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.mapper.SysUserMapper;
import com.ruoyi.system.service.ISysDictDataService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 业务查询
 *
 * @author lihao
 * @date 2021-09-02
 */
@Service
public class THmReportServiceImpl implements THmReportService {

    @Resource
    private THmCustomerMapper tHmCustomerMapper;

    @Resource
    private THmPayhistoryMapper tHmPayhistoryMapper;

    @Resource
    private SysUserMapper sysUserMapper;

    @Resource
    private THmCustomerpaydaysMapper tHmCustomerpaydaysMapper;

    @Resource
    private THmFeedbackMapper tHmFeedbackMapper;

    @Resource
    private ISysDictDataService iSysDictDataService;

    @Override
    public IndexStatisticsVO indexStatistics() {

        IndexStatisticsVO indexStatisticsVO = new IndexStatisticsVO();
        //总用户数
        indexStatisticsVO.setCustomerCount(tHmCustomerMapper.getCustomerCount());
        //欠费用户数
        indexStatisticsVO.setQianCount(tHmCustomerMapper.getCustomerCountByZheng());

        THmCustomer tHmCustomer = new THmCustomer();

        //待关阀总数
        List<THmCustomer> glist = tHmFeedbackMapper.tapCustomerd1(tHmCustomer);
        indexStatisticsVO.setGCount(Math.max(glist.size(), 0));
        //待开阀总数
        List<THmCustomer> klist = tHmFeedbackMapper.tapCustomerd3(tHmCustomer);
        indexStatisticsVO.setKCount(Math.max(klist.size(), 0));

        return indexStatisticsVO;
    }

    @Override
    public List<IndexProportion> indexProportion() {

        //获取年度
        Calendar date = Calendar.getInstance();
        String thisYear = String.valueOf(date.get(Calendar.YEAR));//今年
        String nextYear = String.valueOf(Integer.parseInt(thisYear) + 1);//明年

        List<IndexProportion> list = new ArrayList<>();

        THmCustomerpaydays tHmCustomerpaydays = new THmCustomerpaydays();
        tHmCustomerpaydays.setHzqGrstarttime(thisYear);
        tHmCustomerpaydays.setHzqGrendtime(nextYear);
        List<THmCustomerpaydays> tHmCustomerpaydaysList = tHmCustomerpaydaysMapper.indexProportion(tHmCustomerpaydays);

        //20%
        IndexProportion indexProportion0 = new IndexProportion();
        List<THmCustomerpaydays> list0 = tHmCustomerpaydaysList.stream().filter(o -> o.getWqInt1() == 0).collect(Collectors.toList());
        if (list0.size() > 0) {
            indexProportion0.setType("基础热");
            indexProportion0.setCount(list0.size());
            list.add(indexProportion0);
        } else {
            indexProportion0.setType("基础热");
            indexProportion0.setCount(0);
            list.add(indexProportion0);
        }
        //全额
        IndexProportion indexProportion1 = new IndexProportion();
        List<THmCustomerpaydays> list1 = tHmCustomerpaydaysList.stream().filter(o -> o.getWqInt1() == 1).collect(Collectors.toList());
        if (list1.size() > 0) {
            indexProportion1.setType("全额");
            indexProportion1.setCount(list1.size());
            list.add(indexProportion1);
        } else {
            indexProportion1.setType("全额");
            indexProportion1.setCount(0);
            list.add(indexProportion1);
        }
        //续费
        IndexProportion indexProportion2 = new IndexProportion();
        List<THmCustomerpaydays> list2 = tHmCustomerpaydaysList.stream().filter(o -> o.getWqInt1() == 2).collect(Collectors.toList());
        if (list2.size() > 0) {
            indexProportion2.setType("续费");
            indexProportion2.setCount(list2.size());
            list.add(indexProportion2);
        } else {
            indexProportion2.setType("续费");
            indexProportion2.setCount(0);
            list.add(indexProportion2);
        }
        //延期
        IndexProportion indexProportion3 = new IndexProportion();
        List<THmCustomerpaydays> list3 = tHmCustomerpaydaysList.stream().filter(o -> o.getWqInt1() == 3).collect(Collectors.toList());
        if (list3.size() > 0) {
            indexProportion3.setType("延期");
            indexProportion3.setCount(list3.size());
            list.add(indexProportion3);
        } else {
            indexProportion3.setType("延期");
            indexProportion3.setCount(0);
            list.add(indexProportion3);
        }
        //欠费
        IndexProportion indexProportion99 = new IndexProportion();
        List<THmCustomerpaydays> list99 = tHmCustomerpaydaysList.stream().filter(o -> o.getWqInt1() == 99).collect(Collectors.toList());
        if (list99.size() > 0) {
            indexProportion99.setType("欠费");
            indexProportion99.setCount(list99.size());
            list.add(indexProportion99);
        } else {
            indexProportion99.setType("欠费");
            indexProportion99.setCount(0);
            list.add(indexProportion99);
        }
        return list;
    }

    @Override
    public BusinessList reportBill(BusinessList businessList) {

        BusinessList businessList1 = new BusinessList();
        List<TopUser> topUserList = new ArrayList<>();
        List<ReportBills> reportBillsList = tHmPayhistoryMapper.reportBills(businessList.getStartTime(), businessList.getEndTime(), businessList.getSysName());

        BigDecimal zongM = new BigDecimal("0");
        BigDecimal zongC = new BigDecimal("0");

        for (int i = 0; i <= reportBillsList.size() - 1; i++) {
            BigDecimal xiaoM = new BigDecimal(reportBillsList.get(i).getHpaySum());
            BigDecimal xiaoC = new BigDecimal(reportBillsList.get(i).getCou());
            List<Recharge> rechargeList = new ArrayList<>();
            List<Other> otherList = new ArrayList<>();
            List<Refund> refundList = new ArrayList<>();

            if ("充值".equals(reportBillsList.get(i).getCtype())) {
                Recharge recharge = new Recharge(); //充值
                recharge.setType(iSysDictDataService.selectDictLabel("data_paytype", reportBillsList.get(i).getPaytype()));
                recharge.setCount(reportBillsList.get(i).getCou());
                recharge.setMoney(reportBillsList.get(i).getHpaySum());
                rechargeList.add(recharge);
            } else if ("其他".equals(reportBillsList.get(i).getCtype())) {
                Other other = new Other(); //其他
                other.setType(iSysDictDataService.selectDictLabel("data_paytype", reportBillsList.get(i).getPaytype()));
                other.setCount(reportBillsList.get(i).getCou());
                other.setMoney(reportBillsList.get(i).getHpaySum());
                otherList.add(other);
            } else if ("退费".equals(reportBillsList.get(i).getCtype())) {
                Refund refund = new Refund(); //退费
                refund.setType("");
                refund.setCount(reportBillsList.get(i).getCou());
                refund.setMoney(reportBillsList.get(i).getHpaySum());
                refundList.add(refund);
            }
            for (int j = reportBillsList.size() - 1; j > i; j--) {
                if (reportBillsList.get(j).getUserid().equals(reportBillsList.get(i).getUserid())) {
                    if ("充值".equals(reportBillsList.get(j).getCtype())) {
                        Recharge recharge = new Recharge(); //充值
                        recharge.setType(iSysDictDataService.selectDictLabel("data_paytype", reportBillsList.get(j).getPaytype()));
                        recharge.setCount(reportBillsList.get(j).getCou());
                        recharge.setMoney(reportBillsList.get(j).getHpaySum());
                        rechargeList.add(recharge);
                    } else if ("其他".equals(reportBillsList.get(j).getCtype())) {
                        Other other = new Other(); //其他
                        other.setType(iSysDictDataService.selectDictLabel("data_paytype", reportBillsList.get(j).getPaytype()));
                        other.setCount(reportBillsList.get(j).getCou());
                        other.setMoney(reportBillsList.get(j).getHpaySum());
                        otherList.add(other);
                    } else if ("退费".equals(reportBillsList.get(j).getCtype())) {
                        Refund refund = new Refund(); //退费
                        refund.setType("");
                        refund.setCount(reportBillsList.get(j).getCou());
                        refund.setMoney(reportBillsList.get(j).getHpaySum());
                        refundList.add(refund);
                    }
                    xiaoM = xiaoM.add(new BigDecimal(reportBillsList.get(j).getHpaySum()));
                    xiaoC = xiaoC.add(new BigDecimal(reportBillsList.get(j).getCou()));

                    reportBillsList.remove(j);
                }
            }

            SysUser sysUser = sysUserMapper.selectUserById(reportBillsList.get(i).getUserid());
            //小结
            if (sysUser != null) {
                TopUser topUser = new TopUser();
                topUser.setName(sysUser.getUserName());
                topUser.setSubTotalCount(Integer.parseInt(xiaoC.toString()));
                topUser.setSubTotalMoney(xiaoM.toString());
                topUser.setRechargeList(rechargeList);
                topUser.setOtherList(otherList);
                topUser.setRefundList(refundList);
                topUserList.add(topUser);
                zongM = zongM.add(xiaoM);
                zongC = zongC.add(xiaoC);
            }
            businessList1.setTotalCount(Integer.valueOf(zongC.toString()));
            businessList1.setTotalMoney(zongM.toString());
            businessList1.setTopUserList(topUserList);
        }

        return businessList1;
    }
}
