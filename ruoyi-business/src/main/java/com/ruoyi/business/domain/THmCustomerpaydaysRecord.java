package com.ruoyi.business.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * 账期操作日志对象 t_hm_customerpaydays_record
 *
 * @author ruoyi
 * @date 2021-12-29
 */
@Data
public class THmCustomerpaydaysRecord extends BaseEntity {

    /**
     * 主键
     */
    private Long id;

    private String qianTotal;
    private String houTotal;

    /**
     * 账期id
     */
    @Excel(name = "账期id")
    private Integer customerpaydaysId;

    /**
     * 类型：1变更2平账3调整
     */
    @Excel(name = "类型：1变更2平账3调整")
    private Integer type;

    /**
     * 操作人
     */
    private Long addUser;

    /**
     * 操作人
     */
    @Excel(name = "操作人")
    private String addUserName;

    /**
     * 操作时间
     */
    @Excel(name = "操作时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date addTime;

    //0正常 1删除
    private Integer status;
}
