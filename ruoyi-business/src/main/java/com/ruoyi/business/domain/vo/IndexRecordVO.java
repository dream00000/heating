package com.ruoyi.business.domain.vo;

import lombok.Data;

/**
 * 返回首页记录数据的实体类
 */
@Data
public class IndexRecordVO {

    private String addTime;//时间
    private String context;//内容
}
