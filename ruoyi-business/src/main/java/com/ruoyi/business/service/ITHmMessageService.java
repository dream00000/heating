package com.ruoyi.business.service;

import com.ruoyi.business.domain.THmMessage;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 推送消息记录Service接口
 *
 * @author ruoyi
 * @date 2022-03-12
 */
public interface ITHmMessageService {
    /**
     * 查询推送消息记录
     *
     * @param id
     * @return 推送消息记录
     */
    public THmMessage selectTHmMessageById(@Param("id") int id);

    /**
     * 查询推送消息记录列表
     *
     * @param tHmMessage 推送消息记录
     * @return 推送消息记录集合
     */
    public List<THmMessage> selectTHmMessageList(THmMessage tHmMessage);

    /**
     * 新增推送消息记录
     *
     * @param tHmMessage 推送消息记录
     * @return 结果
     */
    public int insertTHmMessage(THmMessage tHmMessage);

    /**
     * 修改推送消息记录
     *
     * @param tHmMessage 推送消息记录
     * @return 结果
     */
    public int updateTHmMessage(THmMessage tHmMessage);

}
