package com.ruoyi.business.service.impl;

import com.ruoyi.business.domain.THmCustomer;
import com.ruoyi.business.domain.THmMessage;
import com.ruoyi.business.domain.WechatUser;
import com.ruoyi.business.domain.WxPayBean;
import com.ruoyi.business.mapper.THmCustomerMapper;
import com.ruoyi.business.mapper.THmMessageMapper;
import com.ruoyi.business.mapper.WechatUserMapper;
import com.ruoyi.business.service.IWechatUserService;
import me.chanjar.weixin.mp.api.WxMpInMemoryConfigStorage;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateData;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateMessage;
import org.apache.shiro.util.ThreadContext;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Calendar;
import java.util.List;

/**
 * 微信用户Service业务层处理
 */
@Service
@EnableScheduling
public class WechatUserServiceImpl implements IWechatUserService {

    @Resource
    private WechatUserMapper wechatUserMapper;

    @Resource
    private WxPayBean wxPayBean;

    @Resource
    private THmMessageMapper tHmMessageMapper;

    @Resource
    private THmCustomerMapper tHmCustomerMapper;

    /**
     * 查询微信用户
     *
     * @param wechatId 微信用户ID
     * @return 微信用户
     */
    @Override
    public WechatUser selectWechatUserById(Long wechatId) {
        return wechatUserMapper.selectWechatUserById(wechatId);
    }

    @Override
    public WechatUser getWechatUser(String openId) {
        return wechatUserMapper.getWechatUser(openId);
    }

    /**
     * 查询微信用户列表
     *
     * @param wechatUser 微信用户
     * @return 微信用户
     */
    @Override
    public List<WechatUser> selectWechatUserList(WechatUser wechatUser) {
        return wechatUserMapper.selectWechatUserList(wechatUser);
    }

    @Override
    public List<WechatUser> selectWechatUserListByOpenid(String openid) {
        WechatUser wechatUser = new WechatUser();
        wechatUser.setOpenId(openid);
        return wechatUserMapper.selectWechatUserList(wechatUser);
    }

    @Override
    public List<WechatUser> selectWechatUserListByCustomerId(String customerId) {
        return wechatUserMapper.selectWechatUserListByCustomerId(customerId);
    }

    /**
     * 新增微信用户
     *
     * @param wechatUser 微信用户
     * @return 结果
     */
    @Override
    public int insertWechatUser(WechatUser wechatUser) {
        return wechatUserMapper.insertWechatUser(wechatUser);
    }

    /**
     * 修改微信用户
     *
     * @param wechatUser 微信用户
     * @return 结果
     */
    @Override
    public int updateWechatUser(WechatUser wechatUser) {
        return wechatUserMapper.updateWechatUser(wechatUser);
    }

    /**
     * 删除微信用户信息
     *
     * @param wechatId 微信用户ID
     * @return 结果
     */
    @Override
    public int deleteWechatUserById(Long wechatId) {
        return wechatUserMapper.deleteWechatUserById(wechatId);
    }

    //微信消息推送
    @Override
    public void sendWxMessage() {
        System.out.println("微信推送 任务启动");
        WxMpInMemoryConfigStorage wxMpInMemoryConfigStorage = new WxMpInMemoryConfigStorage();
        wxMpInMemoryConfigStorage.setAppId(wxPayBean.getAppId());
        wxMpInMemoryConfigStorage.setSecret(wxPayBean.getAppSecret());

        THmMessage tHmMessage = new THmMessage();
        tHmMessage.setType(0);
        List<THmMessage> messageList = tHmMessageMapper.selectTHmMessageList(tHmMessage);

        Calendar cal = Calendar.getInstance();
        int day = cal.get(Calendar.DATE);
        int month = cal.get(Calendar.MONTH) + 1;

        if (messageList.size() > 0) {
            for (THmMessage hmMessage : messageList) {
                String[] strings = hmMessage.getFtime().split("-");
                System.out.println("微信推送 进入循环");
                if (Integer.parseInt(strings[0]) - month == 0 && Integer.parseInt(strings[1]) - day == 0) {
                    WxMpTemplateMessage wxMpTemplateMessage = WxMpTemplateMessage.builder()
                            .toUser(hmMessage.getOpenId())
                            .templateId(hmMessage.getTemplateId())
                            .build();
                    System.out.println("微信推送 同一天");
                    System.out.println("用户ID ：" + hmMessage.getCustomerId());
                    //查用户
                    if (hmMessage.getCustomerId() != null) {
                        System.out.println("用户ID ：" + hmMessage.getCustomerId());
                        THmCustomer tHmCustomer = tHmCustomerMapper.selectTHmCustomerById(hmMessage.getCustomerId());
                        System.out.println("用户 ：" + tHmCustomer);
                        if (tHmCustomer != null) {
                            wxMpTemplateMessage.addWxMpTemplateData(new WxMpTemplateData("first", "尊敬的用户" + tHmCustomer.getHcustomerName() + " 您好：您位于" + tHmCustomer.getHareaName() + tHmCustomer.getHcustomerDetailaddress() + "的房屋已生成" + hmMessage.getNian() + "全热费账单"));
                            wxMpTemplateMessage.addWxMpTemplateData(new WxMpTemplateData("keyword1", hmMessage.getTotal() + "元"));
                            wxMpTemplateMessage.addWxMpTemplateData(new WxMpTemplateData("keyword2", "本年度" + hmMessage.getJtime()));
                            wxMpTemplateMessage.addWxMpTemplateData(new WxMpTemplateData("remark", "如果您想办理停供请于本年度9月20日前到供热窗口办理。"));

                            WxMpService wxMpService = new WxMpServiceImpl();
                            wxMpService.setWxMpConfigStorage(wxMpInMemoryConfigStorage);

                            try {
                                wxMpService.getTemplateMsgService().sendTemplateMsg(wxMpTemplateMessage);
                                System.out.println("微信推送 发送了");
                                tHmMessage.setType(1);
                                tHmMessage.setId(hmMessage.getId());
                                tHmMessageMapper.updateTHmMessage(tHmMessage);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        }
    }

    //微信推送 定时任务
    @Scheduled(cron = "0 0 10 * * ?")
    public void sendWxMessages() {
        DefaultWebSecurityManager manager = new DefaultWebSecurityManager();
        ThreadContext.bind(manager);
        System.out.println("微信推送 任务启动");
        WxMpInMemoryConfigStorage wxMpInMemoryConfigStorage = new WxMpInMemoryConfigStorage();
        wxMpInMemoryConfigStorage.setAppId(wxPayBean.getAppId());
        wxMpInMemoryConfigStorage.setSecret(wxPayBean.getAppSecret());

        THmMessage tHmMessage = new THmMessage();
        tHmMessage.setType(0);
        List<THmMessage> messageList = tHmMessageMapper.selectTHmMessageList(tHmMessage);

        Calendar cal = Calendar.getInstance();
        int day = cal.get(Calendar.DATE);
        int month = cal.get(Calendar.MONTH) + 1;

        if (messageList.size() > 0) {
            for (THmMessage hmMessage : messageList) {
                String[] strings = hmMessage.getFtime().split("-");
                System.out.println("微信推送 进入循环");
                if (Integer.parseInt(strings[0]) - month == 0 && Integer.parseInt(strings[1]) - day == 0) {
                    WxMpTemplateMessage wxMpTemplateMessage = WxMpTemplateMessage.builder()
                            .toUser(hmMessage.getOpenId())
                            .templateId(hmMessage.getTemplateId())
                            .build();
                    System.out.println("微信推送 同一天");
                    System.out.println("用户ID ：" + hmMessage.getCustomerId());
                    //查用户
                    if (hmMessage.getCustomerId() != null) {
                        System.out.println("用户ID ：" + hmMessage.getCustomerId());
                        THmCustomer tHmCustomer = tHmCustomerMapper.selectTHmCustomerById(hmMessage.getCustomerId());
                        System.out.println("用户 ：" + tHmCustomer);
                        if (tHmCustomer != null) {
                            wxMpTemplateMessage.addWxMpTemplateData(new WxMpTemplateData("first", "尊敬的用户" + tHmCustomer.getHcustomerName() + " 您好：您位于" + tHmCustomer.getHareaName() + tHmCustomer.getHcustomerDetailaddress() + "的房屋已生成" + hmMessage.getNian() + "全热费账单"));
                            wxMpTemplateMessage.addWxMpTemplateData(new WxMpTemplateData("keyword1", hmMessage.getTotal() + "元"));
                            wxMpTemplateMessage.addWxMpTemplateData(new WxMpTemplateData("keyword2", "本年度" + hmMessage.getJtime()));
                            wxMpTemplateMessage.addWxMpTemplateData(new WxMpTemplateData("remark", "如果您想办理停供请于本年度9月20日前到供热窗口办理。"));

                            WxMpService wxMpService = new WxMpServiceImpl();
                            wxMpService.setWxMpConfigStorage(wxMpInMemoryConfigStorage);

                            try {
                                wxMpService.getTemplateMsgService().sendTemplateMsg(wxMpTemplateMessage);
                                System.out.println("微信推送 发送了");
                                tHmMessage.setType(1);
                                tHmMessage.setId(hmMessage.getId());
                                tHmMessageMapper.updateTHmMessage(tHmMessage);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        }
    }
}
