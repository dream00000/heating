package com.ruoyi.business.domain.vo;

import lombok.Data;

@Data
public class BaoData {

    private String wxr;
    private String khmc;
    private String djsj;
    private String wxdh;
    private String gdbh;
    private String dz;
    private String gdzt;
    private String gdlx;
}
