package com.ruoyi.business.service;

import com.ruoyi.business.domain.THmNameList;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 黑白名单
 */
public interface THmNameListService {

    public THmNameList getById(@Param("id") int id);

    public List<THmNameList> selectTHmNameList(THmNameList tHmNameList);

    public int insertTHmNameList(THmNameList tHmNameList);

    public List<THmNameList> getByCustomerId(@Param("customerId") int customerId);
}
