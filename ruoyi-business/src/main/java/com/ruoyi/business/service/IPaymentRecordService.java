package com.ruoyi.business.service;


import com.ruoyi.business.domain.PaymentRecord;

import java.util.List;

/**
 * 订单Service接口
 */
public interface IPaymentRecordService {

    /**
     * 查询订单
     */
    public PaymentRecord selectPaymentRecordById(Long paymentId);

    /**
     * 查询订单列表
     */
    public List<PaymentRecord> selectPaymentRecordList(PaymentRecord paymentRecord);

    /**
     * 校验订单是否支付成功
     */
    public boolean orderReset(Long paymentId);

    /**
     * 新增订单
     */
    public int insertPaymentRecord(PaymentRecord paymentRecord);
}
